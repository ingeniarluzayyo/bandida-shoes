﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using Zapateria.Models;

namespace Zapateria.Libs
{
    class CargadorDeDatos
    {

        public static void CargarComboBox(string consulta,List<MySqlParameter> param,ComboBox comboBox,string texto,string campoValor,string campoDato)
        {            
            DataTable ds = BaseDeDatos.GetInstance.ExecuteCustomQuery(consulta, param, texto);

            comboBox.DataSource = ds;
            comboBox.ValueMember = campoValor;
            comboBox.DisplayMember = campoDato;
            comboBox.SelectedItem = null;           
        }

        public static void CargarCheckedListBox(string consulta, List<MySqlParameter> param, CheckedListBox checkedListBox, string texto, string campoValor, string campoDato)
        {
            DataTable ds = BaseDeDatos.GetInstance.ExecuteCustomQuery(consulta, param, texto);

            checkedListBox.DataSource = ds;
            checkedListBox.ValueMember = campoValor;
            checkedListBox.DisplayMember = campoDato;
            checkedListBox.SelectedItem = null; 
        }

        //Cargas de COMBO BOX Especificos
        public static void CargarFamiliasComboBox(ComboBox comboBox,string txt)
        {
            CargadorDeDatos.CargarComboBox("select id,nombre from familias", new List<MySqlParameter>(), comboBox, txt, "id", "nombre");
        }

        public static void CargarFamiliasGastosComboBox(ComboBox comboBox, string txt)
        {
            CargadorDeDatos.CargarComboBox("select id,nombre from familias_gastos", new List<MySqlParameter>(), comboBox, txt, "id", "nombre");
        }

        public static void CargarSucursalesComboBox(ComboBox comboBox, string txt)
        {
            CargadorDeDatos.CargarComboBox("select id,nombre from sucursal", new List<MySqlParameter>(), comboBox, txt, "id", "nombre");
        }

        public static void CargarZonasComboBox(ComboBox comboBox, string txt)
        {
            CargadorDeDatos.CargarComboBox("select id,nombre from zona", new List<MySqlParameter>(), comboBox, txt, "id", "nombre");
        }

        public static void CargarColoresComboBox(ComboBox comboBox, string txt)
        {
            CargadorDeDatos.CargarComboBox("select id,nombre from colores", new List<MySqlParameter>(), comboBox, txt, "id", "nombre");
        }

        public static void CargarTallesComboBox(ComboBox comboBox, string txt)
        {
            CargadorDeDatos.CargarComboBox("select id,nombre from talles", new List<MySqlParameter>(), comboBox, txt, "id", "nombre");
        }

        public static void CargarSubFamiliasGastosComboBox(ComboBox comboBox, string txt, int familia)
        {
            CargadorDeDatos.CargarComboBox("select id,nombre from subfamilia_gastos where familia_gasto=" + familia + " order by nombre", new List<MySqlParameter>(), comboBox, txt, "id", "nombre");
        }

        public static void CargarVendedoresComboBox(ComboBox comboBox, string txt)
        {
            CargadorDeDatos.CargarComboBox("select id,CONCAT(apellido,' ',nombre) as nombre from vendedores", new List<MySqlParameter>(), comboBox, txt, "id", "nombre");
        }

        public static void CargarRolesComboBox(ComboBox comboBox, string txt)
        {
            CargadorDeDatos.CargarComboBox("select id,nombre from roles", new List<MySqlParameter>(), comboBox, txt, "id", "nombre");
        }

        public static void CargarProveedorComboBox(ComboBox comboBox, string txt)
        {
            CargadorDeDatos.CargarComboBox("select id,razon_social from proveedores", new List<MySqlParameter>(), comboBox, txt, "id", "razon_social");
        }

        public static void CargarFormatoVentaComboBox(ComboBox comboBox, string txt)
        {
            CargadorDeDatos.CargarComboBox("select id,round(formato/1000,2) as formato from formato_venta", new List<MySqlParameter>(), comboBox, txt, "id", "formato");
        }

        public static void CargarFormatoCompraComboBox(ComboBox comboBox, string txt)
        {
            CargadorDeDatos.CargarComboBox("select id,round(formato/1000,2) as formato from formato_compra", new List<MySqlParameter>(), comboBox, txt, "id", "formato");
        }
        
        public static void CargarFormatoCompraProductoComboBox(ComboBox comboBox, Producto producto, string txt)
        {
            //me guardo el id del pfc
            CargadorDeDatos.CargarComboBox("select pfc.id,fc.formato from producto_x_formato_compra as pfc join formato_compra as fc on fc.id=pfc.formato_compra_id where pfc.producto_id="+producto.id, new List<MySqlParameter>(), comboBox, txt, "id", "formato");
        }

        public static void CargarFormatoVentaProductoComboBox(ComboBox comboBox, Producto producto, string txt)
        {
            //me guardo el id del pfv
            CargadorDeDatos.CargarComboBox("select pfv.id,fc.formato from producto_x_formato_venta as pfv join formato_venta as fv on fv.id=pfv.formato_venta_id where pfv.producto_id=" + producto.id, new List<MySqlParameter>(), comboBox, txt, "id", "formato");
        }

        public static void CargarRepartosComboBox(ComboBox comboBox, string txt)
        {
            CargadorDeDatos.CargarComboBox("select id,nombre from repartos", new List<MySqlParameter>(), comboBox, txt, "id", "nombre");
        }

        public static void CargarClienteComboBox(ComboBox comboBox, string txt)
        {
            CargadorDeDatos.CargarComboBox("select id,razon_social as nombre from clientes where habilitado=1 order by razon_social", new List<MySqlParameter>(), comboBox, txt, "id", "nombre");
        }

        public static void CargarClienteXRepartoComboBox(ComboBox comboBox, string txt, string reparto_id)
        {
            CargadorDeDatos.CargarComboBox("select id,cliente as nombre from clientes where reparto_id=" + reparto_id + " and habilitado=1 order by orden", new List<MySqlParameter>(), comboBox, txt, "id", "nombre");
        }

        public static void CargarFormaPagoComboBox(ComboBox comboBox, string txt)
        {
            CargadorDeDatos.CargarComboBox("select id,nombre from forma_de_pago", new List<MySqlParameter>(), comboBox, txt, "id", "nombre");
        }

        public static void CargarFormaPagoNoACreditoComboBox(ComboBox comboBox, string txt)
        {
            CargadorDeDatos.CargarComboBox("select id,nombre from forma_de_pago WHERE NOT a_credito", new List<MySqlParameter>(), comboBox, txt, "id", "nombre");
        }

        public static void CargarStockComboBox(ComboBox comboBox, string txt, int max_stock)
        {
            List<int> stockList = Enumerable.Range(1, max_stock).Reverse().ToList();

            comboBox.DataSource = stockList;
            comboBox.SelectedItem = null;
        }

    }
}
