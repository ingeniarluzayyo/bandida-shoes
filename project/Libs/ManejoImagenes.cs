﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Drawing;

namespace Zapateria.Libs
{
    class ManejoImagenes
    {
        public static Image byteToImage(byte[] arr)
        {
            MemoryStream ms = new MemoryStream(arr);
            return Image.FromStream(ms);  
        }

        public static byte[] imagetoByte(Image file)
        {
            ImageConverter converter = new ImageConverter();
            byte[] x = (byte[])converter.ConvertTo(file, typeof(byte[]));
            return x;
        }
    }
}
