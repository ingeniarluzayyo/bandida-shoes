﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Security.Cryptography;

namespace Zapateria.Libs
{
    public class Security
    {

        private List<string> listaDeCaracteresInvalidos = new List<string>();

        public Security(){
            this.listaDeCaracteresInvalidos.Add("'");
            this.listaDeCaracteresInvalidos.Add("--");
        }

        public bool ValidarSqlInjection(GroupBox GroupBox)
        {
            foreach (Control objeto in GroupBox.Controls)
            {
                if (objeto is TextBox)
                {
                    foreach (string caracterInvalido in listaDeCaracteresInvalidos)
                    {

                        if (((TextBox)objeto).Text.Contains(caracterInvalido))
                        {
                            MessageBox.Show("Error la consulta contiene caracteres extraños.");
                            return false;
                        }
                    }
                }
            }


            return true;
        }

        //encriptacion SHA256
        public static string Encriptar(string pass)
        {
            SHA256 ShaM = SHA256.Create();
            byte[] data = ShaM.ComputeHash(Encoding.Default.GetBytes(pass));
            StringBuilder sbuilder = new StringBuilder();

            for (int i = 0; i < data.Length; i++)
            {
                sbuilder.Append(data[i].ToString("x2"));
            }

            return sbuilder.ToString();
        }

    }
}
