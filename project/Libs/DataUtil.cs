﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Zapateria.Libs
{
    class DataUtil
    {

        public static DataTable ToDataTables<T>(IList<T> data)
        {
            PropertyDescriptorCollection props = TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            for (int i = 0; i < props.Count; i++)
            {
                PropertyDescriptor prp = props[i];
                table.Columns.Add(prp.Name, typeof(string));
            }
            object[] values = new object[props.Count];
            foreach (T item in data)
            {
                for (int i = 0; i < values.Length; i++)
                {                                       
                    if (!(props[i].GetValue(item).GetType().Namespace == "NHibernate.Collection.Generic"))
                    {
                        if (props[i].GetValue(item).GetType() == typeof(DateTime))
                        {
                            values[i] = ((DateTime)(props[i].GetValue(item))).ToShortDateString();
                        }else{
                            values[i] = props[i].GetValue(item).ToString();
                        }
                    }
                }
                table.Rows.Add(values);
            }
            return table;
        }

        public static DataTable ToDataTables(DataGridView dgv)
        {

            var dt = new DataTable();
            foreach (DataGridViewColumn column in dgv.Columns)
            {
                if (column.Visible)
                {
                    // You could potentially name the column based on the DGV column name (beware of dupes)
                    // or assign a type based on the data type of the data bound to this DGV column.
                    dt.Columns.Add();
                }
            }

            object[] cellValues = new object[dgv.Columns.Count];
            foreach (DataGridViewRow row in dgv.Rows)
            {
                for (int i = 0; i < row.Cells.Count; i++)
                {
                    cellValues[i] = row.Cells[i].Value;
                }
                dt.Rows.Add(cellValues);
            }

            return dt;
        } 
    }
}
