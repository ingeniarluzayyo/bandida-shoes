﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using Zapateria.Mappings;
using System.Windows.Forms;

namespace Zapateria.Libs
{
    class FluentNHibernateDB
    {
        public static ISessionFactory CreateSessionFactory()
        {
            try
            {
                ISessionFactory isessionFactory = Fluently.Configure()
                    .Database(MySQLConfiguration.Standard
                    .ConnectionString(Configuracion.Instance.obtenerConnectionString()))
                    .Mappings(m => m
                    .FluentMappings
                    .AddFromAssemblyOf<UsuarioMap>()
                                    .AddFromAssemblyOf<RolMap>()
                                    .AddFromAssemblyOf<ModuloMap>()
                                    .AddFromAssemblyOf<RepartoMap>()
                                    .AddFromAssemblyOf<ProductoMap>()
                                    .AddFromAssemblyOf<ProveedorMap>()
                                    .AddFromAssemblyOf<ItemCompraMap>()
                                    .AddFromAssemblyOf<FacturaCompraMap>()
                                    .AddFromAssemblyOf<ClienteMap>()
                                    .AddFromAssemblyOf<FormaPagoMap>()
                                    .AddFromAssemblyOf<VendedorMap>()
                                    .AddFromAssemblyOf<PagoMap>()
                                    .AddFromAssemblyOf<CtaCteMap>()
                    )
                    .BuildSessionFactory();

                return isessionFactory;
            }
            catch (FluentNHibernate.Cfg.FluentConfigurationException ex)
            {
                String text = "";
                text += "Message: " + ex.InnerException.Message + "\n";
                text += "HelpLink: " + ex.InnerException.HelpLink + "\n";
                text += "InnerException: " + ex.InnerException.InnerException + "\n";
                text += "Source: " + ex.InnerException.Source + "\n";
                text += "StackTrace: " + ex.InnerException.StackTrace + "\n";
                text += "TargetSite: " + ex.InnerException.TargetSite + "\n";

                Logs.GetInstance.LogError(text, "FluentNHibernateDB.CreateSessionFactory()");
                //MessageBox.Show(text, "Exception Msg");
                return null;
            }
        }
    }
}
