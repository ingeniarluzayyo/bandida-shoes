﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data;
using System.Configuration;
using System.Windows.Forms;

using MySql.Data.MySqlClient;

using Zapateria.Libs;

namespace Zapateria
{
    class BaseDeDatos
    {
        private static BaseDeDatos instance;
        private static MySqlConnection connection;
        private string esquema;
        private List<string> listaDeCaracteresInvalidos = new List<string>();

        private BaseDeDatos(string connectionString)
        {
            connection = new MySqlConnection(connectionString);           
            this.esquema = "";
            //Todo agregar caracteres invalidos
            this.listaDeCaracteresInvalidos.Add("'");
            this.listaDeCaracteresInvalidos.Add("--");
        }

        public static BaseDeDatos GetInstance
        {
            get
            {
                if (instance == null)
                    instance = new BaseDeDatos(Configuracion.Instance.obtenerConnectionString());
                return instance;
            }
        }

        public bool ejecutarProcedimiento(string spName, List<MySqlParameter> parameters,string NombreForm)
        {
            if (ValidarParametros(parameters))
            {
                MySqlCommand cmd = null;
                bool Termine;
                try
                {
                    connection.Open();
                    cmd = new MySqlCommand(spName, connection);
                    cmd.Parameters.AddRange(parameters.ToArray());
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.ExecuteNonQuery();

                    Termine = true;
                    return Termine;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Logs.GetInstance.LogError(ex.Message, NombreForm);

                    Termine = false;
                    return Termine;

                }
                finally
                {
                    if (cmd != null) cmd.Connection.Close();
                }
            }
            else
            {
                return false;
            }
        }

        public DataSet ejecutarConsulta(string spName, List<MySqlParameter> parameters, string nombre, string NombreForm)
        {
              if (ValidarParametros(parameters))
              {
                MySqlDataAdapter da = null;
                DataSet ds = new DataSet();
                try
                {
                    connection.Open();
                    da = new MySqlDataAdapter(spName, connection);
                    da.SelectCommand.Parameters.AddRange(parameters.ToArray());
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;

                    da.Fill(ds, nombre);

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Logs.GetInstance.LogError(ex.Message, NombreForm);
                }
                finally
                {
                    if (da != null) da.SelectCommand.Connection.Close();
                }
                return ds;
            }else{
                return null;
            }
        }


        public DataSet ejecutarConsultaCargarDosTablas(string spName, List<MySqlParameter> parameters, string nombre, string NombreForm, DataSet ds)
        {
            if (ValidarParametros(parameters))
            {
                MySqlDataAdapter da = null;
                try
                {
                    connection.Open();
                    da = new MySqlDataAdapter(spName, connection);
                    da.SelectCommand.Parameters.AddRange(parameters.ToArray());
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;

                    da.Fill(ds, nombre);

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Logs.GetInstance.LogError(ex.Message, NombreForm);
                }
                finally
                {
                    if (da != null) da.SelectCommand.Connection.Close();
                }
                return ds;
            }
            else
            {
                return null;
            }
        }

        public DataTable ExecuteCustomQuery(string strQuery, List<MySqlParameter> parameters, string NombreForm)
        {
            if (ValidarParametros(parameters))
            {
                MySqlDataAdapter da = null;
                try
                {
                    connection.Open();
                    da = new MySqlDataAdapter(strQuery, connection);
                    da.SelectCommand.Parameters.AddRange(parameters.ToArray());
                    da.SelectCommand.CommandType = CommandType.Text;

                    DataTable dt = new DataTable();
                    da.Fill(dt);
                    if (dt.Rows.Count > 0)
                        return dt;
                    else
                        return null;
                }
                catch (Exception ex)
                {
                    Logs.GetInstance.LogError(ex.Message, NombreForm);
                    //throw new Exception("Hubo inconvenientes al querer ejecutar la query: '" + strQuery + "'", ex);
                    MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return null;

                }
                finally
                {
                    if (da != null) da.SelectCommand.Connection.Close();
                }
            }
            else
            {
                return null;
            }
        }

        private bool ValidarParametros(List<MySqlParameter> parametros)
        {
            foreach (MySqlParameter param in parametros)
                {
                    foreach (string caracterInvalido in listaDeCaracteresInvalidos)
                    {
                        if (param.Value.ToString().Contains(caracterInvalido))
                        {
                            MessageBox.Show("Error la consulta contiene caracteres extraños.");
                            return false;
                        }
                    }
                }
                return true;
            
        }
    }
}
