﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using Zapateria.Mappings;
using System.Windows.Forms;

namespace Zapateria.Libs
{
    class SessionManager
    {
        static readonly object factorylock = new object();

        private static ISessionFactory sessionFactory;
        private static ISession sessionGlobal;

        public static ISession OpenSession()
        {
            var sFactory =  FluentNHibernateDB.CreateSessionFactory();

            if (sFactory is ISessionFactory)
            {
                sessionGlobal = sFactory.OpenSession();
                return sessionGlobal;
            }
            else {
                return null;
            }
        }

        public static void EndTasks()
        {
            sessionGlobal.Flush();
            sessionGlobal.Close();
            sessionGlobal = null;
        }

        public static ISession OpenSessionOld()
        {
            try
            {
                lock (factorylock)
                {
                    if (sessionFactory == null)
                    {
                        sessionFactory = Fluently.Configure()
                            .ExposeConfiguration(x => x.SetProperty("current_session_context_class", "thread_static"))
                            .Database(MySQLConfiguration.Standard
                            .ConnectionString(Configuracion.Instance.obtenerConnectionString()))
                            .Mappings(m => m
                                .FluentMappings
                                .AddFromAssemblyOf<UsuarioMap>()
                                .AddFromAssemblyOf<RolMap>()
                                .AddFromAssemblyOf<ModuloMap>()
                                .AddFromAssemblyOf<ProductoMap>()
                                .AddFromAssemblyOf<ProveedorMap>()
                                .AddFromAssemblyOf<ItemCompraMap>()
                                .AddFromAssemblyOf<FacturaCompraMap>()
                                .AddFromAssemblyOf<FacturaVentaMap>()
                                .AddFromAssemblyOf<ClienteMap>()
                                .AddFromAssemblyOf<RepartoMap>()
                                .AddFromAssemblyOf<FormaPagoMap>()
                                .AddFromAssemblyOf<VendedorMap>()
                                .AddFromAssemblyOf<PagoMap>()
                                .AddFromAssemblyOf<CtaCteMap>()
                            )
                            .BuildSessionFactory();
                       
                    }
                }
            }
            catch (FluentNHibernate.Cfg.FluentConfigurationException ex)
            {
                String text = "";
                text += "Message: " +ex.InnerException.Message + "\n";
                text += "HelpLink: " +ex.InnerException.HelpLink + "\n";
                text += "InnerException: " +ex.InnerException.InnerException + "\n";
                text += "Source: " +ex.InnerException.Source + "\n";
                text += "StackTrace: " +ex.InnerException.StackTrace + "\n";
                text += "TargetSite: " +ex.InnerException.TargetSite + "\n";

                Logs.GetInstance.LogError(text, "SessionManager.OpenSession()");
                MessageBox.Show(text, "Exception Msg");
                return null;
            }
            return sessionFactory.OpenSession();
        }
    }
}
