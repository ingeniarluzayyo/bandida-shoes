﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Zapateria.Libs
{
    class FormsUtils
    {

        public static  void CheckUncheckAll(CheckedListBox c)
        {
            for (int index = 0; index < c.Items.Count; ++index)
            {
                c.SetItemChecked(index, false);
            }
        }
    }
}
