﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Mail;
using System.Net;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace Zapateria.Libs
{
    class Mailer
    {
        SmtpClient client = new SmtpClient();

        public string host { get; set; }
        public int port { get; set; }
        public string email { get; set; }
        public string password { get; set; }

        private bool connect() {
            try
            {
                client.Host = host;
                client.Port = port;
                client.UseDefaultCredentials = false;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.EnableSsl = false;
                client.Credentials = new NetworkCredential(email, password);

                return true;
            }
            catch(Exception e) {
                return false;
            }
        }

        public bool send(String from, String to, String subject, String body)
        {
            // Set cursor as hourglass
            Cursor.Current = Cursors.WaitCursor;

            if (!this.connect()) { return false; }
                
            MailMessage objeto_mail = new MailMessage();
            objeto_mail.From = new MailAddress(from);
            objeto_mail.To.Add(new MailAddress(to));
            objeto_mail.Subject = subject;
            objeto_mail.Body = body;

            try
            {
                client.Send(objeto_mail);
                return true;
            }
            catch(Exception e) {
                return false;
            }
            finally
            {
                // Set cursor as default arrow
                Cursor.Current = Cursors.Default;
            }
             
        }

    }
}
