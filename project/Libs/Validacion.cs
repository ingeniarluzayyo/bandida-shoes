﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace Zapateria.Libs
{
    class Validacion
    {
        public static bool validarLetrasTextBox(KeyPressEventArgs e,string nombreTextBox)
        {
            if (Char.IsLetter(e.KeyChar))
            {
                e.Handled = false;
                return true;
            }
            else if (Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
                return true;
            }
            else if (Char.IsSeparator(e.KeyChar))
            {
                e.Handled = false;
                return true;
            }
            else
            {
                e.Handled = true;
                MessageBox.Show("El campo " + nombreTextBox + " solo puede contener letras.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            return false;
        }

        public static bool validarNumerosTextBox(KeyPressEventArgs e, string nombreTextBox)
        {
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
                return true;
            }
            else if (Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
                return true;
            }
            else if (Char.IsSeparator(e.KeyChar))
            {
                e.Handled = false;
                return true;
            }
            else
            {
                e.Handled = true;
                MessageBox.Show("El campo " + nombreTextBox + " solo puede contener números.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            return false;
        }

        public static bool validarDecimalTextBox(KeyPressEventArgs e, string nombreTextBox)
        {
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
                return true;
            }
            else if (Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
                return true;
            }
            else if (Char.IsSeparator(e.KeyChar))
            {
                e.Handled = false;
                return true;
            }
            else if (e.KeyChar == ',')
            {
                e.Handled = false;
                return true;
            }
            else if (e.KeyChar == '.')
            {
                e.KeyChar = (char)44;
                e.Handled = false; ;
                return true;
            }
            else
            {
                e.Handled = true;
                MessageBox.Show("El campo " + nombreTextBox + " solo puede contener números decimales (ej: 3.4).", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            return false;
        }

        public static bool validarLetrasYNumerosTextBox(KeyPressEventArgs e, string nombreTextBox)
        {
            if (Char.IsLetter(e.KeyChar))
            {
                e.Handled = false;
                return true;
            }
            else if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
                return true;
            }else if (Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
                return true;
            }
            else if (Char.IsSeparator(e.KeyChar))
            {
                e.Handled = false;
                return true;
            }
            else
            {
                e.Handled = true;
                MessageBox.Show("El campo " + nombreTextBox + " solo puede contener ser letras y números.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            return false;
        }

        public static bool isDecimal(String cadena)
        {
            if (cadena == "")
            {
                return false;
            }

            if (cadena.Count(caracter => caracter == ',') > 0)//Un decimal no puede tener punto "." o "," segun el idioma
            {
                return false;
            }
            /*if (cadena == "")
            {
                return false;
            }

            if (cadena.Count(caracter => caracter == '.') > 1 || cadena.Count(caracter => caracter == ',') > 1 )//Un decimal no puede tenr mas de un '.'
            {
                return false;
            }

            if (cadena.Count(caracter => caracter == '.') ==  1 && cadena.Count(caracter => caracter == ',') == 1)//Un decimal no puede tenr mas de un '.'
            {
                return false;
            }

            for (int i = 0; i < cadena.Length; i++)
            {
                if ((!Char.IsNumber(cadena[i])) & cadena[i] != ',')
                {
                    return false;
                }
            }*/
            decimal x;
            return decimal.TryParse(cadena, out x);
        }

        public static bool isNumber(String cadena)
        {
            if (cadena == "")
            {
                return false;
            }

            if (cadena.Count(caracter => caracter == '.') > 1)//Un decimal no puede tenr mas de un '.'
            {
                return false;
            }

            for (int i = 0; i < cadena.Length; i++)
            {
                if (!cadena[i].Equals(".")){
                    if ((!Char.IsNumber(cadena[i])))
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        public static bool isValidEmail(string emailaddress)
        {
            try
            {
                Regex rx = new Regex(
            @"^[-!#$%&'*+/0-9=?A-Z^_a-z{|}~](\.?[-!#$%&'*+/0-9=?A-Z^_a-z{|}~])*@[a-zA-Z](-?[a-zA-Z0-9])*(\.[a-zA-Z](-?[a-zA-Z0-9])*)+$");
                return rx.IsMatch(emailaddress);
            }
            catch (FormatException)
            {
                return false;
            }
        }
    }
}
