﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Zapateria.Libs
{
    class Configuracion
    {
        private static Configuracion instance;

        public static Configuracion Instance
        {
            get
            {
                if (instance == null)
                    instance = new Configuracion();
                return instance;
            }
        }

        public string obtenerConnectionString()
        {
            return Zapateria.Properties.Settings.Default.ConnectionStringProd;
        }

    }
}
