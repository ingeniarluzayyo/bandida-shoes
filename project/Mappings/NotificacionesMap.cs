﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;
using Zapateria.Models;

namespace Zapateria.Mappings
{
    class NotificacionesMap : ClassMap<Notificaciones>
    {
        public NotificacionesMap()
        {
            Table("notificaciones");
            Id(x => x.id).GeneratedBy.Identity();
            Map(x => x.descripcion);
            Map(x => x.fecha);
            Map(x => x.estado);
        }
    }
}
