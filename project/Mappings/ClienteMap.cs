﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Zapateria.Models;
using FluentNHibernate.Mapping;

namespace Zapateria.Mappings
{
    class ClienteMap : ClassMap<Cliente>
    {
        public ClienteMap()
        {
            Table("clientes");
            Id(x => x.id).GeneratedBy.Identity();
            Map(x => x.razon_social);
            Map(x => x.cuit);
            Map(x => x.direccion);
            Map(x => x.tel);
            Map(x => x.localidad);            
            Map(x => x.observacion);
            Map(x => x.orden);
            Map(x => x.habilitado);
        }    
    }
}
