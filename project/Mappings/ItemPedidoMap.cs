﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Mayorista.Models;
using FluentNHibernate.Mapping;

namespace Mayorista.Mappings
{
    class ItemPedidoMap : ClassMap<ItemPedido>
    {
        public ItemPedidoMap()
        {
            Table("item_orden_pedido");
            Id(x => x.id);
            References(x => x.pedido).Column("pedido");
            References(x => x.producto).Column("producto").Cascade.SaveUpdate();
            Map(x => x.precio);
            Map(x => x.cantidad);
        }
    }
}
