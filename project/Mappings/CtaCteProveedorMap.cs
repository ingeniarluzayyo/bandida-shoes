﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;
using Zapateria.Models;

namespace Zapateria.Mappings
{
    class CtaCteProveedorMap : ClassMap<CtaCteProveedor>
    {
        public CtaCteProveedorMap()
        {
            Table("cta_cte_proveedores");
            Id(x => x.id).GeneratedBy.Identity();
            Map(x => x.ultima_actualizacion);
            References(x => x.proveedor).Column("proveedor").Fetch.Join(); ;
            Map(x => x.total_facturado);
            Map(x => x.total_pagado);
            Map(x => x.saldo);
        }
    }
}
