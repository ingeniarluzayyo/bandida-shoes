﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;
using Zapateria.Models;

namespace Zapateria.Mappings
{
    class ColorZapatoMap : ClassMap<ColorZapato>
    {
        public ColorZapatoMap()
        {
            Table("colores");
            Id(x => x.id);
            Map(x => x.nombre);
        }
    }
}
