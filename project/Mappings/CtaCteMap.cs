﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;
using Zapateria.Models;

namespace Zapateria.Mappings
{
    class CtaCteMap : ClassMap<CtaCte>
    {
        public CtaCteMap()
        {
            Table("cta_cte");
            Id(x => x.id).GeneratedBy.Identity();
            Map(x => x.ultima_actualizacion);
            References(x => x.cliente).Column("cliente").Fetch.Join(); ;
            Map(x => x.total_facturado);
            Map(x => x.total_pagado);
            Map(x => x.saldo);
        }
    }
}
