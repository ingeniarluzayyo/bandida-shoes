﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Zapateria.Models;
using FluentNHibernate.Mapping;

namespace Zapateria.Mappings
{
    class FormatoVentaMap: ClassMap<FormatoVenta>
    {
        public FormatoVentaMap()
        {
            Table("formato_venta");
            Id(x => x.id).GeneratedBy.Identity();
            Map(x => x.formato);
        }
    }
}
