﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;
using Zapateria.Models;

namespace Zapateria.Mappings
{
    class StockMap : ClassMap<Stock>
    {
        public StockMap()
        {
            Table("stock");
            Id(x => x.id).GeneratedBy.Identity();
            References(x => x.producto).Column("producto").Fetch.Join();
            References(x => x.sucursal).Column("sucursal").Fetch.Join();
            References(x => x.zona).Column("zona").Fetch.Join();
            References(x => x.talle).Column("talle").Fetch.Join();
            References(x => x.color).Column("color").Fetch.Join();
            Map(x => x.stock);
        }
    }
}
