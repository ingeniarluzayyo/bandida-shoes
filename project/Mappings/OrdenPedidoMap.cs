﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Mayorista.Models;
using FluentNHibernate.Mapping;

namespace Mayorista.Mappings
{
    class OrdenPedidoMap : ClassMap<OrdenPedido>
    {
        public OrdenPedidoMap()
        {
            Table("orden_pedido");
            Id(x => x.id).GeneratedBy.Identity();
            Map(x => x.fecha);
            References(x => x.cliente).Column("cliente").Fetch.Join();
            References(x => x.vendedor).Column("vendedor").Fetch.Join();
            References(x => x.forma_de_pago).Column("forma_de_pago").Fetch.Join();
            Map(x => x.descuento);
            Map(x => x.total);
            Map(x => x.total_comision);
            HasMany(x => x.items).Cascade.All(); 
        }
    }
}
