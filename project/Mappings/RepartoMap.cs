﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;
using Zapateria.Models;

namespace Zapateria.Mappings
{
    class RepartoMap : ClassMap<Reparto>
    {
        public RepartoMap()
        {
            Table("repartos");
            Id(x => x.id);
            Map(x => x.nombre);
        }
    }
}
