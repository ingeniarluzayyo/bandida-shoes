﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Zapateria.Models;
using FluentNHibernate.Mapping;

namespace Zapateria.Mappings
{
    class FamiliaGastoMap: ClassMap<FamiliaGasto>
    {
        public FamiliaGastoMap()
        {
            Table("familias_gastos");
            Id(x => x.id).GeneratedBy.Identity();
            Map(x => x.nombre);
        }
    }
}
