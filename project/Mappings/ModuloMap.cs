﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Zapateria.Models;
using FluentNHibernate.Mapping;

namespace Zapateria.Mappings
{
    class ModuloMap: ClassMap<Modulo>
    {
        public ModuloMap()
        {
            Table("modulos");
            Id(x => x.id);
            Map(x => x.nombre);
            HasManyToMany(x => x.roles)
               .Table("roles_x_modulos")
               .ParentKeyColumn("modulo")
               .ChildKeyColumn("rol")
               .Cascade.All();
      
        }
    }
}
