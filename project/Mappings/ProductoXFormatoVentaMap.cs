﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Zapateria.Models;
using FluentNHibernate.Mapping;

namespace Zapateria.Mappings
{
    class ProductoXFormatoVentaMap : ClassMap<ProductoXFormatoVenta>
    {
        public ProductoXFormatoVentaMap()
        {
            Table("producto_x_formato_venta");
            Id(x => x.id).GeneratedBy.Identity();
            References(x => x.producto).Column("producto_id").Fetch.Join();
            References(x => x.formato).Column("formato_venta_id").Fetch.Join();
            Map(x => x.precio_venta);
        }
    }
}
