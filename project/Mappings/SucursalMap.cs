﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;
using Zapateria.Models;

namespace Zapateria.Mappings
{
    class SucursalMap : ClassMap<Sucursal>
    {
        public SucursalMap()
        {
            Table("sucursal");
            Id(x => x.id).GeneratedBy.Identity();
            Map(x => x.nombre);            
        }
    }
}
