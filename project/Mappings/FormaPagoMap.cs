﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Zapateria.Models;
using FluentNHibernate.Mapping;

namespace Zapateria.Mappings
{
    class FormaPagoMap : ClassMap<FormaPago>
    {
        public FormaPagoMap()
        {
            Table("forma_de_pago");
            Id(x => x.id).GeneratedBy.Identity();
            Map(x => x.nombre);
        }
    }
}
