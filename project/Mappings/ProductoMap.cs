﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Zapateria.Models;
using FluentNHibernate.Mapping;

namespace Zapateria.Mappings
{
    class ProductoMap : ClassMap<Producto>
    {
        public ProductoMap()
        {
            Not.LazyLoad();
            Table("productos");
            Id(x => x.id).Column("id").GeneratedBy.Assigned();
            References<Familia>(x => x.familia).Column("familia").Fetch.Join();
            Map(x => x.nombre);
            Map(x => x.unidad);
            Map(x => x.fraccionado);
            Map(x => x.observacion);
            Map(x => x.stock);
            Map(x => x.precio_compra);
            Map(x => x.precio_venta_x3);
            Map(x => x.precio_venta_x6);
            Map(x => x.precio_venta_x12);
            Map(x => x.habilitado);
            Map(x => x.orden);
        }
    }
}
