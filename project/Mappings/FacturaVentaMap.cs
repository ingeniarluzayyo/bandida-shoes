﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Zapateria.Models;
using FluentNHibernate.Mapping;

namespace Zapateria.Mappings
{
    class FacturaVentaMap : ClassMap<FacturaVenta>
    {
        public FacturaVentaMap()
        {
            Table("factura_venta");
            Id(x => x.id).GeneratedBy.Identity();
            References(x => x.sucursal).Column("sucursal").Fetch.Join();
            Map(x => x.fecha);
            References(x => x.cliente).Column("cliente").Fetch.Join();
            References(x => x.vendedor).Column("vendedor").Fetch.Join();
            References(x => x.forma_de_pago).Column("forma_de_pago").Fetch.Join();
            Map(x => x.descuento);
            Map(x => x.total);
            Map(x => x.total_comision);
            Map(x => x.anulada);
            HasMany(x => x.items)
                .Cascade.All();
                //.Fetch.Join();
        }
    }
}
