﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Zapateria.Models;
using FluentNHibernate.Mapping;

namespace Zapateria.Mappings
{
    class RolMap : ClassMap<Rol>
    {
        public RolMap()
        {
            Table("roles");
            Id(x => x.id).GeneratedBy.Identity();
            Map(x => x.nombre);
            HasManyToMany(x => x.modulos)
               //.Fetch.Join()
               .Table("roles_x_modulos")
               .ParentKeyColumn("rol")
               .ChildKeyColumn("modulo")               
               .Cascade.All();
        }
    }
}
