﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Zapateria.Models;
using FluentNHibernate.Mapping;

namespace Zapateria.Mappings
{
    class FacturaCompraMap : ClassMap<FacturaCompra>
    {
        public FacturaCompraMap()
        {
            Table("factura_compra");
            Id(x => x.id).GeneratedBy.Identity();
            Map(x => x.num_fact_externo);
            Map(x => x.fecha);
            References(x => x.proveedor).Column("proveedor").Fetch.Join();
            References(x => x.forma_de_pago).Column("forma_de_pago").Fetch.Join();
            References(x => x.sucursal).Column("sucursal").Fetch.Join();
            Map(x => x.descuento);
            Map(x => x.total);
            Map(x => x.anulada);
            HasMany(x => x.items)
                .Cascade.All();
                //.Fetch.Join();
        }
    }
}
