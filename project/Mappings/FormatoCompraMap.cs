﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Zapateria.Models;
using FluentNHibernate.Mapping;

namespace Zapateria.Mappings
{
    class FormatoCompraMap: ClassMap<FormatoCompra>
    {
        public FormatoCompraMap()
        {
            Table("formato_compra");
            Id(x => x.id).GeneratedBy.Identity();
            Map(x => x.formato);
        }
    }
}
