﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Zapateria.Models;
using FluentNHibernate.Mapping;

namespace Zapateria.Mappings
{
    class UsuarioMap : ClassMap<Usuario>
    {
        public UsuarioMap()
        {
            Table("usuarios");
            Id(x => x.username);
            Map(x => x.password);
            Map(x => x.email);
            //HasOne(x => x.rol).Constrained();
            References<Rol>(x => x.rol).Column("rol").Fetch.Join();
            References<Sucursal>(x => x.sucursal_default).Column("sucursal_default").Fetch.Join();
        }
    }
}
