﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Zapateria.Models;
using FluentNHibernate.Mapping;

namespace Zapateria.Mappings
{
    class ItemCompraMap : ClassMap<ItemCompra>
    {
        public ItemCompraMap()
        {
            Table("item_factura_compra");
            Id(x => x.id);
            References(x => x.factura).Column("factura");
            References(x => x.producto).Column("producto");
            References(x => x.talle).Column("talle").Not.LazyLoad();
            References(x => x.color).Column("color").Not.LazyLoad();
            Map(x => x.precio);
            Map(x => x.formato);
            Map(x => x.cantidad);
        }
    }
}
