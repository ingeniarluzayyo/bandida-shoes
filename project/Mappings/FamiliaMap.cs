﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Zapateria.Models;
using FluentNHibernate.Mapping;

namespace Zapateria.Mappings
{
    class FamiliaMap: ClassMap<Familia>
    {
        public FamiliaMap()
        {
            Table("familias");
            Id(x => x.id).GeneratedBy.Identity();
            Map(x => x.nombre);
        }
    }
}
