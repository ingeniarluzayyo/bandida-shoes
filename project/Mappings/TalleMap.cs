﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;
using Zapateria.Models;

namespace Zapateria.Mappings
{
    class TalleMap : ClassMap<Talle>
    {
        public TalleMap()
        {
            Table("talles");
            Id(x => x.id);
            Map(x => x.nombre);
        }
    }
}
