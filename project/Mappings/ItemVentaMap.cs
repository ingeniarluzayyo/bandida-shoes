﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Zapateria.Models;
using FluentNHibernate.Mapping;

namespace Zapateria.Mappings
{
    class ItemVentaMap : ClassMap<ItemVenta>
    {
        public ItemVentaMap()
        {
            Table("item_factura_venta");
            Id(x => x.id);
            References(x => x.factura).Column("factura");
            References(x => x.producto).Column("producto");
            References(x => x.talle).Column("talle").Not.LazyLoad();
            References(x => x.color).Column("color").Not.LazyLoad();
            References(x => x.sucursal).Column("sucursal").Not.LazyLoad();
            References(x => x.zona).Column("zona").Not.LazyLoad();
            Map(x => x.precio);
            Map(x => x.formato);
            Map(x => x.cantidad);
            Map(x => x.descuento);
        }
    }
}
