﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Zapateria.Models;
using FluentNHibernate.Mapping;

namespace Zapateria.Mappings
{
    class SubfamiliaGastoMap: ClassMap<SubfamiliaGasto>
    {
        public SubfamiliaGastoMap()
        {
            Table("subfamilia_gastos");
            Id(x => x.id).GeneratedBy.Identity();
            Map(x => x.nombre);
            References(x => x.familia_gasto).Column("familia_gasto").Fetch.Join();
        }
    }
}
