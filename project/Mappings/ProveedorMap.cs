﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Zapateria.Models;
using FluentNHibernate.Mapping;

namespace Zapateria.Mappings
{
    class ProveedorMap : ClassMap<Proveedor>
    {
        public ProveedorMap()
        {
            Table("proveedores");
            Id(x => x.id).GeneratedBy.Identity();
            Map(x => x.razon_social);
            Map(x => x.cuit);
            Map(x => x.direccion);
            Map(x => x.tel);
            Map(x => x.cel);
            Map(x => x.mail);
            Map(x => x.observacion);            
        }    
    }
}
