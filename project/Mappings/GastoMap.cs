﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;
using Zapateria.Models;

namespace Zapateria.Mappings
{
    class GastoMap: ClassMap<Gasto>
    {
        public GastoMap()
        {
            Table("gastos");
            Id(x => x.id).GeneratedBy.Identity();
            References(x => x.sucursal).Column("sucursal").Fetch.Join();
            Map(x => x.fecha);
            References(x => x.forma_de_pago).Column("forma_de_pago").Fetch.Join();
            References(x => x.familia).Column("familia_gasto").Fetch.Join();
            References(x => x.subfamilia_gasto).Column("subfamilia_gasto").Fetch.Join().Nullable();
            References(x => x.vendedor).Column("vendedor").Fetch.Join().Nullable();
            Map(x => x.descripcion);
            Map(x => x.monto);
            Map(x => x.anulado);
        }
    }
}
