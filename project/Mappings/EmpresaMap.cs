﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Zapateria.Models;
using FluentNHibernate.Mapping;

namespace Zapateria.Mappings
{
    class EmpresaMap : ClassMap<Empresa>
    {
        public EmpresaMap()
        {
            Table("empresa");
            Id(x => x.id).GeneratedBy.Identity();
            Map(x => x.razon_social);
            Map(x => x.cuit);
            Map(x => x.direccion);
            Map(x => x.telefono);
            Map(x => x.etiqueta);
            Map(x => x.servidor);
            Map(x => x.puerto);
            Map(x => x.email);
            Map(x => x.pass);
            Map(x => x.imagen);
        }
    }
}
