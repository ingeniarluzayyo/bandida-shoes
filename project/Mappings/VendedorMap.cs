﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Zapateria.Models;
using FluentNHibernate.Mapping;

namespace Zapateria.Mappings
{
    class VendedorMap : ClassMap<Vendedor>
    {
        public VendedorMap()
        {
            Table("vendedores");
            Id(x => x.id).GeneratedBy.Identity();
            References(x => x.sucursal).Column("sucursal").Fetch.Join();
            Map(x => x.nombre);
            Map(x => x.apellido);
            Map(x => x.dni);
            Map(x => x.fechanacimiento);
            Map(x => x.direccion);
            Map(x => x.tel);
            Map(x => x.cel);
            Map(x => x.mail);            
            Map(x => x.observacion);
            Map(x => x.comision);
            Map(x => x.username);
            Map(x => x.password);
        }    
    }
}
