﻿using System;
using System.Collections.Generic;
using System.Linq;
using Zapateria.Models;
using FluentNHibernate.Mapping;
using System.Text;

namespace Zapateria.Mappings
{
    class PagoProveedorMap : ClassMap<PagoProveedor>
    {
        public PagoProveedorMap()
        {
            Table("pagos_proveedor");
            Id(x => x.id).GeneratedBy.Identity();
            Map(x => x.fecha);
            References(x => x.proveedor).Column("proveedor").Fetch.Join(); 
            References(x => x.forma_de_pago).Column("forma_de_pago").Fetch.Join();
            Map(x => x.monto);
            Map(x => x.anulada);
        }
    }
}
