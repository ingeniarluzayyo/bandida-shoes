﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Zapateria.Modules.Reportes;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;

namespace Zapateria.Modules.Cta_Cte_Proveedor
{
    public partial class ListadoCtaCteProveedorReporteForm : Form
    {
        public ListadoCtaCteProveedorReporteForm(DataTable dt,string saldo)
        {
            InitializeComponent();
            this.dt = dt;
            this.saldo = saldo;
        }

        DataTable dt;
        string saldo;

        private void crystalReportViewer1_Load(object sender, EventArgs e)
        {
            ListadoCtaCteProveedorCrystalReport Repo = new ListadoCtaCteProveedorCrystalReport();

            //Variables
            TextObject FechaRepo;
            FechaRepo = (TextObject)Repo.ReportDefinition.ReportObjects["FechaText"];
            FechaRepo.Text = DateTime.Now.ToShortDateString();

            TextObject SaldoRepo;
            SaldoRepo = (TextObject)Repo.ReportDefinition.ReportObjects["SaldoAcumuladoText"];
            SaldoRepo.Text = saldo;

            Repo.SetDataSource(dt);

            crystalReportViewer1.ReportSource = Repo;
        }
    }
}
