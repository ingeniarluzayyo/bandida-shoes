﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using Zapateria.Models;
using Zapateria.Libs;

namespace Zapateria.Modules.Cta_Cte_Proveedor
{
    public partial class ListCtaCteProveedorForm : Form
    {
        public ListCtaCteProveedorForm()
        {
            InitializeComponent();
        }

        IList<CtaCteProveedor> ctas = null;

        private void BuscarButton_Click(object sender, EventArgs e)
        {
            this.Cta_Cte_DataGridView.Columns.Clear();
            CtaCteProveedor cteCte = new CtaCteProveedor();
            decimal saldo_total = 0;
            int proveedor_id = 0;

            if (ProveedorComboBox.SelectedValue != null && ProveedorComboBox.SelectedValue.ToString() != "")
            {
                proveedor_id = Convert.ToInt32(ProveedorComboBox.SelectedValue.ToString());
                
                cteCte = cteCte.getByProveedor(proveedor_id);

                if (cteCte != null)
                {
                    ctas = new List<CtaCteProveedor>() { (cteCte) };
                    saldo_total = ctas[0].saldo;
                }
            }
            else
            {
                ctas = cteCte.getAll();
                if (ctas != null) {
                    foreach (CtaCteProveedor c in ctas)
                    {
                        saldo_total += c.saldo;
                    }
                }
            }

            if (ctas != null && ctas.Count() == 0)
            {
                MessageBox.Show("No se encontraron resultados que coincidan con la busqueda.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            else
            {
                if (ctas != null)
                {
                    Cta_Cte_DataGridView.DataSource = ctas;
                    cteCte.customColumns(Cta_Cte_DataGridView, proveedor_id);
                }
                else {
                    MessageBox.Show("No hay datos en la cuenta corriente del proveedor.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                
            }

            TotalTextBox.Text = saldo_total.ToString();


            this.Cta_Cte_DataGridView.Focus();
        }

        private void ListCtaCteForm_Load(object sender, EventArgs e)
        {
            CargadorDeDatos.CargarProveedorComboBox(ProveedorComboBox, this.Text);
        }

        private void Cta_Cte_DataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == this.Cta_Cte_DataGridView.Columns["Ver Detalle"].Index && e.RowIndex >= 0)
            {
                CtaCteProveedorForm abrir = new CtaCteProveedorForm(Convert.ToInt32(Cta_Cte_DataGridView.CurrentRow.Cells["Proveedor_Id"].Value.ToString()));
                abrir.ShowDialog();
            }
        }

        private void LimpiarButton_Click(object sender, EventArgs e)
        {
            foreach (Control objeto in this.GroupBox.Controls)
            {
                if (objeto is TextBox)
                {
                    ((TextBox)objeto).Clear();
                }
                if (objeto is ComboBox)
                {
                    ((ComboBox)objeto).SelectedItem = null;
                }
            }

            TotalTextBox.Text = "0";

            ModDataGridView.limpiarDataGridView(Cta_Cte_DataGridView, "");
            Cta_Cte_DataGridView.Columns.Clear();
        }

        private void ImprimirEtiquetaButton_Click(object sender, EventArgs e)
        {
            if (Cta_Cte_DataGridView.DataSource == null)
            {
                return;
            }
            
            Thread hilo = new Thread(AbrirFormReporte);
            hilo.SetApartmentState(System.Threading.ApartmentState.STA);
            hilo.Start();
             
        }

        private void AbrirFormReporte()
        {
            DataTable dt = DataUtil.ToDataTables((IList<CtaCteProveedor>)Cta_Cte_DataGridView.DataSource);
            ListadoCtaCteProveedorReporteForm abrir = new ListadoCtaCteProveedorReporteForm(dt, TotalTextBox.Text);
            abrir.ShowDialog();            
        }        
    }
}
