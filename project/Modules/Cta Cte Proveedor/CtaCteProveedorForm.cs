﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using Zapateria.Libs;
using System.Threading;
using Zapateria.Models;

namespace Zapateria.Modules.Cta_Cte_Proveedor
{
    public partial class CtaCteProveedorForm : Form
    {
        CtaCteProveedor ctaCte = new CtaCteProveedor();
        DataTable dt = null;

        public CtaCteProveedorForm(int proveedor_id)
        {
            InitializeComponent();
            ctaCte = ctaCte.getByProveedor(proveedor_id);
        }        

        private void CancelarButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private string ArmarConsulta()
        {
            string Consulta = "SELECT * FROM (SELECT CONCAT('Factura N°',V.id) as 'Elemento',fecha as 'Fecha',total as 'Facturado',0 as 'Pagado',F.nombre as 'Forma de pago' FROM factura_compra as V "; 
            Consulta += "INNER JOIN forma_de_pago as F ON forma_de_pago = F.id ";
            Consulta += "WHERE proveedor = " + ctaCte.proveedor.id + " AND F.id = 2 ";
            Consulta += "UNION ";
            Consulta += "SELECT CONCAT('Factura N°',V.id) as 'Elemento',fecha as 'Fecha',total as 'Facturado',total as 'Pagado',F.nombre as 'Forma de pago' FROM factura_compra as V ";
            Consulta += "INNER JOIN forma_de_pago as F ON forma_de_pago = F.id ";
            Consulta += "WHERE proveedor = " + ctaCte.proveedor.id + " AND F.id != 2 ";
            Consulta += "UNION ";
            Consulta += "SELECT CONCAT('Pago N°',P.id) as 'Elemento',fecha as 'Fecha',0 as 'Facturado',monto as 'Pagado',F.nombre as 'Forma de pago' FROM pagos_proveedor as P ";
            Consulta += "INNER JOIN forma_de_pago as F ON forma_de_pago = F.id ";
            Consulta += "WHERE proveedor = " + ctaCte.proveedor.id;
            Consulta += ") as R ORDER BY fecha";
    
            return Consulta;
        }

        private void CtaCteForm_Load(object sender, EventArgs e)
        {
            TotalTextBox.Text = String.Format("{0:#,###0.00}", 0);

            NombreTextBox.Text = ctaCte.proveedor.razon_social;
            ApellidoTextBox.Text = ctaCte.proveedor.cuit;
            TelefonotextBox.Text = ctaCte.proveedor.tel;
            CelularTextBox.Text = ctaCte.proveedor.cel;
            EmailTextBox.Text = ctaCte.proveedor.mail;            

            TotalTextBox.Text = ctaCte.saldo.ToString();

            string Consulta = ArmarConsulta();

            List<MySqlParameter> param = new List<MySqlParameter>();
            dt = BaseDeDatos.GetInstance.ExecuteCustomQuery(Consulta, param, this.Text);

            dt.Columns.Add("Saldo");

            Decimal saldo_total = 0;

            foreach (DataRow row in dt.Rows)
            {
                Decimal saldo_parcial = (-1 * Convert.ToDecimal(row["Facturado"].ToString())) + Convert.ToDecimal(row["Pagado"].ToString());
                saldo_total = saldo_total + saldo_parcial;
                row["Saldo"] = saldo_total;
            }

            Cta_Cte_DataGridView.DataSource = dt;

            this.Cta_Cte_DataGridView.Columns["Forma de Pago"].Width = 160;
            this.Cta_Cte_DataGridView.Focus();
        }

        private void ImprimirEtiquetaButton_Click(object sender, EventArgs e)
        {
            if (Cta_Cte_DataGridView.DataSource == null)
            {
                return;
            }
            
            Thread hilo = new Thread(AbrirFormReporte);
            hilo.SetApartmentState(System.Threading.ApartmentState.STA);
            hilo.Start();
             
        }

        private void AbrirFormReporte()
        {
            MovimientosProveedorReporteForm abrir = new MovimientosProveedorReporteForm(ctaCte, dt);
            abrir.ShowDialog();
        }
    }
}
