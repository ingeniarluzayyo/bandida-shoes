﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using MySql.Data.MySqlClient;
using Zapateria.Models;
using Zapateria.Libs;

namespace Zapateria.Abm_Formato_Compra
{
    public partial class AltaFormatoCompraForm : Form
    {
        public AltaFormatoCompraForm()
        {
            InitializeComponent();
        }

        private void GuardarButton_Click(object sender, EventArgs e)
        {
            if (Validaciones() == -1) return;

            FormatoCompra fc = new FormatoCompra();
            fc.formato = Convert.ToInt32(this.FCTextBox.Text);

            if (fc.save() == true)
            {
                MessageBox.Show("Se ha dado de alta el formato '" + FCTextBox.Text + "' correctamente.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.LimpiarButton.PerformClick();
                this.FCTextBox.Focus();
            } 

        }

        private void LimpiarButton_Click(object sender, EventArgs e)
        {
            foreach (Control objeto in this.GroupBox.Controls)
            {
                if (objeto is TextBox)
                {
                    ((TextBox)objeto).Clear();
                }                
            }
        }

        private void CancelarButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        private int Validaciones()
        {
            foreach (Control objeto in this.GroupBox.Controls)
            {
                if (objeto is TextBox && ((TextBox)objeto).Text == "")
                {
                    MessageBox.Show("Debe completar el campo vacio.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return -1;
                }
            }
            return 1;
        }

        private void AltaCategoriaForm_Load(object sender, EventArgs e)
        {
            this.FCTextBox.Select();
        }

    }
}
