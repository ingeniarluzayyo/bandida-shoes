﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Threading;
using Zapateria.Estadisticas.Impresion;
using Zapateria.Libs;
using Zapateria.Models;

namespace Zapateria.Estadisticas
{
    public partial class ProductosEstadisticasForm : Form
    {
        public ProductosEstadisticasForm()
        {
            InitializeComponent();
        }

        private void LimpiarButton_Click(object sender, EventArgs e)
        {
            this.DesdeDateTimePicker.Format = DateTimePickerFormat.Custom;
            this.DesdeDateTimePicker.CustomFormat = " ";

            this.HastaDateTimePicker.Format = DateTimePickerFormat.Custom;
            this.HastaDateTimePicker.CustomFormat = " ";

            ModDataGridView.limpiarDataGridView(Results_DataGridView, "");
        }

        private void CancelarButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }      

        private void BuscarButton_Click(object sender, EventArgs e)
        {
            if (!Validaciones()) return;

            Producto f = new Producto();

            var result = f.Estadistica(DesdeDateTimePicker.Value.ToString("yyyy-MM-dd"), HastaDateTimePicker.Value.ToString("yyyy-MM-dd"));

            if (result != null)
            {
                Results_DataGridView.DataSource = result;
                this.Results_DataGridView.Columns["Nombre"].Width = 280;
            }
            else
            {
                ModDataGridView.limpiarDataGridView(Results_DataGridView, "");
                MessageBox.Show("No se encontraron resultados que coincidan con la busqueda.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private bool Validaciones()
        {
            if (DesdeDateTimePicker.Value.ToString() == " " || HastaDateTimePicker.Value.ToString() == " ")
            {
                MessageBox.Show("Debe seleccionar una fecha desde y hasta.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }
            return true;
        }

        private void VentasEstadisticasForm_Load(object sender, EventArgs e)
        {

        }
    }
}
