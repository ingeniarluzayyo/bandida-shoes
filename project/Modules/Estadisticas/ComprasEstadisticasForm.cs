﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

using Zapateria.Estadisticas.Impresion;
using System.Threading;
using Zapateria.Libs;
using Zapateria.Models;

namespace Zapateria.Estadisticas
{
    public partial class ComprasEstadisticasForm : Form
    {
        public ComprasEstadisticasForm()
        {
            InitializeComponent();
        }

        private void LimpiarButton_Click(object sender, EventArgs e)
        {
            this.DesdeDateTimePicker.Format = DateTimePickerFormat.Custom;
            this.DesdeDateTimePicker.CustomFormat = " ";

            this.HastaDateTimePicker.Format = DateTimePickerFormat.Custom;
            this.HastaDateTimePicker.CustomFormat = " ";

            TotalTextBox.Text = "";

            ModDataGridView.limpiarDataGridView(Results_DataGridView, "");
        }

        private void CancelarButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
       
        private void BuscarButton_Click(object sender, EventArgs e)
        {
            if (!Validaciones()) return;

            TotalTextBox.Text = String.Format("{0:#,###0.00}", 0);

            FacturaCompra f = new FacturaCompra();

            var result = f.Estadistica(DesdeDateTimePicker.Value.ToString("yyyy-MM-dd"), HastaDateTimePicker.Value.ToString("yyyy-MM-dd"));

            if (result != null)
            {
                Results_DataGridView.DataSource = result;
                f.customColumns(Results_DataGridView);
                dt = DataUtil.ToDataTables(result);

                foreach (DataGridViewRow row in Results_DataGridView.Rows)
                {
                    this.TotalTextBox.Text = (Convert.ToDecimal(TotalTextBox.Text) + Convert.ToDecimal(row.Cells["Total"].Value)).ToString();
                }
            }
            else
            {
                ModDataGridView.limpiarDataGridView(Results_DataGridView, "");
                TotalTextBox.Text = String.Format("{0:#,###0.00}", 0);
                MessageBox.Show("No se encontraron resultados que coincidan con la busqueda.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private bool Validaciones()
        {
            if (DesdeDateTimePicker.Value.ToString() == " " || HastaDateTimePicker.Value.ToString() == " ")
            {
                MessageBox.Show("Debe seleccionar una fecha desde y hasta.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }
            return true;
        }

        string desde;
        string hasta;
        string total;
        DataTable dt;

        private void ImprimirEtiquetaButton_Click(object sender, EventArgs e)
        {
            if (Results_DataGridView.RowCount <= 0)
            {
                MessageBox.Show("Debe calcular la estadistica para poder imprimir.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            desde = DesdeDateTimePicker.Value.ToShortDateString();
            hasta = HastaDateTimePicker.Value.ToShortDateString();
            total = this.TotalTextBox.Text;

            Thread hilo = new Thread(AbrirFormReporte);
            hilo.SetApartmentState(System.Threading.ApartmentState.STA);
            hilo.Start();
        }

        private void AbrirFormReporte()
        {
            ComprasReportForm abrir = new ComprasReportForm(dt, desde, hasta,total);
            abrir.ShowDialog();
        }

        private void VentasEstadisticasForm_Load(object sender, EventArgs e)
        {
            TotalTextBox.Text = String.Format("{0:#,###0.00}", 0);
        }
    }
}
