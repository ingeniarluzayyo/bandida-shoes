﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Zapateria.Modules.Estadisticas;

namespace Zapateria.Estadisticas
{
    public partial class EstadisticasForm : Form
    {
        public EstadisticasForm()
        {
            InitializeComponent();
        }

        private void VentasButton_Click(object sender, EventArgs e)
        {
            VentasEstadisticasForm abrir = new VentasEstadisticasForm();
            abrir.Show();
        }

        private void ComprasButton_Click(object sender, EventArgs e)
        {
            ComprasEstadisticasForm abrir = new ComprasEstadisticasForm();
            abrir.Show();
        }

        private void CreditosButton_Click(object sender, EventArgs e)
        {
            ComisionesEstadisticasForm abrir = new ComisionesEstadisticasForm();
            abrir.Show();
        }

        private void CancelarButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            GastosEstadisticasForm abrir = new GastosEstadisticasForm();
            abrir.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ProductosEstadisticasForm abrir = new ProductosEstadisticasForm();
            abrir.Show();
        }


    }
}
