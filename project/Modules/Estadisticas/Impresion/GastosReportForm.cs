﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Zapateria.Modules.Reportes;
using Zapateria.Models;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;

namespace Zapateria.Modules.Estadisticas.Impresion
{
    public partial class GastosReportForm : Form
    {
        public GastosReportForm(DataTable ds,string desde,string hasta,string total)
        {
            InitializeComponent();

            this.dt = ds;
            this.desde = desde;
            this.hasta = hasta;
            this.total = total;
        }

        DataTable dt;
        string desde;
        string hasta;
        string total;

        private void crystalReportViewer1_Load(object sender, EventArgs e)
        {
            EstadisticasGastosCrystalReport Repo = new EstadisticasGastosCrystalReport();

            //Variables
            
            TextObject DesdeRepo;
            DesdeRepo = (TextObject)Repo.ReportDefinition.ReportObjects["DesdeText"];
            DesdeRepo.Text = desde;

            TextObject HastaRepo;
            HastaRepo = (TextObject)Repo.ReportDefinition.ReportObjects["HastaText"];
            HastaRepo.Text = hasta;

            TextObject TotalRepo;
            TotalRepo = (TextObject)Repo.ReportDefinition.ReportObjects["TotalText"];
            TotalRepo.Text = total;
            

            Repo.SetDataSource(dt);

            crystalReportViewer1.ReportSource = Repo;
        }
    }
}
