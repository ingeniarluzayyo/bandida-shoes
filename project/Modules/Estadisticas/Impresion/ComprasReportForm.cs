﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Zapateria.Reportes;
using Zapateria.Models;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using Zapateria.Modules.Reportes;

namespace Zapateria.Estadisticas.Impresion
{
    public partial class ComprasReportForm : Form
    {
        public ComprasReportForm(DataTable ds,string desde,string hasta,string total)
        {
            InitializeComponent();

            this.dt = ds;
            this.desde = desde;
            this.hasta = hasta;
            this.total = total;
        }

        DataTable dt;
        string desde;
        string hasta;
        string total;

        private void crystalReportViewer1_Load(object sender, EventArgs e)
        {
            EstadisticasComprasCrystalReport Repo = new EstadisticasComprasCrystalReport();

            //Variables
            TextObject DesdeRepo;
            DesdeRepo = (TextObject)Repo.ReportDefinition.ReportObjects["DesdeText"];
            DesdeRepo.Text = desde;

            //Variables
            TextObject HastaRepo;
            HastaRepo = (TextObject)Repo.ReportDefinition.ReportObjects["HastaText"];
            HastaRepo.Text = hasta;

            TextObject TotalRepo;
            TotalRepo = (TextObject)Repo.ReportDefinition.ReportObjects["TotalText"];
            TotalRepo.Text = total;

            Repo.SetDataSource(dt);

            crystalReportViewer1.ReportSource = Repo;
        }
    }
}
