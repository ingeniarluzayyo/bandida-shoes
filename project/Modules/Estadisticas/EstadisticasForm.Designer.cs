﻿namespace Zapateria.Estadisticas
{
    partial class EstadisticasForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EstadisticasForm));
            this.GroupBox = new System.Windows.Forms.GroupBox();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.CreditosButton = new System.Windows.Forms.Button();
            this.ComprasButton = new System.Windows.Forms.Button();
            this.VentasButton = new System.Windows.Forms.Button();
            this.CancelarButton = new System.Windows.Forms.Button();
            this.GroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // GroupBox
            // 
            this.GroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.GroupBox.Controls.Add(this.button2);
            this.GroupBox.Controls.Add(this.button1);
            this.GroupBox.Controls.Add(this.CreditosButton);
            this.GroupBox.Controls.Add(this.ComprasButton);
            this.GroupBox.Controls.Add(this.VentasButton);
            this.GroupBox.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GroupBox.Location = new System.Drawing.Point(12, 12);
            this.GroupBox.Name = "GroupBox";
            this.GroupBox.Size = new System.Drawing.Size(690, 308);
            this.GroupBox.TabIndex = 59;
            this.GroupBox.TabStop = false;
            this.GroupBox.Text = "Estadisticas";
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Image = ((System.Drawing.Image)(resources.GetObject("button2.Image")));
            this.button2.Location = new System.Drawing.Point(518, 110);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(111, 89);
            this.button2.TabIndex = 10;
            this.button2.Text = "Productos";
            this.button2.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Image = ((System.Drawing.Image)(resources.GetObject("button1.Image")));
            this.button1.Location = new System.Drawing.Point(401, 110);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(111, 89);
            this.button1.TabIndex = 9;
            this.button1.Text = "Gastos";
            this.button1.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // CreditosButton
            // 
            this.CreditosButton.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CreditosButton.Image = ((System.Drawing.Image)(resources.GetObject("CreditosButton.Image")));
            this.CreditosButton.Location = new System.Drawing.Point(284, 110);
            this.CreditosButton.Name = "CreditosButton";
            this.CreditosButton.Size = new System.Drawing.Size(111, 89);
            this.CreditosButton.TabIndex = 8;
            this.CreditosButton.Text = "Comisiones";
            this.CreditosButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.CreditosButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.CreditosButton.UseVisualStyleBackColor = true;
            this.CreditosButton.Click += new System.EventHandler(this.CreditosButton_Click);
            // 
            // ComprasButton
            // 
            this.ComprasButton.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ComprasButton.Image = ((System.Drawing.Image)(resources.GetObject("ComprasButton.Image")));
            this.ComprasButton.Location = new System.Drawing.Point(167, 110);
            this.ComprasButton.Name = "ComprasButton";
            this.ComprasButton.Size = new System.Drawing.Size(111, 89);
            this.ComprasButton.TabIndex = 7;
            this.ComprasButton.Text = "Compras";
            this.ComprasButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.ComprasButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.ComprasButton.UseVisualStyleBackColor = true;
            this.ComprasButton.Click += new System.EventHandler(this.ComprasButton_Click);
            // 
            // VentasButton
            // 
            this.VentasButton.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.VentasButton.Image = ((System.Drawing.Image)(resources.GetObject("VentasButton.Image")));
            this.VentasButton.Location = new System.Drawing.Point(50, 110);
            this.VentasButton.Name = "VentasButton";
            this.VentasButton.Size = new System.Drawing.Size(111, 89);
            this.VentasButton.TabIndex = 6;
            this.VentasButton.Text = "Ventas";
            this.VentasButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.VentasButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.VentasButton.UseVisualStyleBackColor = true;
            this.VentasButton.Click += new System.EventHandler(this.VentasButton_Click);
            // 
            // CancelarButton
            // 
            this.CancelarButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.CancelarButton.AutoSize = true;
            this.CancelarButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.CancelarButton.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CancelarButton.Location = new System.Drawing.Point(12, 326);
            this.CancelarButton.Name = "CancelarButton";
            this.CancelarButton.Size = new System.Drawing.Size(75, 25);
            this.CancelarButton.TabIndex = 23;
            this.CancelarButton.Text = "Cancelar";
            this.CancelarButton.UseVisualStyleBackColor = true;
            this.CancelarButton.Click += new System.EventHandler(this.CancelarButton_Click);
            // 
            // EstadisticasForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.CancelarButton;
            this.ClientSize = new System.Drawing.Size(714, 362);
            this.Controls.Add(this.CancelarButton);
            this.Controls.Add(this.GroupBox);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "EstadisticasForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Estadisticas";
            this.GroupBox.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox GroupBox;
        private System.Windows.Forms.Button VentasButton;
        private System.Windows.Forms.Button CreditosButton;
        private System.Windows.Forms.Button ComprasButton;
        private System.Windows.Forms.Button CancelarButton;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
    }
}