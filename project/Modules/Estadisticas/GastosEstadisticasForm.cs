﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Zapateria.Models;

using Zapateria.Libs;

using Zapateria.Modules.Estadisticas.Impresion;
using System.Threading;


namespace Zapateria.Modules.Estadisticas
{
    public partial class GastosEstadisticasForm : Form
    {
        public GastosEstadisticasForm()
        {
            InitializeComponent();
        }

        private void LimpiarButton_Click(object sender, EventArgs e)
        {
            this.DesdeDateTimePicker.Value = DateTime.Today;
            this.HastaDateTimePicker.Value = DateTime.Today;

            ModDataGridView.limpiarDataGridView(Results_DataGridView, "");
        }

        private void CancelarButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BuscarButton_Click(object sender, EventArgs e)
        {
            if (!Validaciones()) return;

            Gasto f = new Gasto();
            string familia = null;

            if (this.FamiliaComboBox.SelectedValue != null)
            {
                familia = this.FamiliaComboBox.SelectedValue.ToString();
            }

            DataTable result = f.Estadistica(DesdeDateTimePicker.Value.ToString("yyyy-MM-dd"), HastaDateTimePicker.Value.ToString("yyyy-MM-dd"), familia);

            if (result != null)
            {
                this.dt = result;
                Results_DataGridView.DataSource = result;
            }
            else
            {
                ModDataGridView.limpiarDataGridView(Results_DataGridView, "");
                MessageBox.Show("No se encontraron resultados que coincidan con la busqueda.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private bool Validaciones()
        {
            if (DesdeDateTimePicker.Value.ToString() == " " || HastaDateTimePicker.Value.ToString() == " ")
            {
                MessageBox.Show("Debe seleccionar una fecha desde y hasta.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }
            return true;
        }

        string desde;
        string hasta;
        string total;
        DataTable dt;

        private void ImprimirEtiquetaButton_Click(object sender, EventArgs e)
        {
            if (Results_DataGridView.RowCount <= 0)
            {
                MessageBox.Show("Debe calcular la estadistica para poder imprimir.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            desde = DesdeDateTimePicker.Value.ToShortDateString();
            hasta = HastaDateTimePicker.Value.ToShortDateString();

            Thread hilo = new Thread(AbrirFormReporte);
            hilo.SetApartmentState(System.Threading.ApartmentState.STA);
            hilo.Start();
        }

        private void AbrirFormReporte()
        {
            GastosReportForm abrir = new GastosReportForm(dt, desde, hasta, total);
            abrir.ShowDialog();
        }

        private void GastosEstadisticasForm_Load(object sender, EventArgs e)
        {
            CargadorDeDatos.CargarFamiliasGastosComboBox(this.FamiliaComboBox, this.Text);
        }
    }
}
