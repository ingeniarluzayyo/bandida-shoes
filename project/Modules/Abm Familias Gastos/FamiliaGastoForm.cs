﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using MySql.Data.MySqlClient;
using Zapateria.Libs;
using Zapateria.Models;

namespace Zapateria.Abm_Familias_Gastos
{
    public partial class FamiliaGastoForm : Form
    {

        public FamiliaGastoForm()
        {
            InitializeComponent();
        }

        private void AgregarButton_Click(object sender, EventArgs e)
        {
            AltaFamiliaGastoForm abrir = new AltaFamiliaGastoForm();
            abrir.ShowDialog();
        }

        private void EliminarButton_Click(object sender, EventArgs e)
        {
            if (this.Categorias_DataGridView.Rows.Count == 0 )
            {
                return;
            }

            if (this.Categorias_DataGridView.CurrentCell == null)
            {
                MessageBox.Show("Debe seleccionar una fila.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            
            string CategoriaBorrada = Categorias_DataGridView.CurrentRow.Cells["nombre"].Value.ToString();

            DialogResult Resultado = MessageBox.Show("¿Esta seguro que desea eliminar la categoria '" + CategoriaBorrada + "'?", this.Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            
            if (Resultado == DialogResult.Yes)
            {
                FamiliaGasto familia = new FamiliaGasto();
                familia = familia.getById(Convert.ToInt32(Categorias_DataGridView.CurrentRow.Cells["id"].Value));

                if (familia.delete() == true)
                {
                    if (FamiliaTextBox.TextLength > 0)
                    {
                        familia.nombre = this.FamiliaTextBox.Text;
                    }

                    this.Categorias_DataGridView.DataSource = familia.getLike();
                    MessageBox.Show("Se ha dado de baja la familia '" + CategoriaBorrada + "' correctamente.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }

        private void ModificarButton_Click(object sender, EventArgs e)
        {
            if (this.Categorias_DataGridView.Rows.Count == 0)
            {
                return;
            }
            if (this.Categorias_DataGridView.CurrentCell == null)
            {
                MessageBox.Show("Debe seleccionar una fila.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }


            ModificarFamiliaGastoForm abrir = new ModificarFamiliaGastoForm(this.Categorias_DataGridView.CurrentRow.Cells["id"].Value.ToString(), this.Categorias_DataGridView.CurrentRow.Cells["Nombre"].Value.ToString());
            DialogResult Resultado = abrir.ShowDialog();
            if (Resultado == DialogResult.OK)
            {
                ModDataGridView.limpiarDataGridView(Categorias_DataGridView, "");
            }
        }

        private void BuscarButton_Click(object sender, EventArgs e)
        {
            Security Busqueda = new Security();
            if (!Busqueda.ValidarSqlInjection(this.GroupBox)) { return; }

            FamiliaGasto familia = new FamiliaGasto();

            if (FamiliaTextBox.TextLength > 0)
            {
                familia.nombre = this.FamiliaTextBox.Text;
            }
            
            var familias = familia.getLike();

            if (familias.Count() == 0)
            {
                MessageBox.Show("No se encontraron resultados que coincidan con la busqueda.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            else
            {
                this.Categorias_DataGridView.DataSource = familias;
                familia.customColumns(this.Categorias_DataGridView);
            }

            this.Categorias_DataGridView.Focus();
        }

        private void LimpiarButton_Click(object sender, EventArgs e)
        {
            foreach (Control objeto in this.GroupBox.Controls)
            {
                if (objeto is TextBox)
                {
                    ((TextBox)objeto).Clear();
                }
            }

            ModDataGridView.limpiarDataGridView(Categorias_DataGridView, "");
        }

        private void CancelarButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }



        private void CategoriasForm_Load(object sender, EventArgs e)
        {
            this.Categorias_DataGridView.MultiSelect = false;
            this.FamiliaTextBox.Select();
        }


    }
}
