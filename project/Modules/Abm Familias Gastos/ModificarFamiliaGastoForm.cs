﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using MySql.Data.MySqlClient;
using Zapateria.Models;
using Zapateria.Libs;


namespace Zapateria.Abm_Familias_Gastos
{
    public partial class ModificarFamiliaGastoForm : Form
    {
        public ModificarFamiliaGastoForm(string Id, string Categoria)
        {
            InitializeComponent();

            familia = familia.getById(Convert.ToInt32(Id));
        }

        FamiliaGasto familia = new FamiliaGasto();


        private void GuardarButton_Click(object sender, EventArgs e)
        {

            if (Validaciones() == -1) return;
            
            familia.nombre = this.CategoriaTextBox.Text;

            if (familia.update() == true)
            {
                MessageBox.Show("Se ha modificado la categoria '" + familia.nombre + "' por '" + CategoriaTextBox.Text + "' correctamente.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                DialogResult = DialogResult.OK;
            } 

        }

        private void LimpiarButton_Click(object sender, EventArgs e)
        {
            foreach (Control objeto in this.GroupBox.Controls)
            {
                if (objeto is TextBox)
                {
                    ((TextBox)objeto).Clear();
                }
            }
        }

        private void CancelarButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        private int Validaciones()
        {
            foreach (Control objeto in this.GroupBox.Controls)
            {
                if (objeto is TextBox && ((TextBox)objeto).Text == "")
                {
                    MessageBox.Show("Debe completar el campo vacio.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return -1;
                }
            }
            return 1;
        }

        private void AltaCategoriaForm_Load(object sender, EventArgs e)
        {
            this.CategoriaTextBox.Text = familia.nombre;
            this.CategoriaTextBox.Select();
        }

    }
}
