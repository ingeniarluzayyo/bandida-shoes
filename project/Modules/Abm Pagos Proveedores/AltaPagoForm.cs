﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Zapateria.Models;
using Zapateria.Libs;

namespace Zapateria.AbmPagosProveedores
{
    public partial class AltaPagoProveedoresForm : Form
    {
        public AltaPagoProveedoresForm()
        {
            InitializeComponent();
        }

        private void CancelarButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void GuardarButton_Click(object sender, EventArgs e)
        {
            if (Validaciones() == -1) return;

            PagoProveedor pago = new PagoProveedor();
            pago.fecha = FechaDateTimePicker.Value;
            Proveedor proveedor = new Proveedor();
            pago.proveedor = proveedor.getById(Convert.ToInt32(ProveedorComboBox.SelectedValue.ToString()));
            pago.monto = Convert.ToDecimal(this.MontoTextBox.Text.Replace(".",","));
            pago.anulada = false;
            FormaPago forma_de_pago = new FormaPago();
            pago.forma_de_pago = forma_de_pago.getById(Convert.ToInt32(formaDePagoComboBox.SelectedValue.ToString()));

            if (pago.save())
            {
                MessageBox.Show("Se ha realizado el pago correctamente.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                DialogResult = DialogResult.OK;
            } 
        }

        private int Validaciones()
        {
            foreach (Control objeto in this.GroupBox.Controls)
            {
                if (objeto is ComboBox && ((ComboBox)objeto).SelectedItem == null)
                {
                    MessageBox.Show("Debe completar los campos vacios.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return -1;
                }
                if (objeto is TextBox && ((TextBox)objeto).Text == "")
                {
                    MessageBox.Show("Debe completar el campo vacio.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return -1;
                }
                if (objeto is TextBox && Convert.ToDecimal(((TextBox)objeto).Text) < 0)
                {
                    MessageBox.Show("El monto debe ser mayor a cero.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return -1;
                }
            }
            return 1;
        }

        private void AltaPagoCreditoForm_Load(object sender, EventArgs e)
        {
            CargadorDeDatos.CargarProveedorComboBox(ProveedorComboBox, this.Text);
            CargadorDeDatos.CargarFormaPagoNoACreditoComboBox(formaDePagoComboBox, this.Text);
            MontoTextBox.Text = String.Format("{0:#,###0.00}", 0);
        }

        private void MontoTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar)
              && !char.IsDigit(e.KeyChar)
              && e.KeyChar != '.')
            {
                e.Handled = true;
            }
        }


    }
}
