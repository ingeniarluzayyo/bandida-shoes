﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Zapateria.Libs;
using MySql.Data.MySqlClient;
using Zapateria.Models;

namespace Zapateria.AbmPagosProveedores
{
    public partial class PagosProveedoresForm : Form
    {
        public PagosProveedoresForm()
        {
            InitializeComponent();
        }

        private void CancelarButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void LimpiarButton_Click(object sender, EventArgs e)
        {
            foreach (Control objeto in this.GroupBox.Controls)
            {
                if (objeto is ComboBox)
                {
                    ((ComboBox)objeto).SelectedItem = null; 
                }
            }

            this.FechaDateTimePicker.Format = DateTimePickerFormat.Custom;
            this.FechaDateTimePicker.CustomFormat = " ";

            ModDataGridView.limpiarDataGridView(Categorias_DataGridView, "");
        }

        private void BuscarButton_Click(object sender, EventArgs e)
        {
            Security Busqueda = new Security();
            if (!Busqueda.ValidarSqlInjection(this.GroupBox)) { return; }

            string Consulta = ArmarConsulta();

            List<MySqlParameter> param = new List<MySqlParameter>();


            Categorias_DataGridView.DataSource = BaseDeDatos.GetInstance.ExecuteCustomQuery(Consulta, param, this.Text);


            if (Categorias_DataGridView.DataSource == null)
            {
                MessageBox.Show("No se encontraron resultados que coincidan con la busqueda.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            this.Categorias_DataGridView.Columns["Proveedor"].Width = 160;
            this.Categorias_DataGridView.Columns["Forma de Pago"].Width = 160;
            this.Categorias_DataGridView.Columns["Proveedor_id"].Visible = false;
            this.Categorias_DataGridView.Focus();
        }

        private string ArmarConsulta()
        {
            string Consulta = "SELECT CP.id as Numero,fecha as Fecha,C.razon_social as Proveedor,monto as Monto,F.nombre as 'Forma de Pago',CP.proveedor as Proveedor_id FROM pagos_proveedor as CP JOIN proveedores as C ON C.id=CP.proveedor  INNER JOIN forma_de_pago as F ON CP.forma_de_pago = F.id where ";

            if (AnuladoCheckBox.Checked)
            {
                Consulta += "anulada=1 ";
            }
            else
            {
                Consulta += "anulada=0 ";
            }
            
            if (ProveedorComboBox.SelectedItem != null)
            {
                Consulta += "AND proveedor= '" + ProveedorComboBox.SelectedValue.ToString() + "' ";
            }

            if (FechaDateTimePicker.Text != " ")
            {
                Consulta += "AND fecha= '" + FechaDateTimePicker.Value.ToString("yyyy-MM-dd") + "' ";
            }

            Consulta += " ORDER BY CP.id desc";

            return Consulta;
        }

        private void AgregarButton_Click(object sender, EventArgs e)
        {
            AltaPagoProveedoresForm abrir = new AltaPagoProveedoresForm();
            abrir.ShowDialog();
        }

        private void EliminarButton_Click(object sender, EventArgs e)
        {
            if (this.Categorias_DataGridView.Rows.Count == 0)
            {
                return;
            }

            if (this.Categorias_DataGridView.CurrentCell == null)
            {
                MessageBox.Show("Debe seleccionar una fila.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }            
            string PagoAnulada = Categorias_DataGridView.CurrentRow.Cells["Numero"].Value.ToString();

            DialogResult Resultado = MessageBox.Show("¿Esta seguro que desea anular el pago Nº '" + PagoAnulada + "'?", this.Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (Resultado == DialogResult.Yes)
            {
                PagoProveedor pago = new PagoProveedor();
                pago = pago.getById(Convert.ToInt32(PagoAnulada));

                if(pago.anulada)
                {
                    MessageBox.Show("El pago Nº '" + PagoAnulada + " ya estaba anulado.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return;
                }

                pago.anulada = true;
                if (pago.update())
                {
                    Categorias_DataGridView.Rows.Remove(Categorias_DataGridView.CurrentRow);
                    MessageBox.Show("Se ha anulado el pago Nº '" + PagoAnulada + "' correctamente.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

            }
        }

        private void CreditosForm_Load(object sender, EventArgs e)
        {
            this.FechaDateTimePicker.Format = DateTimePickerFormat.Custom;
            this.FechaDateTimePicker.CustomFormat = " ";
            CargadorDeDatos.CargarProveedorComboBox(ProveedorComboBox, this.Text);
            this.BuscarButton.Select();
        }

        private void FechaDateTimePicker_CloseUp(object sender, EventArgs e)
        {
            this.FechaDateTimePicker.Format = DateTimePickerFormat.Short;
        }
    }
}
