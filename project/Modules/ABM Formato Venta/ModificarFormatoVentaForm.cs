﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using MySql.Data.MySqlClient;
using Zapateria.Models;
using Zapateria.Libs;


namespace Zapateria.Abm_Formato_Venta
{
    public partial class ModificarFormatoVentaForm : Form
    {
        public ModificarFormatoVentaForm(string Id, string Categoria)
        {
            InitializeComponent();

            fc = fc.getById(Convert.ToInt32(Id));
        }

        FormatoVenta fc = new FormatoVenta();


        private void GuardarButton_Click(object sender, EventArgs e)
        {

            if (Validaciones() == -1) return;
            
            fc.formato = Convert.ToDecimal(this.CategoriaTextBox.Text);

            if (fc.update() == true)
            {
                MessageBox.Show("Se ha modificado el formato venta correctamente.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                DialogResult = DialogResult.OK;
            } 

        }

        private void LimpiarButton_Click(object sender, EventArgs e)
        {
            foreach (Control objeto in this.GroupBox.Controls)
            {
                if (objeto is TextBox)
                {
                    ((TextBox)objeto).Clear();
                }
            }
        }

        private void CancelarButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        private int Validaciones()
        {
            foreach (Control objeto in this.GroupBox.Controls)
            {
                if (objeto is TextBox && ((TextBox)objeto).Text == "")
                {
                    MessageBox.Show("Debe completar el campo vacio.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return -1;
                }
            }
            return 1;
        }

        private void AltaCategoriaForm_Load(object sender, EventArgs e)
        {
            this.CategoriaTextBox.Text = fc.formato.ToString();
            this.CategoriaTextBox.Select();
        }
    }
}
