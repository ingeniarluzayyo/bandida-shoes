﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using Zapateria.Models;

namespace Zapateria.Abm_Proveedores
{
    public partial class ModificarProveedorForm : Form
    {
        public ModificarProveedorForm(string id)
        {
            InitializeComponent();
            this.id = id;
            proveedor = proveedor.getById(Convert.ToInt32(id));
        }

        string id;
        Proveedor proveedor = new Proveedor();

        private void GuardarButton_Click(object sender, EventArgs e)
        {
            if (Validaciones() == -1) return;

            proveedor.razon_social = this.Razon_SocialTextBox.Text;
            proveedor.cuit = this.CuitTextBox.Text;
            proveedor.tel = this.TelTextBox.Text;
            proveedor.cel = this.CelTextBox.Text;
            proveedor.direccion = this.DirTextBox.Text;
            proveedor.mail = this.MailTextBox.Text;
            proveedor.observacion = this.NotaTextBox.Text;
           
            if (proveedor.update())
            {
                MessageBox.Show("Se ha modificado el proveedor '" + Razon_SocialTextBox.Text + "' correctamente.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                DialogResult = DialogResult.OK;
            } 
        }

        private int Validaciones()
        {

            if (Razon_SocialTextBox.Text == "")
            {
                MessageBox.Show("Debe completar los campos obligatorios.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return -1;
            }
            return 1;
        }

        private void LimpiarButton_Click(object sender, EventArgs e)
        {
            foreach (Control objeto in this.GroupBox.Controls)
            {
                if (objeto is TextBox)
                {
                    ((TextBox)objeto).Clear();
                }
                if (objeto is ComboBox)
                {
                    ((ComboBox)objeto).SelectedItem = null;
                }
            }
        }

        private void CancelarButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ModificarProveedorForm_Load(object sender, EventArgs e)
        {
            this.Razon_SocialTextBox.Text = proveedor.razon_social;
            this.CuitTextBox.Text = proveedor.cuit;
            this.TelTextBox.Text = proveedor.tel;
            this.CelTextBox.Text = proveedor.cel;
            this.DirTextBox.Text = proveedor.direccion;
            this.MailTextBox.Text = proveedor.mail;
            this.NotaTextBox.Text = proveedor.observacion;
        }
    }
}
