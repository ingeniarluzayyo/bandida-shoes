﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using Zapateria.Models;

namespace Zapateria.Abm_Proveedores
{
    public partial class AltaProveedorForm : Form
    {
        public AltaProveedorForm()
        {
            InitializeComponent();
        }

        private void GuardarButton_Click(object sender, EventArgs e)
        {
            if (Validaciones() == -1) return;

            Proveedor proveedor = new Proveedor();
            proveedor.razon_social = this.Razon_SocialTextBox.Text;
            proveedor.cuit = this.CuitTextBox.Text;
            proveedor.tel = this.TelTextBox.Text;
            proveedor.cel = this.CelTextBox.Text;
            proveedor.direccion = this.DirTextBox.Text;
            proveedor.mail = this.MailTextBox.Text;
            proveedor.observacion = this.NotaTextBox.Text;            

            if (proveedor.save())
            {
                MessageBox.Show("Se ha dado de alta el proveedor '" + Razon_SocialTextBox.Text + "' correctamente.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.LimpiarButton.PerformClick();
                this.Razon_SocialTextBox.Focus();
            } 
        }

        private void LimpiarButton_Click(object sender, EventArgs e)
        {
            foreach (Control objeto in this.GroupBox.Controls)
            {
                if (objeto is TextBox)
                {
                    ((TextBox)objeto).Clear();
                }
                if (objeto is ComboBox)
                {
                    ((ComboBox)objeto).SelectedItem = null;
                }
            }
        }

        private void CancelarButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private int Validaciones()
        {

            if (Razon_SocialTextBox.Text == "")
            {
                MessageBox.Show("Debe completar los campos obligatorios.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return -1;
            }
            return 1;
        }

        private void AltaProveedorForm_Load(object sender, EventArgs e)
        {
            this.Razon_SocialTextBox.Select();
        }
    }
}
