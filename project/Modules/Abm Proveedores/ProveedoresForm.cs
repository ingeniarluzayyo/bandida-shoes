﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using Zapateria.Libs;
using Zapateria.Models;

namespace Zapateria.Abm_Proveedores
{
    public partial class ProveedoresForm : Form
    {
        public ProveedoresForm()
        {
            InitializeComponent();
        }

        private void BuscarButton_Click(object sender, EventArgs e)
        {
            Security Busqueda = new Security();
            if (!Busqueda.ValidarSqlInjection(this.GroupBox)) { return; }

            string Consulta = ArmarConsulta();

            List<MySqlParameter> param = new List<MySqlParameter>();

            Clientes_DataGridView.DataSource = BaseDeDatos.GetInstance.ExecuteCustomQuery(Consulta, param, this.Text);


            if (Clientes_DataGridView.DataSource == null)
            {
                MessageBox.Show("No se encontraron resultados que coincidan con la busqueda.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            this.Clientes_DataGridView.Columns["id"].Visible = false;
            this.Clientes_DataGridView.Columns["Razon Social"].Width = 170;
            this.Clientes_DataGridView.Columns["Cuit"].Width = 150;
            this.Clientes_DataGridView.Columns["Direccion"].Width = 200;
            this.Clientes_DataGridView.Columns["E-Mail"].Width = 170;
            this.Clientes_DataGridView.Focus();
        }

        private string ArmarConsulta()
        {
            string Consulta = "SELECT id,razon_social as 'Razon Social',cuit as Cuit,direccion as Direccion,tel as Tel,cel as Cel,mail as 'E-mail' FROM proveedores WHERE 1=1 ";

            if (RazonSocialTextBox.TextLength > 0)
            {
                Consulta += "AND razon_social LIKE '%" + RazonSocialTextBox.Text + "%' ";
            }

            return Consulta;
        }

        private void LimpiarButton_Click(object sender, EventArgs e)
        {
            foreach (Control objeto in this.GroupBox.Controls)
            {
                if (objeto is TextBox)
                {
                    ((TextBox)objeto).Clear();
                }
                if (objeto is ComboBox)
                {
                    ((ComboBox)objeto).SelectedItem = null;
                }
            }

            ModDataGridView.limpiarDataGridView(Clientes_DataGridView, "");
        }

        private void CancelarButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void AgregarButton_Click(object sender, EventArgs e)
        {
            AltaProveedorForm abrir = new AltaProveedorForm();
            abrir.ShowDialog();
        }

        private void EliminarButton_Click(object sender, EventArgs e)
        {
            if (this.Clientes_DataGridView.Rows.Count == 0)
            {
                return;
            }

            if (this.Clientes_DataGridView.CurrentCell == null)
            {
                MessageBox.Show("Debe seleccionar una fila.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            string ProveedorBorrado = Clientes_DataGridView.CurrentRow.Cells["Razon Social"].Value.ToString();

            DialogResult Resultado = MessageBox.Show("¿Esta seguro que desea eliminar el proveedor '" + ProveedorBorrado + "'?", this.Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (Resultado == DialogResult.Yes)
            {
                Proveedor prov = new Proveedor();
                prov = prov.getById(Convert.ToInt32(Clientes_DataGridView.CurrentRow.Cells["id"].Value));

                if (prov.delete())
                {
                    Clientes_DataGridView.Rows.Remove(Clientes_DataGridView.CurrentRow);
                    MessageBox.Show("Se ha dado de baja el proveedor '" + ProveedorBorrado + "' correctamente.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }

        private void ModificarButton_Click(object sender, EventArgs e)
        {
            if (this.Clientes_DataGridView.Rows.Count == 0)
            {
                return;
            }

            if (this.Clientes_DataGridView.CurrentCell == null)
            {
                MessageBox.Show("Debe seleccionar una fila.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            string id = Clientes_DataGridView.CurrentRow.Cells["id"].Value.ToString();

            ModificarProveedorForm abrir = new ModificarProveedorForm(id);
            DialogResult Resultado = abrir.ShowDialog();
            if (Resultado == DialogResult.OK)
            {
                ModDataGridView.limpiarDataGridView(Clientes_DataGridView, "");
            }
        }

        private void ProveedoresForm_Load(object sender, EventArgs e)
        {
            this.BuscarButton.Select();
        }
    }
}
