﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Globalization;
using Zapateria.Libs;
using Zapateria.Models;
using MySql.Data.MySqlClient;
using System.Drawing;

namespace Zapateria.Abm_Compras
{
    public partial class AltaCompraForm : Form
    {
        public AltaCompraForm()
        {
            InitializeComponent();
        }

        public static Producto producto = new Producto();

        private void CodigoTextBox_TextChanged(object sender, EventArgs e)
        {
            /*if(ProveedorComboBox.SelectedValue != null)
            {
                if (CodigoTextBox.Text.Length >= 10)
                {
                    string Consulta = "SELECT p.nombre FROM productos as p WHERE p.id='" + CodigoTextBox.Text + "'";

                    List<MySqlParameter> param = new List<MySqlParameter>();

                    DataTable dt = BaseDeDatos.GetInstance.ExecuteCustomQuery(Consulta, param, this.Text);

                    if (dt != null)
                    {
                        if (dt.Rows.Count == 1)
                        {
                            NombreTextBox.Text = dt.Rows[0]["nombre"].ToString();

                            producto = producto.getById(Convert.ToInt32(CodigoTextBox.Text));

                            if (producto != null)
                            {
                                PrecioTextBox.Text = producto.precio_compra.ToString();
                            }

                            CantidadTextBox.Text = "1";
                            CantidadTextBox.ReadOnly = false;
                        }
                    }
                    else
                    {
                        NombreTextBox.Text = "";
                        CantidadTextBox.Text = "";
                        CantidadTextBox.ReadOnly = true;
                    }
                }
            }
            else
            {
                if (this.CodigoTextBox.Text != "")
                {
                    MessageBox.Show("Debe seleccionar un proveedor, para poder utilizar la carga rapida de productos.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.CodigoTextBox.Text = "";
                }
            }*/
        }

        private void CodigoTextBox_Enter(object sender, EventArgs e)
        {
            if (CodigoTextBox.Text == "")
            {
                this.CodigoTextBox.Text = "000000";
            }
        }

        private void CodigoTextBox_Leave(object sender, EventArgs e)
        {
            /*if (CodigoTextBox.Text == "")
            {
                CodigoTextBox.Text = "Codigo";
            }*/
        }

        private void AltaCompraForm_Load(object sender, EventArgs e)
        {
            //NUMERO DE FACTURA
            string Consulta = "SELECT MAX(id) FROM factura_compra";
            List<MySqlParameter> param = new List<MySqlParameter>();
            DataTable ds = BaseDeDatos.GetInstance.ExecuteCustomQuery(Consulta, param, this.Text);

            foreach (DataRow row in ds.Rows)
            {
                if (Convert.ToString(row[0]) == "")
                {
                    row[0] = 0;
                }
                int NumeroFactura = (Convert.ToInt32(row[0]) + 1);
                this.FactIdTextBox.Text = Convert.ToString(NumeroFactura);
            }

            
            CargadorDeDatos.CargarProveedorComboBox(ProveedorComboBox, this.Text);
            CargadorDeDatos.CargarFormaPagoComboBox(FormaPagoComboBox, this.Text);
            CargadorDeDatos.CargarSucursalesComboBox(SucursalComboBox, this.Text);
            CargadorDeDatos.CargarTallesComboBox(TalleComboBox, this.Text);
            CargadorDeDatos.CargarColoresComboBox(ColorComboBox, this.Text);
            TotalTextBox.Text = String.Format("{0:#,###0.00}", 0);
            SubTotalTextBox.Text = String.Format("{0:#,###0.00}", 0);
            DescuentoTextBox.Text = String.Format("{0:#,###0.00}", 0);

            this.Compras_DataGridView.Columns["Nombre"].Width = 200;

            SucursalComboBox.SelectedValue = Main.getUsuario().sucursal_default.id;
            this.numFactExternotextBox.Select();
        }

        private void AgregarArticuloButton_Click(object sender, EventArgs e)
        {
            producto = new Producto();

            SeleccionarArticuloForm abrir = new SeleccionarArticuloForm();
            DialogResult Resultado = abrir.ShowDialog();
            if (Resultado == DialogResult.OK)
            {
                producto = producto.getById(producto.id);
                CodigoTextBox.Text = producto.id.ToString("D10");
                PrecioTextBox.Text = producto.precio_compra.ToString();
                NombreTextBox.Text = producto.nombre;

                TalleComboBox.SelectedItem = null;
                ColorComboBox.SelectedItem = null;
                CantidadTextBox.Text = "1";
                CantidadTextBox.ReadOnly = false;
                CantidadTextBox.Focus();
            }
        }

        private void AddArticuloButton_Click(object sender, EventArgs e)
        {
            if (!Validaciones()) { return; }

            Compras_DataGridView.Rows.Add(producto.id.ToString("D10"), producto.nombre, TalleComboBox.Text, ColorComboBox.Text, Decimal.Parse(PrecioTextBox.Text), CantidadTextBox.Text, Decimal.Parse(PrecioTextBox.Text) * Convert.ToInt32(CantidadTextBox.Text));

            SubTotalTextBox.Text = (Convert.ToDecimal(SubTotalTextBox.Text) + Decimal.Parse(PrecioTextBox.Text) * Convert.ToInt32(CantidadTextBox.Text)).ToString();
            TotalTextBox.Text = (Convert.ToDecimal(SubTotalTextBox.Text) - Convert.ToDecimal(DescuentoTextBox.Text)).ToString();

            TalleComboBox.SelectedItem = null;
            ColorComboBox.SelectedItem = null;
            CantidadTextBox.Text = "1";

            this.CodigoTextBox.Focus();
        }

        private bool Validaciones()
        {
            if (this.NombreTextBox.Text == "" && this.CantidadTextBox.Text == "")
            {
                return false;
            }

            //Solo numeros
            int v;
            decimal x;
            if (!Int32.TryParse(this.CantidadTextBox.Text.Trim(), out v))
            {
                MessageBox.Show("Error: La cantidad solo puede ser numerica.");
                return false;
            }

            if(!Decimal.TryParse(PrecioTextBox.Text, out x))
            {
                MessageBox.Show("Error: El precio solo puede ser numerica.");
                return false;
            }

            return true;
        }

        private void GuardarButton_Click(object sender, EventArgs e)
        {
            if (Validaciones_guardar() == -1) return;
            Proveedor proveedor = new Proveedor();
            Producto producto = new Producto();
            Talle talle = new Talle();
            ColorZapato color = new ColorZapato();
            FormaPago forma_de_pago = new FormaPago();
            Stock stock = new Stock();

            FacturaCompra fact = new FacturaCompra();

            Sucursal sucursal = new Sucursal();
            fact.sucursal = sucursal.getById(Convert.ToInt32(this.SucursalComboBox.SelectedValue.ToString()));

            fact.num_fact_externo = numFactExternotextBox.Text.ToString();
            fact.proveedor = proveedor.getById(Convert.ToInt32(ProveedorComboBox.SelectedValue.ToString()));
            fact.forma_de_pago = forma_de_pago.getById(Convert.ToInt32(FormaPagoComboBox.SelectedValue.ToString()));
            fact.fecha = FechaDateTimePicker.Value;
            fact.descuento = Decimal.Parse(DescuentoTextBox.Text);
            fact.total = Decimal.Parse(TotalTextBox.Text);

            foreach (DataGridViewRow row in Compras_DataGridView.Rows)
            {
                ItemCompra item = new ItemCompra();
                item.producto = producto.getById(Int32.Parse(row.Cells["Codigo"].Value.ToString()));
                item.talle = talle.Search(row.Cells["Talle"].Value.ToString());
                item.color = color.Search(row.Cells["Color"].Value.ToString());
                item.cantidad = Convert.ToInt32(row.Cells["Cantidad"].Value); 
                item.precio = Convert.ToDecimal(row.Cells["PrecioUnitario"].Value);

                item.formato = "";
                item.producto.precio_compra = Convert.ToDecimal(row.Cells["PrecioUnitario"].Value);
                item.producto.stock = item.producto.stock + item.cantidad; 

                item.factura = fact;

                fact.items.Add(item);
            }

            if (fact.save() == true)
            {
                Zona zona = new Zona();
                Zona zonaDeposito = zona.Search("Depósito");

                //Actualizo stock una vez guardada la factura, por que si lo realizamos con casacade, no deja repetir productos.
                foreach(ItemCompra item in fact.items)
                {
                    Producto p = new Producto();
                    p = p.getById(item.producto.id);

                    p.precio_compra = item.producto.precio_compra;
                    //p.stock = p.stock + item.cantidad;
                    
                    // Chequear si la nueva combinación producto/sucursal/zona/talle/color existe.
                    IList<Stock> stocksExistentes = stock.Search(item.producto.id, fact.sucursal.id, zonaDeposito.id, item.talle.id, item.color.id);

                    if (stocksExistentes.Count == 0)
                    {
                        Stock nuevoStock = new Stock();

                        nuevoStock.producto = item.producto;
                        nuevoStock.sucursal = fact.sucursal;
                        nuevoStock.zona = zonaDeposito;
                        nuevoStock.talle = item.talle;
                        nuevoStock.color = item.color;
                        nuevoStock.stock = item.cantidad;

                        nuevoStock.save();
                    }
                    else
                    {
                        stocksExistentes[0].stock += item.cantidad;

                        stocksExistentes[0].update();
                    }

                    p.update();
                }

                MessageBox.Show("Se ha dado de alta la factura correctamente.",this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                DialogResult = DialogResult.OK;
            }            
        }

        private int Validaciones_guardar()
        {
            foreach (Control objeto in this.GroupBox.Controls)
            {
                if (objeto is ComboBox && ((ComboBox)objeto).SelectedItem == null)
                {
                    MessageBox.Show("Debe completar los campos vacios.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return -1;
                }
            }
            if (Compras_DataGridView.RowCount <= 0)
            {
                MessageBox.Show("Debe agregar al menos un articulo a la factura.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return -1;
            }
            return 1;
        }

        private void EliminarArticuloButton_Click(object sender, EventArgs e)
        {
            if (this.Compras_DataGridView.Rows.Count == 0)
            {
                return;
            }

            if (this.Compras_DataGridView.CurrentCell == null)
            {
                MessageBox.Show("Debe seleccionar una fila.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            SubTotalTextBox.Text = (Convert.ToDecimal(SubTotalTextBox.Text) - Convert.ToDecimal(Compras_DataGridView.CurrentRow.Cells["Importe"].Value)).ToString();
            TotalTextBox.Text = (Convert.ToDecimal(SubTotalTextBox.Text) - Convert.ToDecimal(DescuentoTextBox.Text)).ToString();

            Compras_DataGridView.Rows.Remove(Compras_DataGridView.CurrentRow);
        }

        private void DescuentoTextBox_TextChanged(object sender, EventArgs e)
        {
            if (DescuentoTextBox.Text != "")
            {
                decimal total = (Convert.ToDecimal(SubTotalTextBox.Text) - Convert.ToDecimal(DescuentoTextBox.Text));
                if (total >= 0)
                {
                    TotalTextBox.Text = (Convert.ToDecimal(SubTotalTextBox.Text) - Convert.ToDecimal(DescuentoTextBox.Text)).ToString();
                }
                else
                {
                    MessageBox.Show("Error, El descuento no puede ser mayor al subtotal.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    DescuentoTextBox.Text = String.Format("{0:#,###0.00}", 0);
                    return;
                }
            }
        }

        private void DescuentoTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar)
               && !char.IsDigit(e.KeyChar)
               && e.KeyChar != '.')
            {
                e.Handled = true;
            }
        }

        private void ProveedorComboBox_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (Compras_DataGridView.Rows.Count > 0)
            {
                MessageBox.Show("No puede cambiar el proveedor mientras haya articulos en la factura.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
        }


        private void CantidadTextBox_MouseClick(object sender, MouseEventArgs e)
        {
            ((TextBox)sender).SelectAll();
        }

        private void PrecioTextBox_MouseClick(object sender, MouseEventArgs e)
        {
            ((TextBox)sender).SelectAll();
        }

        private void PrecioTextBox_TextChanged(object sender, EventArgs e)
        {
            if (!Validacion.isDecimal(this.PrecioTextBox.Text))
            {
                if (!Validacion.isDecimal(this.PrecioTextBox.Text.Replace(",", ".")))
                {
                    return;
                }
                else
                {
                    this.PrecioTextBox.Text = this.PrecioTextBox.Text.Replace(",", ".");
                    PrecioTextBox.SelectionStart = PrecioTextBox.Text.Length;
                }
            }
        }

        private void DescuentoTextBox_Leave(object sender, EventArgs e)
        {
            if (DescuentoTextBox.Text == "") {
                this.DescuentoTextBox.Text = "0";
            }
        }



    }
}
