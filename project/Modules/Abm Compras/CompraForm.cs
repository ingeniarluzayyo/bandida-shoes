﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using Zapateria.Libs;
using Zapateria.Models;

namespace Zapateria.Abm_Compras
{
    public partial class CompraForm : Form
    {
        public CompraForm()
        {
            InitializeComponent();
        }

        IList<FacturaCompra> facturas;

        private void BuscarButton_Click(object sender, EventArgs e)
        {
            Security Busqueda = new Security();
            if (!Busqueda.ValidarSqlInjection(this.GroupBox)) { return; }

            FacturaCompra factura = new FacturaCompra();

            int compra_id = 0;
            int proveedor_id = 0;
            int producto_id = 0;
            int sucursal_id = 0;
            string desde = "";
            string hasta = "";

            if (NumeroTextBox.TextLength > 0)
            {
                compra_id = Convert.ToInt32(NumeroTextBox.Text);
            }

            if (this.codigoProductoTextBox.TextLength > 0)
            {
                producto_id = Convert.ToInt32(codigoProductoTextBox.Text);
            }

            if (ProveedorComboBox.SelectedItem != null)
            {
                proveedor_id = Convert.ToInt32(ProveedorComboBox.SelectedValue.ToString());
            }

            if (SucursalComboBox.SelectedItem != null)
            {
                sucursal_id = Convert.ToInt32(SucursalComboBox.SelectedValue.ToString());
            }

            if (desdeDateTimePicker.Text != " ")
            {
                desde = desdeDateTimePicker.Value.ToShortDateString();
            }

            if (hastaDateTimePicker.Text != " ")
            {
                hasta = hastaDateTimePicker.Value.ToShortDateString();
            }

            facturas = factura.Search(AnuladoCheckBox.Checked, compra_id, proveedor_id, desde, hasta, producto_id,sucursal_id);
            Compras_DataGridView.DataSource = facturas;
            
            if (Compras_DataGridView.DataSource == null)
            {
                ModDataGridView.limpiarDataGridView(Compras_DataGridView, "Ver Detalle");
                MessageBox.Show("No se encontraron resultados que coincidan con la busqueda.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            ModDataGridView.agregarBoton(Compras_DataGridView, "Ver Detalle");
            factura.customColumns(Compras_DataGridView);
            this.Compras_DataGridView.Focus();
        }


        private string ArmarConsulta()
        {
            string Consulta = "SELECT FC.id as 'Numero',fecha as Fecha,P.razon_social as Proveedor,total as Total,anulada FROM factura_compra as FC JOIN proveedores as P ON P.id=FC.proveedor ";

            if (codigoProductoTextBox.TextLength > 0)
            {
                Consulta += "JOIN item_factura_compra as Item ON FC.id = Item.factura ";
            }

            Consulta += " WHERE ";

            if (AnuladoCheckBox.Checked)
            {
                Consulta += "anulada=1 ";
            }
            else
            {
                Consulta += "anulada=0 ";
            }

            if (NumeroTextBox.TextLength > 0)
            {
                Consulta += "AND FC.id='" + NumeroTextBox.Text + "'";
            }
            if (codigoProductoTextBox.TextLength > 0)
            {
                Consulta += "AND Item.producto='" + codigoProductoTextBox.Text + "'";
            }

            if (ProveedorComboBox.SelectedItem != null)
            {
                Consulta += "AND proveedor= '" + ProveedorComboBox.SelectedValue.ToString() + "' ";
            }

            if (desdeDateTimePicker.Text != " ")
            {
                Consulta += "AND fecha= '" + desdeDateTimePicker.Value.ToString("yyyy-MM-dd") +"' ";
            }

            Consulta += " ORDER BY FC.id desc";

            return Consulta;
        }

        private void LimpiarButton_Click(object sender, EventArgs e)
        {
            foreach (Control objeto in this.GroupBox.Controls)
            {
                if (objeto is TextBox)
                {
                    ((TextBox)objeto).Clear();
                }
                if (objeto is ComboBox)
                {
                    ((ComboBox)objeto).SelectedItem = null;
                }
            }

            ModDataGridView.limpiarDataGridView(Compras_DataGridView, "Ver Detalle");
            this.desdeDateTimePicker.Value = DateTime.Today;
            this.hastaDateTimePicker.Value = DateTime.Today;
        }

        private void CancelarButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void EliminarButton_Click(object sender, EventArgs e)
        {
            if (this.Compras_DataGridView.Rows.Count == 0)
            {
                return;
            }

            if (this.Compras_DataGridView.CurrentCell == null)
            {
                MessageBox.Show("Debe seleccionar una fila.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            if (Convert.ToInt32(Compras_DataGridView.CurrentRow.Cells["anulada"].Value) == 1)
            {
                MessageBox.Show("Error,La factura ya esta anulada.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            string FacturaAnuladaId = Compras_DataGridView.CurrentRow.Cells["id"].Value.ToString();

            DialogResult Resultado = MessageBox.Show("¿Esta seguro que desea anular la Factura Nº '" + FacturaAnuladaId + "'?", this.Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (Resultado == DialogResult.Yes)
            {
                FacturaCompra fact = new FacturaCompra();
                fact = fact.getById(Convert.ToInt32(FacturaAnuladaId),true);
                fact.anulada = true;

                if (fact.update())
                {
                    fact = fact.getById(Convert.ToInt32(FacturaAnuladaId));
                    //Actualizo stock
                    foreach (ItemCompra item in fact.items)
                    {

                        if (item.producto.unidad)
                        {
                            item.producto.stock = item.producto.stock - item.cantidad;
                        }
                        else
                        {
                            item.producto.stock = item.producto.stock - (Convert.ToDecimal(item.formato) * item.cantidad);
                        }

                        item.producto.update();
                    }

                    facturas.RemoveAt(Compras_DataGridView.CurrentRow.Index);
                    ModDataGridView.limpiarDataGridView(Compras_DataGridView, "Ver Detalle");
                    Compras_DataGridView.DataSource = facturas;
                    ModDataGridView.agregarBoton(Compras_DataGridView, "Ver Detalle");
                    fact.customColumns(Compras_DataGridView);
                    this.Compras_DataGridView.Focus();

                     MessageBox.Show("Se ha anulado la Factura Nº '" + FacturaAnuladaId + "' correctamente.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                
            }
        }

        private void AgregarButton_Click(object sender, EventArgs e)
        {
            AltaCompraForm abrir = new AltaCompraForm();
            DialogResult Resultado = abrir.ShowDialog();
            if (Resultado == DialogResult.OK)
            {
                //ModDataGridView.limpiarDataGridView(Compras_DataGridView, "Ver Detalle");
                this.LimpiarButton.PerformClick();
                this.BuscarButton.PerformClick();
            }
        }

        private void CompraForm_Load(object sender, EventArgs e)
        {
            CargadorDeDatos.CargarProveedorComboBox(ProveedorComboBox, this.Text);
            CargadorDeDatos.CargarSucursalesComboBox(SucursalComboBox, "Sucursales");
            //this.desdeDateTimePicker.Format = DateTimePickerFormat.Custom;
            //this.desdeDateTimePicker.CustomFormat = " ";
            //this.hastaDateTimePicker.Format = DateTimePickerFormat.Custom;
            //this.hastaDateTimePicker.CustomFormat = " ";
            this.BuscarButton.PerformClick();
        }

        private void dateTimePicker1_CloseUp(object sender, EventArgs e)
        {
            this.desdeDateTimePicker.Format = DateTimePickerFormat.Short;
        }

        private void Compras_DataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == this.Compras_DataGridView.Columns["Ver Detalle"].Index && e.RowIndex >= 0)
            {
                string id = Compras_DataGridView.CurrentRow.Cells["id"].Value.ToString();

                DetalleCompraForm abrir = new DetalleCompraForm(Convert.ToInt32(id));
                abrir.ShowDialog();
            }
        }

        private void NumeroTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar)
              && !char.IsDigit(e.KeyChar)
              )
            {
                e.Handled = true;
            }
        }

        private void hastaDateTimePicker_CloseUp(object sender, EventArgs e)
        {
            this.hastaDateTimePicker.Format = DateTimePickerFormat.Short;
        }
    }
}
