﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using Zapateria.Libs;
using Zapateria.Models;

namespace Zapateria.Abm_Compras
{
    public partial class DetalleCompraForm : Form
    {
        public DetalleCompraForm(int id)
        {
            InitializeComponent();

            this.id = id;
        }

        int id;

        private void DetalleCompraForm_Load(object sender, EventArgs e)
        {
            CargadorDeDatos.CargarProveedorComboBox(ProveedorComboBox, this.Text);
            CargadorDeDatos.CargarFormaPagoComboBox(FormaPagoComboBox, this.Text);
            CargadorDeDatos.CargarSucursalesComboBox(SucursalComboBox, "Sucursales");

            FacturaCompra fact = new FacturaCompra();
            fact = fact.getById(id);

            FactIdTextBox.Text = id.ToString();
            numFactExternotextBox.Text = fact.num_fact_externo;
            FormaPagoComboBox.SelectedValue = fact.forma_de_pago.id;
            ProveedorComboBox.SelectedValue = fact.proveedor.id;
            SucursalComboBox.SelectedValue = fact.sucursal.id;
            FechaDateTimePicker.Value = Convert.ToDateTime(fact.fecha);
            DescuentoTextBox.Text = fact.descuento.ToString();
            TotalTextBox.Text = fact.total.ToString();
            SubTotalTextBox.Text = (Convert.ToDecimal(TotalTextBox.Text) + Convert.ToDecimal(DescuentoTextBox.Text)).ToString();
            foreach(ItemCompra item in fact.items)
            {
                /*if (item.formato == "")
                {
                    Compras_DataGridView.Rows.Add(item.producto.id.ToString("D10"), item.producto.nombre, item.formato, item.precio, item.cantidad, item.precio * item.cantidad);
                }
                else {
                    Compras_DataGridView.Rows.Add(item.producto.id.ToString("D10"), item.producto.nombre, FormatoCompra.GetFormatoKG(Convert.ToDecimal(item.formato)), item.precio, item.cantidad, item.precio * item.cantidad);
                }
                */

                Compras_DataGridView.Rows.Add(item.producto.id.ToString("D10"), item.producto.nombre, item.talle.nombre,item.color.nombre, item.precio, item.cantidad, item.precio * item.cantidad);
            }
        }

        /*private void get_item_factura()
        {
            string Consulta = "SELECT ATC.id as Codigo,A.descripcion as Descripcion,T.nombre as Talle,ATC.color as Color,IFC.precio as 'Precio Unitario',IFC.cantidad as Cantidad,IFC.precio*IFC.cantidad as Importe ";
            Consulta += "FROM articulos as A ";
            Consulta += "JOIN articulosxtallecolor as ATC ON ATC.articulo=A.id ";
            Consulta += "JOIN talles as T ON T.id=ATC.talle ";
            Consulta += "JOIN item_factura_compra as IFC ON IFC.articulo=ATC.id WHERE factura=" + id;

            List<MySqlParameter> param = new List<MySqlParameter>();

            Compras_DataGridView.DataSource = BaseDeDatos.GetInstance.ExecuteCustomQuery(Consulta, param, this.Text);

            foreach (DataGridViewRow row in Compras_DataGridView.Rows)
            {
                string[] separators = { ",", "(", ")" };
                string color = row.Cells["Color"].Value.ToString();
                string[] color_split = color.Split(separators, StringSplitOptions.None);

                row.Cells["Color"].Style.BackColor = Color.FromArgb(Convert.ToInt32(color_split[1]), Convert.ToInt32(color_split[2]), Convert.ToInt32(color_split[3]));
                row.Cells["Color"].Style.ForeColor = Color.FromArgb(Convert.ToInt32(color_split[1]), Convert.ToInt32(color_split[2]), Convert.ToInt32(color_split[3]));
            }
            
            if (Compras_DataGridView.DataSource != null)
            {
                this.Compras_DataGridView.Columns["Descripcion"].Width = 200;
                this.Compras_DataGridView.Columns["Talle"].Width = 60;
            }
         }
         * */

        







    }
}
