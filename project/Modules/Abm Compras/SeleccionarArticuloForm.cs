﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using Zapateria.Libs;
using Zapateria.Models;

namespace Zapateria.Abm_Compras
{
    public partial class SeleccionarArticuloForm : Form
    {
        public SeleccionarArticuloForm()
        {
            InitializeComponent();
        }

        Producto producto;
        IList<Producto> listaProductos;

        private void SeleccionarArticuloForm_Load(object sender, EventArgs e)
        {
            CargadorDeDatos.CargarFamiliasComboBox(FamiliaComboBox, this.Text);

            producto = new Producto();

            /*listaProductos = producto.GetAll("1");
            
            DataTable dtProductos = new DataTable();
            dtProductos.Columns.Add("Codigo");
            dtProductos.Columns.Add("Familia");
            dtProductos.Columns.Add("Producto");
            dtProductos.Columns.Add("Stock");
            dtProductos.Columns.Add("Tipo de stock");
            dtProductos.Columns.Add("Formato Kg");
            dtProductos.Columns.Add("Ultimo precio de compra");
            dtProductos.Columns.Add("producto_formato_id");

            foreach (Producto p in listaProductos)
            {
                ProductoXFormatoCompra pfc = new ProductoXFormatoCompra();
                p.producto_x_formato_compra = pfc.getAllByProducto(p);
                String tipodestock = "";
                if(p.unidad){
                    tipodestock = "Unidad";
                    dtProductos.Rows.Add(p.id.ToString("D10"), p.familia.nombre, p.nombre, p.stock, tipodestock, "", p.precio_compra,"");
                }else{
                    tipodestock = "Fraccionado";
                    foreach (ProductoXFormatoCompra pfc_aux in p.producto_x_formato_compra)
                    {
                        dtProductos.Rows.Add(p.id.ToString("D10"), p.familia.nombre, p.nombre, p.getStockKg(), tipodestock, FormatoCompra.GetFormatoKG(pfc_aux.formato.formato), pfc_aux.precio_compra,pfc_aux.id);
                    }
                }

            }*/

            this.Productos_DataGridView.DataSource = producto.SearchCompras();
            producto.customColumnsSelect(this.Productos_DataGridView);
            this.NombreTextBox.Select();
        }

        private void Filtrar()
        {
            //this.Productos_DataGridView.DataSource = producto.DoSearch(listaProductos, this.CodigoTextBox.Text, this.NombreTextBox.Text, this.FamiliaComboBox.Text);
            ((DataTable)Productos_DataGridView.DataSource).DefaultView.RowFilter = FiltroCambios();
        }


        private string FiltroCambios()
        {
            string consulta = "";

            if (this.CodigoTextBox.Text != "")
            {
                consulta += "Codigo LIKE '%" + this.CodigoTextBox.Text + "%'";
            }

            if (this.NombreTextBox.Text != "")
            {
                if (consulta != "") { consulta += " AND "; }
                consulta += "Producto LIKE '%" + this.NombreTextBox.Text + "%'";
            }

            if (this.FamiliaComboBox.SelectedValue != null)
            {
                if (consulta != "") { consulta += " AND "; }
                consulta += "Familia = '" + this.FamiliaComboBox.Text + "'";
            }
            return consulta;
        }


        private void CancelarButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void CodigoTextBox_TextChanged(object sender, EventArgs e)
        {
            Filtrar();
        }

        private void DescripcionTextBox_TextChanged(object sender, EventArgs e)
        {
            Filtrar();
        }

        private void CategoriasComboBox_SelectionChangeCommitted(object sender, EventArgs e)
        {
            Filtrar();
        }

        private void LimpiarButton_Click(object sender, EventArgs e)
        {
            foreach (Control objeto in this.GroupBox.Controls)
            {
                if (objeto is TextBox)
                {
                    ((TextBox)objeto).Clear();
                }
                if (objeto is ComboBox)
                {
                    ((ComboBox)objeto).SelectedItem = null;
                }
            }
        }

        private void EliminarArticuloButton_Click(object sender, EventArgs e)
        {
            if (Productos_DataGridView.RowCount > 0)
            {
                AltaCompraForm.producto.id = Int32.Parse(Productos_DataGridView.CurrentRow.Cells["Codigo"].Value.ToString());
                AltaCompraForm.producto.nombre = Productos_DataGridView.CurrentRow.Cells["Producto"].Value.ToString();
                AltaCompraForm.producto.precio_compra = Convert.ToDecimal(Productos_DataGridView.CurrentRow.Cells["Ultimo precio de compra"].Value);

                DialogResult = DialogResult.OK;
            }
        }
    }
}
