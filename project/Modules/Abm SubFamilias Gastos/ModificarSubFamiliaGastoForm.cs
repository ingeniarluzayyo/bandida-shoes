﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using MySql.Data.MySqlClient;
using Zapateria.Models;
using Zapateria.Libs;


namespace Zapateria.Abm_SubFamilias_Gastos
{
    public partial class ModificarSubFamiliaGastoForm : Form
    {
        public ModificarSubFamiliaGastoForm(string Id, string Categoria)
        {
            InitializeComponent();

            subfamilia = subfamilia.getById(Convert.ToInt32(Id));
        }

        SubfamiliaGasto subfamilia = new SubfamiliaGasto();


        private void GuardarButton_Click(object sender, EventArgs e)
        {

            if (Validaciones() == -1) return;

            FamiliaGasto familia = new FamiliaGasto();

            subfamilia.familia_gasto = familia.getById(Convert.ToInt32(this.TipoComboBox.SelectedValue));
            subfamilia.nombre = this.CategoriaTextBox.Text;

            if (subfamilia.update() == true)
            {
                MessageBox.Show("Se ha modificado la subfamilia '" + subfamilia.nombre + "' por '" + CategoriaTextBox.Text + "' correctamente.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                DialogResult = DialogResult.OK;
            } 

        }

        private void LimpiarButton_Click(object sender, EventArgs e)
        {
            foreach (Control objeto in this.GroupBox.Controls)
            {
                if (objeto is TextBox)
                {
                    ((TextBox)objeto).Clear();
                }
            }
        }

        private void CancelarButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        private int Validaciones()
        {
            foreach (Control objeto in this.GroupBox.Controls)
            {
                if (objeto is TextBox && ((TextBox)objeto).Text == "")
                {
                    MessageBox.Show("Debe completar el campo vacio.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return -1;
                }
                if (objeto is ComboBox && ((ComboBox)objeto).SelectedItem == null)
                {
                    MessageBox.Show("Debe completar los campos vacios.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return -1;
                }
            }
            return 1;
        }

        private void AltaCategoriaForm_Load(object sender, EventArgs e)
        {
            CargadorDeDatos.CargarFamiliasGastosComboBox(this.TipoComboBox, this.Text);

            this.TipoComboBox.SelectedValue = subfamilia.familia_gasto.id;
            this.CategoriaTextBox.Text = subfamilia.nombre;
            this.CategoriaTextBox.Select();
        }

    }
}
