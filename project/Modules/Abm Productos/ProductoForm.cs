﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using System.Threading;
using Zapateria.Libs;
using Zapateria.Models;
using System.IO;
using Zapateria.Abm_Productos;
using Zapateria.Abm_Stock;

namespace Zapateria.Abm_Articulos
{
    public partial class ArticulosForm : Form
    {
        public ArticulosForm()
        {
            InitializeComponent();
        }

        Producto producto = new Producto();

        private void ArticulosForm_Load(object sender, EventArgs e)
        {
            this.BuscarButton.Select();
            //CargadorDeDatos.CargarFamiliasComboBox(FamiliaComboBox, this.Text);
            this.BuscarButton.PerformClick();
        }

        private void BuscarButton_Click(object sender, EventArgs e)
        {
            Security Busqueda = new Security();
            if (!Busqueda.ValidarSqlInjection(this.GroupBox)) { return; }

            string Consulta = ArmarConsulta();

            List<MySqlParameter> param = new List<MySqlParameter>();

            DataTable dt = BaseDeDatos.GetInstance.ExecuteCustomQuery(Consulta, param, this.Text);
            Productos_DataGridView.DataSource = dt;

            ModDataGridView.agregarBoton(Productos_DataGridView, "Ver stock");

            if (Productos_DataGridView.DataSource == null)
            {
                
                MessageBox.Show("No se encontraron resultados que coincidan con la búsqueda.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            
            this.Productos_DataGridView.Columns["Nombre"].Width = 315;
            this.Productos_DataGridView.Columns["Familia"].Width = 120;

            this.Productos_DataGridView.Columns["Ver stock"].DisplayIndex = this.Productos_DataGridView.Columns.Count - 1;

            this.Productos_DataGridView.Focus();
        }

        private string ArmarConsulta()
        {
            string Consulta = "SELECT CAST(P.id AS CHAR(20)) AS Codigo, F.nombre AS Familia, P.nombre AS Nombre, P.orden AS Orden FROM productos AS P ";
            Consulta += "JOIN familias as F ON F.id=P.familia WHERE 1=1 ";

            if (CodigoTextBox.TextLength > 0)
            {
                Consulta += "AND P.id LIKE '%" + CodigoTextBox.Text + "%' ";
            }

            if (NombreTextBox.TextLength > 0)
            {
                Consulta += "AND P.nombre LIKE '%" + NombreTextBox.Text.Trim() + "%' ";
            }

            if (FamiliaComboBox.SelectedItem != null)
            {
                Consulta += "AND P.familia= '" + FamiliaComboBox.SelectedValue.ToString() + "' ";
            }

            if (!this.InhabilitadosCheckBox.Checked)
            {
                Consulta += "AND P.habilitado= '1' ";
            }
            else {
                Consulta += "AND P.habilitado= '0' ";
            }

            Consulta += " GROUP BY P.id ORDER BY P.orden asc";

            return Consulta;
        }


        private void LimpiarButton_Click(object sender, EventArgs e)
        {
            foreach (Control objeto in this.GroupBox.Controls)
            {
                if (objeto is TextBox)
                {
                    ((TextBox)objeto).Clear();
                }
                if (objeto is ComboBox)
                {
                    ((ComboBox)objeto).SelectedItem = null;
                }
            }

            ModDataGridView.limpiarDataGridView(Productos_DataGridView, "Ver Detalle");
        }

        private void CancelarButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void AgregarButton_Click(object sender, EventArgs e)
        {
            AltaArticuloForm abrir = new AltaArticuloForm();
            DialogResult Resultado = abrir.ShowDialog();

            if (Resultado == DialogResult.OK) {
                BuscarButton.PerformClick();
            }
            
        }

        private void EliminarButton_Click(object sender, EventArgs e)
        {
            if (this.Productos_DataGridView.Rows.Count == 0)
            {
                return;
            }

            if (this.Productos_DataGridView.CurrentCell == null)
            {
                MessageBox.Show("Debe seleccionar una fila.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            int stockProducto = producto.GetStock(Int32.Parse(this.Productos_DataGridView.CurrentRow.Cells["Codigo"].Value.ToString()));
            if (stockProducto > 0)
            {
                MessageBox.Show("Error, No puede eliminar un artículo que posea stock mayor a cero.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            int aux;
            if (!Int32.TryParse(Productos_DataGridView.CurrentRow.Cells["Codigo"].Value.ToString(),out aux))
            {
                MessageBox.Show("Error, El codigo del producto no es un número.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            string ArticuloBorrado = Productos_DataGridView.CurrentRow.Cells["Codigo"].Value.ToString();

            DialogResult Resultado = MessageBox.Show("¿Esta seguro que desea eliminar el articulo '" + ArticuloBorrado + "'?", this.Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (Resultado == DialogResult.Yes)
            {
                
                producto.id = Int32.Parse(Productos_DataGridView.CurrentRow.Cells["Codigo"].Value.ToString());

                if (producto.delete())
                {
                    Productos_DataGridView.Rows.Remove(Productos_DataGridView.CurrentRow);
                    MessageBox.Show("Se ha dado de baja el articulo '" + ArticuloBorrado + "' correctamente.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }

        private void ModificarButton_Click(object sender, EventArgs e)
        {
            if (this.Productos_DataGridView.Rows.Count == 0)
            {
                return;
            }
            if (this.Productos_DataGridView.CurrentCell == null)
            {
                MessageBox.Show("Debe seleccionar una fila.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            int index_row = this.Productos_DataGridView.CurrentRow.Index;
            int scroll_position = this.Productos_DataGridView.FirstDisplayedScrollingRowIndex;

            ModificarProductoForm abrir = new ModificarProductoForm(producto.getById(Int32.Parse(this.Productos_DataGridView.CurrentRow.Cells["Codigo"].Value.ToString())));
            DialogResult Resultado = abrir.ShowDialog();
            if (Resultado == DialogResult.OK)
            {
                BuscarButton.PerformClick();
                this.Productos_DataGridView.FirstDisplayedScrollingRowIndex = scroll_position;
                Productos_DataGridView.Rows[index_row].Selected = true;
            }
        }

        private void ImprimirEtiquetaButton_Click(object sender, EventArgs e)
        {
            if (this.Productos_DataGridView.Rows.Count == 0)
            {
                return;
            }
            if (this.Productos_DataGridView.CurrentCell == null)
            {
                MessageBox.Show("Debe seleccionar una fila.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            producto = producto.getById(Int32.Parse(Productos_DataGridView.CurrentRow.Cells["Codigo"].Value.ToString()));

            Thread hilo = new Thread(AbrirFormReporte);
            hilo.SetApartmentState(System.Threading.ApartmentState.STA);
            hilo.Start();
        }

        private void AbrirFormReporte()
        {
            EtiquetaForm abrir = new EtiquetaForm(producto);
            abrir.ShowDialog();
        }

        private void Productos_DataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == this.Productos_DataGridView.Columns["Ver stock"].Index && e.RowIndex >= 0)
            {
                int producto_id = Int32.Parse(Productos_DataGridView.CurrentRow.Cells["Codigo"].Value.ToString());

                StockForm abrir = new StockForm(producto_id);
                abrir.ShowDialog();
            }
        }
    }
}
