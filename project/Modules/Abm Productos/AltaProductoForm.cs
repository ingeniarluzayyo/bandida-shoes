﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using MySql.Data.MySqlClient;

using Zapateria.Models;
using Zapateria.Libs;

namespace Zapateria.Abm_Articulos
{
    public partial class AltaArticuloForm : Form
    {
        public AltaArticuloForm()
        {
            InitializeComponent();
        }

        //Variables
        OpenFileDialog abrir = new OpenFileDialog();
        Producto producto = new Producto();
        ProductoXFormatoCompra pfc = new ProductoXFormatoCompra();
        ProductoXFormatoVenta pfv = new ProductoXFormatoVenta();
        FormatoVenta fv = new FormatoVenta();
        FormatoCompra fc = new FormatoCompra();

        private void CancelarButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void LimpiarButton_Click(object sender, EventArgs e)
        {
            foreach (Control objeto in this.GroupBox.Controls)
            {
                if (objeto is TextBox)
                {
                    ((TextBox)objeto).Clear();
                }
                if (objeto is ComboBox)
                {
                    ((ComboBox)objeto).SelectedItem = null;
                }
                if (objeto is NumericUpDown)
                {
                    ((NumericUpDown)objeto).Value = 0;
                }
                if (objeto is GroupBox)
                {
                    foreach (Control subobjeto in objeto.Controls)
                    {
                        if (subobjeto is TextBox)
                        {
                            ((TextBox)subobjeto).Clear();
                        }
                        if (subobjeto is ComboBox)
                        {
                            ((ComboBox)subobjeto).SelectedItem = null;
                        }
                        if (subobjeto is NumericUpDown)
                        {
                            ((NumericUpDown)subobjeto).Value = 0;
                        }
                    }
                }
            }

            producto = new Producto();
            pfc = new ProductoXFormatoCompra();
            pfv = new ProductoXFormatoVenta();
            fv = new FormatoVenta();
            fc = new FormatoCompra();

            SelectCodigo();
        }

        private void GuardarButton_Click(object sender, EventArgs e)
        {
            if (Validaciones() == -1) return;

            Proveedor proveedor = new Proveedor();

            producto.id = Int32.Parse(CodigoTextBox.Text);
            producto.nombre = NombreTextBox.Text;
            producto.familia = new Familia(Int32.Parse(FamiliaComboBox.SelectedValue.ToString()), FamiliaComboBox.SelectedText);
            producto.observacion = NotaTextBox.Text;
            producto.habilitado = "1";
            producto.orden = Int32.Parse(this.OrdenTextBox.Text);
            producto.unidad = true;
            producto.fraccionado = false;
            producto.precio_venta_x3 = Convert.ToDecimal(this.PrecioVentaX3TextBox.Text);
            producto.precio_venta_x6 = Convert.ToDecimal(this.PrecioVentaX6TextBox.Text);
            producto.precio_venta_x12 = Convert.ToDecimal(this.PrecioVentaX12TextBox.Text);

            if (producto.save() == true)
            {
                MessageBox.Show("Se ha dado de alta el producto '" + NombreTextBox.Text + "' correctamente.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.LimpiarButton.PerformClick();
                this.NombreTextBox.Focus();
            }                     
        }

        private int Validaciones()
        {
            foreach (Control objeto in this.GroupBox.Controls)
            {
                if ((objeto is TextBox && ((TextBox)objeto).Text == "") && ((TextBox)objeto).Name != "NotaTextBox" && ((TextBox)objeto).Name != "PrecioVentaUnidadTextBox" && ((TextBox)objeto).Name != "PrecioVentaFVTextBox" || (objeto is ComboBox && ((ComboBox)objeto).SelectedItem == null))
                {
                    MessageBox.Show("Debe completar los campos vacios.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return -1;
                }
            }

            int aux;
            if (!Int32.TryParse(CodigoTextBox.Text,out aux))
            {
                MessageBox.Show("El campo codigo de producto debe ser un numero.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return -1;
            }

            if (!Int32.TryParse(this.OrdenTextBox.Text, out aux))
            {
                MessageBox.Show("El campo Orden debe ser un numero.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return -1;
            }

            return 1;
        }        

        private void AltaArticuloForm_Load(object sender, EventArgs e)
        {
            CargadorDeDatos.CargarFamiliasComboBox(FamiliaComboBox, this.Text);

            producto.producto_x_formato_compra = new List<ProductoXFormatoCompra>();
            producto.producto_x_formato_venta = new List<ProductoXFormatoVenta>();

            SelectCodigo();

            NombreTextBox.Select();
         }

        private void SelectCodigo()
        {
            string Consulta = "SELECT (COALESCE( MAX( id ) , 0 ))+1 FROM productos";

            List<MySqlParameter> param = new List<MySqlParameter>();

            DataTable dt = BaseDeDatos.GetInstance.ExecuteCustomQuery(Consulta, param, this.Text);

            this.CodigoTextBox.Text = dt.Rows[0][0].ToString();

            int cantidad = (10 - CodigoTextBox.TextLength);

            for (int i = 0; i < cantidad; i++)
            {
                CodigoTextBox.Text = "0" + CodigoTextBox.Text;
            }
        }

        private void PrecioVentaX3TextBox_TextChanged(object sender, EventArgs e)
        {
            if (!Validacion.isDecimal(this.PrecioVentaX3TextBox.Text))
            {
                if (!Validacion.isDecimal(this.PrecioVentaX3TextBox.Text.Replace(",", ".")))
                {
                    return;
                }
                else
                {
                    this.PrecioVentaX3TextBox.Text = this.PrecioVentaX3TextBox.Text.Replace(",", ".");
                    PrecioVentaX3TextBox.SelectionStart = PrecioVentaX3TextBox.Text.Length;
                }
            }
        }

        private void PrecioVentaX6TextBox_TextChanged(object sender, EventArgs e)
        {
            if (!Validacion.isDecimal(this.PrecioVentaX6TextBox.Text))
            {
                if (!Validacion.isDecimal(this.PrecioVentaX6TextBox.Text.Replace(",", ".")))
                {
                    return;
                }
                else
                {
                    this.PrecioVentaX6TextBox.Text = this.PrecioVentaX6TextBox.Text.Replace(",", ".");
                    PrecioVentaX6TextBox.SelectionStart = PrecioVentaX6TextBox.Text.Length;
                }
            }
        }

        private void PrecioVentaX12TextBox_TextChanged(object sender, EventArgs e)
        {
            if (!Validacion.isDecimal(this.PrecioVentaX12TextBox.Text))
            {
                if (!Validacion.isDecimal(this.PrecioVentaX12TextBox.Text.Replace(",", ".")))
                {
                    return;
                }
                else
                {
                    this.PrecioVentaX12TextBox.Text = this.PrecioVentaX12TextBox.Text.Replace(",", ".");
                    PrecioVentaX12TextBox.SelectionStart = PrecioVentaX12TextBox.Text.Length;
                }
            }
        }

        private void AltaArticuloForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            DialogResult = DialogResult.OK;
        }
    }
}
