﻿namespace Zapateria.Abm_Productos
{
    partial class ModificarProductoForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ModificarProductoForm));
            this.FamiliaComboBox = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.NombreTextBox = new System.Windows.Forms.TextBox();
            this.CodigoTextBox = new System.Windows.Forms.TextBox();
            this.NombreUsuarioLabel = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.NotaTextBox = new System.Windows.Forms.TextBox();
            this.GroupBox = new System.Windows.Forms.GroupBox();
            this.HabilitadoCheckBox = new System.Windows.Forms.CheckBox();
            this.PrecioCompraLabel = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.PrecioCompraTextBox = new System.Windows.Forms.TextBox();
            this.OrdenTextBox = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.LimpiarButton = new System.Windows.Forms.Button();
            this.CancelarButton = new System.Windows.Forms.Button();
            this.GuardarButton = new System.Windows.Forms.Button();
            this.precioDeVentaGroupBox = new System.Windows.Forms.GroupBox();
            this.x12Label = new System.Windows.Forms.Label();
            this.PrecioVentaX12TextBox = new System.Windows.Forms.TextBox();
            this.x6Label = new System.Windows.Forms.Label();
            this.PrecioVentaX6TextBox = new System.Windows.Forms.TextBox();
            this.x3Label = new System.Windows.Forms.Label();
            this.PrecioVentaX3TextBox = new System.Windows.Forms.TextBox();
            this.GroupBox.SuspendLayout();
            this.precioDeVentaGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // FamiliaComboBox
            // 
            this.FamiliaComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.FamiliaComboBox.FormattingEnabled = true;
            this.FamiliaComboBox.Location = new System.Drawing.Point(131, 103);
            this.FamiliaComboBox.Name = "FamiliaComboBox";
            this.FamiliaComboBox.Size = new System.Drawing.Size(227, 23);
            this.FamiliaComboBox.TabIndex = 26;
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(68, 105);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(57, 16);
            this.label6.TabIndex = 27;
            this.label6.Text = "Familia*";
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(63, 69);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(62, 16);
            this.label1.TabIndex = 25;
            this.label1.Text = "Nombre*";
            // 
            // NombreTextBox
            // 
            this.NombreTextBox.Location = new System.Drawing.Point(131, 67);
            this.NombreTextBox.Name = "NombreTextBox";
            this.NombreTextBox.Size = new System.Drawing.Size(227, 21);
            this.NombreTextBox.TabIndex = 24;
            // 
            // CodigoTextBox
            // 
            this.CodigoTextBox.Location = new System.Drawing.Point(131, 27);
            this.CodigoTextBox.Name = "CodigoTextBox";
            this.CodigoTextBox.ReadOnly = true;
            this.CodigoTextBox.Size = new System.Drawing.Size(167, 21);
            this.CodigoTextBox.TabIndex = 23;
            // 
            // NombreUsuarioLabel
            // 
            this.NombreUsuarioLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.NombreUsuarioLabel.AutoSize = true;
            this.NombreUsuarioLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NombreUsuarioLabel.Location = new System.Drawing.Point(63, 29);
            this.NombreUsuarioLabel.Name = "NombreUsuarioLabel";
            this.NombreUsuarioLabel.Size = new System.Drawing.Size(57, 16);
            this.NombreUsuarioLabel.TabIndex = 22;
            this.NombreUsuarioLabel.Text = "Codigo*";
            // 
            // label9
            // 
            this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(46, 143);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(79, 16);
            this.label9.TabIndex = 41;
            this.label9.Text = "Observacion";
            // 
            // NotaTextBox
            // 
            this.NotaTextBox.Location = new System.Drawing.Point(131, 141);
            this.NotaTextBox.Multiline = true;
            this.NotaTextBox.Name = "NotaTextBox";
            this.NotaTextBox.Size = new System.Drawing.Size(227, 75);
            this.NotaTextBox.TabIndex = 40;
            // 
            // GroupBox
            // 
            this.GroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GroupBox.Controls.Add(this.precioDeVentaGroupBox);
            this.GroupBox.Controls.Add(this.HabilitadoCheckBox);
            this.GroupBox.Controls.Add(this.PrecioCompraLabel);
            this.GroupBox.Controls.Add(this.label11);
            this.GroupBox.Controls.Add(this.PrecioCompraTextBox);
            this.GroupBox.Controls.Add(this.OrdenTextBox);
            this.GroupBox.Controls.Add(this.label10);
            this.GroupBox.Controls.Add(this.CodigoTextBox);
            this.GroupBox.Controls.Add(this.label9);
            this.GroupBox.Controls.Add(this.NombreUsuarioLabel);
            this.GroupBox.Controls.Add(this.NotaTextBox);
            this.GroupBox.Controls.Add(this.NombreTextBox);
            this.GroupBox.Controls.Add(this.label1);
            this.GroupBox.Controls.Add(this.label6);
            this.GroupBox.Controls.Add(this.FamiliaComboBox);
            this.GroupBox.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GroupBox.Location = new System.Drawing.Point(12, 12);
            this.GroupBox.Name = "GroupBox";
            this.GroupBox.Size = new System.Drawing.Size(387, 480);
            this.GroupBox.TabIndex = 42;
            this.GroupBox.TabStop = false;
            this.GroupBox.Text = "Datos Producto";
            // 
            // HabilitadoCheckBox
            // 
            this.HabilitadoCheckBox.AutoSize = true;
            this.HabilitadoCheckBox.Location = new System.Drawing.Point(136, 447);
            this.HabilitadoCheckBox.Name = "HabilitadoCheckBox";
            this.HabilitadoCheckBox.Size = new System.Drawing.Size(15, 14);
            this.HabilitadoCheckBox.TabIndex = 46;
            this.HabilitadoCheckBox.UseVisualStyleBackColor = true;
            // 
            // PrecioCompraLabel
            // 
            this.PrecioCompraLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PrecioCompraLabel.AutoSize = true;
            this.PrecioCompraLabel.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PrecioCompraLabel.Location = new System.Drawing.Point(34, 408);
            this.PrecioCompraLabel.Name = "PrecioCompraLabel";
            this.PrecioCompraLabel.Size = new System.Drawing.Size(136, 16);
            this.PrecioCompraLabel.TabIndex = 68;
            this.PrecioCompraLabel.Text = "Ult. Precio de Compra";
            // 
            // label11
            // 
            this.label11.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(60, 445);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(65, 16);
            this.label11.TabIndex = 45;
            this.label11.Text = "Habilitado";
            // 
            // PrecioCompraTextBox
            // 
            this.PrecioCompraTextBox.Location = new System.Drawing.Point(176, 406);
            this.PrecioCompraTextBox.Name = "PrecioCompraTextBox";
            this.PrecioCompraTextBox.Size = new System.Drawing.Size(99, 21);
            this.PrecioCompraTextBox.TabIndex = 67;
            // 
            // OrdenTextBox
            // 
            this.OrdenTextBox.Location = new System.Drawing.Point(131, 228);
            this.OrdenTextBox.Name = "OrdenTextBox";
            this.OrdenTextBox.Size = new System.Drawing.Size(227, 21);
            this.OrdenTextBox.TabIndex = 65;
            // 
            // label10
            // 
            this.label10.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(78, 230);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(50, 16);
            this.label10.TabIndex = 66;
            this.label10.Text = "Orden*";
            // 
            // LimpiarButton
            // 
            this.LimpiarButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.LimpiarButton.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LimpiarButton.Location = new System.Drawing.Point(88, 498);
            this.LimpiarButton.Name = "LimpiarButton";
            this.LimpiarButton.Size = new System.Drawing.Size(75, 25);
            this.LimpiarButton.TabIndex = 43;
            this.LimpiarButton.Text = "Limpiar";
            this.LimpiarButton.UseVisualStyleBackColor = true;
            this.LimpiarButton.Click += new System.EventHandler(this.LimpiarButton_Click);
            // 
            // CancelarButton
            // 
            this.CancelarButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.CancelarButton.AutoSize = true;
            this.CancelarButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.CancelarButton.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CancelarButton.Location = new System.Drawing.Point(10, 498);
            this.CancelarButton.Name = "CancelarButton";
            this.CancelarButton.Size = new System.Drawing.Size(75, 25);
            this.CancelarButton.TabIndex = 44;
            this.CancelarButton.Text = "Cancelar";
            this.CancelarButton.UseVisualStyleBackColor = true;
            this.CancelarButton.Click += new System.EventHandler(this.CancelarButton_Click);
            // 
            // GuardarButton
            // 
            this.GuardarButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.GuardarButton.BackColor = System.Drawing.SystemColors.Control;
            this.GuardarButton.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GuardarButton.ForeColor = System.Drawing.SystemColors.ControlText;
            this.GuardarButton.Location = new System.Drawing.Point(322, 499);
            this.GuardarButton.Name = "GuardarButton";
            this.GuardarButton.Size = new System.Drawing.Size(75, 23);
            this.GuardarButton.TabIndex = 42;
            this.GuardarButton.Text = "Guardar";
            this.GuardarButton.UseVisualStyleBackColor = true;
            this.GuardarButton.Click += new System.EventHandler(this.GuardarButton_Click);
            // 
            // precioDeVentaGroupBox
            // 
            this.precioDeVentaGroupBox.Controls.Add(this.x12Label);
            this.precioDeVentaGroupBox.Controls.Add(this.PrecioVentaX12TextBox);
            this.precioDeVentaGroupBox.Controls.Add(this.x6Label);
            this.precioDeVentaGroupBox.Controls.Add(this.PrecioVentaX6TextBox);
            this.precioDeVentaGroupBox.Controls.Add(this.x3Label);
            this.precioDeVentaGroupBox.Controls.Add(this.PrecioVentaX3TextBox);
            this.precioDeVentaGroupBox.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.precioDeVentaGroupBox.Location = new System.Drawing.Point(31, 265);
            this.precioDeVentaGroupBox.Name = "precioDeVentaGroupBox";
            this.precioDeVentaGroupBox.Size = new System.Drawing.Size(327, 125);
            this.precioDeVentaGroupBox.TabIndex = 45;
            this.precioDeVentaGroupBox.TabStop = false;
            this.precioDeVentaGroupBox.Text = "Precio de venta";
            // 
            // x12Label
            // 
            this.x12Label.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.x12Label.AutoSize = true;
            this.x12Label.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.x12Label.Location = new System.Drawing.Point(41, 86);
            this.x12Label.Name = "x12Label";
            this.x12Label.Size = new System.Drawing.Size(33, 16);
            this.x12Label.TabIndex = 27;
            this.x12Label.Text = "x 12";
            // 
            // PrecioVentaX12TextBox
            // 
            this.PrecioVentaX12TextBox.Location = new System.Drawing.Point(82, 84);
            this.PrecioVentaX12TextBox.Name = "PrecioVentaX12TextBox";
            this.PrecioVentaX12TextBox.Size = new System.Drawing.Size(227, 21);
            this.PrecioVentaX12TextBox.TabIndex = 26;
            this.PrecioVentaX12TextBox.TextChanged += new System.EventHandler(this.PrecioVentaX12TextBox_TextChanged);
            // 
            // x6Label
            // 
            this.x6Label.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.x6Label.AutoSize = true;
            this.x6Label.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.x6Label.Location = new System.Drawing.Point(41, 59);
            this.x6Label.Name = "x6Label";
            this.x6Label.Size = new System.Drawing.Size(26, 16);
            this.x6Label.TabIndex = 25;
            this.x6Label.Text = "x 6";
            // 
            // PrecioVentaX6TextBox
            // 
            this.PrecioVentaX6TextBox.Location = new System.Drawing.Point(82, 57);
            this.PrecioVentaX6TextBox.Name = "PrecioVentaX6TextBox";
            this.PrecioVentaX6TextBox.Size = new System.Drawing.Size(227, 21);
            this.PrecioVentaX6TextBox.TabIndex = 24;
            this.PrecioVentaX6TextBox.TextChanged += new System.EventHandler(this.PrecioVentaX6TextBox_TextChanged);
            // 
            // x3Label
            // 
            this.x3Label.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.x3Label.AutoSize = true;
            this.x3Label.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.x3Label.Location = new System.Drawing.Point(41, 32);
            this.x3Label.Name = "x3Label";
            this.x3Label.Size = new System.Drawing.Size(26, 16);
            this.x3Label.TabIndex = 23;
            this.x3Label.Text = "x 3";
            // 
            // PrecioVentaX3TextBox
            // 
            this.PrecioVentaX3TextBox.Location = new System.Drawing.Point(82, 30);
            this.PrecioVentaX3TextBox.Name = "PrecioVentaX3TextBox";
            this.PrecioVentaX3TextBox.Size = new System.Drawing.Size(227, 21);
            this.PrecioVentaX3TextBox.TabIndex = 22;
            this.PrecioVentaX3TextBox.TextChanged += new System.EventHandler(this.PrecioVentaX3TextBox_TextChanged);
            // 
            // ModificarProductoForm
            // 
            this.AcceptButton = this.GuardarButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.CancelarButton;
            this.ClientSize = new System.Drawing.Size(411, 535);
            this.Controls.Add(this.LimpiarButton);
            this.Controls.Add(this.CancelarButton);
            this.Controls.Add(this.GroupBox);
            this.Controls.Add(this.GuardarButton);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ModificarProductoForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Modificar Producto";
            this.Load += new System.EventHandler(this.ModificarProductoForm2_Load);
            this.GroupBox.ResumeLayout(false);
            this.GroupBox.PerformLayout();
            this.precioDeVentaGroupBox.ResumeLayout(false);
            this.precioDeVentaGroupBox.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox FamiliaComboBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox NombreTextBox;
        private System.Windows.Forms.TextBox CodigoTextBox;
        private System.Windows.Forms.Label NombreUsuarioLabel;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox NotaTextBox;
        private System.Windows.Forms.GroupBox GroupBox;
        private System.Windows.Forms.Button LimpiarButton;
        private System.Windows.Forms.Button CancelarButton;
        private System.Windows.Forms.Button GuardarButton;
        private System.Windows.Forms.TextBox OrdenTextBox;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label PrecioCompraLabel;
        private System.Windows.Forms.TextBox PrecioCompraTextBox;
        private System.Windows.Forms.CheckBox HabilitadoCheckBox;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.GroupBox precioDeVentaGroupBox;
        private System.Windows.Forms.Label x12Label;
        private System.Windows.Forms.TextBox PrecioVentaX12TextBox;
        private System.Windows.Forms.Label x6Label;
        private System.Windows.Forms.TextBox PrecioVentaX6TextBox;
        private System.Windows.Forms.Label x3Label;
        private System.Windows.Forms.TextBox PrecioVentaX3TextBox;
    }
}