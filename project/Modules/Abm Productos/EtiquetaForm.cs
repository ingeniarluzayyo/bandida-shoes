﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Zapateria.Reportes;
using Zapateria.Models;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;

namespace Zapateria.Abm_Articulos
{
    public partial class EtiquetaForm : Form
    {
        public EtiquetaForm(Producto prod)
        {
            InitializeComponent();

            this.producto = prod;            
        }

        Producto producto = new Producto();
        Empresa empresa = new Empresa();

        Zapateria.Modules.Reportes.Etiqueta Repo;

        private void crystalReportViewer1_Load(object sender, EventArgs e)
        {
            Repo = new Zapateria.Modules.Reportes.Etiqueta();
            
            //Variables            
            TextObject CodigoRepo;
            CodigoRepo = (TextObject)Repo.ReportDefinition.ReportObjects["Codigo"];
            CodigoRepo.Text = producto.id.ToString("D10");

            empresa = empresa.getById(1);
            
            /*TextObject CategoriaRepo;
            CategoriaRepo = (TextObject)Repo.ReportDefinition.ReportObjects["Empresa"];
            CategoriaRepo.Text = empresa.razon_social;*/
             

            TextObject EtiquetaRepo;
            EtiquetaRepo = (TextObject)Repo.ReportDefinition.ReportObjects["DatosEtiqueta"];
            EtiquetaRepo.Text = empresa.etiqueta;

            TextObject DescripcionRepo;
            DescripcionRepo = (TextObject)Repo.ReportDefinition.ReportObjects["Nombre"];
            DescripcionRepo.Text = producto.nombre;

            TextObject CodigoBarraRepo;
            CodigoBarraRepo = (TextObject)Repo.ReportDefinition.ReportObjects["CodigoBarraText"];
            CodigoBarraRepo.Text = "*" + producto.id.ToString("D10") + "*";//CODE39 agregar * al final y inicio de codigo. tamaño 28

            try
            {
                Repo.PrintOptions.PrinterName = "ZDesigner GT800 (EPL)";
                //Repo.PrintToPrinter(1, false, 0, 0);
            }
            catch (Exception exep) { 
            }

            crystalReportViewer1.ReportSource = Repo;
        }
    }
}
