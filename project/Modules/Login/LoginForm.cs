﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Zapateria.Libs;
using System.Security.Cryptography;
using NHibernate;
using Zapateria.Models;
using NHibernate.Criterion;

namespace Zapateria.Modules.Login
{
    public partial class LoginForm : Form
    {
        public LoginForm()
        {
            InitializeComponent();
        }

        private void cancelarButton_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void loginButton_Click(object sender, EventArgs e)
        {            
            if (Validaciones() == -1) return;

            Usuario user = new Usuario();
            user.username = this.usuarioTextBox.Text;
            user.password = this.passwordTextBox.Text;

            var usuarios = user.getUsuarios();

            if (usuarios != null) {
                if (usuarios.Count() == 1)
                {
                    Zapateria.Properties.Settings.Default.User = this.usuarioTextBox.Text;

                    Main abrir = new Main(usuarios[0]);
                    abrir.Show();

                    this.Close();
                }
                else
                {
                    MessageBox.Show("Usuario o contraseña inválidos.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
            }
        }

        
        
        private int Validaciones()
        {
            if (usuarioTextBox.Text == "")
            {
                MessageBox.Show("Debe completar el campo usuario.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return -1;
            }
            if (passwordTextBox.Text == "")
            {
                MessageBox.Show("Debe completar el campo contraseña.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return -1;
            }

            Security Busqueda = new Security();
            if (!Busqueda.ValidarSqlInjection(this.GroupBox)) { return -1; }

            return 0;
        }

        private void LoginForm_Load(object sender, EventArgs e)
        {
            Cifrar.CifrarConnection();
            this.MaximizeBox = false;
            this.MinimizeBox = false;

            if (System.Diagnostics.Debugger.IsAttached){
                this.usuarioTextBox.Text = "admin";
                this.passwordTextBox.Text = "admin";
                this.loginButton.PerformClick();
            }

        }

    }
}
