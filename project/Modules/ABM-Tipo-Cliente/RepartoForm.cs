﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using MySql.Data.MySqlClient;
using Zapateria.Libs;
using Zapateria.Models;

namespace Zapateria.ABM_Tipo_Cliente
{
    public partial class TipoClienteForm : Form
    {
        public TipoClienteForm()
        {
            InitializeComponent();
        }

        private void AgregarButton_Click(object sender, EventArgs e)
        {
            AltaTipoClienteForm abrir = new AltaTipoClienteForm();
            abrir.ShowDialog();
        }

        private void EliminarButton_Click(object sender, EventArgs e)
        {
            if (this.TipoClientes_DataGridView.Rows.Count == 0 )
            {
                return;
            }

            if (this.TipoClientes_DataGridView.CurrentCell == null)
            {
                MessageBox.Show("Debe seleccionar una fila.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            
            string CategoriaBorrada = TipoClientes_DataGridView.CurrentRow.Cells["Nombre"].Value.ToString();

            DialogResult Resultado = MessageBox.Show("¿Esta seguro que desea eliminar la categoria '" + CategoriaBorrada + "'?", this.Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (Resultado == DialogResult.Yes)
            {
                Reparto reparto = new Reparto();
                reparto.id = Convert.ToInt32(TipoClientes_DataGridView.CurrentRow.Cells["id"].Value);

                if (reparto.delete() == true)
                {
                    if (this.TipoClienteTextBox.TextLength > 0)
                    {
                        reparto.nombre = this.TipoClienteTextBox.Text;
                    }

                    this.TipoClientes_DataGridView.DataSource = reparto.getRepartosLike();

                    MessageBox.Show("Se ha dado de baja la categoria '" + CategoriaBorrada + "' correctamente.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }

        private void ModificarButton_Click(object sender, EventArgs e)
        {
            if (this.TipoClientes_DataGridView.Rows.Count == 0)
            {
                return;
            }
            if (this.TipoClientes_DataGridView.CurrentCell == null)
            {
                MessageBox.Show("Debe seleccionar una fila.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            Reparto reparto = new Reparto();
            reparto.id = Convert.ToInt32(this.TipoClientes_DataGridView.CurrentRow.Cells["id"].Value.ToString());
            reparto.nombre = this.TipoClientes_DataGridView.CurrentRow.Cells["nombre"].Value.ToString();
            //tipo_cliente.ganancia = Convert.ToDecimal(this.TipoClientes_DataGridView.CurrentRow.Cells["ganancia"].Value.ToString());

            ModificarTipoClienteForm abrir = new ModificarTipoClienteForm(reparto);
            DialogResult Resultado = abrir.ShowDialog();
            if (Resultado == DialogResult.OK)
            {
                ModDataGridView.limpiarDataGridView(TipoClientes_DataGridView, "");
            }
        }

        private void BuscarButton_Click(object sender, EventArgs e)
        {
            Security Busqueda = new Security();
            if (!Busqueda.ValidarSqlInjection(this.GroupBox)) { return; }

            Reparto reparto = new Reparto();

            if (TipoClienteTextBox.TextLength > 0)
            {
                reparto.nombre = this.TipoClienteTextBox.Text;
            }

            var tipo_clientes = reparto.getRepartosLike();

            if (tipo_clientes.Count() == 0)
            {
                MessageBox.Show("No se encontraron resultados que coincidan con la busqueda.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            else
            {
                this.TipoClientes_DataGridView.DataSource = tipo_clientes;
                reparto.customColumns(this.TipoClientes_DataGridView);
            }

            this.TipoClientes_DataGridView.Focus();
        }

        private void LimpiarButton_Click(object sender, EventArgs e)
        {
            foreach (Control objeto in this.GroupBox.Controls)
            {
                if (objeto is TextBox)
                {
                    ((TextBox)objeto).Clear();
                }
            }

            ModDataGridView.limpiarDataGridView(TipoClientes_DataGridView, "");
        }

        private void CancelarButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void CategoriasForm_Load(object sender, EventArgs e)
        {
            this.TipoClientes_DataGridView.MultiSelect = false;
            this.TipoClienteTextBox.Select();
        }


    }
}
