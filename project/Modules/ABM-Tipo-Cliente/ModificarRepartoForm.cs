﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using MySql.Data.MySqlClient;
using Zapateria.Models;


namespace Zapateria.ABM_Tipo_Cliente
{
    public partial class ModificarTipoClienteForm : Form
    {
        public ModificarTipoClienteForm(Reparto reparto)
        {
            InitializeComponent();

            reparto_load = reparto;
        }

        Reparto reparto_load;

        private void GuardarButton_Click(object sender, EventArgs e)
        {

            if (Validaciones() == -1) return;

            Reparto reparto = new Reparto();
            reparto.id = Convert.ToInt32(reparto_load.id);
            reparto.nombre = this.NombreTextBox.Text;

            if (reparto.update() == true)
            {
                MessageBox.Show("Se ha modificado el reparto correctamente.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                DialogResult = DialogResult.OK;
            } 

        }

        private void LimpiarButton_Click(object sender, EventArgs e)
        {
            foreach (Control objeto in this.GroupBox.Controls)
            {
                if (objeto is TextBox)
                {
                    ((TextBox)objeto).Clear();
                }
            }
        }

        private void CancelarButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        private int Validaciones()
        {
            foreach (Control objeto in this.GroupBox.Controls)
            {
                if (objeto is TextBox && ((TextBox)objeto).Text == "")
                {
                    MessageBox.Show("Debe completar el campo vacio.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return -1;
                }
            }
            return 1;
        }

        private void AltaCategoriaForm_Load(object sender, EventArgs e)
        {
            this.NombreTextBox.Text = reparto_load.nombre;
            this.NombreTextBox.Select();
        }
    }
}
