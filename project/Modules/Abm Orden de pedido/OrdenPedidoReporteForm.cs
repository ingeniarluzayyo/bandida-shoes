﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Mayorista.Models;
using CrystalDecisions.CrystalReports.Engine;
using Mayorista.Modules.Reportes;

namespace Mayorista.Modules.Abm_Orden_de_pedido
{
    public partial class OrdenPedidoReporteForm : Form
    {
        public OrdenPedidoReporteForm(OrdenPedido pedido)
        {
            InitializeComponent();
            this.pedido = pedido;
        }

        Empresa empresa = new Empresa();
        OrdenPedido pedido;
        DataTable dt;

        private void crystalReportViewer1_Load(object sender, EventArgs e)
        {
            DetallePedidoCrystalReport Repo = new DetallePedidoCrystalReport();

            empresa = empresa.getById(1);

            //Variables
            TextObject EmpresaNombreRepo;
            EmpresaNombreRepo = (TextObject)Repo.ReportDefinition.ReportObjects["EmpresaNombreText"];
            EmpresaNombreRepo.Text = empresa.razon_social;
            EmpresaNombreRepo = (TextObject)Repo.ReportDefinition.ReportObjects["EmpresaNombreText2"];
            EmpresaNombreRepo.Text = empresa.razon_social;

            TextObject FechaRepo;
            FechaRepo = (TextObject)Repo.ReportDefinition.ReportObjects["FechaText"];
            FechaRepo.Text = pedido.fecha.ToShortDateString();
            FechaRepo = (TextObject)Repo.ReportDefinition.ReportObjects["FechaText2"];
            FechaRepo.Text = pedido.fecha.ToShortDateString();

            TextObject NumeroFacturaRepo;
            NumeroFacturaRepo = (TextObject)Repo.ReportDefinition.ReportObjects["NumeroFacturaText"];
            NumeroFacturaRepo.Text = pedido.id.ToString("D10");
            NumeroFacturaRepo = (TextObject)Repo.ReportDefinition.ReportObjects["NumeroFacturaText2"];
            NumeroFacturaRepo.Text = pedido.id.ToString("D10");

            TextObject RazonSocialEmpresaRepo;
            RazonSocialEmpresaRepo = (TextObject)Repo.ReportDefinition.ReportObjects["EmpresaText"];
            RazonSocialEmpresaRepo.Text = empresa.razon_social;
            RazonSocialEmpresaRepo = (TextObject)Repo.ReportDefinition.ReportObjects["EmpresaText2"];
            RazonSocialEmpresaRepo.Text = empresa.razon_social;

            TextObject CUITEmpresaRepo;
            CUITEmpresaRepo = (TextObject)Repo.ReportDefinition.ReportObjects["CUITEmpresaText"];
            CUITEmpresaRepo.Text = empresa.cuit;
            CUITEmpresaRepo = (TextObject)Repo.ReportDefinition.ReportObjects["CUITEmpresaText2"];
            CUITEmpresaRepo.Text = empresa.cuit;

            TextObject DomicilioEmpresaRepo;
            DomicilioEmpresaRepo = (TextObject)Repo.ReportDefinition.ReportObjects["DireccionEmpresaText"];
            DomicilioEmpresaRepo.Text = empresa.direccion;
            DomicilioEmpresaRepo = (TextObject)Repo.ReportDefinition.ReportObjects["DireccionEmpresaText2"];
            DomicilioEmpresaRepo.Text = empresa.direccion;

            TextObject TelefonoEmpresaRepo;
            TelefonoEmpresaRepo = (TextObject)Repo.ReportDefinition.ReportObjects["TelefonoEmpresaText"];
            TelefonoEmpresaRepo.Text = empresa.telefono;
            TelefonoEmpresaRepo = (TextObject)Repo.ReportDefinition.ReportObjects["TelefonoEmpresaText2"];
            TelefonoEmpresaRepo.Text = empresa.telefono;

            TextObject RazonSocialClienteRepo;
            RazonSocialClienteRepo = (TextObject)Repo.ReportDefinition.ReportObjects["RazonSocialClienteText"];
            RazonSocialClienteRepo.Text = pedido.cliente.cliente;
            RazonSocialClienteRepo = (TextObject)Repo.ReportDefinition.ReportObjects["RazonSocialClienteText2"];
            RazonSocialClienteRepo.Text = pedido.cliente.cliente;

            TextObject CUITClienteRepo;
            CUITClienteRepo = (TextObject)Repo.ReportDefinition.ReportObjects["CUITClienteText"];
            CUITClienteRepo.Text = pedido.cliente.cuit;
            CUITClienteRepo = (TextObject)Repo.ReportDefinition.ReportObjects["CUITClienteText2"];
            CUITClienteRepo.Text = pedido.cliente.cuit;

            TextObject DomicilioClienteRepo;
            DomicilioClienteRepo = (TextObject)Repo.ReportDefinition.ReportObjects["DireccionClienteText"];
            DomicilioClienteRepo.Text = pedido.cliente.direccion;
            DomicilioClienteRepo = (TextObject)Repo.ReportDefinition.ReportObjects["DireccionClienteText2"];
            DomicilioClienteRepo.Text = pedido.cliente.direccion;

            TextObject TelefonoClienteRepo;
            TelefonoClienteRepo = (TextObject)Repo.ReportDefinition.ReportObjects["TelefonoClienteText"];
            TelefonoClienteRepo.Text = pedido.cliente.tel;
            TelefonoClienteRepo = (TextObject)Repo.ReportDefinition.ReportObjects["TelefonoClienteText2"];
            TelefonoClienteRepo.Text = pedido.cliente.tel;


            TextObject SubTotalRepo;
            SubTotalRepo = (TextObject)Repo.ReportDefinition.ReportObjects["SubtotalText"];
            SubTotalRepo.Text = (pedido.total + pedido.descuento).ToString();
            SubTotalRepo = (TextObject)Repo.ReportDefinition.ReportObjects["SubtotalText2"];
            SubTotalRepo.Text = (pedido.total + pedido.descuento).ToString();

            TextObject DescuentoRepo;
            DescuentoRepo = (TextObject)Repo.ReportDefinition.ReportObjects["DescuentoText"];
            DescuentoRepo.Text = pedido.descuento.ToString();
            DescuentoRepo = (TextObject)Repo.ReportDefinition.ReportObjects["DescuentoText2"];
            DescuentoRepo.Text = pedido.descuento.ToString();

            TextObject TotalRepo;
            TotalRepo = (TextObject)Repo.ReportDefinition.ReportObjects["TotalText"];
            TotalRepo.Text = pedido.total.ToString();
            TotalRepo = (TextObject)Repo.ReportDefinition.ReportObjects["TotalText2"];
            TotalRepo.Text = pedido.total.ToString();

            DataTable dt = new DataTable();
            dt.Columns.Add("codigo");
            dt.Columns.Add("nombre");
            dt.Columns.Add("precio");
            dt.Columns.Add("cantidad");
            dt.Columns.Add("importe");

            foreach (ItemPedido item in pedido.items)
            {
                dt.Rows.Add(item.producto.id.ToString("D10"), item.producto.nombre, item.precio, item.cantidad, item.precio * item.cantidad);
            }


            Repo.SetDataSource(dt);

            crystalReportViewer1.ReportSource = Repo;
        }
    }
}
