﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using Mayorista.Libs;
using System.Threading;
using Mayorista.Models;
using Mayorista.Modules.Abm_Ventas;
using Mayorista.Modules.Abm_Orden_de_pedido;

namespace Mayorista.Abm_Orden_de_pedido
{
    public partial class PedidoForm : Form
    {
        public PedidoForm()
        {
            InitializeComponent();
        }

        IList<OrdenPedido> pedidos;

        private void BuscarButton_Click(object sender, EventArgs e)
        {
            Security Busqueda = new Security();
            if (!Busqueda.ValidarSqlInjection(this.GroupBox)) { return; }

            OrdenPedido pedido = new OrdenPedido();

            int venta_id = 0;
            int cliente_id = 0;
            int vendedor_id = 0;
            string desde = "";
            string hasta = "";

            if (NumeroTextBox.TextLength > 0)
            {
                venta_id = Convert.ToInt32(NumeroTextBox.Text);
            }

            if (ClienteComboBox.SelectedItem != null)
            {
                cliente_id = Convert.ToInt32(ClienteComboBox.SelectedValue.ToString());
            }

            if (vendedorComboBox.SelectedItem != null)
            {
                vendedor_id = Convert.ToInt32(vendedorComboBox.SelectedValue.ToString());
            }

            if (DesdeDateTimePicker.Text != " ")
            {
                desde = DesdeDateTimePicker.Value.ToShortDateString();
            }

            if (HastaDateTimePicker.Text != " ")
            {
                hasta = HastaDateTimePicker.Value.ToShortDateString();
            }

            pedidos = pedido.Search(venta_id, cliente_id, vendedor_id, desde, hasta);

            Ventas_DataGridView.DataSource = pedidos;
       
            if (Ventas_DataGridView.DataSource == null)
            {
                ModDataGridView.limpiarDataGridView(Ventas_DataGridView, "Ver Detalle");
                MessageBox.Show("No se encontraron resultados que coincidan con la busqueda.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            ModDataGridView.agregarBoton(Ventas_DataGridView, "Ver Detalle");            
            pedido.customColumns(Ventas_DataGridView);
            this.Ventas_DataGridView.Focus();
        }        

        private void LimpiarButton_Click(object sender, EventArgs e)
        {
            foreach (Control objeto in this.GroupBox.Controls)
            {
                if (objeto is TextBox)
                {
                    ((TextBox)objeto).Clear();
                }
                if (objeto is ComboBox)
                {
                    ((ComboBox)objeto).SelectedItem = null;
                }
            }

            ModDataGridView.limpiarDataGridView(Ventas_DataGridView, "Ver Detalle");
            this.DesdeDateTimePicker.Format = DateTimePickerFormat.Custom;
            this.DesdeDateTimePicker.CustomFormat = " ";

            this.HastaDateTimePicker.Format = DateTimePickerFormat.Custom;
            this.HastaDateTimePicker.CustomFormat = " ";
        }

        private void CancelarButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void EliminarButton_Click(object sender, EventArgs e)
        {
            if (this.Ventas_DataGridView.Rows.Count == 0)
            {
                return;
            }

            if (this.Ventas_DataGridView.CurrentCell == null)
            {
                MessageBox.Show("Debe seleccionar una fila.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            string PedidoBorrado = Ventas_DataGridView.CurrentRow.Cells["id"].Value.ToString();

            DialogResult Resultado = MessageBox.Show("¿Esta seguro que desea eliminar el Pedido Nº '" + PedidoBorrado + "'?", this.Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (Resultado == DialogResult.Yes)
            {
                OrdenPedido pedido = new OrdenPedido();
                pedido = pedido.getById(Convert.ToInt32(PedidoBorrado), true);

                if (pedido.delete())
                {
                    pedidos.RemoveAt(Ventas_DataGridView.CurrentRow.Index);
                    ModDataGridView.limpiarDataGridView(Ventas_DataGridView, "Ver Detalle");
                    Ventas_DataGridView.DataSource = pedidos;
                    ModDataGridView.agregarBoton(Ventas_DataGridView, "Ver Detalle");
                    pedido.customColumns(Ventas_DataGridView);
                    this.Ventas_DataGridView.Focus();

                    MessageBox.Show("Se ha borrado el Pedido Nº '" + PedidoBorrado + "' correctamente.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                
            }
        }

        private void AgregarButton_Click(object sender, EventArgs e)
        {
            AltaPedidoForm abrir = new AltaPedidoForm();
            DialogResult Resultado = abrir.ShowDialog();
            if (Resultado == DialogResult.OK)
            {
                ModDataGridView.limpiarDataGridView(Ventas_DataGridView, "Ver Detalle");
            }
        }

        private void CompraForm_Load(object sender, EventArgs e)
        {
            CargadorDeDatos.CargarClienteComboBox(ClienteComboBox, this.Text);
            CargadorDeDatos.CargarVendedoresComboBox(vendedorComboBox, this.Text);
            this.DesdeDateTimePicker.Format = DateTimePickerFormat.Custom;
            this.DesdeDateTimePicker.CustomFormat = " ";
            this.HastaDateTimePicker.Format = DateTimePickerFormat.Custom;
            this.HastaDateTimePicker.CustomFormat = " ";
            this.BuscarButton.Select();
        }

        private void Compras_DataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == this.Ventas_DataGridView.Columns["Ver Detalle"].Index && e.RowIndex >= 0)
            {
                string id = Ventas_DataGridView.CurrentRow.Cells["id"].Value.ToString();

                DetalleOrdenForm abrir = new DetalleOrdenForm(id);
                abrir.ShowDialog();
            }
        }

        private void NumeroTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar)
               && !char.IsDigit(e.KeyChar)
               )
            {
                e.Handled = true;
            }
        }

        DateTime desde;
        DateTime hasta;        

        private void DesdeDateTimePicker_CloseUp(object sender, EventArgs e)
        {
            this.DesdeDateTimePicker.Format = DateTimePickerFormat.Short;
        }

        private void HastaDateTimePicker_CloseUp(object sender, EventArgs e)
        {
            this.HastaDateTimePicker.Format = DateTimePickerFormat.Short;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (this.Ventas_DataGridView.Rows.Count == 0)
            {
                return;
            }
            if (this.Ventas_DataGridView.CurrentCell == null)
            {
                MessageBox.Show("Debe seleccionar una fila.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            Thread hilo = new Thread(AbrirFormReporteDetalleFactura);
            hilo.SetApartmentState(System.Threading.ApartmentState.STA);
            hilo.Start();
        }

        private void AbrirFormReporteDetalleFactura()
        {
            if (this.Ventas_DataGridView.Rows.Count == 0)
            {
                return;
            }
            if (this.Ventas_DataGridView.CurrentCell == null)
            {
                MessageBox.Show("Debe seleccionar una fila.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            int id = Convert.ToInt32(Ventas_DataGridView.CurrentRow.Cells["id"].Value.ToString());
            OrdenPedido pedido = new OrdenPedido();
            pedido = pedido.getById(id);
            OrdenPedidoReporteForm abrir = new OrdenPedidoReporteForm(pedido);
            abrir.ShowDialog();            
            
        }

        private void FacturaButton_Click(object sender, EventArgs e)
        {
            if (this.Ventas_DataGridView.Rows.Count == 0)
            {
                return;
            }

            if (this.Ventas_DataGridView.CurrentCell == null)
            {
                MessageBox.Show("Debe seleccionar una fila.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            string pedido_seleccionado = Ventas_DataGridView.CurrentRow.Cells["id"].Value.ToString();

            OrdenPedido pedido = new OrdenPedido();
            pedido = pedido.getById(Convert.ToInt32(pedido_seleccionado), false);
            
            foreach(ItemPedido item in pedido.items){
                Producto producto = new Producto();
                producto = producto.getById(item.producto.id);

                if (producto.stock < item.cantidad) {
                    MessageBox.Show("Hay productos en la Orden de Pedido que no tienen stock disponible, por favor modifique el pedido.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return;
                }
            }

            FacturaVenta fact = new FacturaVenta();

            fact.cliente = pedido.cliente;
            fact.vendedor = pedido.vendedor;
            fact.forma_de_pago = pedido.forma_de_pago;
            fact.fecha = pedido.fecha;
            fact.descuento = pedido.descuento;
            fact.total = pedido.total;
            fact.total_comision = pedido.total_comision;

            foreach (ItemPedido item in pedido.items)
            {
                ItemVenta itemVenta = new ItemVenta();
                itemVenta.producto = item.producto;
                itemVenta.cantidad = item.cantidad;
                itemVenta.producto.stock = item.producto.stock;

                itemVenta.precio = item.precio;
                itemVenta.producto.precio_compra = item.producto.precio_compra;
                itemVenta.factura = fact;

                fact.items.Add(itemVenta);
            }

            if (fact.save() == true)
            {
                if (pedido.delete()) {
                    pedidos.RemoveAt(Ventas_DataGridView.CurrentRow.Index);
                    ModDataGridView.limpiarDataGridView(Ventas_DataGridView, "Ver Detalle");
                    Ventas_DataGridView.DataSource = pedidos;
                    ModDataGridView.agregarBoton(Ventas_DataGridView, "Ver Detalle");
                    pedido.customColumns(Ventas_DataGridView);
                    this.Ventas_DataGridView.Focus();
                }

                MessageBox.Show("Se genero la factura correctamente.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }   

        }

        private void ModificarButton_Click(object sender, EventArgs e)
        {
            string pedido_seleccionado = Ventas_DataGridView.CurrentRow.Cells["id"].Value.ToString();
            ModificarPedidoForm form = new ModificarPedidoForm(pedido_seleccionado);
            form.ShowDialog();
        }


    }
}
