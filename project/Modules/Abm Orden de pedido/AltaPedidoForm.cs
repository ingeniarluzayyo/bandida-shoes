﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Globalization;
using Mayorista.Libs;
using Mayorista.Models;
using MySql.Data.MySqlClient;
using System.Threading;
using Mayorista.Modules.Abm_Orden_de_pedido;

namespace Mayorista.Abm_Orden_de_pedido
{
    public partial class AltaPedidoForm : Form
    {
        public AltaPedidoForm()
        {
            InitializeComponent();
        }

        public static Producto producto = new Producto();

        OrdenPedido pedido = new OrdenPedido();
        Cliente cliente = new Cliente();
        Vendedor vendedor = new Vendedor();
        FormaPago forma_de_pago = new FormaPago();

        private void CodigoTextBox_TextChanged(object sender, EventArgs e)
        {
           if (CodigoTextBox.Text.Length >= 10)
           {
                string Consulta = "SELECT p.nombre FROM productos as p WHERE p.id='" + CodigoTextBox.Text + "'";

                List<MySqlParameter> param = new List<MySqlParameter>();

                DataTable dt = BaseDeDatos.GetInstance.ExecuteCustomQuery(Consulta, param, this.Text);

                if (dt != null)
                {
                    if (dt.Rows.Count == 1)
                    {
                        NombreTextBox.Text = dt.Rows[0]["nombre"].ToString(); 
                        CantidadTextBox.Text = "1";
                        CantidadTextBox.ReadOnly = false;
                    }
                }
                else
                {
                    NombreTextBox.Text = "Nombre";
                    CantidadTextBox.Text = "Cantidad";
                    CantidadTextBox.ReadOnly = true;
                }
           }
        }

        private void CodigoTextBox_Enter(object sender, EventArgs e)
        {
            if (CodigoTextBox.Text == "Codigo")
            {
                this.CodigoTextBox.Text = "000000";
            }
        }

        private void CodigoTextBox_Leave(object sender, EventArgs e)
        {
            if (CodigoTextBox.Text == "")
            {
                CodigoTextBox.Text = "Codigo";
            }
        }

        private void AltaCompraForm_Load(object sender, EventArgs e)
        {
            //NUMERO DE FACTURA
            string Consulta = "SELECT MAX(id) FROM factura_venta";
            List<MySqlParameter> param = new List<MySqlParameter>();
            DataTable ds = BaseDeDatos.GetInstance.ExecuteCustomQuery(Consulta, param, this.Text);

            foreach (DataRow row in ds.Rows)
            {
                if (Convert.ToString(row[0]) == "")
                {
                    row[0] = 0;
                }
                int NumeroFactura = (Convert.ToInt32(row[0]) + 1);
                this.FactIdTextBox.Text = Convert.ToString(NumeroFactura);
            }


            CargadorDeDatos.CargarClienteComboBox(ClienteComboBox, this.Text);
            CargadorDeDatos.CargarFormaPagoComboBox(FormaPagoComboBox, this.Text);
            CargadorDeDatos.CargarVendedoresComboBox(vendedorComboBox, this.Text);
            TotalTextBox.Text = String.Format("{0:#,###0.00}", 0);
            SubTotalTextBox.Text = String.Format("{0:#,###0.00}", 0);
            DescuentoTextBox.Text = String.Format("{0:#,###0.00}", 0);

            this.Ventas_DataGridView.Columns["Nombre"].Width = 200;
        }

        private void AgregarArticuloButton_Click(object sender, EventArgs e)
        {
            if (ClienteComboBox.SelectedValue != null && vendedorComboBox.SelectedValue != null)
            {
                SeleccionarArticuloForm abrir = new SeleccionarArticuloForm();
                DialogResult Resultado = abrir.ShowDialog();
                if (Resultado == DialogResult.OK)
                {
                    producto = producto.getById(producto.id);
                    CodigoTextBox.Text = producto.id.ToString("D10");
                    decimal precio = producto.getPrecioVenta();
                    PrecioTextBox.Text = precio.ToString();
                    NombreTextBox.Text = producto.nombre;
                    CantidadTextBox.Text = "1";
                    CantidadTextBox.ReadOnly = false;
                    CantidadTextBox.Focus();
                }
            }
            else
            {
                MessageBox.Show("Debe seleccionar un cliente y un vendedor antes.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void AddArticuloButton_Click(object sender, EventArgs e)
        {
            if (!Validaciones()) { return; }

            Ventas_DataGridView.Rows.Add(producto.id.ToString("D10"), producto.nombre, Decimal.Parse(PrecioTextBox.Text), CantidadTextBox.Text, Decimal.Parse(PrecioTextBox.Text) * Convert.ToInt32(CantidadTextBox.Text));

            SubTotalTextBox.Text = (Convert.ToDecimal(SubTotalTextBox.Text) + Decimal.Parse(PrecioTextBox.Text) * Convert.ToInt32(CantidadTextBox.Text)).ToString();
            TotalTextBox.Text = (Convert.ToDecimal(SubTotalTextBox.Text) - Convert.ToDecimal(DescuentoTextBox.Text)).ToString();
            ComisionTextBox.Text = ((double)((Convert.ToDecimal(SubTotalTextBox.Text) - Convert.ToDecimal(DescuentoTextBox.Text)) * vendedor.comision / 100)).ToString();

            CodigoTextBox.Text = "Codigo";
            NombreTextBox.Text = "Nombre";
            PrecioTextBox.Text = "Precio";
            CantidadTextBox.Text = "Cantidad";
            CantidadTextBox.ReadOnly = true;

            this.CodigoTextBox.Focus();
        }

        private bool Validaciones()
        {
            if (this.NombreTextBox.Text == "Nombre" && this.CantidadTextBox.Text == "Cantidad")
            {
                return false;
            }

            int v;
            decimal x;
            if (!Int32.TryParse(this.CantidadTextBox.Text.Trim(), out v))
            {
                MessageBox.Show("Error: La cantidad solo puede ser numerica.");
                return false;
            }

            if (!Decimal.TryParse(PrecioTextBox.Text, out x))
            {
                MessageBox.Show("Error: El precio solo puede ser numerico.");
                return false;
            }

            foreach (DataGridViewRow row in Ventas_DataGridView.Rows)
            {
                if (producto.id == Convert.ToInt32(row.Cells["Codigo"].Value.ToString()))
                {
                    MessageBox.Show("Error: El producto ya esta agregado en la orden.");
                    return false; 
                }
            }

            return true;
        }

        private void GuardarButton_Click(object sender, EventArgs e)
        {
            if (Validaciones_guardar() == -1) return;
            Producto producto = new Producto();

            pedido = new OrdenPedido();
            pedido.cliente = cliente.getById(Convert.ToInt32(ClienteComboBox.SelectedValue.ToString()));
            pedido.vendedor = vendedor.getById(Convert.ToInt32(vendedorComboBox.SelectedValue.ToString()));
            pedido.forma_de_pago = forma_de_pago.getById(Convert.ToInt32(FormaPagoComboBox.SelectedValue.ToString()));
            pedido.fecha = FechaDateTimePicker.Value;
            pedido.descuento = Decimal.Parse(DescuentoTextBox.Text);
            pedido.total = Decimal.Parse(TotalTextBox.Text);
            pedido.total_comision = Decimal.Parse(ComisionTextBox.Text);

            foreach (DataGridViewRow row in Ventas_DataGridView.Rows)
            {
                ItemPedido item = new ItemPedido();
                item.producto = producto.getById(Int32.Parse(row.Cells["Codigo"].Value.ToString()));
                item.cantidad = Convert.ToInt32(row.Cells["Cantidad"].Value);

                if (item.producto.stock < 0)
                {
                    MessageBox.Show("Ocurrio un error con el calculo de stock del producto " + item.producto.nombre + ".", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                item.precio = Convert.ToDecimal(row.Cells["PrecioUnitario"].Value);
                item.producto.precio_compra = item.precio;
                item.pedido = pedido;

                pedido.items.Add(item);
            }

            if (pedido.save() == true)
            {
                MessageBox.Show("Se ha dado de alta la Orden de Pedido correctamente.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                
                DialogResult Resultado = MessageBox.Show("¿Desea imprimir la Orden de pedido?", this.Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (Resultado == DialogResult.Yes)
                {
                    Thread hilo = new Thread(AbrirFormReporteDetalleFactura);
                    hilo.SetApartmentState(System.Threading.ApartmentState.STA);
                    hilo.Start();
                }
                
                this.Close();
            }   
        }

        private void AbrirFormReporteDetalleFactura()
        {
            OrdenPedidoReporteForm abrir = new OrdenPedidoReporteForm(pedido);
            abrir.ShowDialog();

        }

        private int Validaciones_guardar()
        {
            foreach (Control objeto in this.GroupBox.Controls)
            {
                if (objeto is ComboBox && ((ComboBox)objeto).SelectedItem == null)
                {
                    MessageBox.Show("Debe completar los campos vacios.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return -1;
                }
            }
            if (Ventas_DataGridView.RowCount <= 0)
            {
                MessageBox.Show("Debe agregar al menos un articulo a la Orden de Pedido.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return -1;
            }
            return 1;
        }

        private void EliminarArticuloButton_Click(object sender, EventArgs e)
        {
            if (this.Ventas_DataGridView.Rows.Count == 0)
            {
                return;
            }

            if (this.Ventas_DataGridView.CurrentCell == null)
            {
                MessageBox.Show("Debe seleccionar una fila.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            SubTotalTextBox.Text = (Convert.ToDecimal(SubTotalTextBox.Text) - Convert.ToDecimal(Ventas_DataGridView.CurrentRow.Cells["Importe"].Value)).ToString();
            TotalTextBox.Text = (Convert.ToDecimal(SubTotalTextBox.Text) - Convert.ToDecimal(DescuentoTextBox.Text)).ToString();
            ComisionTextBox.Text = ((double)((Convert.ToDecimal(SubTotalTextBox.Text) - Convert.ToDecimal(DescuentoTextBox.Text)) * vendedor.comision / 100)).ToString();

            Ventas_DataGridView.Rows.Remove(Ventas_DataGridView.CurrentRow);
        }

        private void DescuentoTextBox_TextChanged(object sender, EventArgs e)
        {
            decimal total = (Convert.ToDecimal(SubTotalTextBox.Text) - Convert.ToDecimal(DescuentoTextBox.Text));
            if (total >= 0)
            {
                TotalTextBox.Text = (Convert.ToDecimal(SubTotalTextBox.Text) - Convert.ToDecimal(DescuentoTextBox.Text)).ToString();
            }
            else
            {
                MessageBox.Show("Error, El descuento no puede ser mayor al subtotal.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                DescuentoTextBox.Text = String.Format("{0:#,###0.00}", 0);
                return;
            }
        }

        private void DescuentoTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar)
               && !char.IsDigit(e.KeyChar)
               && e.KeyChar != '.')
            {
                e.Handled = true;
            }
        }

        private void ClienteComboBox_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (ClienteComboBox.SelectedValue.ToString() != "")
            {
                cliente = cliente.getById(Convert.ToInt32(ClienteComboBox.SelectedValue));
            }
        }

        private void vendedorComboBox_SelectionChangeCommitted(object sender, EventArgs e)
        {
            vendedor = vendedor.getById(Convert.ToInt32(vendedorComboBox.SelectedValue.ToString()));
        }

        private void NombreTextBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void PrecioTextBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void CantidadTextBox_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
