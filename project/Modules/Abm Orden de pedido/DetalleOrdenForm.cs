﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using Mayorista.Libs;
using Mayorista.Models;

namespace Mayorista.Abm_Orden_de_pedido
{
    public partial class DetalleOrdenForm : Form
    {
        public DetalleOrdenForm(string id)
        {
            InitializeComponent();

            this.id = Convert.ToInt32(id);
        }

        int id;

        private void DetalleCompraForm_Load(object sender, EventArgs e)
        {
            CargadorDeDatos.CargarClienteComboBox(ClienteComboBox, this.Text);
            CargadorDeDatos.CargarVendedoresComboBox(VendedorComboBox, this.Text);
            CargadorDeDatos.CargarFormaPagoComboBox(FormaPagoComboBox, this.Text);
            
            OrdenPedido pedido = new OrdenPedido();
            pedido = pedido.getById(id);

            FactIdTextBox.Text = id.ToString();
            ClienteComboBox.SelectedValue = pedido.cliente.id;
            VendedorComboBox.SelectedValue = pedido.vendedor.id;
            FormaPagoComboBox.SelectedValue = pedido.forma_de_pago.id;
            FechaDateTimePicker.Value = Convert.ToDateTime(pedido.fecha);
            DescuentoTextBox.Text = pedido.descuento.ToString();
            TotalTextBox.Text = pedido.total.ToString();
            totalComisionTextBox.Text = pedido.total_comision.ToString();
            SubTotalTextBox.Text = (Convert.ToDecimal(TotalTextBox.Text) + Convert.ToDecimal(DescuentoTextBox.Text)).ToString();
           
            foreach (ItemPedido item in pedido.items)
            {
                Compras_DataGridView.Rows.Add(item.producto.id.ToString("D10"), item.producto.nombre, item.precio, item.cantidad, item.precio * item.cantidad);
            }
        }

        DataTable dt;
    }
}
