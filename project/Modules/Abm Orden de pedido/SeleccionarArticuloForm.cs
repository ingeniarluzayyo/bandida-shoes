﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using Mayorista.Libs;
using Mayorista.Models;

namespace Mayorista.Abm_Orden_de_pedido
{
    public partial class SeleccionarArticuloForm : Form
    {
        public SeleccionarArticuloForm(bool update = false)
        {
            InitializeComponent();
            this.update = update;
        }

        bool update;
        Producto producto;
        IList<Producto> listaProductos;

        private void SeleccionarArticuloForm_Load(object sender, EventArgs e)
        {
            CargadorDeDatos.CargarFamiliasComboBox(FamiliaComboBox, this.Text);

            producto = new Producto();
            listaProductos = producto.GetAll("1");
            Productos_DataGridView.DataSource = listaProductos;

            if (Productos_DataGridView.DataSource != null)
            {
                producto.customColumnsSelect(this.Productos_DataGridView,false);

                this.CodigoTextBox.Select();
            }
        }

        private void Filtrar()
        {
            this.Productos_DataGridView.DataSource = producto.DoSearch(listaProductos, this.CodigoTextBox.Text,this.NombreTextBox.Text, this.FamiliaComboBox.Text);
        }

        private void CancelarButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void CodigoTextBox_TextChanged(object sender, EventArgs e)
        {
            Filtrar();
        }

        private void DescripcionTextBox_TextChanged(object sender, EventArgs e)
        {
            Filtrar();
        }

        private void FamiliaComboBox_SelectionChangeCommitted(object sender, EventArgs e)
        {
            Filtrar();
        }

        private void LimpiarButton_Click(object sender, EventArgs e)
        {
            foreach (Control objeto in this.GroupBox.Controls)
            {
                if (objeto is TextBox)
                {
                    ((TextBox)objeto).Clear();
                }
                if (objeto is ComboBox)
                {
                    ((ComboBox)objeto).SelectedItem = null;
                }
            }

            Productos_DataGridView.DataSource = listaProductos;
        }

        private void EliminarArticuloButton_Click(object sender, EventArgs e)
        {
            if (Productos_DataGridView.RowCount > 0)
            {
                if (update)
                {
                    ModificarPedidoForm.producto.id = Int32.Parse(Productos_DataGridView.CurrentRow.Cells["id"].Value.ToString());
                    ModificarPedidoForm.producto.nombre = Productos_DataGridView.CurrentRow.Cells["nombre"].Value.ToString();
                    ModificarPedidoForm.producto.precio_compra = Convert.ToDecimal(Productos_DataGridView.CurrentRow.Cells["precio_compra"].Value);
                    ModificarPedidoForm.producto.stock = Convert.ToInt32(Productos_DataGridView.CurrentRow.Cells["stock"].Value);
                }
                else
                {
                    AltaPedidoForm.producto.id = Int32.Parse(Productos_DataGridView.CurrentRow.Cells["id"].Value.ToString());
                    AltaPedidoForm.producto.nombre = Productos_DataGridView.CurrentRow.Cells["nombre"].Value.ToString();
                    AltaPedidoForm.producto.precio_compra = Convert.ToDecimal(Productos_DataGridView.CurrentRow.Cells["precio_compra"].Value);
                    AltaPedidoForm.producto.stock = Convert.ToInt32(Productos_DataGridView.CurrentRow.Cells["stock"].Value);
                }

                DialogResult = DialogResult.OK;
                
            }
        }



    }
}
