﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using Mayorista.Libs;
using Mayorista.Models;

namespace Mayorista.Abm_Orden_de_pedido
{
    public partial class ModificarPedidoForm : Form
    {
        public ModificarPedidoForm(string id)
        {
            InitializeComponent();

            this.id = Convert.ToInt32(id);
        }

        public static Producto producto = new Producto();
        Cliente cliente = new Cliente();
        Vendedor vendedor = new Vendedor();
        FormaPago forma_de_pago = new FormaPago();
        int id;

        private void DetalleCompraForm_Load(object sender, EventArgs e)
        {
            CargadorDeDatos.CargarClienteComboBox(ClienteComboBox, this.Text);
            CargadorDeDatos.CargarVendedoresComboBox(VendedorComboBox, this.Text);
            CargadorDeDatos.CargarFormaPagoComboBox(FormaPagoComboBox, this.Text);
            
            OrdenPedido pedido = new OrdenPedido();
            pedido = pedido.getById(id);

            FactIdTextBox.Text = id.ToString();
            ClienteComboBox.SelectedValue = pedido.cliente.id;
            VendedorComboBox.SelectedValue = pedido.vendedor.id;
            FormaPagoComboBox.SelectedValue = pedido.forma_de_pago.id;
            FechaDateTimePicker.Value = Convert.ToDateTime(pedido.fecha);
            DescuentoTextBox.Text = pedido.descuento.ToString();
            TotalTextBox.Text = pedido.total.ToString();
            ComisionTextBox.Text = pedido.total_comision.ToString();
            SubTotalTextBox.Text = (Convert.ToDecimal(TotalTextBox.Text) + Convert.ToDecimal(DescuentoTextBox.Text)).ToString();


            cliente = cliente.getById(Convert.ToInt32(ClienteComboBox.SelectedValue));
            vendedor = vendedor.getById(Convert.ToInt32(VendedorComboBox.SelectedValue.ToString()));
            cliente = cliente.getById(Convert.ToInt32(ClienteComboBox.SelectedValue));


            foreach (ItemPedido item in pedido.items)
            {
                Ventas_DataGridView.Rows.Add(item.producto.id.ToString("D10"), item.producto.nombre, item.precio, item.cantidad, item.precio * item.cantidad);

                Producto producto = new Producto();
                producto = producto.getById(item.producto.id);

                if (producto.stock < item.cantidad)
                {
                    Ventas_DataGridView.Rows[(Ventas_DataGridView.Rows.Count - 1)].DefaultCellStyle.BackColor = Color.FromArgb(255, 67, 48);
                }
                

            }
        }

        private void AgregarArticuloButton_Click(object sender, EventArgs e)
        {
            if (ClienteComboBox.SelectedValue != null && VendedorComboBox.SelectedValue != null)
            {
                SeleccionarArticuloForm abrir = new SeleccionarArticuloForm(true);
                DialogResult Resultado = abrir.ShowDialog();
                if (Resultado == DialogResult.OK)
                {
                    producto = producto.getById(producto.id);
                    CodigoTextBox.Text = producto.id.ToString("D10");
                    decimal precio = producto.getPrecioVenta();
                    PrecioTextBox.Text = precio.ToString();
                    NombreTextBox.Text = producto.nombre;
                    CantidadTextBox.Text = "1";
                    CantidadTextBox.ReadOnly = false;
                    CantidadTextBox.Focus();
                }
            }
            else
            {
                MessageBox.Show("Debe seleccionar un cliente y un vendedor antes.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void EliminarArticuloButton_Click(object sender, EventArgs e)
        {
            if (this.Ventas_DataGridView.Rows.Count == 0)
            {
                return;
            }

            if (this.Ventas_DataGridView.CurrentCell == null)
            {
                MessageBox.Show("Debe seleccionar una fila.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            SubTotalTextBox.Text = (Convert.ToDecimal(SubTotalTextBox.Text) - Convert.ToDecimal(Ventas_DataGridView.CurrentRow.Cells["Importe"].Value)).ToString();
            TotalTextBox.Text = (Convert.ToDecimal(SubTotalTextBox.Text) - Convert.ToDecimal(DescuentoTextBox.Text)).ToString();
            ComisionTextBox.Text = ((double)((Convert.ToDecimal(SubTotalTextBox.Text) - Convert.ToDecimal(DescuentoTextBox.Text)) * vendedor.comision / 100)).ToString();

            Ventas_DataGridView.Rows.Remove(Ventas_DataGridView.CurrentRow);
        }

        private void GuardarButton_Click(object sender, EventArgs e)
        {
            if (Validaciones_guardar() == -1) return;
            Producto producto = new Producto();

            OrdenPedido pedido = new OrdenPedido();
            pedido = pedido.getById(id);

            pedido.cliente = cliente.getById(Convert.ToInt32(ClienteComboBox.SelectedValue.ToString()));
            pedido.vendedor = vendedor.getById(Convert.ToInt32(VendedorComboBox.SelectedValue.ToString()));
            pedido.forma_de_pago = forma_de_pago.getById(Convert.ToInt32(FormaPagoComboBox.SelectedValue.ToString()));
            pedido.fecha = FechaDateTimePicker.Value;
            pedido.descuento = Decimal.Parse(DescuentoTextBox.Text);
            pedido.total = Decimal.Parse(TotalTextBox.Text);
            pedido.total_comision = Decimal.Parse(ComisionTextBox.Text);

            //limpio todos los items
            foreach (ItemPedido item in pedido.items) {
                item.delete();
            }

            pedido.items.Clear();


            foreach (DataGridViewRow row in Ventas_DataGridView.Rows)
            {
                ItemPedido item = new ItemPedido();
                item.producto = producto.getById(Int32.Parse(row.Cells["Codigo"].Value.ToString()));
                item.cantidad = Convert.ToInt32(row.Cells["Cantidad"].Value);

                if (item.producto.stock < 0)
                {
                    MessageBox.Show("Ocurrio un error con el calculo de stock del producto " + item.producto.nombre + ".", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                item.precio = Convert.ToDecimal(row.Cells["PrecioUnitario"].Value);
                item.producto.precio_compra = item.precio;
                item.pedido = pedido;

                pedido.items.Add(item);
            }

            if (pedido.update() == true)
            {
                MessageBox.Show("Se ha modificado la Orden de Pedido correctamente.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Close();
            }   

        }

        private int Validaciones_guardar()
        {
            foreach (Control objeto in this.GroupBox.Controls)
            {
                if (objeto is ComboBox && ((ComboBox)objeto).SelectedItem == null)
                {
                    MessageBox.Show("Debe completar los campos vacios.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return -1;
                }
            }
            if (Ventas_DataGridView.RowCount <= 0)
            {
                MessageBox.Show("Debe agregar al menos un articulo a la Orden de Pedido.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return -1;
            }
            return 1;
        }

        private void ClienteComboBox_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (ClienteComboBox.SelectedValue.ToString() != "")
            {
                cliente = cliente.getById(Convert.ToInt32(ClienteComboBox.SelectedValue));
            }
        }

        private void AddArticuloButton_Click(object sender, EventArgs e)
        {
            if (!Validaciones()) { return; }

            Ventas_DataGridView.Rows.Add(producto.id.ToString("D10"), producto.nombre, Decimal.Parse(PrecioTextBox.Text), CantidadTextBox.Text, Decimal.Parse(PrecioTextBox.Text) * Convert.ToInt32(CantidadTextBox.Text));

            if (producto.stock < Convert.ToInt32(CantidadTextBox.Text))
            {
                Ventas_DataGridView.Rows[(Ventas_DataGridView.Rows.Count - 1)].DefaultCellStyle.BackColor = Color.FromArgb(255, 67, 48);
            }

            SubTotalTextBox.Text = (Convert.ToDecimal(SubTotalTextBox.Text) + Decimal.Parse(PrecioTextBox.Text) * Convert.ToInt32(CantidadTextBox.Text)).ToString();
            TotalTextBox.Text = (Convert.ToDecimal(SubTotalTextBox.Text) - Convert.ToDecimal(DescuentoTextBox.Text)).ToString();
            ComisionTextBox.Text = ((double)((Convert.ToDecimal(SubTotalTextBox.Text) - Convert.ToDecimal(DescuentoTextBox.Text)) * vendedor.comision / 100)).ToString();

            CodigoTextBox.Text = "Codigo";
            NombreTextBox.Text = "Nombre";
            PrecioTextBox.Text = "Precio";
            CantidadTextBox.Text = "Cantidad";
            CantidadTextBox.ReadOnly = true;

            this.CodigoTextBox.Focus();
        }

        private bool Validaciones()
        {
            if (this.NombreTextBox.Text == "Nombre" && this.CantidadTextBox.Text == "Cantidad")
            {
                return false;
            }

            int v;
            decimal x;
            if (!Int32.TryParse(this.CantidadTextBox.Text.Trim(), out v))
            {
                MessageBox.Show("Error: La cantidad solo puede ser numerica.");
                return false;
            }

            if (!Decimal.TryParse(PrecioTextBox.Text, out x))
            {
                MessageBox.Show("Error: El precio solo puede ser numerico.");
                return false;
            }

            foreach (DataGridViewRow row in Ventas_DataGridView.Rows)
            {
                if (producto.id == Convert.ToInt32(row.Cells["Codigo"].Value.ToString()))
                {
                    MessageBox.Show("Error: El producto ya esta agregado en la orden.");
                    return false;
                }
            }

            return true;
        }

        private void VendedorComboBox_SelectionChangeCommitted(object sender, EventArgs e)
        {
            vendedor = vendedor.getById(Convert.ToInt32(VendedorComboBox.SelectedValue.ToString()));
        }

    }
}
