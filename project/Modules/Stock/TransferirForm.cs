﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Zapateria.Libs;
using Zapateria.Models;

namespace Zapateria.Abm_Stock
{
    public partial class TransferirForm : Form
    {
        Stock stock;

        public TransferirForm(Stock stock)
        {
            InitializeComponent();
            this.stock = stock;
        }

        private void TransferirForm_Load(object sender, EventArgs e)
        {
            CodigoTextBox.Text = stock.producto.id.ToString();
            NombreTextBox.Text = stock.producto.nombre;
            SucursalTextBox.Text = stock.sucursal.nombre;
            ZonaTextBox.Text = stock.zona.nombre;
            TalleTextBox.Text = stock.talle.nombre;
            ColorTextBox.Text = stock.color.nombre;
            StockTextBox.Text = stock.stock.ToString();

            CargadorDeDatos.CargarSucursalesComboBox(SucursalComboBox, this.Text);
            CargadorDeDatos.CargarZonasComboBox(ZonaComboBox, this.Text);

            SucursalComboBox.SelectedValue = stock.sucursal.id;
            ZonaComboBox.SelectedValue = stock.zona.id;

            CantidadTrackBar.Maximum = stock.stock;
            CantidadTrackBar.Value = stock.stock;
            CantidadValue.Text = stock.stock.ToString();
        }

        private void TransferirButton_Click(object sender, EventArgs e)
        {
            int nuevaSucursal = 0;
            int nuevaZona = 0;
            int nuevaCantidad = CantidadTrackBar.Value;

            if (SucursalComboBox.SelectedItem != null)
            {
                nuevaSucursal = Int32.Parse(SucursalComboBox.SelectedValue.ToString());
            }

            if (ZonaComboBox.SelectedItem != null)
            {
                nuevaZona = Int32.Parse(ZonaComboBox.SelectedValue.ToString());
            }

            if (stock.sucursal.id == nuevaSucursal && stock.zona.id == nuevaZona)
            {
                MessageBox.Show("Se debe seleccionar una sucursal o zona distintas a las de origen.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            // Chequear si la nueva combinación producto/sucursal/zona/talle/color existe.
            IList<Stock> stocksExistentes = stock.Search(stock.producto.id, nuevaSucursal, nuevaZona, stock.talle.id, stock.color.id);

            if (stocksExistentes.Count == 0)
            {
                // Si no existe, insertar un nuevo registro con el stock indicado.
                CrearNuevoStock(nuevaSucursal, nuevaZona, nuevaCantidad);
            }
            else
            {
                // Si existe, sumar el stock indicado.
                //ActualizarNuevoStock(stocksExistente[0].id, nuevaCantidad);
                ActualizarNuevoStock(stocksExistentes[0], nuevaCantidad);
            }
        }

        private void CrearNuevoStock(int nuevaSucursal, int nuevaZona, int nuevaCantidad)
        {
            Sucursal sucursal = new Sucursal();
            Zona zona = new Zona();

            Stock nuevoStock = new Stock();

            nuevoStock.producto = stock.producto;
            nuevoStock.sucursal = sucursal.getById(nuevaSucursal);
            nuevoStock.zona = zona.getById(nuevaZona);
            nuevoStock.talle = stock.talle;
            nuevoStock.color = stock.color;
            nuevoStock.stock = nuevaCantidad;

            if (nuevoStock.save())
            {
                // A la combinación original, restar el stock transferido.
                ActualizarStockExistente(nuevaCantidad);
            }
        }

        //private void ActualizarNuevoStock(int idStockExistente, int nuevaCantidad)
        private void ActualizarNuevoStock(Stock stockExistente, int nuevaCantidad)
        {
            //Stock stockExistente = stock.getById(idStockExistente);

            stockExistente.stock += nuevaCantidad;

            if (stockExistente.update())
            {
                // A la combinación original, restar el stock transferido.
                ActualizarStockExistente(nuevaCantidad);
            }
        }

        private void ActualizarStockExistente(int nuevaCantidad)
        {
            stock.stock -= nuevaCantidad;

            if (stock.update())
            {
                MessageBox.Show("Se " + (nuevaCantidad != 1 ? "transfirieron" : "transfirió") + " " + nuevaCantidad + " " + (nuevaCantidad != 1 ? "unidades" : "unidad") + " correctamente.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                CancelarButton.PerformClick();
                DialogResult = DialogResult.OK;
            }
        }

        private void CantidadTrackBar_Scroll(object sender, EventArgs e)
        {
            CantidadValue.Text = CantidadTrackBar.Value.ToString();
        }
    }
}
