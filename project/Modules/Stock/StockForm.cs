﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Zapateria.Libs;
using Zapateria.Models;

namespace Zapateria.Abm_Stock
{
    public partial class StockForm : Form
    {
        int producto_id;
        IList<Stock> stocks;

        public StockForm(int id)
        {
            InitializeComponent();
            producto_id = id;
        }

        private void StockForm_Load(object sender, EventArgs e)
        {
            CargadorDeDatos.CargarSucursalesComboBox(SucursalComboBox, this.Text);
            CargadorDeDatos.CargarColoresComboBox(ColorComboBox, this.Text);
            CargadorDeDatos.CargarTallesComboBox(TalleComboBox, this.Text);

            SucursalComboBox.SelectedValue = Main.getUsuario().sucursal_default.id;

            this.BuscarButton.PerformClick();
        }

        private void TransferirButton_Click(object sender, EventArgs e)
        {
            if (this.Stocks_DataGridView.CurrentCell == null)
            {
                MessageBox.Show("Debe seleccionar una fila.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            Stock stock = new Stock();
            stock = stock.getById(Convert.ToInt32(Stocks_DataGridView.CurrentRow.Cells["id"].Value));
            TransferirForm abrir = new TransferirForm(stock);
            DialogResult Resultado = abrir.ShowDialog();

            if (Resultado == DialogResult.OK)
            {
                BuscarButton.PerformClick();
            }
        }

        private void BuscarButton_Click(object sender, EventArgs e)
        {
            Security Busqueda = new Security();
            if (!Busqueda.ValidarSqlInjection(this.GroupBox)) { return; }

            int sucursal_id = 0;
            int talle_id = 0;
            int color_id = 0;

            if (SucursalComboBox.SelectedItem != null)
            {
                sucursal_id = Convert.ToInt32(SucursalComboBox.SelectedValue.ToString());
            }

            if (TalleComboBox.SelectedItem != null)
            {
                talle_id = Convert.ToInt32(TalleComboBox.SelectedValue.ToString());
            }

            if (ColorComboBox.SelectedItem != null)
            {
                color_id = Convert.ToInt32(ColorComboBox.SelectedValue.ToString());
            }

            Stock stock = new Stock();
            stocks = stock.Search(producto_id, sucursal_id, 0, talle_id, color_id);

            Stocks_DataGridView.DataSource = stocks;
            stock.customColumns(Stocks_DataGridView);
            this.Stocks_DataGridView.Focus();
        }

        private void Stocks_DataGridView_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            DataGridViewColumn column = Stocks_DataGridView.Columns[e.ColumnIndex];
            if (column.DataPropertyName.Contains("."))
            {
                object data = Stocks_DataGridView.Rows[e.RowIndex].DataBoundItem;
                string[] properties = column.DataPropertyName.Split('.');
                for (int i = 0; i < properties.Length && data != null; i++)
                    data = data.GetType().GetProperty(properties[i]).GetValue(data);
                Stocks_DataGridView.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = data;
            }
        }

        private void LimpiarButton_Click(object sender, EventArgs e)
        {
            foreach (Control objeto in this.GroupBox.Controls)
            {
                if (objeto is TextBox)
                {
                    ((TextBox)objeto).Clear();
                }
                if (objeto is ComboBox)
                {
                    ((ComboBox)objeto).SelectedItem = null;
                }
            }

            ModDataGridView.limpiarDataGridView(Stocks_DataGridView, "Ver Detalle");
        }
    }
}
