﻿namespace Zapateria.Abm_Stock
{
    partial class TransferirForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TransferirForm));
            this.CancelarButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.NombreTextBox = new System.Windows.Forms.TextBox();
            this.SucursalTextBox = new System.Windows.Forms.TextBox();
            this.ZonaTextBox = new System.Windows.Forms.TextBox();
            this.TalleTextBox = new System.Windows.Forms.TextBox();
            this.ColorTextBox = new System.Windows.Forms.TextBox();
            this.StockTextBox = new System.Windows.Forms.TextBox();
            this.CodigoTextBox = new System.Windows.Forms.TextBox();
            this.TransferirAGroupBox = new System.Windows.Forms.GroupBox();
            this.ZonaComboBox = new System.Windows.Forms.ComboBox();
            this.SucursalComboBox = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.TransferirButton = new System.Windows.Forms.Button();
            this.CantidadTrackBar = new System.Windows.Forms.TrackBar();
            this.label11 = new System.Windows.Forms.Label();
            this.CantidadValue = new System.Windows.Forms.Label();
            this.TransferirAGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CantidadTrackBar)).BeginInit();
            this.SuspendLayout();
            // 
            // CancelarButton
            // 
            this.CancelarButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.CancelarButton.AutoSize = true;
            this.CancelarButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.CancelarButton.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CancelarButton.Location = new System.Drawing.Point(12, 306);
            this.CancelarButton.Name = "CancelarButton";
            this.CancelarButton.Size = new System.Drawing.Size(75, 25);
            this.CancelarButton.TabIndex = 15;
            this.CancelarButton.Text = "Cancelar";
            this.CancelarButton.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.label1.Location = new System.Drawing.Point(19, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Código";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.label2.Location = new System.Drawing.Point(268, 29);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 16);
            this.label2.TabIndex = 2;
            this.label2.Text = "Nombre";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.label3.Location = new System.Drawing.Point(11, 102);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(60, 16);
            this.label3.TabIndex = 4;
            this.label3.Text = "Sucursal";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.label4.Location = new System.Drawing.Point(286, 102);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(39, 16);
            this.label4.TabIndex = 6;
            this.label4.Text = "Zona";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.label5.Location = new System.Drawing.Point(32, 65);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(39, 16);
            this.label5.TabIndex = 8;
            this.label5.Text = "Talle";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.label6.Location = new System.Drawing.Point(285, 65);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(40, 16);
            this.label6.TabIndex = 10;
            this.label6.Text = "Color";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.label7.Location = new System.Drawing.Point(29, 138);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(42, 16);
            this.label7.TabIndex = 12;
            this.label7.Text = "Stock";
            // 
            // NombreTextBox
            // 
            this.NombreTextBox.Enabled = false;
            this.NombreTextBox.Location = new System.Drawing.Point(331, 28);
            this.NombreTextBox.Name = "NombreTextBox";
            this.NombreTextBox.Size = new System.Drawing.Size(160, 20);
            this.NombreTextBox.TabIndex = 3;
            // 
            // SucursalTextBox
            // 
            this.SucursalTextBox.Enabled = false;
            this.SucursalTextBox.Location = new System.Drawing.Point(77, 101);
            this.SucursalTextBox.Name = "SucursalTextBox";
            this.SucursalTextBox.Size = new System.Drawing.Size(160, 20);
            this.SucursalTextBox.TabIndex = 5;
            // 
            // ZonaTextBox
            // 
            this.ZonaTextBox.Enabled = false;
            this.ZonaTextBox.Location = new System.Drawing.Point(331, 101);
            this.ZonaTextBox.Name = "ZonaTextBox";
            this.ZonaTextBox.Size = new System.Drawing.Size(160, 20);
            this.ZonaTextBox.TabIndex = 7;
            // 
            // TalleTextBox
            // 
            this.TalleTextBox.Enabled = false;
            this.TalleTextBox.Location = new System.Drawing.Point(77, 64);
            this.TalleTextBox.Name = "TalleTextBox";
            this.TalleTextBox.Size = new System.Drawing.Size(160, 20);
            this.TalleTextBox.TabIndex = 9;
            // 
            // ColorTextBox
            // 
            this.ColorTextBox.Enabled = false;
            this.ColorTextBox.Location = new System.Drawing.Point(331, 64);
            this.ColorTextBox.Name = "ColorTextBox";
            this.ColorTextBox.Size = new System.Drawing.Size(160, 20);
            this.ColorTextBox.TabIndex = 11;
            // 
            // StockTextBox
            // 
            this.StockTextBox.Enabled = false;
            this.StockTextBox.Location = new System.Drawing.Point(77, 137);
            this.StockTextBox.Name = "StockTextBox";
            this.StockTextBox.Size = new System.Drawing.Size(160, 20);
            this.StockTextBox.TabIndex = 13;
            // 
            // CodigoTextBox
            // 
            this.CodigoTextBox.Enabled = false;
            this.CodigoTextBox.Location = new System.Drawing.Point(77, 28);
            this.CodigoTextBox.Name = "CodigoTextBox";
            this.CodigoTextBox.Size = new System.Drawing.Size(160, 20);
            this.CodigoTextBox.TabIndex = 1;
            // 
            // TransferirAGroupBox
            // 
            this.TransferirAGroupBox.Controls.Add(this.CantidadValue);
            this.TransferirAGroupBox.Controls.Add(this.label11);
            this.TransferirAGroupBox.Controls.Add(this.CantidadTrackBar);
            this.TransferirAGroupBox.Controls.Add(this.ZonaComboBox);
            this.TransferirAGroupBox.Controls.Add(this.SucursalComboBox);
            this.TransferirAGroupBox.Controls.Add(this.label8);
            this.TransferirAGroupBox.Controls.Add(this.label9);
            this.TransferirAGroupBox.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.TransferirAGroupBox.Location = new System.Drawing.Point(12, 183);
            this.TransferirAGroupBox.Name = "TransferirAGroupBox";
            this.TransferirAGroupBox.Size = new System.Drawing.Size(479, 111);
            this.TransferirAGroupBox.TabIndex = 14;
            this.TransferirAGroupBox.TabStop = false;
            this.TransferirAGroupBox.Text = "Transferir a";
            // 
            // ZonaComboBox
            // 
            this.ZonaComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ZonaComboBox.FormattingEnabled = true;
            this.ZonaComboBox.Location = new System.Drawing.Point(303, 23);
            this.ZonaComboBox.Name = "ZonaComboBox";
            this.ZonaComboBox.Size = new System.Drawing.Size(160, 23);
            this.ZonaComboBox.TabIndex = 3;
            // 
            // SucursalComboBox
            // 
            this.SucursalComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.SucursalComboBox.FormattingEnabled = true;
            this.SucursalComboBox.Location = new System.Drawing.Point(82, 24);
            this.SucursalComboBox.Name = "SucursalComboBox";
            this.SucursalComboBox.Size = new System.Drawing.Size(160, 23);
            this.SucursalComboBox.TabIndex = 1;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.label8.Location = new System.Drawing.Point(258, 26);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(39, 16);
            this.label8.TabIndex = 2;
            this.label8.Text = "Zona";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.label9.Location = new System.Drawing.Point(16, 26);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(60, 16);
            this.label9.TabIndex = 0;
            this.label9.Text = "Sucursal";
            // 
            // TransferirButton
            // 
            this.TransferirButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.TransferirButton.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TransferirButton.Location = new System.Drawing.Point(406, 308);
            this.TransferirButton.Name = "TransferirButton";
            this.TransferirButton.Size = new System.Drawing.Size(86, 23);
            this.TransferirButton.TabIndex = 16;
            this.TransferirButton.Text = "Transferir";
            this.TransferirButton.UseVisualStyleBackColor = true;
            this.TransferirButton.Click += new System.EventHandler(this.TransferirButton_Click);
            // 
            // CantidadTrackBar
            // 
            this.CantidadTrackBar.LargeChange = 10;
            this.CantidadTrackBar.Location = new System.Drawing.Point(82, 63);
            this.CantidadTrackBar.Maximum = 100;
            this.CantidadTrackBar.Minimum = 1;
            this.CantidadTrackBar.Name = "CantidadTrackBar";
            this.CantidadTrackBar.Size = new System.Drawing.Size(346, 45);
            this.CantidadTrackBar.TabIndex = 18;
            this.CantidadTrackBar.TickStyle = System.Windows.Forms.TickStyle.TopLeft;
            this.CantidadTrackBar.Value = 100;
            this.CantidadTrackBar.Scroll += new System.EventHandler(this.CantidadTrackBar_Scroll);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.label11.Location = new System.Drawing.Point(13, 63);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(62, 16);
            this.label11.TabIndex = 19;
            this.label11.Text = "Cantidad";
            // 
            // CantidadValue
            // 
            this.CantidadValue.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.CantidadValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CantidadValue.Location = new System.Drawing.Point(431, 63);
            this.CantidadValue.Name = "CantidadValue";
            this.CantidadValue.Size = new System.Drawing.Size(32, 16);
            this.CantidadValue.TabIndex = 20;
            this.CantidadValue.Text = "100";
            this.CantidadValue.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // TransferirForm
            // 
            this.AcceptButton = this.TransferirButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.CancelarButton;
            this.ClientSize = new System.Drawing.Size(504, 343);
            this.Controls.Add(this.TransferirButton);
            this.Controls.Add(this.TransferirAGroupBox);
            this.Controls.Add(this.CodigoTextBox);
            this.Controls.Add(this.StockTextBox);
            this.Controls.Add(this.ColorTextBox);
            this.Controls.Add(this.TalleTextBox);
            this.Controls.Add(this.ZonaTextBox);
            this.Controls.Add(this.SucursalTextBox);
            this.Controls.Add(this.NombreTextBox);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.CancelarButton);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "TransferirForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Transferir stock a sucursal y/o zona";
            this.Load += new System.EventHandler(this.TransferirForm_Load);
            this.TransferirAGroupBox.ResumeLayout(false);
            this.TransferirAGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CantidadTrackBar)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button CancelarButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox NombreTextBox;
        private System.Windows.Forms.TextBox SucursalTextBox;
        private System.Windows.Forms.TextBox ZonaTextBox;
        private System.Windows.Forms.TextBox TalleTextBox;
        private System.Windows.Forms.TextBox ColorTextBox;
        private System.Windows.Forms.TextBox StockTextBox;
        private System.Windows.Forms.TextBox CodigoTextBox;
        private System.Windows.Forms.GroupBox TransferirAGroupBox;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox ZonaComboBox;
        private System.Windows.Forms.ComboBox SucursalComboBox;
        private System.Windows.Forms.Button TransferirButton;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TrackBar CantidadTrackBar;
        private System.Windows.Forms.Label CantidadValue;
    }
}