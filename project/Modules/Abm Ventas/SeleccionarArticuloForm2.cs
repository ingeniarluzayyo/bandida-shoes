﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using Zapateria.Libs;
using Zapateria.Models;

namespace Zapateria.Abm_Ventas
{
    public partial class SeleccionarArticuloForm2 : Form
    {
        Sucursal sucursal;

        public SeleccionarArticuloForm2(Sucursal sucursal)
        {
            InitializeComponent();
            this.sucursal = sucursal;
        }

        Producto producto;
        IList<Producto> listaProductos;

        private void SeleccionarArticuloForm_Load(object sender, EventArgs e)
        {
            CargadorDeDatos.CargarFamiliasComboBox(FamiliaComboBox, this.Text);

            Stock stock = new Stock();
            DataTable stockEnSucursal = stock.GetStockInSucursal(sucursal.id);
            this.Productos_DataGridView.DataSource = stockEnSucursal;

            if (stockEnSucursal.Rows.Count > 0)
                stock.customColumns(Productos_DataGridView);

            this.NombreTextBox.Select();
        }

        private void Filtrar()
        {
            //this.Productos_DataGridView.DataSource = producto.DoSearch(listaProductos, this.CodigoTextBox.Text,this.NombreTextBox.Text, this.FamiliaComboBox.Text);
            ((DataTable)Productos_DataGridView.DataSource).DefaultView.RowFilter = FiltroCambios();
        }

        private string FiltroCambios()
        {
            string consulta = "";

            if (this.CodigoTextBox.Text != "")
            {
                consulta += "Codigo LIKE '%" + this.CodigoTextBox.Text + "%'";
            }

            if (this.NombreTextBox.Text != "")
            {
                if (consulta != "") { consulta += " AND "; }
                consulta += "Producto LIKE '%" + this.NombreTextBox.Text + "%'";
            }

            if (this.FamiliaComboBox.SelectedValue != null)
            {
                if (consulta != "") { consulta += " AND "; }
                consulta += "Familia = '" + this.FamiliaComboBox.Text + "'";
            }
            return consulta;
        }

        private void CancelarButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void CodigoTextBox_TextChanged(object sender, EventArgs e)
        {
            Filtrar();
        }

        private void DescripcionTextBox_TextChanged(object sender, EventArgs e)
        {
            Filtrar();
        }

        private void FamiliaComboBox_SelectionChangeCommitted(object sender, EventArgs e)
        {
            Filtrar();
        }

        private void LimpiarButton_Click(object sender, EventArgs e)
        {
            foreach (Control objeto in this.GroupBox.Controls)
            {
                if (objeto is TextBox)
                {
                    ((TextBox)objeto).Clear();
                }
                if (objeto is ComboBox)
                {
                    ((ComboBox)objeto).SelectedItem = null;
                }
            }

            producto = new Producto();
            this.Productos_DataGridView.DataSource = producto.SearchVentas();
        }

        private void EliminarArticuloButton_Click(object sender, EventArgs e)
        {
            if (Productos_DataGridView.RowCount > 0)
            {
                Stock stock = new Stock();
                stock = stock.getById(Convert.ToInt32(Productos_DataGridView.CurrentRow.Cells["id"].Value));

                this.SeleccionadosDataGridView.Rows.Add(stock.producto.id, stock.producto.nombre, stock.sucursal.nombre, stock.zona.nombre, stock.talle.nombre, stock.color.nombre, stock.stock);

                AltaVentaForm.lista_stocks.Add(stock);
            }
        }

        private void AgregarButton_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }
    }
}
