﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using Zapateria.Libs;
using System.Threading;
using Zapateria.Models;
using Zapateria.Modules.Abm_Ventas;

namespace Zapateria.Abm_Ventas
{
    public partial class VentaForm : Form
    {
        public VentaForm()
        {
            InitializeComponent();
        }

        IList<FacturaVenta> facturas;

        private void BuscarButton_Click(object sender, EventArgs e)
        {
            Security Busqueda = new Security();
            if (!Busqueda.ValidarSqlInjection(this.GroupBox)) { return; }
          
            FacturaVenta venta = new FacturaVenta();

            int venta_id = 0;
            int cliente_id = 0;
            int vendedor_id = 0;
            int sucursal_id = 0;
            string desde = "";
            string hasta = "";

            if (NumeroTextBox.TextLength > 0)
            {
                venta_id = Convert.ToInt32(NumeroTextBox.Text);
            }

            if (ClienteComboBox.SelectedItem != null)
            {
                cliente_id = Convert.ToInt32(ClienteComboBox.SelectedValue.ToString());
            }

            if (vendedorComboBox.SelectedItem != null)
            {
                vendedor_id = Convert.ToInt32(vendedorComboBox.SelectedValue.ToString());
            }

            if (SucursalComboBox.SelectedItem != null)
            {
                sucursal_id = Convert.ToInt32(SucursalComboBox.SelectedValue.ToString());
            }

            if (DesdeDateTimePicker.Text != " ")
            {
                desde = DesdeDateTimePicker.Value.ToShortDateString();
            }

            if (HastaDateTimePicker.Text != " ")
            {
                hasta = HastaDateTimePicker.Value.ToShortDateString();
            }

            facturas = venta.Search(AnuladoCheckBox.Checked, venta_id, cliente_id, vendedor_id, desde, hasta, sucursal_id);

            Decimal total = 0;

            if (facturas != null)
            {
                foreach (FacturaVenta fact in facturas)
                {
                    total = total + fact.total;
                }

                this.TotalLabel.Text = string.Format("{0:C}", total);

            }
            else {
                this.TotalLabel.Text = "$0";
            }

            Ventas_DataGridView.DataSource = facturas;
       
            if (Ventas_DataGridView.DataSource == null)
            {
                ModDataGridView.limpiarDataGridView(Ventas_DataGridView, "Ver Detalle");
                MessageBox.Show("No se encontraron resultados que coincidan con la busqueda.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            ModDataGridView.agregarBoton(Ventas_DataGridView, "Ver Detalle");            
            venta.customColumns(Ventas_DataGridView);
            this.Ventas_DataGridView.Focus();
        }        

        private void LimpiarButton_Click(object sender, EventArgs e)
        {
            foreach (Control objeto in this.GroupBox.Controls)
            {
                if (objeto is TextBox)
                {
                    ((TextBox)objeto).Clear();
                }
                if (objeto is ComboBox)
                {
                    ((ComboBox)objeto).SelectedItem = null;
                }
            }

            CargadorDeDatos.CargarClienteComboBox(ClienteComboBox, this.Text);

            this.TotalLabel.Text = "$0";
            ModDataGridView.limpiarDataGridView(Ventas_DataGridView, "Ver Detalle");
            this.DesdeDateTimePicker.Value = DateTime.Today;
            this.HastaDateTimePicker.Value = DateTime.Today;
        }

        private void CancelarButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void EliminarButton_Click(object sender, EventArgs e)
        {
            if (this.Ventas_DataGridView.Rows.Count == 0)
            {
                return;
            }

            if (this.Ventas_DataGridView.CurrentCell == null)
            {
                MessageBox.Show("Debe seleccionar una fila.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            if (Convert.ToInt32(Ventas_DataGridView.CurrentRow.Cells["anulada"].Value) == 1)
            {
                MessageBox.Show("Error,La factura ya esta anulada.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            string FacturaAnulada = Ventas_DataGridView.CurrentRow.Cells["id"].Value.ToString();

            DialogResult Resultado = MessageBox.Show("¿Esta seguro que desea anular la Factura Nº '" + FacturaAnulada + "'?", this.Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (Resultado == DialogResult.Yes)
            {
                FacturaVenta fact = new FacturaVenta();
                ItemVenta iv = new ItemVenta();
                fact = fact.getById(Convert.ToInt32(FacturaAnulada),true);
                fact.anulada = true;

                if(fact.update())
                {
                    fact = fact.getById(Convert.ToInt32(FacturaAnulada));
                    //Actualizo stock
                    foreach (ItemVenta item in fact.items)
                    {
                        if (item.producto.unidad)
                        {
                            item.producto.stock = item.producto.stock + item.cantidad;
                        }
                        else
                        {
                            decimal multi = Convert.ToDecimal(item.formato) * item.cantidad;

                            item.producto.stock = item.producto.stock + multi;
                        }

                        item.producto.update();
                    }

                    facturas.RemoveAt(Ventas_DataGridView.CurrentRow.Index);
                    ModDataGridView.limpiarDataGridView(Ventas_DataGridView, "Ver Detalle");
                    Ventas_DataGridView.DataSource = facturas;
                    ModDataGridView.agregarBoton(Ventas_DataGridView, "Ver Detalle");
                    fact.customColumns(Ventas_DataGridView);
                    this.Ventas_DataGridView.Focus();
                    
                    MessageBox.Show("Se ha anulado la Factura Nº '" + FacturaAnulada + "' correctamente.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                
            }
        }

        private void AgregarButton_Click(object sender, EventArgs e)
        {
            AltaVentaForm abrir = new AltaVentaForm();
            DialogResult Resultado = abrir.ShowDialog();
            if (Resultado == DialogResult.OK)
            {
                //ModDataGridView.limpiarDataGridView(Ventas_DataGridView, "Ver Detalle");
                this.LimpiarButton.PerformClick();
                this.BuscarButton.PerformClick();
            }
        }

        private void CompraForm_Load(object sender, EventArgs e)
        {
            CargadorDeDatos.CargarClienteComboBox(ClienteComboBox, this.Text);
            CargadorDeDatos.CargarVendedoresComboBox(vendedorComboBox, this.Text);
            CargadorDeDatos.CargarSucursalesComboBox(SucursalComboBox, this.Text);

            SucursalComboBox.SelectedValue = Main.getUsuario().sucursal_default.id;

            /*this.DesdeDateTimePicker.Format = DateTimePickerFormat.Custom;
            this.DesdeDateTimePicker.CustomFormat = " ";
            this.HastaDateTimePicker.Format = DateTimePickerFormat.Custom;
            this.HastaDateTimePicker.CustomFormat = " ";*/
            this.BuscarButton.PerformClick();
        }

        private void Compras_DataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == this.Ventas_DataGridView.Columns["Ver Detalle"].Index && e.RowIndex >= 0)
            {
                string id = Ventas_DataGridView.CurrentRow.Cells["id"].Value.ToString();

                DetalleVentaForm abrir = new DetalleVentaForm(id);
                abrir.ShowDialog();
            }
        }

        private void NumeroTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar)
               && !char.IsDigit(e.KeyChar)
               )
            {
                e.Handled = true;
            }
        }

        DateTime desde;
        DateTime hasta;        

        private void ImprimirEtiquetaButton_Click(object sender, EventArgs e)
        {
            if (DesdeDateTimePicker.Text == " " && HastaDateTimePicker.Text == " ")
            {
                MessageBox.Show("Debe seleccionar un rango de fechas para imprimir el listado.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            BuscarButton.PerformClick();

            desde = DesdeDateTimePicker.Value;
            hasta = HastaDateTimePicker.Value;     

            Thread hilo = new Thread(AbrirFormReporteCajaDiaria);
            hilo.SetApartmentState(System.Threading.ApartmentState.STA);
            hilo.Start();                     
        }

        private void AbrirFormReporteCajaDiaria()
        {
            IList<FacturaVenta> list = (IList<FacturaVenta>)Ventas_DataGridView.DataSource;
            decimal total = 0;

            foreach (FacturaVenta v in list)
            {
                total += v.total;
            }

            DataTable dt = DataUtil.ToDataTables((IList<FacturaVenta>)list);
            CajaDiariaReporte abrir = new CajaDiariaReporte(desde,hasta,total.ToString(),dt);
            abrir.ShowDialog();
        }

        private void DesdeDateTimePicker_CloseUp(object sender, EventArgs e)
        {
            this.DesdeDateTimePicker.Format = DateTimePickerFormat.Short;
        }

        private void HastaDateTimePicker_CloseUp(object sender, EventArgs e)
        {
            this.HastaDateTimePicker.Format = DateTimePickerFormat.Short;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (this.Ventas_DataGridView.Rows.Count == 0)
            {
                return;
            }
            if (this.Ventas_DataGridView.CurrentCell == null)
            {
                MessageBox.Show("Debe seleccionar una fila.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            Thread hilo = new Thread(AbrirFormReporteDetalleFactura);
            hilo.SetApartmentState(System.Threading.ApartmentState.STA);
            hilo.Start();
        }

        private void AbrirFormReporteDetalleFactura()
        {
            if (this.Ventas_DataGridView.Rows.Count == 0)
            {
                return;
            }
            if (this.Ventas_DataGridView.CurrentCell == null)
            {
                MessageBox.Show("Debe seleccionar una fila.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            int id = Convert.ToInt32(Ventas_DataGridView.CurrentRow.Cells["id"].Value.ToString());
            FacturaVenta fv = new FacturaVenta();
            fv = fv.getById(id);
            FacturaVentaReporteForm abrir = new FacturaVentaReporteForm(fv);
            abrir.ShowDialog();            
        }
    }
}
