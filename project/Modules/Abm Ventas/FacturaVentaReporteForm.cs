﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Zapateria.Models;
using Zapateria.Modules.Reportes;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;

namespace Zapateria.Modules.Abm_Ventas
{
    public partial class FacturaVentaReporteForm : Form
    {
        public FacturaVentaReporteForm(FacturaVenta fv)
        {
            InitializeComponent();

            this.facturaVenta = fv;
            
        }

        Empresa empresa = new Empresa();      
        FacturaVenta facturaVenta;
        DataTable dt;

        private void crystalReportViewer1_Load(object sender, EventArgs e)
        {
            DetalleFacturaCrystalReport Repo = new DetalleFacturaCrystalReport();

            empresa = empresa.getById(1);

            //Variables
            TextObject EmpresaNombreRepo;
            EmpresaNombreRepo = (TextObject)Repo.ReportDefinition.ReportObjects["EmpresaNombreText"];
            EmpresaNombreRepo.Text = empresa.razon_social;
            EmpresaNombreRepo = (TextObject)Repo.ReportDefinition.ReportObjects["EmpresaNombreText2"];
            EmpresaNombreRepo.Text = empresa.razon_social;

            TextObject FechaRepo;
            FechaRepo = (TextObject)Repo.ReportDefinition.ReportObjects["FechaText"];
            FechaRepo.Text = facturaVenta.fecha.ToShortDateString();
            FechaRepo = (TextObject)Repo.ReportDefinition.ReportObjects["FechaText2"];
            FechaRepo.Text = facturaVenta.fecha.ToShortDateString();

            TextObject NumeroFacturaRepo;
            NumeroFacturaRepo = (TextObject)Repo.ReportDefinition.ReportObjects["NumeroFacturaText"];
            NumeroFacturaRepo.Text = facturaVenta.id.ToString("D10");
            NumeroFacturaRepo = (TextObject)Repo.ReportDefinition.ReportObjects["NumeroFacturaText2"];
            NumeroFacturaRepo.Text = facturaVenta.id.ToString("D10");

            TextObject RazonSocialEmpresaRepo;
            RazonSocialEmpresaRepo = (TextObject)Repo.ReportDefinition.ReportObjects["EmpresaText"];
            RazonSocialEmpresaRepo.Text = empresa.razon_social;
            RazonSocialEmpresaRepo = (TextObject)Repo.ReportDefinition.ReportObjects["EmpresaText2"];
            RazonSocialEmpresaRepo.Text = empresa.razon_social;

            TextObject CUITEmpresaRepo;
            CUITEmpresaRepo = (TextObject)Repo.ReportDefinition.ReportObjects["CUITEmpresaText"];
            CUITEmpresaRepo.Text = empresa.cuit;
            CUITEmpresaRepo = (TextObject)Repo.ReportDefinition.ReportObjects["CUITEmpresaText2"];
            CUITEmpresaRepo.Text = empresa.cuit;

            TextObject DomicilioEmpresaRepo;
            DomicilioEmpresaRepo = (TextObject)Repo.ReportDefinition.ReportObjects["DireccionEmpresaText"];
            DomicilioEmpresaRepo.Text = empresa.direccion;
            DomicilioEmpresaRepo = (TextObject)Repo.ReportDefinition.ReportObjects["DireccionEmpresaText2"];
            DomicilioEmpresaRepo.Text = empresa.direccion;

            TextObject TelefonoEmpresaRepo;
            TelefonoEmpresaRepo = (TextObject)Repo.ReportDefinition.ReportObjects["TelefonoEmpresaText"];
            TelefonoEmpresaRepo.Text = empresa.telefono;
            TelefonoEmpresaRepo = (TextObject)Repo.ReportDefinition.ReportObjects["TelefonoEmpresaText2"];
            TelefonoEmpresaRepo.Text = empresa.telefono;

            TextObject RazonSocialClienteRepo;
            RazonSocialClienteRepo = (TextObject)Repo.ReportDefinition.ReportObjects["RazonSocialClienteText"];
            //RazonSocialClienteRepo.Text = facturaVenta.cliente.cliente;
            RazonSocialClienteRepo = (TextObject)Repo.ReportDefinition.ReportObjects["RazonSocialClienteText2"];
            //RazonSocialClienteRepo.Text = facturaVenta.cliente.cliente;

            TextObject CUITClienteRepo;
            CUITClienteRepo = (TextObject)Repo.ReportDefinition.ReportObjects["CUITClienteText"];
            CUITClienteRepo.Text = facturaVenta.cliente.cuit;
            CUITClienteRepo = (TextObject)Repo.ReportDefinition.ReportObjects["CUITClienteText2"];
            CUITClienteRepo.Text = facturaVenta.cliente.cuit;

            TextObject DomicilioClienteRepo;
            DomicilioClienteRepo = (TextObject)Repo.ReportDefinition.ReportObjects["DireccionClienteText"];
            DomicilioClienteRepo.Text = facturaVenta.cliente.direccion;
            DomicilioClienteRepo = (TextObject)Repo.ReportDefinition.ReportObjects["DireccionClienteText2"];
            DomicilioClienteRepo.Text = facturaVenta.cliente.direccion;

            TextObject TelefonoClienteRepo;
            TelefonoClienteRepo = (TextObject)Repo.ReportDefinition.ReportObjects["TelefonoClienteText"];
            TelefonoClienteRepo.Text = facturaVenta.cliente.tel;
            TelefonoClienteRepo = (TextObject)Repo.ReportDefinition.ReportObjects["TelefonoClienteText2"];
            TelefonoClienteRepo.Text = facturaVenta.cliente.tel;

            
            TextObject SubTotalRepo;
            SubTotalRepo = (TextObject)Repo.ReportDefinition.ReportObjects["SubtotalText"];
            SubTotalRepo.Text = (facturaVenta.total + facturaVenta.descuento).ToString();
            SubTotalRepo = (TextObject)Repo.ReportDefinition.ReportObjects["SubtotalText2"];
            SubTotalRepo.Text = (facturaVenta.total + facturaVenta.descuento).ToString();

            TextObject DescuentoRepo;
            DescuentoRepo = (TextObject)Repo.ReportDefinition.ReportObjects["DescuentoText"];
            DescuentoRepo.Text = facturaVenta.descuento.ToString();
            DescuentoRepo = (TextObject)Repo.ReportDefinition.ReportObjects["DescuentoText2"];
            DescuentoRepo.Text = facturaVenta.descuento.ToString();

            TextObject TotalRepo;
            TotalRepo = (TextObject)Repo.ReportDefinition.ReportObjects["TotalText"];
            TotalRepo.Text = facturaVenta.total.ToString();
            TotalRepo = (TextObject)Repo.ReportDefinition.ReportObjects["TotalText2"];
            TotalRepo.Text = facturaVenta.total.ToString();

            DataTable dt = new DataTable();
            dt.Columns.Add("codigo");
            dt.Columns.Add("nombre");            
            dt.Columns.Add("precio");
            dt.Columns.Add("cantidad");
            dt.Columns.Add("importe");

            foreach (ItemVenta item in facturaVenta.items)
            {
               dt.Rows.Add(item.producto.id.ToString("D10"), item.producto.nombre, item.precio, item.cantidad, item.precio * item.cantidad);
            }
             

            Repo.SetDataSource(dt);

            crystalReportViewer1.ReportSource = Repo;
        } 
    }
}
