﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Globalization;
using Zapateria.Libs;
using Zapateria.Models;
using MySql.Data.MySqlClient;
using System.Threading;
using Zapateria.Modules.Abm_Ventas;
using Zapateria.Abm_Clientes;

namespace Zapateria.Abm_Ventas
{
    public partial class AltaVentaForm : Form
    {
        public AltaVentaForm()
        {
            InitializeComponent();
        }

        public static Producto producto = new Producto();
        public static FormatoVenta fv = new FormatoVenta();
        public static ProductoXFormatoVenta pfv = new ProductoXFormatoVenta();
        public static List<Stock> lista_stocks = new List<Stock>();

        FacturaVenta fact = new FacturaVenta();
        Cliente cliente = new Cliente();
        Vendedor vendedor = new Vendedor();
        FormaPago forma_de_pago = new FormaPago();
        string ultimo_precio_ingresado = null;

        private void CodigoTextBox_TextChanged(object sender, EventArgs e)
        {
            if (ClienteComboBox.SelectedValue != null && vendedorComboBox.SelectedValue != null)
            {
               if (CodigoTextBox.Text.Length >= 10)
               {
                    string Consulta = "SELECT p.nombre FROM productos as p WHERE p.id='" + CodigoTextBox.Text + "'";

                    List<MySqlParameter> param = new List<MySqlParameter>();

                    DataTable dt = BaseDeDatos.GetInstance.ExecuteCustomQuery(Consulta, param, this.Text);

                    if (dt != null)
                    {
                        if (dt.Rows.Count == 1)
                        {
                            NombreTextBox.Text = dt.Rows[0]["nombre"].ToString();

                            producto = producto.getById(Convert.ToInt32(CodigoTextBox.Text));

                            if (producto != null) {
                                PrecioTextBox.Text = producto.getPrecioVentaX3().ToString(); // ???
                            }

                            CantidadTextBox.Text = "1";
                            CantidadTextBox.ReadOnly = false;
                        }
                    }
                    else
                    {
                        NombreTextBox.Text = "Nombre";
                        CantidadTextBox.Text = "Cantidad";
                        CantidadTextBox.ReadOnly = true;
                    }
               }
            }
            else
            {
                if (this.CodigoTextBox.Text != "Codigo")
                {
                    MessageBox.Show("Debe seleccionar un cliente y un vendedor, para poder utilizar la carga rapida de productos.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.CodigoTextBox.Text = "Codigo";
                } 
            }
        }

        private void CodigoTextBox_Enter(object sender, EventArgs e)
        {
            if (CodigoTextBox.Text == "Codigo")
            {
                this.CodigoTextBox.Text = "000000";
            }
        }

        private void CodigoTextBox_Leave(object sender, EventArgs e)
        {
            if (CodigoTextBox.Text == "")
            {
                CodigoTextBox.Text = "Codigo";
            }
        }

        private void AltaCompraForm_Load(object sender, EventArgs e)
        {
            //NUMERO DE FACTURA
            string Consulta = "SELECT MAX(id) FROM factura_venta";
            List<MySqlParameter> param = new List<MySqlParameter>();
            DataTable ds = BaseDeDatos.GetInstance.ExecuteCustomQuery(Consulta, param, this.Text);

            foreach (DataRow row in ds.Rows)
            {
                if (Convert.ToString(row[0]) == "")
                {
                    row[0] = 0;
                }
                int NumeroFactura = (Convert.ToInt32(row[0]) + 1);
                this.FactIdTextBox.Text = Convert.ToString(NumeroFactura);
            }

            CargadorDeDatos.CargarClienteComboBox(ClienteComboBox, this.Text);
            CargadorDeDatos.CargarFormaPagoComboBox(FormaPagoComboBox, this.Text);
            CargadorDeDatos.CargarVendedoresComboBox(vendedorComboBox, this.Text);
            CargadorDeDatos.CargarSucursalesComboBox(SucursalComboBox, this.Text);
            TotalTextBox.Text = String.Format("{0:#,###0.00}", 0);
            SubTotalTextBox.Text = String.Format("{0:#,###0.00}", 0);
            DescuentoTextBox.Text = String.Format("{0:#,###0.00}", 0);

            SucursalComboBox.SelectedValue = Main.getUsuario().sucursal_default.id;
            this.Ventas_DataGridView.Columns["Nombre"].Width = 200;
            this.FechaDateTimePicker.Select();
        }

        private void AgregarArticuloButton_Click(object sender, EventArgs e)
        {
            if (ClienteComboBox.SelectedValue != null && vendedorComboBox.SelectedValue != null)
            {
                Sucursal sucursal = new Sucursal();

                lista_stocks = new List<Stock>();

                SeleccionarArticuloForm2 abrir = new SeleccionarArticuloForm2(sucursal.getById(Convert.ToInt32(this.SucursalComboBox.SelectedValue.ToString())));
                DialogResult Resultado = abrir.ShowDialog();
                if (Resultado == DialogResult.OK)
                {
                    int i = 0;
                    foreach (Stock stock in lista_stocks)
                    {
                        this.Ventas_DataGridView.Rows.Add(stock.producto.id, stock.producto.nombre,stock.zona.nombre, stock.color.nombre, stock.talle.nombre, null, 0, 0, stock.sucursal.id, stock.zona.id);

                        List<string> precios_unitarios = new List<string>() { stock.producto.precio_venta_x3.ToString(), stock.producto.precio_venta_x6.ToString(), stock.producto.precio_venta_x12.ToString() };
                        DataGridViewComboBoxCell precioUnitarioComboBox = (DataGridViewComboBoxCell)Ventas_DataGridView.Rows[i].Cells["Precio_Unitario"];
                        precioUnitarioComboBox.DataSource = precios_unitarios;

                        Ventas_DataGridView.Rows[i].Cells["Precio_Unitario"].Value = precioUnitarioComboBox.Items[0];

                        i++;
                    }

                }
            }
            else
            {
                MessageBox.Show("Debe seleccionar un cliente y un vendedor antes.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void AddArticuloButton_Click(object sender, EventArgs e)
        {
            if (!Validaciones()) { return; }

            Ventas_DataGridView.Rows.Add(producto.id.ToString("D10"), producto.nombre, "", Decimal.Parse(PrecioTextBox.Text), CantidadTextBox.Text, this.DescuentoUnidadTextBox.Text,Decimal.Parse(PrecioTextBox.Text) * Convert.ToInt32(CantidadTextBox.Text));

            SubTotalTextBox.Text = (Convert.ToDecimal(SubTotalTextBox.Text) + Decimal.Parse(PrecioTextBox.Text) * Convert.ToInt32(CantidadTextBox.Text)).ToString();
            DescuentoTextBox.Text = (Convert.ToDecimal(this.DescuentoTextBox.Text) + Decimal.Parse(PrecioTextBox.Text) * Convert.ToInt32(this.DescuentoUnidadTextBox.Text)).ToString();
            TotalTextBox.Text = (Convert.ToDecimal(SubTotalTextBox.Text) - Convert.ToDecimal(DescuentoTextBox.Text)).ToString();
            ComisionTextBox.Text = ((double)((Convert.ToDecimal(SubTotalTextBox.Text) - Convert.ToDecimal(DescuentoTextBox.Text)) * vendedor.comision / 100)).ToString();

            CodigoTextBox.Text = "";
            NombreTextBox.Text = "";
            PrecioTextBox.Text = "";
            CantidadTextBox.Text = "";
            DescuentoUnidadTextBox.Text = "";
            this.FormatoTextBox.Text = "";
            CantidadTextBox.ReadOnly = true;
        }

        private bool Validaciones()
        {
            if (this.NombreTextBox.Text == "Nombre" && this.CantidadTextBox.Text == "Cantidad")
            {
                return false;
            }

            int v;
            decimal x;
            if (!Int32.TryParse(this.CantidadTextBox.Text.Trim(), out v))
            {
                MessageBox.Show("Error: La cantidad solo puede ser numerica.");
                return false;
            }

            if (!Int32.TryParse(this.DescuentoUnidadTextBox.Text.Trim(), out v))
            {
                MessageBox.Show("Error: El descuento solo puede ser numerico.");
                return false;
            }

            if (!Decimal.TryParse(PrecioTextBox.Text, out x))
            {
                MessageBox.Show("Error: El precio solo puede ser numerico.");
                return false;
            }

            if (producto.stock < Convert.ToInt32(CantidadTextBox.Text))
            {
                //MessageBox.Show("Error: El producto solo cuenta con stock disponible de " + producto.stock.ToString() + " unidades.");
                //return false;
                DialogResult Resultado = MessageBox.Show("El producto no cuenta con stock disponible, ¿Desea agregar el producto de todas formas?, recuerde que el mismo tendra un stock negativo.", this.Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (Resultado == DialogResult.No)
                {
                    return false;
                }
            }

            /*foreach (DataGridViewRow row in Ventas_DataGridView.Rows)
            {
                if (producto.id == Convert.ToInt32(row.Cells["Codigo"].Value.ToString()))
                {
                    MessageBox.Show("Error: El producto ya esta agregado en la venta.");
                    return false; 
                }
            }*/

            return true;
        }

        private void GuardarButton_Click(object sender, EventArgs e)
        {
            if (Validaciones_guardar() == -1) return;
            Producto producto = new Producto();
            Talle talle = new Talle();
            ColorZapato color = new ColorZapato();
            Stock stock = new Stock();
            Zona zona = new Zona();

            fact = new FacturaVenta();

            Sucursal sucursal = new Sucursal();
            fact.sucursal = sucursal.getById(Convert.ToInt32(this.SucursalComboBox.SelectedValue.ToString()));


            fact.cliente = cliente.getById(Convert.ToInt32(ClienteComboBox.SelectedValue.ToString()));
            fact.vendedor = vendedor.getById(Convert.ToInt32(vendedorComboBox.SelectedValue.ToString()));
            fact.forma_de_pago = forma_de_pago.getById(Convert.ToInt32(FormaPagoComboBox.SelectedValue.ToString()));
            fact.fecha = FechaDateTimePicker.Value;
            fact.descuento = Decimal.Parse(DescuentoTextBox.Text);
            fact.total = Decimal.Parse(TotalTextBox.Text);
            fact.total_comision = Decimal.Parse(ComisionTextBox.Text);

            foreach (DataGridViewRow row in Ventas_DataGridView.Rows)
            {
                ItemVenta item = new ItemVenta();
                item.producto = producto.getById(Int32.Parse(row.Cells["Codigo"].Value.ToString()));
                item.talle = talle.Search(row.Cells["Talle"].Value.ToString());
                item.color = color.Search(row.Cells["Color"].Value.ToString());
                item.sucursal = sucursal.getById(Int32.Parse(row.Cells["Sucursal"].Value.ToString()));
                item.zona = zona.getById(Int32.Parse(row.Cells["Zona"].Value.ToString()));
                item.cantidad = Convert.ToInt32(row.Cells["Cantidad"].Value);

                //Stock negativo
                /*if (item.producto.stock < 0)
                {
                    MessageBox.Show("Ocurrio un error con el calculo de stock del producto " + item.producto.nombre + ".", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }*/

                item.formato = "";
                item.producto.stock = item.producto.stock - item.cantidad;

                item.precio = Convert.ToDecimal(row.Cells["Precio_Unitario"].Value);
                item.factura = fact;

                fact.items.Add(item);
            }

            if (fact.save() == true)
            {
                //Actualizo stock una vez guardada la factura, por que si lo realizamos con casacade, no deja repetir productos.
                foreach (ItemVenta item in fact.items)
                {
                    Producto p = new Producto();
                    p = p.getById(item.producto.id);

                    IList<Stock> stocksExistentes = stock.Search(item.producto.id, item.sucursal.id, item.zona.id, item.talle.id, item.color.id);
                    stocksExistentes[0].stock -= item.cantidad;
                    stocksExistentes[0].update();

                    p.update();
                }

                MessageBox.Show("Se ha dado de alta la factura correctamente.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                
                DialogResult Resultado = MessageBox.Show("¿Desea imprimir la Factura?", this.Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                 
                if (Resultado == DialogResult.Yes)
                {
                    Thread hilo = new Thread(AbrirFormReporteDetalleFactura);
                    hilo.SetApartmentState(System.Threading.ApartmentState.STA);
                    hilo.Start();
                }

                DialogResult = DialogResult.OK;
            }   
        }

        private void AbrirFormReporteDetalleFactura()
        {
            FacturaVentaReporteForm abrir = new FacturaVentaReporteForm(fact);
            abrir.ShowDialog();
        }

        private int Validaciones_guardar()
        {
            foreach (Control objeto in this.GroupBox.Controls)
            {
                if (objeto is ComboBox && ((ComboBox)objeto).SelectedItem == null)
                {
                    MessageBox.Show("Debe completar los campos vacios.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return -1;
                }
            }
            if (Ventas_DataGridView.RowCount <= 0)
            {
                MessageBox.Show("Debe agregar al menos un articulo a la factura.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return -1;
            }
            return 1;
        }

        private void EliminarArticuloButton_Click(object sender, EventArgs e)
        {
            if (this.Ventas_DataGridView.Rows.Count == 0)
            {
                return;
            }

            if (this.Ventas_DataGridView.CurrentCell == null)
            {
                MessageBox.Show("Debe seleccionar una fila.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            /*SubTotalTextBox.Text = (Convert.ToDecimal(SubTotalTextBox.Text) - Convert.ToDecimal(Ventas_DataGridView.CurrentRow.Cells["Importe"].Value)).ToString();
            DescuentoTextBox.Text = ( Convert.ToDecimal(DescuentoTextBox.Text) - ( Convert.ToDecimal(Ventas_DataGridView.CurrentRow.Cells["PrecioUnitario"].Value) *  Convert.ToInt32(Ventas_DataGridView.CurrentRow.Cells["Descuento"].Value) ) ).ToString();         
            TotalTextBox.Text = (Convert.ToDecimal(SubTotalTextBox.Text) - Convert.ToDecimal(DescuentoTextBox.Text)).ToString();
            ComisionTextBox.Text = ((double)((Convert.ToDecimal(SubTotalTextBox.Text) - Convert.ToDecimal(DescuentoTextBox.Text)) * vendedor.comision / 100)).ToString();
            */
            Ventas_DataGridView.Rows.Remove(Ventas_DataGridView.CurrentRow);

            UpdateTotales();
        }

        private void DescuentoTextBox_TextChanged(object sender, EventArgs e)
        {
            decimal total = (Convert.ToDecimal(SubTotalTextBox.Text) - Convert.ToDecimal(DescuentoTextBox.Text));
            if (total >= 0)
            {
                TotalTextBox.Text = (Convert.ToDecimal(SubTotalTextBox.Text) - Convert.ToDecimal(DescuentoTextBox.Text)).ToString();
            }
            else
            {
                MessageBox.Show("Error, El descuento no puede ser mayor al subtotal.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                DescuentoTextBox.Text = String.Format("{0:#,###0.00}", 0);
                return;
            }
        }

        private void DescuentoTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar)
               && !char.IsDigit(e.KeyChar)
               && e.KeyChar != '.')
            {
                e.Handled = true;
            }
        }

        private void ClienteComboBox_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (ClienteComboBox.SelectedValue.ToString() != "")
            {
                cliente = cliente.getById(Convert.ToInt32(ClienteComboBox.SelectedValue));
            }
        }

        private void vendedorComboBox_SelectionChangeCommitted(object sender, EventArgs e)
        {
            vendedor = vendedor.getById(Convert.ToInt32(vendedorComboBox.SelectedValue.ToString()));
        }

        private void CantidadTextBox_MouseClick(object sender, MouseEventArgs e)
        {
            ((TextBox)sender).SelectAll();
        }

        private void PrecioTextBox_MouseClick(object sender, MouseEventArgs e)
        {
            ((TextBox)sender).SelectAll();
        }

        private void DescuentoUnidadTextBox_MouseClick(object sender, MouseEventArgs e)
        {
            ((TextBox)sender).SelectAll();
        }

        private void PrecioTextBox_TextChanged(object sender, EventArgs e)
        {
            if (!Validacion.isDecimal(this.PrecioTextBox.Text))
            {
                if (!Validacion.isDecimal(this.PrecioTextBox.Text.Replace(",", ".")))
                {
                    return;
                }
                else
                {
                    this.PrecioTextBox.Text = this.PrecioTextBox.Text.Replace(",", ".");
                    PrecioTextBox.SelectionStart = PrecioTextBox.Text.Length;
                }
            }
        }

        private void Ventas_DataGridView_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (CheckCellType(e.RowIndex))
            {
                if (e.ColumnIndex == Ventas_DataGridView.Columns["Precio_Unitario"].Index || e.ColumnIndex == Ventas_DataGridView.Columns["Cantidad"].Index)
                {
                    decimal precio_unitario = 0;
                    int cantidad = Convert.ToInt32(Ventas_DataGridView.Rows[e.RowIndex].Cells["Cantidad"].Value.ToString());
                    if (Ventas_DataGridView.Rows[e.RowIndex].Cells["Precio_Unitario"].Value != null)
                        precio_unitario = Convert.ToDecimal(Ventas_DataGridView.Rows[e.RowIndex].Cells["Precio_Unitario"].FormattedValue.ToString());
                    decimal importe = precio_unitario * cantidad;
                    Ventas_DataGridView.Rows[e.RowIndex].Cells["Importe"].Value = importe.ToString();
                }
            }

            UpdateTotales();
        }

        private Boolean CheckCellType(int index) {
            int v;
            decimal x;
            if (Ventas_DataGridView.Rows[index].Cells["Cantidad"].Value != null && !Int32.TryParse(Ventas_DataGridView.Rows[index].Cells["Cantidad"].Value.ToString(), out v))
            {
                MessageBox.Show("Error: La cantidad solo puede ser numerica.");
                Ventas_DataGridView.Rows[index].Cells["Cantidad"].Value = 0;
                Ventas_DataGridView.Rows[index].Cells["Importe"].Value = 0;
                return false;
            }

            //if (!Int32.TryParse(Ventas_DataGridView.Rows[index].Cells[5].Value.ToString(), out v))
            //{
            //    MessageBox.Show("Error: El descuento solo puede ser numerico.");
            //    Ventas_DataGridView.Rows[index].Cells[5].Value = 0;
            //    return false;
            //}

            //Ventas_DataGridView.Rows[index].Cells["Precio_Unitario"].Value = Ventas_DataGridView.Rows[index].Cells["Precio_Unitario"].Value.ToString().Replace(",", ".");

            //if (!Decimal.TryParse(Ventas_DataGridView.Rows[index].Cells["Precio_Unitario"].Value.ToString(), out x))
            //{
            //    MessageBox.Show("Error: El precio solo puede ser numerico.");
            //    Ventas_DataGridView.Rows[index].Cells["Precio_Unitario"].Value = 0;
            //    Ventas_DataGridView.Rows[index].Cells["Importe"].Value = 0;
            //    return false;
            //}

            return true;
        }

        private void UpdateTotales() {
            
            //decimal descuento = 0;
            decimal subtotal = 0;

            foreach (DataGridViewRow row in Ventas_DataGridView.Rows) {
                subtotal = subtotal + (Convert.ToDecimal(row.Cells["Precio_Unitario"].Value) * Convert.ToDecimal(row.Cells["Cantidad"].Value));
                //descuento = descuento + (Convert.ToDecimal(row.Cells[3].Value) * Convert.ToDecimal(row.Cells[5].Value));
            }

            SubTotalTextBox.Text = subtotal.ToString();
            //DescuentoTextBox.Text = descuento.ToString();
            //TotalTextBox.Text = (subtotal-descuento).ToString();
            TotalTextBox.Text = subtotal.ToString();
            //decimal comision = ((subtotal - descuento) * (vendedor.comision / 100));
            decimal comision = (subtotal * (vendedor.comision / 100));
            ComisionTextBox.Text = comision.ToString("N2");

        }

        private void button1_Click(object sender, EventArgs e)
        {
            ClientesForm abrir = new ClientesForm();
            abrir.ShowDialog();
        }

        private void Ventas_DataGridView_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            if (e.Exception.Message == "DataGridViewComboBoxCell value is not valid." || e.Exception.Message == "El valor de DataGridViewComboBoxCell no es válido.")
            {
                object value = Ventas_DataGridView.Rows[e.RowIndex].Cells[e.ColumnIndex].Value;
                if (!((DataGridViewComboBoxColumn)Ventas_DataGridView.Columns[e.ColumnIndex]).Items.Contains(value))
                {
                    ((DataGridViewComboBoxColumn)Ventas_DataGridView.Columns[e.ColumnIndex]).Items.Add(value);
                    e.ThrowException = false;
                }
            }
        }

        private void Ventas_DataGridView_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (e.Control is ComboBox)
            {
                ComboBox comboBox = (ComboBox)e.Control;
                comboBox.DropDownStyle = ComboBoxStyle.DropDown;
            }
        }

        private void Ventas_DataGridView_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            Ventas_DataGridView.BeginEdit(false);

            if (e.ColumnIndex == Ventas_DataGridView.Columns["Precio_Unitario"].Index)
            {
                if (this.Ventas_DataGridView.EditingControl != null && this.Ventas_DataGridView.EditingControl is ComboBox)
                {
                    ComboBox cmb = this.Ventas_DataGridView.EditingControl as ComboBox;
                    cmb.DroppedDown = true;
                }
            }
        }

        private void Ventas_DataGridView_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            if (e.ColumnIndex == Ventas_DataGridView.Columns["Precio_Unitario"].Index && Decimal.TryParse(e.FormattedValue.ToString(), out decimal precioIngresado))
            {
                Producto productoActual = producto.getById(Int32.Parse(Ventas_DataGridView.Rows[e.RowIndex].Cells["Codigo"].Value.ToString()));
                List<string> precios_unitarios = new List<string>() { productoActual.precio_venta_x3.ToString(), productoActual.precio_venta_x6.ToString(), productoActual.precio_venta_x12.ToString() };
                DataGridViewComboBoxCell precioUnitarioComboBox = (DataGridViewComboBoxCell)Ventas_DataGridView.Rows[e.RowIndex].Cells["Precio_Unitario"];

                ultimo_precio_ingresado = precioIngresado.ToString("0.00");

                if (!precios_unitarios.Contains(ultimo_precio_ingresado))
                {
                    precios_unitarios.Add(ultimo_precio_ingresado);
                    precioUnitarioComboBox.DataSource = precios_unitarios;
                }
            }

            if (ultimo_precio_ingresado != null)
            {
                Ventas_DataGridView.Rows[e.RowIndex].Cells["Precio_Unitario"].Value = ultimo_precio_ingresado;
            }
        }

        private void Ventas_DataGridView_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            if (this.Ventas_DataGridView.IsCurrentCellDirty)
            {
                Ventas_DataGridView.CommitEdit(DataGridViewDataErrorContexts.Commit);
            }
        }

        private void Ventas_DataGridView_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1 && CheckCellType(e.RowIndex))
            {
                if (e.ColumnIndex == Ventas_DataGridView.Columns["Precio_Unitario"].Index || e.ColumnIndex == Ventas_DataGridView.Columns["Cantidad"].Index)
                {
                    decimal precio_unitario = 0;
                    int cantidad = Ventas_DataGridView.Rows[e.RowIndex].Cells["Cantidad"].Value != null ? Convert.ToInt32(Ventas_DataGridView.Rows[e.RowIndex].Cells["Cantidad"].Value.ToString()) : 0;
                    if (Ventas_DataGridView.Rows[e.RowIndex].Cells["Precio_Unitario"].Value != null)
                        precio_unitario = Convert.ToDecimal(Ventas_DataGridView.Rows[e.RowIndex].Cells["Precio_Unitario"].FormattedValue.ToString());
                    decimal importe = precio_unitario * cantidad;
                    Ventas_DataGridView.Rows[e.RowIndex].Cells["Importe"].Value = importe.ToString();
                }
            }

            UpdateTotales();
        }
    }
}
