﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using Zapateria.Libs;
using Zapateria.Models;

namespace Zapateria.Abm_Ventas
{
    public partial class DetalleVentaForm : Form
    {
        public DetalleVentaForm(string id)
        {
            InitializeComponent();

            this.id = Convert.ToInt32(id);
        }

        int id;

        private void DetalleCompraForm_Load(object sender, EventArgs e)
        {
            CargadorDeDatos.CargarClienteComboBox(ClienteComboBox, this.Text);
            CargadorDeDatos.CargarVendedoresComboBox(VendedorComboBox, this.Text);
            CargadorDeDatos.CargarFormaPagoComboBox(FormaPagoComboBox, this.Text);
            CargadorDeDatos.CargarSucursalesComboBox(SucursalComboBox, this.Text);

            FacturaVenta fact = new FacturaVenta();
            fact = fact.getById(id);

            FactIdTextBox.Text = id.ToString();
            SucursalComboBox.SelectedValue = fact.sucursal.id;
            ClienteComboBox.SelectedValue = fact.cliente.id;
            VendedorComboBox.SelectedValue = fact.vendedor.id;
            FormaPagoComboBox.SelectedValue = fact.forma_de_pago.id;
            FechaDateTimePicker.Value = Convert.ToDateTime(fact.fecha);
            DescuentoTextBox.Text = fact.descuento.ToString();
            TotalTextBox.Text = fact.total.ToString();
            totalComisionTextBox.Text = fact.total_comision.ToString();
            SubTotalTextBox.Text = (Convert.ToDecimal(TotalTextBox.Text) + Convert.ToDecimal(DescuentoTextBox.Text)).ToString();
            
            foreach (ItemVenta item in fact.items)
            {
                /*if (item.formato == "")
                {
                    Compras_DataGridView.Rows.Add(item.producto.id.ToString("D10"), item.producto.nombre, item.formato, item.precio, item.cantidad,item.descuento, item.precio * item.cantidad);
                }
                else
                {
                    Compras_DataGridView.Rows.Add(item.producto.id.ToString("D10"), item.producto.nombre, FormatoVenta.GetFormatoKG(Convert.ToDecimal(item.formato)), item.precio, item.cantidad,item.descuento, item.precio * item.cantidad);
                }*/
                Ventas_DataGridView.Rows.Add(item.producto.id.ToString("D10"), item.producto.nombre,item.zona.nombre,item.color.nombre,item.talle.nombre, item.precio, item.cantidad, item.precio * item.cantidad);
            }
        }

        DataTable dt;
    }
}
