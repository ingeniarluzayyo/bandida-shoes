﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using Zapateria.Libs;
using Zapateria.Models;

namespace Zapateria.Abm_Ventas
{
    public partial class SeleccionarArticuloForm : Form
    {
        public SeleccionarArticuloForm()
        {
            InitializeComponent();
        }

        Producto producto;
        IList<Producto> listaProductos;

        private void SeleccionarArticuloForm_Load(object sender, EventArgs e)
        {
            CargadorDeDatos.CargarFamiliasComboBox(FamiliaComboBox, this.Text);

            producto = new Producto();
            /*listaProductos = producto.GetAll("1");

            DataTable dtProductos = new DataTable();
            dtProductos.Columns.Add("Codigo");
            dtProductos.Columns.Add("Familia");
            dtProductos.Columns.Add("Producto");
            dtProductos.Columns.Add("Stock");
            dtProductos.Columns.Add("Tipo de stock");
            dtProductos.Columns.Add("Formato Kg");
            dtProductos.Columns.Add("Precio de Venta");
            dtProductos.Columns.Add("producto_formato_id");

            foreach (Producto p in listaProductos)
            {
                ProductoXFormatoVenta pfv = new ProductoXFormatoVenta();
                p.producto_x_formato_venta = pfv.getAllByProducto(p);
                String tipodestock = "";
                if (p.unidad)
                {
                    tipodestock = "Unidad";
                    dtProductos.Rows.Add(p.id.ToString("D10"), p.familia.nombre, p.nombre, p.stock, tipodestock, "", p.precio_venta, "");
                }
                else
                {
                    tipodestock = "Fraccionado";
                    foreach (ProductoXFormatoVenta pfv_aux in p.producto_x_formato_venta)
                    {
                        dtProductos.Rows.Add(p.id.ToString("D10"), p.familia.nombre, p.nombre, p.getStockKg(), tipodestock, FormatoVenta.GetFormatoKG(pfv_aux.formato.formato), pfv_aux.precio_venta, pfv_aux.id);
                    }
                }

            }*/

            this.Productos_DataGridView.DataSource = producto.SearchVentas();
            producto.customColumnsSelect(this.Productos_DataGridView);
            this.NombreTextBox.Select();

        }

        private void Filtrar()
        {
            //this.Productos_DataGridView.DataSource = producto.DoSearch(listaProductos, this.CodigoTextBox.Text,this.NombreTextBox.Text, this.FamiliaComboBox.Text);
            ((DataTable)Productos_DataGridView.DataSource).DefaultView.RowFilter = FiltroCambios();
        }

        private string FiltroCambios()
        {
            string consulta = "";

            if (this.CodigoTextBox.Text != "")
            {
                consulta += "Codigo LIKE '%" + this.CodigoTextBox.Text + "%'";
            }

            if (this.NombreTextBox.Text != "")
            {
                if (consulta != "") { consulta += " AND "; }
                consulta += "Producto LIKE '%" + this.NombreTextBox.Text + "%'";
            }

            if (this.FamiliaComboBox.SelectedValue != null)
            {
                if (consulta != "") { consulta += " AND "; }
                consulta += "Familia = '" + this.FamiliaComboBox.Text + "'";
            }
            return consulta;
        }

        private void CancelarButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void CodigoTextBox_TextChanged(object sender, EventArgs e)
        {
            Filtrar();
        }

        private void DescripcionTextBox_TextChanged(object sender, EventArgs e)
        {
            Filtrar();
        }

        private void FamiliaComboBox_SelectionChangeCommitted(object sender, EventArgs e)
        {
            Filtrar();
        }

        private void LimpiarButton_Click(object sender, EventArgs e)
        {
            foreach (Control objeto in this.GroupBox.Controls)
            {
                if (objeto is TextBox)
                {
                    ((TextBox)objeto).Clear();
                }
                if (objeto is ComboBox)
                {
                    ((ComboBox)objeto).SelectedItem = null;
                }
            }

            Productos_DataGridView.DataSource = listaProductos;
        }

        private void EliminarArticuloButton_Click(object sender, EventArgs e)
        {
            if (Productos_DataGridView.RowCount > 0)
            {
                if (Convert.ToDecimal(Productos_DataGridView.CurrentRow.Cells["Stock"].Value.ToString()) <= 0)
                {
                    MessageBox.Show("El producto '" + Productos_DataGridView.CurrentRow.Cells["Producto"].Value.ToString() + "' no tiene stock.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                AltaVentaForm.producto.id = Int32.Parse(Productos_DataGridView.CurrentRow.Cells["Codigo"].Value.ToString());
                AltaVentaForm.producto.nombre = Productos_DataGridView.CurrentRow.Cells["Producto"].Value.ToString();
                AltaVentaForm.producto.precio_venta_x3 = Convert.ToDecimal(Productos_DataGridView.CurrentRow.Cells["Precio de Venta"].Value);
                AltaVentaForm.producto.stock = Convert.ToDecimal(Productos_DataGridView.CurrentRow.Cells["Stock"].Value);

                string formato = Productos_DataGridView.CurrentRow.Cells["Formato Kg"].Value.ToString();
                if (formato != "")
                {
                    AltaVentaForm.fv.formato = Convert.ToDecimal(formato.Replace(".",","));
                    AltaVentaForm.pfv.id = Convert.ToInt32(Productos_DataGridView.CurrentRow.Cells["producto_formato_id"].Value);
                }
                
               
                DialogResult = DialogResult.OK;
            }
        }



    }
}
