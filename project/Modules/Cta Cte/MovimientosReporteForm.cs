﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using Zapateria.Modules.Reportes;
using Zapateria.Models;

namespace Zapateria.Modules.Cta_Cte
{
    public partial class MovimientosReporteForm : Form
    {
        public MovimientosReporteForm(CtaCte cta, DataTable dt)
        {
            InitializeComponent();

            ctaCte = cta;
            this.dt = dt;
        }

        CtaCte ctaCte;
        DataTable dt;

        private void crystalReportViewer1_Load(object sender, EventArgs e)
        {
            DetalleCtaCteCrystalReport Repo = new DetalleCtaCteCrystalReport();

            //Variables
            TextObject FechaRepo;
            FechaRepo = (TextObject)Repo.ReportDefinition.ReportObjects["FechaText"];
            FechaRepo.Text = DateTime.Now.ToShortDateString();

            TextObject ClienteRepo;
            ClienteRepo = (TextObject)Repo.ReportDefinition.ReportObjects["ClienteText"];
            //ClienteRepo.Text = ctaCte.cliente.cliente;

            TextObject SaldoRepo;
            SaldoRepo = (TextObject)Repo.ReportDefinition.ReportObjects["SaldoAcumuladoText"];
            SaldoRepo.Text = ctaCte.saldo.ToString(); 

            Repo.SetDataSource(dt);

            crystalReportViewer1.ReportSource = Repo;
        }
    }
}
