﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using Zapateria.Libs;
using System.Threading;
using Zapateria.Models;

namespace Zapateria.Modules.Cta_Cte
{
    public partial class CtaCteForm : Form
    {
        CtaCte ctaCte = new CtaCte();
        DataTable dt = null;

        public CtaCteForm(int cliente_id)
        {
            InitializeComponent();
            ctaCte = ctaCte.getByCliente(cliente_id);
        }        

        private void CancelarButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private string ArmarConsulta()
        {
            /*string Consulta = "SELECT CONCAT('Factura N°',V.id) as 'Elemento',total as 'Facturado',0 as 'Pagado',F.nombre as 'Forma de pago',total as 'Saldo' FROM factura_venta as V "; 
            Consulta += "INNER JOIN forma_de_pago as F ON forma_de_pago = F.id ";
            Consulta += "WHERE cliente = " + ctaCte.cliente.id + " AND F.id = 2 ";
            Consulta += "UNION ";
            Consulta += "SELECT CONCAT('Factura N°',V.id) as 'Elemento',total as 'Facturado',total as 'Pagado',F.nombre as 'Forma de pago',0 as 'Saldo' FROM factura_venta as V ";
            Consulta += "INNER JOIN forma_de_pago as F ON forma_de_pago = F.id ";
            Consulta += "WHERE cliente = " + ctaCte.cliente.id + " AND F.id != 2 ";
            Consulta += "UNION ";
            Consulta += "SELECT CONCAT('Pago N°',P.id) as 'Elemento',0 as 'Facturado',monto as 'Pagado',F.nombre as 'Forma de pago',monto as 'Saldo' FROM pagos as P ";
            Consulta += "INNER JOIN forma_de_pago as F ON forma_de_pago = F.id ";
            Consulta += "WHERE cliente = " + ctaCte.cliente.id;*/

            string Consulta = "SELECT * FROM (SELECT CONCAT('Factura N°',V.id) as 'Elemento',fecha as 'Fecha',total as 'Facturado',0 as 'Pagado',F.nombre as 'Forma de pago' FROM factura_venta as V ";
            Consulta += "INNER JOIN forma_de_pago as F ON forma_de_pago = F.id ";
            Consulta += "WHERE cliente = " + ctaCte.cliente.id + " AND F.id = 2 ";
            Consulta += "UNION ";
            Consulta += "SELECT CONCAT('Factura N°',V.id) as 'Elemento',fecha as 'Fecha',total as 'Facturado',total as 'Pagado',F.nombre as 'Forma de pago' FROM factura_venta as V ";
            Consulta += "INNER JOIN forma_de_pago as F ON forma_de_pago = F.id ";
            Consulta += "WHERE cliente = " + ctaCte.cliente.id + " AND F.id != 2 ";
            Consulta += "UNION ";
            Consulta += "SELECT CONCAT('Pago N°',P.id) as 'Elemento',fecha as 'Fecha',0 as 'Facturado',monto as 'Pagado',F.nombre as 'Forma de pago' FROM pagos as P ";
            Consulta += "INNER JOIN forma_de_pago as F ON forma_de_pago = F.id ";
            Consulta += "WHERE cliente = " + ctaCte.cliente.id;
            Consulta += ") as R ORDER BY fecha";

            return Consulta;
        }

        private void CtaCteForm_Load(object sender, EventArgs e)
        {
            TotalTextBox.Text = String.Format("{0:#,###0.00}", 0);

            //NombreTextBox.Text = ctaCte.cliente.cliente;
            ApellidoTextBox.Text = ctaCte.cliente.razon_social;
            TelefonotextBox.Text = ctaCte.cliente.tel;
            EmailTextBox.Text = ctaCte.cliente.localidad;

            TotalTextBox.Text = ctaCte.saldo.ToString();

            string Consulta = ArmarConsulta();

            List<MySqlParameter> param = new List<MySqlParameter>();
            dt = BaseDeDatos.GetInstance.ExecuteCustomQuery(Consulta, param, this.Text);

            dt.Columns.Add("Saldo");

            Decimal saldo_total = 0;

            foreach (DataRow row in dt.Rows)
            {
                Decimal saldo_parcial = (-1 * Convert.ToDecimal(row["Facturado"].ToString())) + Convert.ToDecimal(row["Pagado"].ToString());
                saldo_total = saldo_total + saldo_parcial;
                row["Saldo"] = saldo_total;
            }

            Cta_Cte_DataGridView.DataSource = dt;

            this.Cta_Cte_DataGridView.Columns["Forma de Pago"].Width = 160;
            this.Cta_Cte_DataGridView.Focus();
        }

        private void ImprimirEtiquetaButton_Click(object sender, EventArgs e)
        {
            if (Cta_Cte_DataGridView.DataSource == null)
            {
                return;
            }
            
            Thread hilo = new Thread(AbrirFormReporte);
            hilo.SetApartmentState(System.Threading.ApartmentState.STA);
            hilo.Start();
             
        }

        private void AbrirFormReporte()
        {
            MovimientosReporteForm abrir = new MovimientosReporteForm(ctaCte, dt);
            abrir.ShowDialog();
        }
    }
}
