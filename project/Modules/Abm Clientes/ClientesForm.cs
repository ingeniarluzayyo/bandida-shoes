﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Zapateria.Libs;
using Zapateria.Models;

using MySql.Data.MySqlClient;
using System.Threading;
using Zapateria.Modules.Abm_Clientes;

namespace Zapateria.Abm_Clientes
{
    public partial class ClientesForm : Form
    {
        public ClientesForm()
        {
            InitializeComponent();
        }

        private void CancelarButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void LimpiarButton_Click(object sender, EventArgs e)
        {
            foreach (Control objeto in this.GroupBox.Controls)
            {
                if (objeto is TextBox)
                {
                    ((TextBox)objeto).Clear();
                }
                if (objeto is ComboBox)
                {
                    ((ComboBox)objeto).SelectedItem = null;
                }
            }

            ModDataGridView.limpiarDataGridView(Clientes_DataGridView, "");
        }

        private string ArmarConsulta()
        {
            string Consulta = "SELECT  C.id,localidad as Localidad,orden as Orden,razon_social as 'Razon Social',cuit as Cuit,tel as Tel,direccion as Direccion,habilitado as Habilitado FROM clientes as C WHERE 1=1 ";

            if (RazonSocialTextBox.TextLength > 0)
            {
                Consulta += "AND razon_social LIKE '%" + RazonSocialTextBox.Text + "%' ";
            }

            if (DirTextBox.TextLength > 0)
            {
                Consulta += "AND direccion LIKE '%" + DirTextBox.Text + "%' ";
            }

            if (AnuladoCheckBox.Checked)
            {
                Consulta += "AND habilitado = '0' ";
            }
            else {
                Consulta += "AND habilitado = '1' ";
            }

            return Consulta;
        }


        private void BuscarButton_Click(object sender, EventArgs e)
        {
            Security Busqueda = new Security();
            if (!Busqueda.ValidarSqlInjection(this.GroupBox)) { return; }

            string Consulta = ArmarConsulta();

            List<MySqlParameter> param = new List<MySqlParameter>();

            Clientes_DataGridView.DataSource = BaseDeDatos.GetInstance.ExecuteCustomQuery(Consulta, param, this.Text);


            if (Clientes_DataGridView.DataSource == null)
            {
                MessageBox.Show("No se encontraron resultados que coincidan con la busqueda.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            foreach (DataGridViewRow row in Clientes_DataGridView.Rows){
                row.HeaderCell.Value = (row.Index + 1).ToString();
            }

            this.Clientes_DataGridView.RowHeadersWidth = 70;
            this.Clientes_DataGridView.Columns["id"].Visible = false;
            this.Clientes_DataGridView.Columns["Razon Social"].Width = 170;
            this.Clientes_DataGridView.Columns["Direccion"].Width = 200;
            this.Clientes_DataGridView.Columns["Localidad"].Width = 150;
            this.Clientes_DataGridView.Columns["Tel"].Width = 150;
            this.Clientes_DataGridView.Focus();
        }

        private void AgregarButton_Click(object sender, EventArgs e)
        {
            AltaClienteForm abrir = new AltaClienteForm();
            abrir.ShowDialog();
        }

        private void EliminarButton_Click(object sender, EventArgs e)
        {
            if (this.Clientes_DataGridView.Rows.Count == 0)
            {
                return;
            }

            if (this.Clientes_DataGridView.CurrentCell == null)
            {
                MessageBox.Show("Debe seleccionar una fila.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            DialogResult Resultado = MessageBox.Show("¿Esta seguro que desea eliminar el cliente?", this.Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (Resultado == DialogResult.Yes)
            {
                Cliente cliente = new Cliente();
                cliente = cliente.getById(Convert.ToInt32(Clientes_DataGridView.CurrentRow.Cells["id"].Value));
                cliente.habilitado = "0";
                
                if (cliente.update())
                {
                    Clientes_DataGridView.Rows.Remove(Clientes_DataGridView.CurrentRow);
                    MessageBox.Show("Se ha dado de baja el cliente correctamente.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }

        private void ModificarButton_Click(object sender, EventArgs e)
        {
            if (this.Clientes_DataGridView.Rows.Count == 0)
            {
                return;
            }

            if (this.Clientes_DataGridView.CurrentCell == null)
            {
                MessageBox.Show("Debe seleccionar una fila.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            string id = Clientes_DataGridView.CurrentRow.Cells["id"].Value.ToString();
            
            ModificarClienteForm abrir = new ModificarClienteForm(id);
            DialogResult Resultado = abrir.ShowDialog();
            if (Resultado == DialogResult.OK)
            {
                //ModDataGridView.limpiarDataGridView(Clientes_DataGridView, "");
                this.BuscarButton.PerformClick();
            }
        }

        private void ClientesForm_Load(object sender, EventArgs e)
        {
            this.BuscarButton.Select();
            
            Notificaciones notificacion = new Notificaciones();
            notificacion = notificacion.getLast();
           
            if (notificacion != null)
            {
                //FechaLabel.Text = notificacion.fecha;
                //EstadoLabel.Text = notificacion.estado;
            }

            this.BuscarButton.PerformClick();
        }

        private void NotificarPreciosButton_Click(object sender, EventArgs e)
        {
            Notificaciones notificacion = new Notificaciones();
            notificacion = notificacion.getEnEspera();

            if (notificacion != null) {
                MessageBox.Show("No puede programar nuevos envios hasta que no finalice el anterior.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            notificacion = new Notificaciones();
            notificacion.descripcion = "Lista de precios";
            notificacion.fecha = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss");
            notificacion.estado = "En espera";

            if (notificacion.save())
            {
                notificacion = new Notificaciones();
                notificacion = notificacion.getLast();

                if (notificacion != null)
                {
                    //FechaLabel.Text = notificacion.fecha;
                    //EstadoLabel.Text = notificacion.estado;
                }

                MessageBox.Show("Se ha programado el envio de la lista de precios correctamente.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else { 
                MessageBox.Show("Hubo un error al programar1 el envio de la lista de precios correctamente.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void ImprimirPreciosButton_Click(object sender, EventArgs e)
        {
            SeleccionTipoClienteForm form = new SeleccionTipoClienteForm();
            form.ShowDialog();
        }

    }
}
