﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using Zapateria.Models;
using Zapateria.Libs;

namespace Zapateria.Modules.Abm_Clientes
{
    public partial class SeleccionTipoClienteForm : Form
    {
        public SeleccionTipoClienteForm()
        {
            InitializeComponent();
        }

        private void ImprimirPreciosButton_Click(object sender, EventArgs e)
        {
            if (TipoClienteComboBox.SelectedValue == null)
            {
                MessageBox.Show("Error, debe seleccionar un tipo de cliente.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            CheckForIllegalCrossThreadCalls = false;

            Thread hilo = new Thread(AbrirFormReportePrecios);
            hilo.SetApartmentState(System.Threading.ApartmentState.STA);
            hilo.Start();
        }

        private void AbrirFormReportePrecios()
        {
            Producto producto = new Producto();
            var productos = producto.GetAll("1", true);

            ReporteListaDePreciosForm abrir = new ReporteListaDePreciosForm(productos);
            abrir.ShowDialog();

        }

        private void SeleccionTipoClienteForm_Load(object sender, EventArgs e)
        {
            CargadorDeDatos.CargarRepartosComboBox(TipoClienteComboBox, this.Text);
        }
    }
}
