﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Zapateria.Models;
using Zapateria.Modules.Reportes;
using CrystalDecisions.CrystalReports.Engine;

namespace Zapateria.Modules.Abm_Clientes
{
    public partial class ReporteListaDePreciosForm : Form
    {
        public ReporteListaDePreciosForm(IList<Producto> productos)
        {
            InitializeComponent();

            this.productos = productos;
            //this.tipo_cliente = tipo_cliente;
        }

        Empresa empresa = new Empresa(); 
        IList<Producto> productos;
        //TipoCliente tipo_cliente;

        private void crystalReportViewer1_Load(object sender, EventArgs e)
        {
            ListaDePreciosCrystalReport Repo = new ListaDePreciosCrystalReport();

            empresa = empresa.getById(1);


            //Variables
            TextObject EmpresaNombreRepo;
            EmpresaNombreRepo = (TextObject)Repo.ReportDefinition.ReportObjects["EmpresaText"];
            EmpresaNombreRepo.Text = empresa.razon_social;

            DataTable dt = new DataTable();
            dt.Columns.Add("id");
            dt.Columns.Add("familia");
            dt.Columns.Add("nombre");
            dt.Columns.Add("precio");

            foreach (Producto p in productos)
            {
                //dt.Rows.Add(p.id.ToString("D10"), p.familia.nombre, p.nombre, p.getPrecioVenta(tipo_cliente));
            }


            Repo.SetDataSource(dt);

            crystalReportViewer1.ReportSource = Repo;
        }
    }
}
