﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using Zapateria.Models;
using Zapateria.Libs;

namespace Zapateria.Abm_Clientes
{
    public partial class ModificarClienteForm : Form
    {
        public ModificarClienteForm(string id)
        {
            InitializeComponent();

            this.id = id;
        }


        string id;
        Cliente cliente = new Cliente();

        private void GuardarButton_Click(object sender, EventArgs e)
        {
            if (Validaciones() == -1) return;            

            cliente.razon_social = this.RazonSocialTextBox.Text;
            cliente.cuit = this.CuitTextBox.Text;
            cliente.tel = this.TelTextBox.Text;
            cliente.direccion = this.DirTextBox.Text;
            cliente.localidad = this.LocalidadTextBox.Text;
            cliente.observacion = this.NotaTextBox.Text;
            cliente.orden = Convert.ToInt32(this.OrdenNumericUpDown.Value);

            if (HabilitadoCheckBox.Checked)
            {
                cliente.habilitado = "1";
            }
            else {
                cliente.habilitado = "0";
            }       

            if (cliente.update())
            {
                MessageBox.Show("Se ha modificado el cliente '" + RazonSocialTextBox.Text + "' correctamente.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                DialogResult = DialogResult.OK;
            } 
        }

        private void LimpiarButton_Click(object sender, EventArgs e)
        {
            foreach (Control objeto in this.GroupBox.Controls)
            {
                if (objeto is TextBox)
                {
                    ((TextBox)objeto).Clear();
                }
                if (objeto is ComboBox)
                {
                    ((ComboBox)objeto).SelectedItem = null;
                }
            }
        }

        private int Validaciones()
        {

            if (RazonSocialTextBox.Text == "")
            {
                MessageBox.Show("Debe completar los campos obligatorios.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return -1;
            }
            return 1;
        }


        private void CancelarButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ModificarClienteForm_Load(object sender, EventArgs e)
        {
            cliente = cliente.getById(Convert.ToInt32(id));
            
            this.RazonSocialTextBox.Text = cliente.razon_social;
            this.CuitTextBox.Text = cliente.cuit;
            this.TelTextBox.Text = cliente.tel;
            this.DirTextBox.Text = cliente.direccion;
            this.LocalidadTextBox.Text = cliente.localidad;
            this.NotaTextBox.Text = cliente.observacion;
            this.OrdenNumericUpDown.Value = cliente.orden;
            if (cliente.habilitado == "1") {
                this.HabilitadoCheckBox.Checked = true;
            }
        }
    }
}
