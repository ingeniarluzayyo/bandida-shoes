﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using Zapateria.Libs;
using Zapateria.Models;

namespace Zapateria.Abm_Clientes
{
    public partial class AltaClienteForm : Form
    {
        public AltaClienteForm()
        {
            InitializeComponent();
        }

        private void CancelarButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void LimpiarButton_Click(object sender, EventArgs e)
        {
            foreach (Control objeto in this.GroupBox.Controls)
            {
                if (objeto is TextBox)
                {
                    ((TextBox)objeto).Clear();
                }
                if (objeto is ComboBox)
                {
                    ((ComboBox)objeto).SelectedItem = null;
                }
            }
        }

        private void GuardarButton_Click(object sender, EventArgs e)
        {
            if (Validaciones() == -1) return;

            Cliente cliente = new Cliente();
            
            cliente.razon_social = this.RazonSocialTextBox.Text;
            cliente.cuit = this.CuitTextBox.Text;
            cliente.tel = this.TelTextBox.Text;
            cliente.direccion = this.DirTextBox.Text;
            cliente.localidad = this.LocalidadTextBox.Text;
            cliente.observacion = this.NotaTextBox.Text;
            cliente.orden = Convert.ToInt32(this.OrdenNumericUpDown.Value);
            cliente.habilitado = "1";         

            if (cliente.save())
            {
                MessageBox.Show("Se ha dado de alta el cliente correctamente.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.LimpiarButton.PerformClick();
                this.RazonSocialTextBox.Focus();
            } 
        }

        private int Validaciones()
        {

            if (RazonSocialTextBox.Text == "")
            {
                MessageBox.Show("Debe completar los campos obligatorios.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return -1;
            }

            return 1;
        }
    }
}
