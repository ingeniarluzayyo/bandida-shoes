﻿namespace Zapateria.Modules.Abm_Clientes
{
    partial class SeleccionTipoClienteForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SeleccionTipoClienteForm));
            this.label9 = new System.Windows.Forms.Label();
            this.TipoClienteComboBox = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.ImprimirPreciosButton = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label9
            // 
            this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(79, 85);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(76, 16);
            this.label9.TabIndex = 53;
            this.label9.Text = "Tipo Cliente";
            // 
            // TipoClienteComboBox
            // 
            this.TipoClienteComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.TipoClienteComboBox.FormattingEnabled = true;
            this.TipoClienteComboBox.Location = new System.Drawing.Point(161, 84);
            this.TipoClienteComboBox.Name = "TipoClienteComboBox";
            this.TipoClienteComboBox.Size = new System.Drawing.Size(183, 23);
            this.TipoClienteComboBox.TabIndex = 52;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.ImprimirPreciosButton);
            this.groupBox1.Controls.Add(this.TipoClienteComboBox);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(439, 236);
            this.groupBox1.TabIndex = 54;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Seleccion";
            // 
            // ImprimirPreciosButton
            // 
            this.ImprimirPreciosButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ImprimirPreciosButton.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ImprimirPreciosButton.Image = ((System.Drawing.Image)(resources.GetObject("ImprimirPreciosButton.Image")));
            this.ImprimirPreciosButton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ImprimirPreciosButton.Location = new System.Drawing.Point(161, 157);
            this.ImprimirPreciosButton.Name = "ImprimirPreciosButton";
            this.ImprimirPreciosButton.Size = new System.Drawing.Size(108, 35);
            this.ImprimirPreciosButton.TabIndex = 62;
            this.ImprimirPreciosButton.Text = " Imprimir";
            this.ImprimirPreciosButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ImprimirPreciosButton.UseVisualStyleBackColor = true;
            this.ImprimirPreciosButton.Click += new System.EventHandler(this.ImprimirPreciosButton_Click);
            // 
            // SeleccionTipoClienteForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(463, 260);
            this.Controls.Add(this.groupBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "SeleccionTipoClienteForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Seleccion tipo cliente";
            this.Load += new System.EventHandler(this.SeleccionTipoClienteForm_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox TipoClienteComboBox;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button ImprimirPreciosButton;

    }
}