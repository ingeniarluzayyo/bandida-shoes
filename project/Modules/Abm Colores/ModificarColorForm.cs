﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using MySql.Data.MySqlClient;
using Zapateria.Models;

namespace Zapateria.Abm_Colores
{
    public partial class ModificarColorForm : Form
    {
        ColorZapato color_load;

        public ModificarColorForm(ColorZapato color)
        {
            InitializeComponent();

            color_load = color;
        }

        private void GuardarButton_Click(object sender, EventArgs e)
        {
            if (Validaciones() == -1) return;

            ColorZapato color = new ColorZapato();
            color.id = Convert.ToInt32(color_load.id);
            color.nombre = this.NombreTextBox.Text;

            if (color.update() == true)
            {
                MessageBox.Show("Se ha modificado el color correctamente.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                DialogResult = DialogResult.OK;
            }
        }

        private void LimpiarButton_Click(object sender, EventArgs e)
        {
            foreach (Control objeto in this.GroupBox.Controls)
            {
                if (objeto is TextBox)
                {
                    ((TextBox)objeto).Clear();
                }
            }
        }

        private void CancelarButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private int Validaciones()
        {
            foreach (Control objeto in this.GroupBox.Controls)
            {
                if (objeto is TextBox && ((TextBox)objeto).Text == "")
                {
                    MessageBox.Show("Debe completar el campo vacio.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return -1;
                }
            }
            return 1;
        }

        private void ModificarColorForm_Load(object sender, EventArgs e)
        {
            this.NombreTextBox.Text = color_load.nombre;
            this.NombreTextBox.Select();
        }
    }
}
