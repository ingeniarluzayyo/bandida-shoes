﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using MySql.Data.MySqlClient;
using Zapateria.Libs;
using Zapateria.Models;

namespace Zapateria.Abm_Colores
{
    public partial class ColoresForm : Form
    {
        public ColoresForm()
        {
            InitializeComponent();
        }

        private void AgregarButton_Click(object sender, EventArgs e)
        {
            AltaColoresForm abrir = new AltaColoresForm();
            abrir.ShowDialog();
        }

        private void EliminarButton_Click(object sender, EventArgs e)
        {
            if (this.Colores_DataGridView.Rows.Count == 0)
            {
                return;
            }

            if (this.Colores_DataGridView.CurrentCell == null)
            {
                MessageBox.Show("Debe seleccionar una fila.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            string ColorBorrado = Colores_DataGridView.CurrentRow.Cells["Nombre"].Value.ToString();

            DialogResult Resultado = MessageBox.Show("¿Está seguro de que desea eliminar el color '" + ColorBorrado + "'?", this.Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (Resultado == DialogResult.Yes)
            {
                ColorZapato color = new ColorZapato();
                color.id = Convert.ToInt32(Colores_DataGridView.CurrentRow.Cells["id"].Value);

                if (color.delete() == true)
                {
                    if (this.ColorTextBox.TextLength > 0)
                    {
                        color.nombre = this.ColorTextBox.Text;
                    }

                    this.Colores_DataGridView.DataSource = color.getColoresLike();

                    MessageBox.Show("Se ha dado de baja el color '" + ColorBorrado + "' correctamente.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }

        private void ModificarButton_Click(object sender, EventArgs e)
        {
            if (this.Colores_DataGridView.Rows.Count == 0)
            {
                return;
            }
            if (this.Colores_DataGridView.CurrentCell == null)
            {
                MessageBox.Show("Debe seleccionar una fila.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            ColorZapato color = new ColorZapato();
            color.id = Convert.ToInt32(this.Colores_DataGridView.CurrentRow.Cells["id"].Value.ToString());
            color.nombre = this.Colores_DataGridView.CurrentRow.Cells["nombre"].Value.ToString();

            ModificarColorForm abrir = new ModificarColorForm(color);
            DialogResult Resultado = abrir.ShowDialog();
            if (Resultado == DialogResult.OK)
            {
                ModDataGridView.limpiarDataGridView(Colores_DataGridView, "");
            }
        }

        private void BuscarButton_Click(object sender, EventArgs e)
        {
            Security Busqueda = new Security();
            if (!Busqueda.ValidarSqlInjection(this.GroupBox)) { return; }

            ColorZapato color = new ColorZapato();

            if (ColorTextBox.TextLength > 0)
            {
                color.nombre = this.ColorTextBox.Text;
            }

            var colores = color.getColoresLike();

            if (colores.Count() == 0)
            {
                MessageBox.Show("No se encontraron resultados que coincidan con la busqueda.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            else
            {
                this.Colores_DataGridView.DataSource = colores;
                color.customColumns(this.Colores_DataGridView);
            }

            this.Colores_DataGridView.Focus();
        }

        private void LimpiarButton_Click(object sender, EventArgs e)
        {
            foreach (Control objeto in this.GroupBox.Controls)
            {
                if (objeto is TextBox)
                {
                    ((TextBox)objeto).Clear();
                }
            }

            ModDataGridView.limpiarDataGridView(Colores_DataGridView, "");
        }

        private void CancelarButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ColoresForm_Load(object sender, EventArgs e)
        {
            this.Colores_DataGridView.MultiSelect = false;
            this.ColorTextBox.Select();
            this.BuscarButton.PerformClick();
        }
    }
}
