﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Zapateria.Libs;
using Zapateria.Models;

namespace Zapateria.Modules.Abm_Roles
{
    public partial class RolesForm : Form
    {
        public RolesForm()
        {
            InitializeComponent();
        }

        private void BuscarButton_Click(object sender, EventArgs e)
        {
            Security Busqueda = new Security();
            if (!Busqueda.ValidarSqlInjection(this.GroupBox)) { return; }

            Rol rol = new Rol();

            if (NombreRolTextBox.TextLength > 0)
            {
                rol.nombre = this.NombreRolTextBox.Text;
            }

            var roles = rol.getRolesLike();

            if (roles.Count() == 0)
            {
                MessageBox.Show("No se encontraron resultados que coincidan con la busqueda.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            else
            {
                Roles_DataGridView.DataSource = roles;
                rol.customColumns(Roles_DataGridView);
            }

            this.Roles_DataGridView.Focus();
        }

        private void LimpiarButton_Click(object sender, EventArgs e)
        {
            foreach (Control objeto in this.GroupBox.Controls)
            {
                if (objeto is TextBox)
                {
                    ((TextBox)objeto).Clear();
                }
            }

            ModDataGridView.limpiarDataGridView(Roles_DataGridView, "");

        }

        private void CancelarButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void AgregarButton_Click(object sender, EventArgs e)
        {
            AltaRolForm f = new AltaRolForm();
            f.ShowDialog();
        }

        private void ModificarButton_Click(object sender, EventArgs e)
        {
            if (this.Roles_DataGridView.Rows.Count == 0)
            {
                return;
            }
            if (this.Roles_DataGridView.CurrentCell == null)
            {
                MessageBox.Show("Debe seleccionar una fila.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            Rol r = new Rol();
            int id = Int32.Parse(Roles_DataGridView.CurrentRow.Cells["id"].Value.ToString());

            ModificarRolForm abrir = new ModificarRolForm(id);
            DialogResult Resultado = abrir.ShowDialog();
            if (Resultado == DialogResult.OK)
            {
                ModDataGridView.limpiarDataGridView(Roles_DataGridView, "");
            }
        }

        private void EliminarButton_Click(object sender, EventArgs e)
        {
            if (this.Roles_DataGridView.Rows.Count == 0)
            {
                return;
            }
            if (this.Roles_DataGridView.CurrentCell == null)
            {
                MessageBox.Show("Debe seleccionar una fila.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            Rol r = new Rol();
            int id = Int32.Parse(Roles_DataGridView.CurrentRow.Cells["id"].Value.ToString());
            r = r.getRoleById(id);

            if (r.delete())
            {
                MessageBox.Show("El rol se elimino correctamente.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                BuscarButton.PerformClick();
            }
            else
            {
                MessageBox.Show("Ocurrio un error al eliminar el rol.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
