﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Zapateria.Libs;
using Zapateria.Models;

namespace Zapateria.Modules.Abm_Roles
{
    public partial class AltaRolForm : Form
    {
        public AltaRolForm()
        {
            InitializeComponent();
        }

        private void CancelarButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void LimpiarButton_Click(object sender, EventArgs e)
        {
            foreach (Control objeto in RolesGroupBox.Controls)
            {
                if (objeto is TextBox)
                {
                    ((TextBox)objeto).Clear();
                }
            }

            FormsUtils.CheckUncheckAll(modulosCheckedListBox);
            
        }        

        private void AltaRolForm_Load(object sender, EventArgs e)
        {
            Modulo modulo = new Modulo();
            var modulos = modulo.getModulosLike();

            modulosCheckedListBox.DisplayMember = "nombre";
            modulosCheckedListBox.ValueMember = "id";
            foreach (Modulo m in modulos)
            {
                modulosCheckedListBox.Items.Add(m);
            }
        }

        private int Validaciones()
        {
            foreach (Control objeto in this.RolesGroupBox.Controls)
            {
                if (objeto is TextBox && ((TextBox)objeto).Text == "")
                {
                    MessageBox.Show("Debe completar los campos vacios.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return -1;
                }
            }

            if (modulosCheckedListBox.CheckedItems.Count == 0)
            {
                MessageBox.Show("Debe seleccionar algun módulo para el rol.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return -1;
            }

            return 1;
        }

        private void GuardarButton_Click(object sender, EventArgs e)
        {
            if (Validaciones() == -1) return;

            Rol rol = new Rol();
            rol.nombre = this.NombreTextBox.Text;
            foreach (Modulo m in modulosCheckedListBox.CheckedItems)
            {
                rol.modulos.Add(m);
            }

            



            if (rol.save() == true)
            {
                MessageBox.Show("Se ha dado de alta el rol '" + NombreTextBox.Text + "' correctamente.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.LimpiarButton.PerformClick();
                this.NombreTextBox.Focus();
            }
        }
    }
}
