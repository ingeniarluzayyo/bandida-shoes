﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Data.SqlClient;
using System.Security.Cryptography;
using MySql.Data.MySqlClient;
using Zapateria.Libs;
using Zapateria.Models;

namespace Zapateria.Modules.ABM_Usuarios
{
    public partial class AltaUsuariosForm : Form
    {

        public AltaUsuariosForm()
        {
            InitializeComponent();
           
        }

        private void CancelarButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void LimpiarButton_Click(object sender, EventArgs e)
        {
            foreach (Control objeto in this.UsuariosGroupBox.Controls)
            {
                if (objeto is TextBox)
                {
                    ((TextBox)objeto).Clear();
                }
                if (objeto is ComboBox)
                {
                    ((ComboBox)objeto).SelectedItem = null;
                }
            }
        }

        private void GuardarButton_Click(object sender, EventArgs e)
        {
            if (Validaciones() == -1) return;

            Usuario user = new Usuario();

            Sucursal sucursal = new Sucursal();
            user.sucursal_default = sucursal.getById(Convert.ToInt32(this.SucursalComboBox.SelectedValue.ToString()));
        
            user.username = this.NombreUsuarioTextBox.Text;
            user.password = Security.Encriptar(this.ContraseñaTextBox.Text);
            user.email = this.EmailTextBox.Text;
            Rol rol = new Rol();
            rol = rol.getRoleById(Convert.ToInt32(RolComboBox.SelectedValue.ToString()));
            user.rol = rol;

            if (user.save() == true)
            {
                MessageBox.Show("Se ha dado de alta el usuario '" + NombreUsuarioTextBox.Text + "' correctamente.",this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.LimpiarButton.PerformClick();
                this.NombreUsuarioTextBox.Focus();
            }

        }

        private int Validaciones()
        {
            foreach (Control objeto in this.UsuariosGroupBox.Controls)
            {
                if (objeto is TextBox && ((TextBox)objeto).Text == "")
                {
                    MessageBox.Show("Debe completar los campos vacios.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return -1;
                }
            }

            if (ContraseñaTextBox.Text.Length < 6)
            {
                MessageBox.Show("Por razones de seguridad la contraseña debe contener como minimo seis caracteres.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return -1;

            }

            if (ContraseñaTextBox.Text != RepContraseñaTextBox.Text)
            {
                MessageBox.Show("Las contraseñas no coinciden. Por favor vuelva a escribirlas.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return -1;
            }

            if (RolComboBox.SelectedValue == null)
            {
                MessageBox.Show("El usuario debe tener un rol asignado.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return -1;
            }

            if (SucursalComboBox.SelectedValue == null)
            {
                MessageBox.Show("Debe completar la sucursal.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return -1;
            }

            Usuario user = new Usuario();
            if (user.getByUsername(NombreUsuarioTextBox.Text) != null)
            {
                MessageBox.Show("Ya existe un usuario con ese nombre.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return -1;
            }

            return 1;
        }

        private void AltaUsuariosForm_Load(object sender, EventArgs e)
        {
            CargadorDeDatos.CargarRolesComboBox(RolComboBox,"Roles");
            CargadorDeDatos.CargarSucursalesComboBox(SucursalComboBox, "Sucursales");
        }

    }
}
