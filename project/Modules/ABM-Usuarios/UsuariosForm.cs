﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using MySql.Data.MySqlClient;
using Zapateria.Libs;
using Zapateria.Models;

namespace Zapateria.Modules.ABM_Usuarios
{
    public partial class UsuariosForm : Form
    {
        public UsuariosForm()
        {
            InitializeComponent();
        }

        private void LimpiarButton_Click(object sender, EventArgs e)
        {
            foreach (Control objeto in this.GroupBox.Controls)
            {
                if (objeto is TextBox)
                {
                    ((TextBox)objeto).Clear();
                }
            }

            ModDataGridView.limpiarDataGridView(Usuario_DataGridView, "");

        }

        private void CancelarButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        private void BuscarButton_Click(object sender, EventArgs e)
        {
            Security Busqueda = new Security();
            if (!Busqueda.ValidarSqlInjection(this.GroupBox)) { return; }

            Usuario usuario = new Usuario();
           
            if (NombreUsuarioTextBox.TextLength > 0)
            {
                usuario.username = this.NombreUsuarioTextBox.Text;
            }

            var users = usuario.getUsuarioLike();

            if (users.Count() == 0)
            {
                MessageBox.Show("No se encontraron resultados que coincidan con la busqueda.",this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }else{
               Usuario_DataGridView.DataSource = users;
               usuario.customColumns(Usuario_DataGridView);
            }

            this.Usuario_DataGridView.Focus();
        }

        private void AgregarButton_Click(object sender, EventArgs e)
        {
            AltaUsuariosForm abrir = new AltaUsuariosForm();
            abrir.ShowDialog();
        }

        private void EliminarButton_Click(object sender, EventArgs e)
        {
            if (this.Usuario_DataGridView.Rows.Count == 0)
            {
                return;
            }
            if (this.Usuario_DataGridView.CurrentCell == null)
            {
                MessageBox.Show("Debe seleccionar una fila.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            

            string UsuarioBorrado = Usuario_DataGridView.CurrentRow.Cells["username"].Value.ToString();

            DialogResult Resultado = MessageBox.Show("¿Esta seguro que desea eliminar el usuario '" + UsuarioBorrado + "'?",this.Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (Resultado == DialogResult.Yes)
            {
                Usuario usuario = new Usuario();
                usuario.username = Convert.ToString(Usuario_DataGridView.CurrentRow.Cells["username"].Value);

                if (usuario.delete() == true)
                {
                    usuario = new Usuario();

                    if (NombreUsuarioTextBox.TextLength > 0)
                    {
                        usuario.username = this.NombreUsuarioTextBox.Text;
                    }

                    this.Usuario_DataGridView.DataSource = usuario.getUsuarioLike();
                    MessageBox.Show("Se ha dado de baja el usuario '" + UsuarioBorrado + "' correctamente.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }

        private void ModificarButton_Click(object sender, EventArgs e)
        {
            if(this.Usuario_DataGridView.Rows.Count == 0)
            {
                return;
            }
            if (this.Usuario_DataGridView.CurrentCell == null)
            {
                MessageBox.Show("Debe seleccionar una fila.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            ModificarPasswordUsuarioForm abrir = new ModificarPasswordUsuarioForm(Usuario_DataGridView.CurrentRow.Cells[0].Value.ToString());
            DialogResult Resultado = abrir.ShowDialog();
            if (Resultado == DialogResult.OK)
            {
                ModDataGridView.limpiarDataGridView(Usuario_DataGridView, "");
            }
        }

        private void UsuariosForm_Load(object sender, EventArgs e)
        {
            this.Usuario_DataGridView.MultiSelect = false;
        }


    }
}
