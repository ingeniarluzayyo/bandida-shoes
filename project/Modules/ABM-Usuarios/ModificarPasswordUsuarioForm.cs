﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Data.SqlClient;
using System.Security.Cryptography;
using MySql.Data.MySqlClient;
using Zapateria.Libs;
using Zapateria.Models;

namespace Zapateria.Modules.ABM_Usuarios
{
    public partial class ModificarPasswordUsuarioForm : Form
    {
        public ModificarPasswordUsuarioForm(string name)
        {
            InitializeComponent();

            user = user.getByUsername(name);
        }

        Usuario user = new Usuario();

        private void GuardarButton_Click(object sender, EventArgs e)
        {
            if (Validaciones() == -1) return;

            user.username = this.NombreUsuarioTextBox.Text;
            user.email = this.EmailTextBox.Text;

            Sucursal sucursal = new Sucursal();
            user.sucursal_default = sucursal.getById(Convert.ToInt32(this.SucursalComboBox.SelectedValue.ToString()));

            if (ContraseñaTextBox.Text.Length > 0)
            {
                user.password = Security.Encriptar(this.ContraseñaTextBox.Text);
            }

            Rol rol = new Rol();
            rol = rol.getRoleById(Convert.ToInt32(RolComboBox.SelectedValue.ToString()));
            user.rol = rol;

            if (user.update() == true)
            {
                MessageBox.Show("Se ha modificado el usuario '" + NombreUsuarioTextBox.Text + "' correctamente.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                DialogResult = DialogResult.OK;
            } 
        }

        private void CancelarButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private int Validaciones()
        {
            if (SucursalComboBox.SelectedValue == null)
            {
                MessageBox.Show("Debe completar la sucursal.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return -1;
            }

            if (ContraseñaTextBox.Text.Length > 0 && ContraseñaTextBox.Text.Length < 6)
            {
                MessageBox.Show("Por razones de seguridad la contraseña debe contener como minimo seis caracteres.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return -1;

            }

            if (ContraseñaTextBox.Text != RepContraseñaTextBox.Text)
            {
                MessageBox.Show("Las contraseñas no coinciden. Por favor vuelva a escribirlas.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return -1;
            }

            return 1;
        }

        private void ModificarPasswordUsuarioForm_Load(object sender, EventArgs e)
        {
            this.NombreUsuarioTextBox.Text = user.username;
            this.EmailTextBox.Text = user.email;
            this.ContraseñaTextBox.Select();
            CargadorDeDatos.CargarRolesComboBox(RolComboBox, "Roles");
            CargadorDeDatos.CargarSucursalesComboBox(SucursalComboBox, "Sucursales");
  
            RolComboBox.SelectedValue = user.rol.id;
            SucursalComboBox.SelectedValue = user.sucursal_default.id;
        }

    }
}
