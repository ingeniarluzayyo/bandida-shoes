﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Data.SqlClient;
using System.Security.Cryptography;
using MySql.Data.MySqlClient;
using Zapateria.Libs;
using Zapateria.Models;


namespace Zapateria.Modules.ABM_Usuarios
{
    public partial class LoginForm : Form
    {
        public LoginForm()
        {
            InitializeComponent();
        }

        //encriptacion SHA256
        static string encriptar(string pass)
        {
            SHA256 ShaM = SHA256.Create();
            byte[] data = ShaM.ComputeHash(Encoding.Default.GetBytes(pass));
            StringBuilder sbuilder = new StringBuilder();

            for (int i = 0; i < data.Length; i++)
            {
                sbuilder.Append(data[i].ToString("x2"));
            }

            return sbuilder.ToString();
        }

        private void cancelarButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void loginButton_Click(object sender, EventArgs e)
        {
            
            if (Validaciones() == -1) return;

            Usuario user = new Usuario();
            user.username = this.usuarioTextBox.Text;
            user.password = this.passwordTextBox.Text;

            if (user.getUsuarios().Count() == 1)
            {
                DialogResult = DialogResult.OK;
            }
            else
            {
                MessageBox.Show("Usuario o contraseña inválidos.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            
        }

        
        
        private int Validaciones()
        {
            if (usuarioTextBox.Text == "")
            {
                MessageBox.Show("Debe completar el campo usuario.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return -1;
            }
            if (passwordTextBox.Text == "")
            {
                MessageBox.Show("Debe completar el campo contraseña.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return -1;
            }

            Security Busqueda = new Security();
            if (!Busqueda.ValidarSqlInjection(this.GroupBox)) { return -1; }

            return 0;
        }

        private void LoginForm_Load(object sender, EventArgs e)
        {
            this.usuarioTextBox.Text = Zapateria.Properties.Settings.Default.User;
            this.passwordTextBox.Select();
        }



    }
}
