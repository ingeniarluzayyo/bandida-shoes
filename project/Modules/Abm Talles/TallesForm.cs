﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using MySql.Data.MySqlClient;
using Zapateria.Libs;
using Zapateria.Models;

namespace Zapateria.Abm_Talles
{
    public partial class TallesForm : Form
    {
        public TallesForm()
        {
            InitializeComponent();
        }

        private void AgregarButton_Click(object sender, EventArgs e)
        {
            AltaTallesForm abrir = new AltaTallesForm();
            abrir.ShowDialog();
        }

        private void EliminarButton_Click(object sender, EventArgs e)
        {
            if (this.Talles_DataGridView.Rows.Count == 0)
            {
                return;
            }

            if (this.Talles_DataGridView.CurrentCell == null)
            {
                MessageBox.Show("Debe seleccionar una fila.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            string talleBorrado = Talles_DataGridView.CurrentRow.Cells["Nombre"].Value.ToString();

            DialogResult Resultado = MessageBox.Show("¿Está seguro de que desea eliminar el talle '" + talleBorrado + "'?", this.Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (Resultado == DialogResult.Yes)
            {
                Talle talle = new Talle();
                talle.id = Convert.ToInt32(Talles_DataGridView.CurrentRow.Cells["id"].Value);

                if (talle.delete() == true)
                {
                    if (this.TalleTextBox.TextLength > 0)
                    {
                        talle.nombre = this.TalleTextBox.Text;
                    }

                    this.Talles_DataGridView.DataSource = talle.getTallesLike();

                    MessageBox.Show("Se ha dado de baja el talle '" + talleBorrado + "' correctamente.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }

        private void ModificarButton_Click(object sender, EventArgs e)
        {
            if (this.Talles_DataGridView.Rows.Count == 0)
            {
                return;
            }
            if (this.Talles_DataGridView.CurrentCell == null)
            {
                MessageBox.Show("Debe seleccionar una fila.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            Talle talle = new Talle();
            talle.id = Convert.ToInt32(this.Talles_DataGridView.CurrentRow.Cells["id"].Value.ToString());
            talle.nombre = this.Talles_DataGridView.CurrentRow.Cells["nombre"].Value.ToString();

            ModificarTalleForm abrir = new ModificarTalleForm(talle);
            DialogResult Resultado = abrir.ShowDialog();
            if (Resultado == DialogResult.OK)
            {
                ModDataGridView.limpiarDataGridView(Talles_DataGridView, "");
            }
        }

        private void BuscarButton_Click(object sender, EventArgs e)
        {
            Security Busqueda = new Security();
            if (!Busqueda.ValidarSqlInjection(this.GroupBox)) { return; }

            Talle talle = new Talle();

            if (TalleTextBox.TextLength > 0)
            {
                talle.nombre = this.TalleTextBox.Text;
            }

            var talles = talle.getTallesLike();

            if (talles.Count() == 0)
            {
                MessageBox.Show("No se encontraron resultados que coincidan con la busqueda.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            else
            {
                this.Talles_DataGridView.DataSource = talles;
                talle.customColumns(this.Talles_DataGridView);
            }

            this.Talles_DataGridView.Focus();
        }

        private void LimpiarButton_Click(object sender, EventArgs e)
        {
            foreach (Control objeto in this.GroupBox.Controls)
            {
                if (objeto is TextBox)
                {
                    ((TextBox)objeto).Clear();
                }
            }

            ModDataGridView.limpiarDataGridView(Talles_DataGridView, "");
        }

        private void CancelarButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void TallesForm_Load(object sender, EventArgs e)
        {
            this.Talles_DataGridView.MultiSelect = false;
            this.TalleTextBox.Select();
            this.BuscarButton.PerformClick();
        }
    }
}
