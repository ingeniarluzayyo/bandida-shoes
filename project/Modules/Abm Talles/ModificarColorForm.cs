﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using MySql.Data.MySqlClient;
using Zapateria.Models;

namespace Zapateria.Abm_Talles
{
    public partial class ModificarTalleForm : Form
    {
        Talle talle_load;

        public ModificarTalleForm(Talle talle)
        {
            InitializeComponent();

            talle_load = talle;
        }

        private void GuardarButton_Click(object sender, EventArgs e)
        {
            if (Validaciones() == -1) return;

            Talle talle = new Talle();
            talle.id = Convert.ToInt32(talle_load.id);
            talle.nombre = this.NombreTextBox.Text;

            if (talle.update() == true)
            {
                MessageBox.Show("Se ha modificado el talle correctamente.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                DialogResult = DialogResult.OK;
            }
        }

        private void LimpiarButton_Click(object sender, EventArgs e)
        {
            foreach (Control objeto in this.GroupBox.Controls)
            {
                if (objeto is TextBox)
                {
                    ((TextBox)objeto).Clear();
                }
            }
        }

        private void CancelarButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private int Validaciones()
        {
            foreach (Control objeto in this.GroupBox.Controls)
            {
                if (objeto is TextBox && ((TextBox)objeto).Text == "")
                {
                    MessageBox.Show("Debe completar el campo vacio.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return -1;
                }
            }
            return 1;
        }

        private void ModificarTalleForm_Load(object sender, EventArgs e)
        {
            this.NombreTextBox.Text = talle_load.nombre;
            this.NombreTextBox.Select();
        }
    }
}
