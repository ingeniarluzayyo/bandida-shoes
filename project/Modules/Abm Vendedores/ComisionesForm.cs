﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Zapateria.Libs;
using Zapateria.Models;
using System.Threading;


namespace Zapateria.Modules.Abm_Vendedores
{
    public partial class ComisionesForm : Form
    {
        int id_vendedor = 0;
        public ComisionesForm(int id)
        {
            InitializeComponent();
            id_vendedor = id;
        }

        private void CancelarButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ComisionesForm_Load(object sender, EventArgs e)
        {
            CargadorDeDatos.CargarVendedoresComboBox(VendedorComboBox, this.Text);

            this.DesdeDateTimePicker.Format = DateTimePickerFormat.Custom;
            this.DesdeDateTimePicker.CustomFormat = " ";
            this.HastaDateTimePicker.Format = DateTimePickerFormat.Custom;
            this.HastaDateTimePicker.CustomFormat = " ";
            this.BuscarButton.Select();

            VendedorComboBox.SelectedValue = id_vendedor;

            if (id_vendedor > 0)
            {
                BuscarButton.PerformClick();
            }
        }

        private void BuscarButton_Click(object sender, EventArgs e)
        {
            Security Busqueda = new Security();
            if (!Busqueda.ValidarSqlInjection(this.groupBox1)) { return; }

            if (VendedorComboBox.SelectedValue != null)
            {
                id_vendedor = Convert.ToInt32(VendedorComboBox.SelectedValue.ToString());
            }
            string desde = DesdeDateTimePicker.Text;
            string hasta = HastaDateTimePicker.Text;
            decimal total = 0;
            decimal total_c = 0;

            FacturaVenta venta = new FacturaVenta();
            var result = venta.Search(false, 0, 0, id_vendedor, desde, hasta,0);

            foreach (FacturaVenta v in result)
            {
                total += v.total;
                total_c += v.total_comision;
            }
            
            Comisiones_DataGridView.DataSource = result;
            venta.customColumns(Comisiones_DataGridView);

            TotalTextBox.Text = total.ToString();
            TotalComisionTextBox.Text = total_c.ToString();
        }

        private void LimpiarButton_Click(object sender, EventArgs e)
        {
            foreach (Control objeto in this.groupBox1.Controls)
            {
                if (objeto is TextBox)
                {
                    ((TextBox)objeto).Clear();
                }
                if (objeto is ComboBox)
                {
                    ((ComboBox)objeto).SelectedItem = null;
                }
            }
            
            this.DesdeDateTimePicker.Format = DateTimePickerFormat.Custom;
            this.DesdeDateTimePicker.CustomFormat = " ";

            this.HastaDateTimePicker.Format = DateTimePickerFormat.Custom;
            this.HastaDateTimePicker.CustomFormat = " ";
        }

        private void DesdeDateTimePicker_CloseUp(object sender, EventArgs e)
        {
            this.DesdeDateTimePicker.Format = DateTimePickerFormat.Short;
        }

        private void HastaDateTimePicker_CloseUp(object sender, EventArgs e)
        {
            this.HastaDateTimePicker.Format = DateTimePickerFormat.Short;
        }

        DateTime desde;
        DateTime hasta;

        private void button1_Click(object sender, EventArgs e)
        {
            if (DesdeDateTimePicker.Text == " " && HastaDateTimePicker.Text == " ")
            {
                MessageBox.Show("Debe seleccionar un rango de fechas para imprimir el listado.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            BuscarButton.PerformClick();

            desde = DesdeDateTimePicker.Value;
            hasta = HastaDateTimePicker.Value;

            Thread hilo = new Thread(AbrirFormReporteCajaDiaria);
            hilo.SetApartmentState(System.Threading.ApartmentState.STA);
            hilo.Start();
        }

        private void AbrirFormReporteCajaDiaria()
        {
            IList<FacturaVenta> list = (IList<FacturaVenta>)Comisiones_DataGridView.DataSource;
            decimal total = 0;
            decimal comisiones = 0;

            foreach (FacturaVenta v in list)
            {
                total += v.total;
                comisiones += v.total_comision;
            }

            DataTable dt = DataUtil.ToDataTables((IList<FacturaVenta>)list);
            ListadoComisionesReporteForm abrir = new ListadoComisionesReporteForm(desde, hasta, total.ToString(), comisiones.ToString(), dt);
            abrir.ShowDialog();
        }
    }
}
