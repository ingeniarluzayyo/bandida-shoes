﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Zapateria.Libs;
using Zapateria.Models;

namespace Zapateria.Abm_Vendedores
{
    public partial class AltaVendedorForm : Form
    {
        public AltaVendedorForm()
        {
            InitializeComponent();
        }

        private void CancelarButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void LimpiarButton_Click(object sender, EventArgs e)
        {
            foreach (Control objeto in this.GroupBox.Controls)
            {
                if (objeto is TextBox)
                {
                    ((TextBox)objeto).Clear();
                }
                if (objeto is ComboBox)
                {
                    ((ComboBox)objeto).SelectedItem = null;
                }
            }
        }

        private void GuardarButton_Click(object sender, EventArgs e)
        {
            if (Validaciones() == -1) return;

            Vendedor vendedor = new Vendedor();

            Sucursal sucursal = new Sucursal();
            vendedor.sucursal = sucursal.getById(Convert.ToInt32(this.SucursalComboBox.SelectedValue.ToString()));

            vendedor.nombre = this.NombreTextBox.Text;
            vendedor.apellido = this.ApellidoTextBox.Text;
            vendedor.dni = this.DniTextBox.Text;
            vendedor.fechanacimiento = this.FechaDateTimePicker.Value;
            vendedor.tel = this.TelTextBox.Text;
            vendedor.cel = this.CelTextBox.Text;
            vendedor.direccion = this.DirTextBox.Text;
            vendedor.mail =  this.MailTextBox.Text;
            vendedor.observacion = this.NotaTextBox.Text;
            vendedor.comision = Convert.ToDecimal(comisionTextBox.Text);
            vendedor.username = this.UsuarioTextBox.Text;
            vendedor.password = Security.Encriptar(this.PassTextBox.Text);
                     

            if (vendedor.save())
            {
                MessageBox.Show("Se ha dado de alta el cliente '" + NombreTextBox.Text + "' correctamente.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.LimpiarButton.PerformClick();
                this.NombreTextBox.Focus();
            } 
        }

        private int Validaciones()
        {

            if (NombreTextBox.Text == "" || ApellidoTextBox.Text == "" || this.UsuarioTextBox.Text == "" || this.PassTextBox.Text == "")
            {
                MessageBox.Show("Debe completar los campos obligatorios.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return -1;
            }

            if (SucursalComboBox.SelectedValue == null)
            {
                MessageBox.Show("Debe completar la sucursal.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return -1;
            }

            decimal aux;
            if (!Decimal.TryParse(comisionTextBox.Text,out aux))
            {
                MessageBox.Show("La comisión debe ser un número.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return -1;
            }

            return 1;
        }

        private void AltaClienteForm_Load(object sender, EventArgs e)
        {
            CargadorDeDatos.CargarSucursalesComboBox(SucursalComboBox, this.Text);

            Usuario user = Main.getUsuario();
            SucursalComboBox.SelectedValue = user.sucursal_default.id;
        }

        private void GroupBox_Enter(object sender, EventArgs e)
        {

        }


    }
}
