﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using Zapateria.Models;
using Zapateria.Libs;

namespace Zapateria.Abm_Vendedores
{
    public partial class ModificarVendedorForm : Form
    {
        public ModificarVendedorForm(string id)
        {
            InitializeComponent();

            this.id = id;
        }

        string id;
        Vendedor vendedor = new Vendedor();

        private void GuardarButton_Click(object sender, EventArgs e)
        {
            if (Validaciones() == -1) return;

            Sucursal sucursal = new Sucursal();
            vendedor.sucursal = sucursal.getById(Convert.ToInt32(this.SucursalComboBox.SelectedValue.ToString()));

            vendedor.nombre = this.NombreTextBox.Text;
            vendedor.apellido = this.ApellidoTextBox.Text;
            vendedor.dni = this.DniTextBox.Text;
            vendedor.fechanacimiento = this.FechaDateTimePicker.Value;
            vendedor.tel = this.TelTextBox.Text;
            vendedor.cel = this.CelTextBox.Text;
            vendedor.direccion = this.DirTextBox.Text;
            vendedor.mail = this.MailTextBox.Text;
            vendedor.observacion = this.NotaTextBox.Text;
            vendedor.comision = Convert.ToDecimal(comisionTextBox.Text);
            vendedor.username = this.UsuarioTextBox.Text;
            if (this.PassTextBox.Text.Length > 0)
            {
                vendedor.password = Security.Encriptar(this.PassTextBox.Text);
            }

            if (vendedor.update())
            {
                MessageBox.Show("Se ha modificado el vendedor '" + NombreTextBox.Text + " " + ApellidoTextBox.Text + "' correctamente.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                DialogResult = DialogResult.OK;
            } 
        }

        private void LimpiarButton_Click(object sender, EventArgs e)
        {
            foreach (Control objeto in this.GroupBox.Controls)
            {
                if (objeto is TextBox)
                {
                    ((TextBox)objeto).Clear();
                }
                if (objeto is ComboBox)
                {
                    ((ComboBox)objeto).SelectedItem = null;
                }
            }
        }

        private int Validaciones()
        {

            if (NombreTextBox.Text == "" || ApellidoTextBox.Text == "" || this.UsuarioTextBox.Text == "" )
            {
                MessageBox.Show("Debe completar los campos obligatorios.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return -1;
            }

            if (SucursalComboBox.SelectedValue == null)
            {
                MessageBox.Show("Debe completar la sucursal.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return -1;
            }

            decimal aux;
            if (!Decimal.TryParse(comisionTextBox.Text, out aux))
            {
                MessageBox.Show("La comisión debe ser un número.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return -1;
            }

            return 1;
        }


        private void CancelarButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ModificarClienteForm_Load(object sender, EventArgs e)
        {
            CargadorDeDatos.CargarSucursalesComboBox(SucursalComboBox, this.Text);

            vendedor = vendedor.getById(Convert.ToInt32(id));

            this.SucursalComboBox.SelectedValue = vendedor.sucursal.id;

            this.NombreTextBox.Text = vendedor.nombre;
            this.ApellidoTextBox.Text = vendedor.apellido;
            this.DniTextBox.Text = vendedor.dni;
            this.FechaDateTimePicker.Value = vendedor.fechanacimiento;
            this.TelTextBox.Text = vendedor.tel;
            this.CelTextBox.Text = vendedor.cel;
            this.DirTextBox.Text = vendedor.direccion;
            this.MailTextBox.Text = vendedor.mail;
            this.NotaTextBox.Text = vendedor.observacion;
            this.comisionTextBox.Text = vendedor.comision.ToString();
            this.UsuarioTextBox.Text = vendedor.username;
        }
    }
}
