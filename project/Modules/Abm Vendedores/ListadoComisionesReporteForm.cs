﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Zapateria.Modules.Reportes;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;

namespace Zapateria.Modules.Abm_Vendedores
{
    public partial class ListadoComisionesReporteForm : Form
    {
        string desde;
        string desde_text;
        string hasta;
        string hasta_text;
        DataTable dt;
        string total;
        string comisiones;

        public ListadoComisionesReporteForm(DateTime desde, DateTime hasta, string total,string comisiones, DataTable dt)
        {
            InitializeComponent();

            this.desde_text = desde.ToShortDateString();
            this.desde = desde.ToString("yyyy-MM-dd");

            this.hasta_text = hasta.ToShortDateString();
            this.hasta = hasta.ToString("yyyy-MM-dd");

            this.dt = dt;
            this.total = total;
            this.comisiones = comisiones;
        }

        private void crystalReportViewer1_Load(object sender, EventArgs e)
        {
            ListadoComisionesCrystalReport Repo = new ListadoComisionesCrystalReport();

            //Variables
            TextObject DesdeRepo;
            DesdeRepo = (TextObject)Repo.ReportDefinition.ReportObjects["DesdeText"];
            DesdeRepo.Text = desde_text;

            TextObject HastaRepo;
            HastaRepo = (TextObject)Repo.ReportDefinition.ReportObjects["HastaText"];
            HastaRepo.Text = desde_text;

            TextObject TotalRepo;
            TotalRepo = (TextObject)Repo.ReportDefinition.ReportObjects["TotalText"];
            TotalRepo.Text = total;

            TextObject ComisionesRepo;
            ComisionesRepo = (TextObject)Repo.ReportDefinition.ReportObjects["ComisionesText"];
            ComisionesRepo.Text = comisiones;

            /*
            TextObject ClienteRepo;
            ClienteRepo = (TextObject)Repo.ReportDefinition.ReportObjects["ClienteText"];
            ClienteRepo.Text = "Cliente";

            TextObject VendedorRepo;
            VendedorRepo = (TextObject)Repo.ReportDefinition.ReportObjects["VendedorText"];
            VendedorRepo.Text = "Vendedor";
             * */

            Repo.SetDataSource(dt);

            crystalReportViewer1.ReportSource = Repo;
        }
    }
}
