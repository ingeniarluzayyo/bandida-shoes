﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Zapateria.Libs;
using Zapateria.Models;
using Zapateria.Modules.Abm_Vendedores;

using MySql.Data.MySqlClient;

namespace Zapateria.Abm_Vendedores
{
    public partial class VendedorForm : Form
    {
        public VendedorForm()
        {
            InitializeComponent();
        }

        private void CancelarButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void LimpiarButton_Click(object sender, EventArgs e)
        {
            foreach (Control objeto in this.GroupBox.Controls)
            {
                if (objeto is TextBox)
                {
                    ((TextBox)objeto).Clear();
                }
                if (objeto is ComboBox)
                {
                    ((ComboBox)objeto).SelectedItem = null;
                }
            }

            ModDataGridView.limpiarDataGridView(Clientes_DataGridView, "");
        }

        private string ArmarConsulta()
        {
            string Consulta = "SELECT C.id,S.nombre as Sucursal,C.nombre as Nombre,apellido as Apellido,dni as Dni,comision as Comision,fechanacimiento as 'Fecha Nacimiento',tel as Tel,cel as Cel,direccion as Direccion,mail as 'E-mail' FROM vendedores as C INNER JOIN sucursal as S ON S.id = C.sucursal WHERE 1=1 ";

            if (NombreTextBox.TextLength > 0)
            {
                Consulta += "AND C.nombre LIKE '%" + NombreTextBox.Text + "%' ";
            }            

            if (ApellidoTextBox.TextLength > 0)
            {
                Consulta += "AND apellido LIKE '%" + ApellidoTextBox.Text + "%' ";
            }

            if (SucursalComboBox.SelectedValue !=  null)
            {
                Consulta += "AND C.sucursal = " + SucursalComboBox.SelectedValue.ToString() + " ";
            }

            if (DniTextBox.TextLength > 0)
            {
                Consulta += "AND dni = '" + DniTextBox.Text + "' ";
            }


            return Consulta;
        }


        private void BuscarButton_Click(object sender, EventArgs e)
        {
            Security Busqueda = new Security();
            if (!Busqueda.ValidarSqlInjection(this.GroupBox)) { return; }

            string Consulta = ArmarConsulta();

            List<MySqlParameter> param = new List<MySqlParameter>();

            Clientes_DataGridView.DataSource = BaseDeDatos.GetInstance.ExecuteCustomQuery(Consulta, param, this.Text);


            if (Clientes_DataGridView.DataSource == null)
            {
                MessageBox.Show("No se encontraron resultados que coincidan con la busqueda.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            this.Clientes_DataGridView.Columns["id"].Visible = false;
            this.Clientes_DataGridView.Columns["Nombre"].Width = 170;
            this.Clientes_DataGridView.Columns["Apellido"].Width = 170;
            this.Clientes_DataGridView.Columns["Comision"].Width = 100;
            this.Clientes_DataGridView.Columns["Direccion"].Width = 200;
            this.Clientes_DataGridView.Focus();
        }

        private void AgregarButton_Click(object sender, EventArgs e)
        {
            AltaVendedorForm abrir = new AltaVendedorForm();
            abrir.ShowDialog();
        }

        private void EliminarButton_Click(object sender, EventArgs e)
        {
            if (this.Clientes_DataGridView.Rows.Count == 0)
            {
                return;
            }

            if (this.Clientes_DataGridView.CurrentCell == null)
            {
                MessageBox.Show("Debe seleccionar una fila.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            string ClienteBorrado = Clientes_DataGridView.CurrentRow.Cells["Nombre"].Value.ToString() + " " + Clientes_DataGridView.CurrentRow.Cells["Apellido"].Value.ToString();

            DialogResult Resultado = MessageBox.Show("¿Esta seguro que desea eliminar el cliente '" + ClienteBorrado + "'?", this.Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (Resultado == DialogResult.Yes)
            {
                Vendedor vendedor = new Vendedor();
                vendedor = vendedor.getById(Convert.ToInt32(Clientes_DataGridView.CurrentRow.Cells["id"].Value));

                if (vendedor.delete())
                {
                    Clientes_DataGridView.Rows.Remove(Clientes_DataGridView.CurrentRow);
                    MessageBox.Show("Se ha dado de baja el cliente '" + ClienteBorrado + "' correctamente.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }

        private void ModificarButton_Click(object sender, EventArgs e)
        {
            if (this.Clientes_DataGridView.Rows.Count == 0)
            {
                return;
            }

            if (this.Clientes_DataGridView.CurrentCell == null)
            {
                MessageBox.Show("Debe seleccionar una fila.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            string id = Clientes_DataGridView.CurrentRow.Cells["id"].Value.ToString();

            ModificarVendedorForm abrir = new ModificarVendedorForm(id);
            DialogResult Resultado = abrir.ShowDialog();
            if (Resultado == DialogResult.OK)
            {
                ModDataGridView.limpiarDataGridView(Clientes_DataGridView, "");
            }
        }

        private void VendedoresForm_Load(object sender, EventArgs e)
        {
            CargadorDeDatos.CargarSucursalesComboBox(SucursalComboBox, this.Text);
            this.BuscarButton.Select();            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int id = 0;
            if (this.Clientes_DataGridView.Rows.Count != 0 && this.Clientes_DataGridView.CurrentCell != null)
            {
                id = Convert.ToInt32(Clientes_DataGridView.CurrentRow.Cells["id"].Value.ToString());
            }           

            ComisionesForm f = new ComisionesForm(id);
            f.ShowDialog();
        }
    }
}
