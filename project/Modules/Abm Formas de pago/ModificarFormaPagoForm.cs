﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Zapateria.Models;


namespace Zapateria.Abm_Formas_de_pago
{
    public partial class ModificarFormaPagoForm : Form
    {
        public ModificarFormaPagoForm(int Id)
        {
            InitializeComponent();

            formaPago = formaPago.getById(Id);
        }

        FormaPago formaPago = new FormaPago();

        private void GuardarButton_Click(object sender, EventArgs e)
        {

            if (Validaciones() == -1) return;

            formaPago.nombre = this.Forma_de_pagoTextBox.Text;

            if (formaPago.update())
            {
                MessageBox.Show("Se ha modificado la forma de pago '" + formaPago.nombre + "' por '" + Forma_de_pagoTextBox.Text + "' correctamente.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                DialogResult = DialogResult.OK;
            } 

        }

        private void LimpiarButton_Click(object sender, EventArgs e)
        {
            foreach (Control objeto in this.GroupBox.Controls)
            {
                if (objeto is TextBox)
                {
                    ((TextBox)objeto).Clear();
                }
            }
        }

        private void CancelarButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        private int Validaciones()
        {
            foreach (Control objeto in this.GroupBox.Controls)
            {
                if (objeto is TextBox && ((TextBox)objeto).Text == "")
                {
                    MessageBox.Show("Debe completar el campo vacio.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return -1;
                }
            }
            return 1;
        }


        private void ModificarFormaPagoForm_Load(object sender, EventArgs e)
        {
            this.Forma_de_pagoTextBox.Text = formaPago.nombre;
            this.Forma_de_pagoTextBox.Select();
        }
    }
}
