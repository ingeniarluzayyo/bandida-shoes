﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using MySql.Data.MySqlClient;
using Zapateria.Libs;
using Zapateria.Models;

namespace Zapateria.Abm_Formas_de_pago
{
    public partial class FormaPagoForm : Form
    {
        public FormaPagoForm()
        {
            InitializeComponent();
        }

        private void AgregarButton_Click(object sender, EventArgs e)
        {
            AltaFormaPagoForm abrir = new AltaFormaPagoForm();
            abrir.ShowDialog();
        }

        private void EliminarButton_Click(object sender, EventArgs e)
        {
            if (this.Forma_de_pago_DataGridView.Rows.Count == 0 )
            {
                return;
            }

            if (this.Forma_de_pago_DataGridView.CurrentCell == null)
            {
                MessageBox.Show("Debe seleccionar una fila.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            
            string Forma_de_pagoBorrada = Forma_de_pago_DataGridView.CurrentRow.Cells["Forma de pago"].Value.ToString();

            DialogResult Resultado = MessageBox.Show("¿Esta seguro que desea eliminar la forma de pago '" + Forma_de_pagoBorrada + "'?", this.Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (Resultado == DialogResult.Yes)
            {
                FormaPago fp = new FormaPago();
                fp = fp.getById(Convert.ToInt32(Forma_de_pago_DataGridView.CurrentRow.Cells["id"].Value.ToString()));                

                if (fp.delete())
                {
                    Forma_de_pago_DataGridView.Rows.Remove(Forma_de_pago_DataGridView.CurrentRow);
                    MessageBox.Show("Se ha dado de baja la forma de pago '" + Forma_de_pagoBorrada + "' correctamente.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }

        private void ModificarButton_Click(object sender, EventArgs e)
        {
            if (this.Forma_de_pago_DataGridView.Rows.Count == 0)
            {
                return;
            }
            if (this.Forma_de_pago_DataGridView.CurrentCell == null)
            {
                MessageBox.Show("Debe seleccionar una fila.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            

            ModificarFormaPagoForm abrir = new ModificarFormaPagoForm(Convert.ToInt32(this.Forma_de_pago_DataGridView.CurrentRow.Cells["id"].Value.ToString()));
            DialogResult Resultado = abrir.ShowDialog();
            if (Resultado == DialogResult.OK)
            {
                ModDataGridView.limpiarDataGridView(Forma_de_pago_DataGridView, "");
            }
        }

        private void BuscarButton_Click(object sender, EventArgs e)
        {
            Security Busqueda = new Security();
            if (!Busqueda.ValidarSqlInjection(this.GroupBox)) { return; }

            string Consulta = ArmarConsulta();

            List<MySqlParameter> param = new List<MySqlParameter>();


            Forma_de_pago_DataGridView.DataSource = BaseDeDatos.GetInstance.ExecuteCustomQuery(Consulta, param, this.Text);
            

            if (Forma_de_pago_DataGridView.DataSource == null)
            {
                MessageBox.Show("No se encontraron resultados que coincidan con la busqueda.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            this.Forma_de_pago_DataGridView.Columns["id"].Visible = false;
            this.Forma_de_pago_DataGridView.Columns["Forma de pago"].Width = 200;
            this.Forma_de_pago_DataGridView.Focus();
        }

        private void LimpiarButton_Click(object sender, EventArgs e)
        {
            foreach (Control objeto in this.GroupBox.Controls)
            {
                if (objeto is TextBox)
                {
                    ((TextBox)objeto).Clear();
                }
            }

            ModDataGridView.limpiarDataGridView(Forma_de_pago_DataGridView, "");
        }

        private void CancelarButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private string ArmarConsulta()
        {
            string Consulta = "SELECT id,nombre as 'Forma de pago' FROM forma_de_pago where 1=1 ";

            if (CategoriaTextBox.TextLength > 0)
            {
                Consulta += "AND nombre='" + CategoriaTextBox.Text + "'";
            }

            return Consulta;
        }

        private void CategoriasForm_Load(object sender, EventArgs e)
        {
            this.Forma_de_pago_DataGridView.MultiSelect = false;
            this.CategoriaTextBox.Select();
        }


    }
}
