﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using MySql.Data.MySqlClient;
using Zapateria.Models;
using Zapateria.Libs;

namespace Zapateria.Abm_Categorias
{
    public partial class AltaCategoriaForm : Form
    {
        public AltaCategoriaForm()
        {
            InitializeComponent();
        }

        private void GuardarButton_Click(object sender, EventArgs e)
        {
            if (Validaciones() == -1) return;

            Familia categoria = new Familia();
            categoria.nombre = this.CategoriaTextBox.Text;

            if (categoria.save() == true)
            {
                MessageBox.Show("Se ha dado de alta la familia '" + CategoriaTextBox.Text + "' correctamente.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.LimpiarButton.PerformClick();
                this.CategoriaTextBox.Focus();
            } 

        }

        private void LimpiarButton_Click(object sender, EventArgs e)
        {
            foreach (Control objeto in this.GroupBox.Controls)
            {
                if (objeto is TextBox)
                {
                    ((TextBox)objeto).Clear();
                }                
            }
        }

        private void CancelarButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        private int Validaciones()
        {
            foreach (Control objeto in this.GroupBox.Controls)
            {
                if (objeto is TextBox && ((TextBox)objeto).Text == "")
                {
                    MessageBox.Show("Debe completar el campo vacio.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return -1;
                }
            }
            return 1;
        }

        private void AltaCategoriaForm_Load(object sender, EventArgs e)
        {
            this.CategoriaTextBox.Select();
        }

    }
}
