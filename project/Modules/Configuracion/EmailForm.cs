﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Zapateria.Models;
using System.IO;
using Zapateria.Libs;

namespace Zapateria.Modules.Configuracion
{
    public partial class EmailForm : Form
    {
        public EmailForm()
        {
            InitializeComponent();
            empresa = empresa.getById(1);
        }

        Empresa empresa = new Empresa();

        private void CancelarButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void EmailForm_Load(object sender, EventArgs e)
        {
            hostTextBox.Text = empresa.servidor;
            portTextBox.Text = empresa.puerto;
            emailTextBox.Text = empresa.email;
            passwordTextBox.Text = empresa.pass;                   
        }

        private void GuardarButton_Click(object sender, EventArgs e)
        {
            if (Validaciones_guardar() == -1) return;

            empresa.servidor = hostTextBox.Text;
            empresa.puerto = portTextBox.Text;
            empresa.email = emailTextBox.Text;
            empresa.pass = passwordTextBox.Text;

            if (empresa.update())
            {
                MessageBox.Show("Se ha guardado la configuracion del email correctamente.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);                               
            } 
        }

        private int Validaciones_guardar()
        {
            if (!Validacion.isNumber(portTextBox.Text))
            {
                MessageBox.Show("Debe ingresar un puerto valido.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return -1;
            }

            if (!Validacion.isValidEmail(emailTextBox.Text))
            {
                MessageBox.Show("Debe ingresar un email valido.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return -1;
            }

            return 1;
        }

        private void testButton_Click(object sender, EventArgs e)
        {
            Mailer mailer = new Mailer();

            if (!Validacion.isValidEmail(emailTextBox.Text) || !Validacion.isValidEmail(emailTestTextBox.Text))
            {
                MessageBox.Show("Debe ingresar un email valido.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if (!Validacion.isNumber(portTextBox.Text)) {
                MessageBox.Show("Debe ingresar un puerto valido.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            mailer.host = hostTextBox.Text;
            mailer.port = Convert.ToInt32(portTextBox.Text);
            mailer.email = emailTextBox.Text;
            mailer.password = passwordTextBox.Text;
           
            if(mailer.send(emailTextBox.Text, emailTestTextBox.Text, "Email prueba", "Email de prueba")){
                MessageBox.Show("Se envio correctamente el Email.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }else{
                MessageBox.Show("Ocurrio un error al enviar el Email.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }     
      
    }
}
