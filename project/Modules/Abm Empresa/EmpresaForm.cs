﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Zapateria.Models;
using System.IO;
using Zapateria.Libs;

namespace Zapateria.Modules.Abm_Empresa
{
    public partial class EmpresaForm : Form
    {
        public EmpresaForm()
        {
            InitializeComponent();
            empresa = empresa.getById(1);
        }

        Empresa empresa = new Empresa();

        private void CancelarButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void EmpresaForm_Load(object sender, EventArgs e)
        {
            RazonSocialTextBox.Text = empresa.razon_social;
            CuitTextBox.Text = empresa.cuit;
            TelefonoTextBox.Text = empresa.telefono;
            DireccionTextBox.Text = empresa.direccion;
            etiquetaRichTextBox.Text = empresa.etiqueta;

            pictureBox1.Image = ManejoImagenes.byteToImage(empresa.imagen);                        
        }

        private void GuardarButton_Click(object sender, EventArgs e)
        {
            if (Validaciones_guardar() == -1) return;

            empresa.razon_social = RazonSocialTextBox.Text;
            empresa.cuit = CuitTextBox.Text;
            empresa.telefono = TelefonoTextBox.Text;
            empresa.direccion = DireccionTextBox.Text;
            empresa.etiqueta = etiquetaRichTextBox.Text;

            //empresa.imagen = ManejoImagenes.imagetoByte(pictureBox1.Image);

            if (empresa.update())
            {
                MessageBox.Show("Se ha actualizado la información de la empresa correctamente.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);                               
            } 
        }

        private int Validaciones_guardar()
        {
            if (RazonSocialTextBox.Text == "")
            {
                MessageBox.Show("Debe completar los campos vacios.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return -1;
            }

            /*Image img = pictureBox1.Image;
            if (img.Width > 500 || img.Height > 500 || img.Width != img.Height)
            {
                MessageBox.Show("La imagen debe ser inferior a 500px y cuadrada.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return -1;
            }*/
            
            return 1;
        }        

        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog fdlg = new OpenFileDialog();
            fdlg.Title = "Logo de la empresa";
            fdlg.InitialDirectory = @"c:\";
            fdlg.Filter = "Image Files|*.jpg;*.jpeg;*.png;";
            //fdlg.Filter = "All files (*.png)|All files (*.jpg)";
            fdlg.FilterIndex = 2;
            fdlg.RestoreDirectory = true;
            if (fdlg.ShowDialog() == DialogResult.OK)
            {
                string file = fdlg.FileName;

                string sourceFile = @file;
                string destinationFile = @AppDomain.CurrentDomain.BaseDirectory;

                // To move a file or folder to a new location:
                //System.IO.File.Move(sourceFile, destinationFile);

                pictureBox1.Image = Image.FromFile(file);
            }
        }
    }
}
