﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Zapateria.Models;
using Zapateria.Libs;

namespace Zapateria.Modules.Abm_Gastos_Varios
{
    public partial class GastosVariosForm : Form
    {
        public GastosVariosForm()
        {
            InitializeComponent();
        }

        private void CancelarButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void LimpiarButton_Click(object sender, EventArgs e)
        {
            foreach (Control objeto in this.GroupBox.Controls)
            {
                if (objeto is TextBox)
                {
                    ((TextBox)objeto).Text = "";
                }
                if (objeto is ComboBox)
                {
                    ((ComboBox)objeto).SelectedItem = null;
                }
            }

            this.DesdeDateTimePicker.Value = DateTime.Today;
            this.HastaDateTimePicker.Value = DateTime.Today;

            ModDataGridView.limpiarDataGridView(GastosVarios_DataGridView, "");
        }

        private void AgregarButton_Click(object sender, EventArgs e)
        {
            AltaGastoForm abrir = new AltaGastoForm();
            DialogResult resultado = abrir.ShowDialog();

            if (resultado == DialogResult.OK) {
                this.LimpiarButton.PerformClick();
                this.BuscarButton.PerformClick();
            }
        }

        private void GastosVariosForm_Load(object sender, EventArgs e)
        {
            /*this.DesdeDateTimePicker.Format = DateTimePickerFormat.Custom;
            this.DesdeDateTimePicker.CustomFormat = " ";

            this.HastaDateTimePicker.Format = DateTimePickerFormat.Custom;
            this.HastaDateTimePicker.CustomFormat = " ";*/

            CargadorDeDatos.CargarFamiliasGastosComboBox(this.FamiliaComboBox, this.Text);
            CargadorDeDatos.CargarSucursalesComboBox(SucursalComboBox, this.Text);
            this.BuscarButton.PerformClick();
        }

        private void BuscarButton_Click(object sender, EventArgs e)
        {
            Security Busqueda = new Security();
            if (!Busqueda.ValidarSqlInjection(this.GroupBox)) { return; }

            Gasto gasto = new Gasto();

  
            int familia_id = 0;
            int sucursal_id = 0;
            int subfamilia_id = 0;
            string desde = "";
            string hasta = "";

            if (this.FamiliaComboBox.SelectedItem != null)
            {
                familia_id = Convert.ToInt32(FamiliaComboBox.SelectedValue.ToString());
            }

            if (this.SucursalComboBox.SelectedItem != null)
            {
                sucursal_id = Convert.ToInt32(SucursalComboBox.SelectedValue.ToString());
            }

            if (this.SubfamiliaComboBox.SelectedItem != null)
            {
                subfamilia_id = Convert.ToInt32(SubfamiliaComboBox.SelectedValue.ToString());
            }

            if (DesdeDateTimePicker.Text != " ")
            {
                desde = DesdeDateTimePicker.Value.ToShortDateString();
            }

            if (HastaDateTimePicker.Text != " ")
            {
                hasta = HastaDateTimePicker.Value.ToShortDateString();
            }


            GastosVarios_DataGridView.DataSource = gasto.Search(AnuladoCheckBox.Checked, familia_id, subfamilia_id,sucursal_id, desde, hasta);

            if (GastosVarios_DataGridView.DataSource == null)
            {
                ModDataGridView.limpiarDataGridView(GastosVarios_DataGridView, "Ver Detalle");
                MessageBox.Show("No se encontraron resultados que coincidan con la busqueda.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            gasto.customColumns(GastosVarios_DataGridView);
            this.GastosVarios_DataGridView.Focus();
        }

        private void EliminarButton_Click(object sender, EventArgs e)
        {
            if (this.GastosVarios_DataGridView.Rows.Count == 0)
            {
                return;
            }

            if (this.GastosVarios_DataGridView.CurrentCell == null)
            {
                MessageBox.Show("Debe seleccionar una fila.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            string GastoAnulada = GastosVarios_DataGridView.CurrentRow.Cells["id"].Value.ToString();

            DialogResult Resultado = MessageBox.Show("¿Esta seguro que desea anular el gasto Nº '" + GastoAnulada + "'?", this.Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (Resultado == DialogResult.Yes)
            {
                Gasto gasto = new Gasto();
                gasto = gasto.getById(Convert.ToInt32(GastoAnulada));

                if (gasto.anulado)
                {
                    MessageBox.Show("El gasto Nº '" + GastoAnulada + " ya estaba anulado.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return;
                }

                gasto.anulado = true;
                if (gasto.update())
                {
                    BuscarButton.PerformClick();
                    MessageBox.Show("Se ha anulado el gasto Nº '" + GastoAnulada + "' correctamente.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

            }
        }

        private void FamiliaComboBox_SelectionChangeCommitted(object sender, EventArgs e)
        {
            CargadorDeDatos.CargarSubFamiliasGastosComboBox(this.SubfamiliaComboBox, this.Text, Convert.ToInt32(this.FamiliaComboBox.SelectedValue.ToString()));
        }
    }
}
