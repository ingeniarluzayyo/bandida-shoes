﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Zapateria.Models;
using Zapateria.Libs;

namespace Zapateria.Modules.Abm_Gastos_Varios
{
    public partial class AltaGastoForm : Form
    {
        public AltaGastoForm()
        {
            InitializeComponent();
        }

        private void CancelarButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void GuardarButton_Click(object sender, EventArgs e)
        {
            if (Validaciones() == -1) return;

            Gasto gasto = new Gasto();
            gasto.fecha = FechaDateTimePicker.Value;
            gasto.monto = Convert.ToDecimal(this.MontoTextBox.Text);

            FormaPago forma_de_pago = new FormaPago();
            gasto.forma_de_pago = forma_de_pago.getById(Convert.ToInt32(formaDePagoComboBox.SelectedValue.ToString()));

            FamiliaGasto familia = new FamiliaGasto();
            gasto.familia = familia.getById(Convert.ToInt32(this.FamiliaComboBox.SelectedValue.ToString()));

            Sucursal sucursal = new Sucursal();
            gasto.sucursal = sucursal.getById(Convert.ToInt32(this.SucursalComboBox.SelectedValue.ToString()));

            if (this.SubfamiliaComboBox.SelectedValue != null)
            {
                SubfamiliaGasto subfamilia = new SubfamiliaGasto();
                gasto.subfamilia_gasto = subfamilia.getById(Convert.ToInt32(this.SubfamiliaComboBox.SelectedValue.ToString()));
            }

            Vendedor vendedor = new Vendedor();
            if (this.vendedorComboBox.SelectedItem != null)
            {
                gasto.vendedor = vendedor.getById(Convert.ToInt32(this.vendedorComboBox.SelectedValue.ToString()));
            }

            gasto.anulado = false;
            gasto.descripcion = DescripcionTextBox.Text;

            if (gasto.save())
            {
                MessageBox.Show("Se ha realizado el gasto correctamente.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                DialogResult = DialogResult.OK;
            }
        }

        private int Validaciones()
        {
            foreach (Control objeto in this.GroupBox.Controls)
            {
                if (objeto is ComboBox && ((ComboBox)objeto).SelectedItem == null && ((ComboBox)objeto).Name != "vendedorComboBox" && ((ComboBox)objeto).Name != "SubfamiliaComboBox" )
                {
                    MessageBox.Show("Debe completar los campos vacios.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return -1;
                }
                if (objeto is TextBox && ((TextBox)objeto).Text == "" && ((TextBox)objeto).Name != "DescripcionTextBox")
                {
                    MessageBox.Show("Debe completar el campo vacio.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return -1;
                }
            }

            decimal x;
            if (!Decimal.TryParse(this.MontoTextBox.Text, out x)){
                MessageBox.Show("El importe debe decimal.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return -1;
            }

            return 1;
        }

        private void AltaGastoForm_Load(object sender, EventArgs e)
        {
            CargadorDeDatos.CargarFormaPagoNoACreditoComboBox(formaDePagoComboBox, this.Text);
            CargadorDeDatos.CargarFamiliasGastosComboBox(this.FamiliaComboBox, this.Text);
            CargadorDeDatos.CargarVendedoresComboBox(vendedorComboBox, this.Text);
            CargadorDeDatos.CargarSucursalesComboBox(SucursalComboBox, this.Text);

            Usuario user = Main.getUsuario();
            SucursalComboBox.SelectedValue = user.sucursal_default.id;
        }        



        private void MontoTextBox_TextChanged(object sender, EventArgs e)
        {
            if (!Validacion.isDecimal(this.MontoTextBox.Text))
            {
                if (!Validacion.isDecimal(this.MontoTextBox.Text.Replace(",", ".")))
                {
                    return;
                }
                else
                {
                    this.MontoTextBox.Text = this.MontoTextBox.Text.Replace(",", ".");
                    MontoTextBox.SelectionStart = MontoTextBox.Text.Length;
                }
            }
        }

        private void FamiliaComboBox_SelectionChangeCommitted(object sender, EventArgs e)
        {
            CargadorDeDatos.CargarSubFamiliasGastosComboBox(this.SubfamiliaComboBox, this.Text, Convert.ToInt32(this.FamiliaComboBox.SelectedValue.ToString()));
        }

    }
}
