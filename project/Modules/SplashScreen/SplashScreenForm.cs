﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Zapateria.Modules.Login;

namespace Zapateria.Modules.SplachScreen
{
    public partial class SplashScreenForm : Form
    {
        private Timer timer;

        public SplashScreenForm()
        {
            InitializeComponent();

            timer = new Timer();
            timer.Interval = 120;
            timer.Tick += new EventHandler(timer_Tick);
            timer.Start();
        }

        private static void WaitSeconds(double nSecs)
        {
            // Esperar los segundos indicados

            // Crear la cadena para convertir en TimeSpan
            string s = "0.00:00:" + nSecs.ToString().Replace(",", ".");
            TimeSpan ts = TimeSpan.Parse(s);

            // Añadirle la diferencia a la hora actual
            DateTime t1 = DateTime.Now.Add(ts);

            // Esta asignación solo es necesaria
            // si la comprobación se hace al principio del bucle
            DateTime t2 = DateTime.Now;

            // Mientras no haya pasado el tiempo indicado
            while (t2 < t1)
            {
                // Un respiro para el sitema
                System.Windows.Forms.Application.DoEvents();
                // Asignar la hora actual
                t2 = DateTime.Now;
            }
        }

        private void SplashScreenForm_Shown(object sender, EventArgs e)
        {
            WaitSeconds(3);

            ShowInTaskbar = false;
            Visible = false;

            LoginForm abrir = new LoginForm();
            abrir.Show();
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            this.progressBar1.Increment(5);
        }

        private void SplashScreenForm_Load(object sender, EventArgs e)
        {

        }

    }
}
