﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using NHibernate;
using NHibernate.Transform;
using System.Windows.Forms;
using Zapateria.Libs;

namespace Zapateria.Models
{
    public class ItemCompra
    {
        public virtual int id { get; set; }
        public virtual FacturaCompra factura { get; set; }
        public virtual Producto producto { get; set; }
        public virtual Talle talle { get; set; }
        public virtual ColorZapato color { get; set; }
        public virtual string formato { get; set; }
        public virtual decimal precio { get; set; }
        public virtual int cantidad { get; set; }

        public ItemCompra() { }

        public virtual DataGridView customColumns(DataGridView dgw){
        
            dgw.Columns["id"].HeaderText = "Id";
            dgw.Columns["producto"].HeaderText = "Producto";
            dgw.Columns["talle"].HeaderText = "Talle";
            dgw.Columns["color"].HeaderText = "Color";
            dgw.Columns["precio"].HeaderText = "Precio";
            dgw.Columns["cantidad"].HeaderText = "Cantidad";
            dgw.Columns["formato"].HeaderText = "Formato";

            ModDataGridView.ocultarColumna(dgw, "id");
            ModDataGridView.ocultarColumna(dgw, "factura");     

            return dgw;
        }

        public virtual Boolean save() {
            using (var session = SessionManager.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    try
                    {
                        session.Save(this);
                        transaction.Commit();

                        SessionManager.EndTasks();
                        //session.Close();

                        return true;
                    }catch (Exception ex)
                    {
                        transaction.Rollback();
                        Logs.GetInstance.LogError(ex.Message, "ItemCompra.save()");
                        MessageBox.Show(ex.Message, "Exception Msg");

                        return false;
                    }

                    
                }
            }
        }

        public virtual Boolean update()
        {
            using (var session = SessionManager.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    try
                    {                       
                        session.Update(this);
                        transaction.Commit();

                        SessionManager.EndTasks();
                        //session.Close();

                        return true;
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        Logs.GetInstance.LogError(ex.Message, "ItemCompra.update()");
                        MessageBox.Show(ex.Message, "Exception Msg");

                        return false;
                    }
                }
            }
        }

        public virtual IList<ItemCompra> getItemsOf(FacturaCompra fact)
        {
            using (var session = FluentNHibernateDB.CreateSessionFactory().OpenSession())
            {
                try
                {
                    var items = session.QueryOver<ItemCompra>()
                                     .Where(c => c.factura.id == fact.id)
                                     .List<ItemCompra>();

                    SessionManager.EndTasks();                    

                    return items;
                }
                catch (Exception ex)
                {
                    Logs.GetInstance.LogError(ex.Message, "ItemCompra.getItemsOf()");
                    MessageBox.Show(ex.Message, "Exception Msg");

                    return null;
                }
            }
        }
    }
}
