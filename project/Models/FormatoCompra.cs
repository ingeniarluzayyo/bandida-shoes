﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NHibernate.Criterion;
using NHibernate;
using NHibernate.Transform;
using System.Windows.Forms;
using Zapateria.Libs;

namespace Zapateria.Models
{
    public class FormatoCompra
    {

        public virtual int id { get; set; }
        public virtual decimal formato { get; set; }


        public virtual DataGridView customColumns(DataGridView dgw){
        
            dgw.Columns["id"].HeaderText = "Id";
            dgw.Columns["formato"].HeaderText = "Formato Compra";

            ModDataGridView.ocultarColumna(dgw, "id");

            return dgw;
        }

        public virtual Boolean save() {
            using (var session = SessionManager.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    try
                    {
                        session.Save(this);
                        transaction.Commit();

                        SessionManager.EndTasks();
                        //session.Close();

                        return true;
                    }catch (Exception ex)
                    {
                        transaction.Rollback();
                        Logs.GetInstance.LogError(ex.Message, "FormatoCompra.save()");
                        
                        if (ex.InnerException.Message.Contains("UNIQUE"))
                        {
                            MessageBox.Show("Error, ya existe un formato de compra igual.", "FormatoCompra", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        else
                        {
                            MessageBox.Show(ex.Message, "Exception Msg");
                        }

                        return false;
                    }

                    
                }
            }
        }

        public virtual Boolean update()
        {
            using (var session = SessionManager.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    try
                    {                       
                        session.Update(this);
                        transaction.Commit();

                        SessionManager.EndTasks();
                        //session.Close();

                        return true;
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        Logs.GetInstance.LogError(ex.Message, "FormatoCompra.update()");
                        MessageBox.Show(ex.Message, "Exception Msg");

                        return false;
                    }
                }
            }
        }

        public virtual Boolean delete()
        {            
            using (var session = SessionManager.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    try
                    {
                        session.Delete(this);
                        transaction.Commit();

                        SessionManager.EndTasks();
                        //session.Close();

                        return true;
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        Logs.GetInstance.LogError(ex.Message, "FormatoCompra.delete()");
                        MessageBox.Show(ex.Message, "Exception Msg");

                        return false;
                    }
                }
            }
        }

        public virtual IList<FormatoCompra> getAll()
        {
            using (var session = SessionManager.OpenSession())
            {
                try
                {
                    var results = session.QueryOver<FormatoCompra>()
                                     .List<FormatoCompra>();

                    SessionManager.EndTasks();
                    //session.Close();

                    return results;
                }
                catch (Exception ex)
                {
                    Logs.GetInstance.LogError(ex.Message, "FormatoCompra.getAll()");
                    MessageBox.Show(ex.Message, "Exception Msg");

                    return null;
                }
            }
        }

        public virtual FormatoCompra getById(int id)
        {
            using (var session = SessionManager.OpenSession())
            {
                try
                {
                    var results = session.QueryOver<FormatoCompra>()
                                     .Where(c => c.id == id)
                                     .List<FormatoCompra>();

                    SessionManager.EndTasks();
                    //session.Close();

                    if (results.Count == 1)
                    {
                        FormatoCompra result = results[0];                        

                        return result;
                    }
                    return null;
                }
                catch (Exception ex)
                {
                    Logs.GetInstance.LogError(ex.Message, "FormatoCompra.getById()");
                    MessageBox.Show(ex.Message, "Exception Msg");

                    return null;
                }
            }
        }

        public virtual IList<FormatoCompra> getByFormato()
        {
            using (var session = SessionManager.OpenSession())
            {
                try
                {

                    if (this.formato == 0) {
                        var r = session.QueryOver<FormatoCompra>()
                                             .List<FormatoCompra>();
                        SessionManager.EndTasks();
                        //session.Close();

                        return r;
                    }
                    
                      var  results = session.QueryOver<FormatoCompra>()
                                            .Where(c => c.formato == this.formato)
                                         .List<FormatoCompra>();

                    SessionManager.EndTasks();
                    //session.Close();

                    return results;
                }
                catch (Exception ex)
                {
                    Logs.GetInstance.LogError(ex.Message, "FormatoCompra.getByFormato()");
                    MessageBox.Show(ex.Message, "Exception Msg");

                    return null;
                }
            }
        }


        public static string GetFormatoKG(decimal formato){
            return (formato / 1000).ToString();
        }

        public static string GetFormatoGr(decimal formato)
        {
            return (formato * 1000).ToString();
        }
    }
}
