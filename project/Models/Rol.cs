﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using NHibernate;
using NHibernate.Transform;
using System.Windows.Forms;

using Zapateria.Libs;

namespace Zapateria.Models
{
    public class Rol
    {
        public virtual int id { get; set; }
        public virtual String nombre { get; set; }
        public virtual IList<Modulo> modulos { get; set; }

        public Rol()
        {
            modulos = new List<Modulo>();
        }

        public virtual DataGridView customColumns(DataGridView dgw){
        
            dgw.Columns["id"].HeaderText = "Id";
            dgw.Columns["nombre"].HeaderText = "Nombre";

            ModDataGridView.ocultarColumna(dgw, "id");
            ModDataGridView.ocultarColumna(dgw, "modulos");

            return dgw;
        }

        public virtual Boolean save() {
            using (var session = SessionManager.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    try
                    {
                        session.Save(this);
                        transaction.Commit();

                        SessionManager.EndTasks();
                        //session.Close();

                        return true;
                    }catch (Exception ex)
                    {
                        transaction.Rollback();
                        Logs.GetInstance.LogError(ex.Message, "Rol.save()");
                        MessageBox.Show(ex.Message, "Exception Msg");

                        return false;
                    }

                    
                }
            }
        }

        public virtual Boolean update()
        {
            using (var session = SessionManager.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    try
                    {                       
                        session.Update(this);
                        transaction.Commit();

                        SessionManager.EndTasks();
                        //session.Close();

                        return true;
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        Logs.GetInstance.LogError(ex.Message, "Rol.update()");
                        MessageBox.Show(ex.Message, "Exception Msg");

                        return false;
                    }
                }
            }
        }

        public virtual Boolean delete()
        {
            this.modulos.Clear();
            using (var session = SessionManager.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    try
                    {
                        session.Delete(this);
                        transaction.Commit();

                        SessionManager.EndTasks();
                        //session.Close();

                        return true;
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        Logs.GetInstance.LogError(ex.Message, "Rol.delete()");
                        MessageBox.Show(ex.Message, "Exception Msg");

                        return false;
                    }
                }
            }
        }

        public virtual IList<Rol> getRolesLike()
        {
            using (var session = SessionManager.OpenSession())
            {
                try
                {
                    var roles = session.QueryOver<Rol>()
                                     .Where(c => c.nombre.IsLike(this.nombre, MatchMode.Anywhere))
                                     .List<Rol>();

                    SessionManager.EndTasks();
                    //session.Close();

                    return roles;
                }
                catch (Exception ex)
                {
                    Logs.GetInstance.LogError(ex.Message, "Rol.getRolesLike()");
                    MessageBox.Show(ex.Message, "Exception Msg");

                    return null;
                }
            }
        }

        public virtual Rol getRoleById(int rol_id)
        {
            using (var session = SessionManager.OpenSession())
            {
                try
                {
                    var roles = session.QueryOver<Rol>()
                                     .Where(c => c.id == rol_id)
                                     .List<Rol>();

                    SessionManager.EndTasks();
                    //session.Close();

                    if (roles.Count == 1)
                    {
                        Rol result = roles[0];
                        result.modulos = SelectModulosByRol(rol_id);

                        return result;
                    }
                    return null;
                }
                catch (Exception ex)
                {
                    Logs.GetInstance.LogError(ex.Message, "Rol.getRoleById()");
                    MessageBox.Show(ex.Message, "Exception Msg");

                    return null;
                }
            }
        }

        public virtual IList<Rol> getRoles()
        {
            using (var session = SessionManager.OpenSession())
            {
               try
               {
                   var roles = session.QueryOver<Rol>()
                                    .Where(c => c.id == this.id)
                                    .List<Rol>();

                   SessionManager.EndTasks();
                   //session.Close();

                   return roles;
               }
               catch (Exception ex)
               {
                   Logs.GetInstance.LogError(ex.Message, "Rol.getRoles()");
                   MessageBox.Show(ex.Message, "Exception Msg");

                   return null;
               }
            }
        }

        public virtual IList<Modulo> SelectModulosByRol(int rol_id)
        {
            using (var session = SessionManager.OpenSession())
            {
                try
                {
                    var modulos = session.QueryOver<Modulo>()
                            .Right.JoinQueryOver<Rol>(x => x.roles)
                            .Where(c => c.id == rol_id)
                            .List();

                    SessionManager.EndTasks();
                    //session.Close();

                    return modulos;
                }
                catch (Exception ex)
                {
                    Logs.GetInstance.LogError(ex.Message, "Rol.SelectModulosByRol()");
                    MessageBox.Show(ex.Message, "Exception Msg");

                    return null;
                }
            }
        }

        public override string ToString()
        {

            return nombre;
        }
    }
}
