﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Zapateria.Libs;
using NHibernate.Criterion;
using NHibernate;
using NHibernate.Transform;

namespace Zapateria.Models
{
    class Empresa
    {
        public virtual int id { get; set; }
        public virtual string razon_social { get; set; }
        public virtual string cuit { get; set; }
        public virtual string direccion { get; set; }
        public virtual string telefono { get; set; }
        public virtual string etiqueta { get; set; }
        public virtual string servidor { get; set; }
        public virtual string puerto { get; set; }
        public virtual string email { get; set; }
        public virtual string pass { get; set; }
        public virtual byte[] imagen { get; set; }

        public Empresa()
        {
        }

        public virtual Boolean update()
        {
            using (var session = SessionManager.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    try
                    {                       
                        session.Update(this);
                        transaction.Commit();

                        SessionManager.EndTasks();
                                                
                        return true;
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        Logs.GetInstance.LogError(ex.Message, "Empresa.update()");
                        MessageBox.Show(ex.Message, "Exception Msg");

                        return false;
                    }
                }
            }
        }           

        public virtual Empresa getById(int rol_id)
        {
            using (var session = SessionManager.OpenSession())
            {
                try
                {
                    var results = session.QueryOver<Empresa>()
                                     .Where(c => c.id == rol_id)
                                     .List<Empresa>();

                    SessionManager.EndTasks();
                    //session.Close();

                    if (results.Count == 1)
                    {
                        Empresa result = results[0];                        

                        return result;
                    }
                    return null;
                }
                catch (Exception ex)
                {
                    Logs.GetInstance.LogError(ex.Message, "Empresa.getById()");
                    MessageBox.Show(ex.Message, "Exception Msg");

                    return null;
                }
            }
        }
    }
}
