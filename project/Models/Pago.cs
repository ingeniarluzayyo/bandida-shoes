﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using NHibernate;
using NHibernate.Transform;
using System.Windows.Forms;
using Zapateria.Libs;

namespace Zapateria.Models
{
    class Pago
    {
        public virtual int id { get; set; }
        public virtual DateTime fecha { get; set; }
        public virtual Cliente cliente { get; set; }
        public virtual FormaPago forma_de_pago { get; set; }
        public virtual decimal monto { get; set; }
        public virtual bool anulada { get; set; }

        public Pago()
        {
        }

        public virtual DataGridView customColumns(DataGridView dgw){
        
            /*
            dgw.Columns["id"].HeaderText = "Id";
            dgw.Columns["nombre"].HeaderText = "Nombre";

            ModDataGridView.ocultarColumna(dgw, "id");
            */
            return dgw;
        }

        public virtual Boolean save() {
            using (var session = SessionManager.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    try
                    {
                        session.Save(this);
                        transaction.Commit();

                        SessionManager.EndTasks();
                        //session.Close();

                        CtaCte ctaCte = new CtaCte();
                        ctaCte = ctaCte.getByCliente(cliente.id);
                        if (ctaCte == null)
                        {
                            ctaCte = new CtaCte();
                            ctaCte.cliente = cliente; 
                            ctaCte.total_facturado = 0;
                        }

                        ctaCte.total_pagado += monto;
                        ctaCte.saldo = ctaCte.total_pagado - ctaCte.total_facturado;
                        ctaCte.ultima_actualizacion = DateTime.Now;

                        ctaCte.save();

                        return true;
                    }catch (Exception ex)
                    {
                        transaction.Rollback();
                        Logs.GetInstance.LogError(ex.Message, "Pago.save()");
                        MessageBox.Show(ex.Message, "Exception Msg");

                        return false;
                    }

                    
                }
            }
        }

        public virtual Boolean update()
        {
            using (var session = SessionManager.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    try
                    {                       
                        session.Update(this);
                        transaction.Commit();

                        SessionManager.EndTasks();
                        //session.Close();

                        if (anulada)
                        {
                            CtaCte ctaCte = new CtaCte();
                            ctaCte = ctaCte.getByCliente(cliente.id);
                            if (ctaCte == null)
                            {
                                ctaCte = new CtaCte();
                                ctaCte.cliente = cliente;
                                ctaCte.total_facturado = 0;
                            }

                            ctaCte.total_pagado -= monto;
                            ctaCte.saldo = ctaCte.total_pagado - ctaCte.total_facturado;
                            ctaCte.ultima_actualizacion = DateTime.Now;

                            ctaCte.save();
                        }

                        return true;
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        Logs.GetInstance.LogError(ex.Message, "Pago.update()");
                        MessageBox.Show(ex.Message, "Exception Msg");

                        return false;
                    }
                }
            }
        }

        public virtual Boolean delete()
        {            
            using (var session = SessionManager.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    try
                    {
                        session.Delete(this);
                        transaction.Commit();

                        SessionManager.EndTasks();
                        //session.Close();

                        return true;
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        Logs.GetInstance.LogError(ex.Message, "Pago.delete()");
                        MessageBox.Show(ex.Message, "Exception Msg");

                        return false;
                    }
                }
            }
        }        

        public virtual Pago getById(int rol_id)
        {
            using (var session = SessionManager.OpenSession())
            {
                try
                {
                    var results = session.QueryOver<Pago>()
                                     .Where(c => c.id == rol_id)
                                     .List<Pago>();

                    SessionManager.EndTasks();
                    //session.Close();

                    if (results.Count == 1)
                    {
                        Pago result = results[0];                        

                        return result;
                    }
                    return null;
                }
                catch (Exception ex)
                {
                    Logs.GetInstance.LogError(ex.Message, "Pago.getById()");
                    MessageBox.Show(ex.Message, "Exception Msg");

                    return null;
                }
            }
        }
    }
}
