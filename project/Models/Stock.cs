﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Zapateria.Libs;
using NHibernate;
using NHibernate.Criterion;
using System.Data;
using MySql.Data.MySqlClient;
using static System.Resources.ResXFileRef;
using System.ComponentModel;

namespace Zapateria.Models
{
    public class Stock
    {
        public virtual int id { get; set; }
        public virtual Producto producto { get; set; }
        public virtual Sucursal sucursal { get; set; }
        public virtual Zona zona { get; set; }
        public virtual Talle talle { get; set; }
        public virtual ColorZapato color { get; set; }
        public virtual int stock { get; set; }

        public Stock() { }

        public virtual DataGridView customColumns(DataGridView dgw)
        {
            if (!ModDataGridView.tieneLaColumna(dgw, "producto_id"))
            {
                DataGridViewTextBoxColumn column = new DataGridViewTextBoxColumn();
                column.Name = "producto_id";
                column.DataPropertyName = "producto.id";
                dgw.Columns.Add(column);
            }

            dgw.Columns["producto_id"].HeaderText = "Código";
            dgw.Columns["producto"].HeaderText = "Nombre";
            dgw.Columns["sucursal"].HeaderText = "Sucursal";
            dgw.Columns["zona"].HeaderText = "Zona";
            dgw.Columns["talle"].HeaderText = "Talle";
            dgw.Columns["color"].HeaderText = "Color";
            dgw.Columns["stock"].HeaderText = "Stock";

            dgw.Columns["id"].Visible = false;
            dgw.Columns["producto_id"].DisplayIndex = 0;
            dgw.Columns["producto"].DisplayIndex = 1;
            dgw.Columns["sucursal"].DisplayIndex = 2;
            dgw.Columns["zona"].DisplayIndex = 3;
            dgw.Columns["talle"].DisplayIndex = 4;
            dgw.Columns["color"].DisplayIndex = 5;
            dgw.Columns["stock"].DisplayIndex = 6;

            return dgw;
        }

        public virtual Boolean save()
        {
            using (var session = SessionManager.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    try
                    {
                        session.Save(this);
                        transaction.Commit();

                        SessionManager.EndTasks();

                        return true;
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        Logs.GetInstance.LogError(ex.Message, "Stock.save()");
                        MessageBox.Show(ex.Message, "Exception Msg");

                        return false;
                    }
                }
            }
        }

        public virtual Boolean update()
        {
            using (var session = SessionManager.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    try
                    {
                        session.Update(this);
                        transaction.Commit();

                        SessionManager.EndTasks();

                        return true;
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        Logs.GetInstance.LogError(ex.Message, "Stock.update()");
                        MessageBox.Show(ex.Message, "Exception Msg");

                        return false;
                    }
                }
            }
        }

        public virtual Boolean delete()
        {
            using (var session = SessionManager.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    try
                    {
                        session.Delete(this);
                        transaction.Commit();

                        SessionManager.EndTasks();

                        return true;
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        Logs.GetInstance.LogError(ex.Message, "Stock.delete()");
                        MessageBox.Show(ex.Message, "Exception Msg");

                        return false;
                    }
                }
            }
        }

        public virtual Stock getById(int id_value)
        {
            using (var session = SessionManager.OpenSession())
            {
                try
                {
                    var results = session.QueryOver<Stock>()
                                     .Where(c => c.id == id_value)
                                     .List<Stock>();

                    SessionManager.EndTasks();

                    if (results.Count == 1)
                    {
                        Stock result = results[0];

                        return result;
                    }
                    return null;
                }
                catch (Exception ex)
                {
                    Logs.GetInstance.LogError(ex.Message, "Stock.getById()");
                    MessageBox.Show(ex.Message, "Exception Msg");

                    return null;
                }
            }
        }

        public virtual IList<Stock> GetAll()
        {
            // Set cursor as hourglass
            Cursor.Current = Cursors.WaitCursor;

            using (var session = SessionManager.OpenSession())
            {
                try
                {
                    var query = session.QueryOver<Stock>();

                    query = query.OrderBy(c => c.id).Desc;


                    var list = query.List<Stock>();
                    SessionManager.EndTasks();

                    return list;
                }
                catch (Exception ex)
                {
                    Logs.GetInstance.LogError(ex.Message, "Stock.GetAll()");
                    MessageBox.Show(ex.Message, "Exception Msg");
                    Clipboard.SetText(ex.Message);

                    return null;
                }
                finally
                {
                    // Set cursor as default arrow
                    Cursor.Current = Cursors.Default;
                }
            }
        }

        public virtual IList<Stock> Search(int producto_id, int sucursal_id, int zona_id, int talle_id, int color_id)
        {
            // Set cursor as hourglass
            Cursor.Current = Cursors.WaitCursor;

            using (var session = SessionManager.OpenSession())
            {
                try
                {
                    var query = session.QueryOver<Stock>();

                    if (producto_id > 0)
                    {
                        query = query.Where(c => c.producto.id == producto_id);
                    }

                    if (sucursal_id > 0)
                    {
                        query = query.Where(c => c.sucursal.id == sucursal_id);
                    }

                    if (zona_id > 0)
                    {
                        query = query.Where(c => c.zona.id == zona_id);
                    }

                    if (talle_id > 0)
                    {
                        query = query.Where(c => c.talle.id == talle_id);
                    }

                    if (color_id > 0)
                    {
                        query = query.Where(c => c.color.id == color_id);
                    }

                    query = query.OrderBy(c => c.id).Desc;


                    var list = query.List<Stock>();
                    SessionManager.EndTasks();

                    return list;
                }
                catch (Exception ex)
                {
                    Logs.GetInstance.LogError(ex.Message, "Stock.Search()");
                    MessageBox.Show(ex.Message, "Exception Msg");
                    Clipboard.SetText(ex.Message);

                    return null;
                }
                finally
                {
                    // Set cursor as default arrow
                    Cursor.Current = Cursors.Default;
                }
            }
        }

        public virtual DataTable GetStockInSucursal(int sucursal_id)
        {
            // Set cursor as hourglass
            Cursor.Current = Cursors.WaitCursor;

            try
            {
                string Consulta = "SELECT s.id AS id, CAST(p.id AS CHAR(20)) AS producto_id, p.nombre AS producto, su.nombre AS 'sucursal', z.nombre AS 'zona', t.nombre AS 'talle', c.nombre AS 'color', s.stock AS 'stock'";
                Consulta += " FROM `stock` AS s";
                Consulta += " LEFT JOIN productos AS p ON p.id = s.producto";
                Consulta += " LEFT JOIN sucursal AS su ON su.id = s.sucursal";
                Consulta += " LEFT JOIN zona AS z ON z.id = s.zona";
                Consulta += " LEFT JOIN talles AS t ON t.id = s.talle";
                Consulta += " LEFT JOIN colores AS c ON c.id = s.color";
                Consulta += " WHERE p.habilitado = '1' AND s.sucursal = '" + sucursal_id + "' ORDER BY p.orden ASC";

                List<MySqlParameter> param = new List<MySqlParameter>();

                return BaseDeDatos.GetInstance.ExecuteCustomQuery(Consulta, param, "GetStockInSucursal");
            }
            catch (Exception ex)
            {
                Logs.GetInstance.LogError(ex.Message, "Stock.GetStockInSucursal()");
                MessageBox.Show(ex.Message, "Exception Msg");

                return null;
            }
            finally
            {
                // Set cursor as default arrow
                Cursor.Current = Cursors.Default;
            }
        }
    }
}
