﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using NHibernate.Criterion;
using NHibernate;
using NHibernate.Transform;

using Zapateria.Libs;

namespace Zapateria.Models
{
    public class Modulo
    {
        public virtual int id { get; set; }
        public virtual String nombre { get; set; }
        public virtual IList<Rol> roles { get; set; }

        public Modulo()
        {
            roles = new List<Rol>();
        }

        public Modulo(int id, String nombre)
        {
            this.id = id;
            this.nombre = nombre;
            roles = new List<Rol>();
        }

        public virtual DataGridView customColumns(DataGridView dgw)
        {

            dgw.Columns["id"].HeaderText = "Id";
            dgw.Columns["nombre"].HeaderText = "Nombre";

            ModDataGridView.ocultarColumna(dgw, "id");

            return dgw;
        }

        public virtual Boolean save()
        {
            using (var session = SessionManager.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    try
                    {
                        session.Save(this);
                        transaction.Commit();

                        SessionManager.EndTasks();                        

                        return true;
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        Logs.GetInstance.LogError(ex.Message, "Modulo.save()");
                        MessageBox.Show(ex.Message, "Exception Msg");

                        return false;
                    }


                }
            }
        }

        public virtual Boolean update()
        {
            using (var session = SessionManager.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    try
                    {                        
                        session.Update(this);
                        transaction.Commit();

                        SessionManager.EndTasks();                        

                        return true;
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        Logs.GetInstance.LogError(ex.Message, "Modulo.update()");
                        MessageBox.Show(ex.Message, "Exception Msg");

                        return false;
                    }
                }
            }
        }


        public virtual IList<Modulo> getModulosLike()
        {
            using (var session = SessionManager.OpenSession())
            {
                try
                {
                    var modulos = session.QueryOver<Modulo>()
                                     .Where(c => c.nombre.IsLike(this.nombre, MatchMode.Anywhere))
                                     .List<Modulo>();

                    SessionManager.EndTasks();                    

                    return modulos;
                }
                catch (Exception ex)
                {
                    Logs.GetInstance.LogError(ex.Message, "Modulo.getRolesLike()");
                    MessageBox.Show(ex.Message, "Exception Msg");

                    return null;
                }
            }
        }

        public virtual IList<Modulo> getModulos()
        {
            using (var session = SessionManager.OpenSession())
            {
                try
                {
                    var modulos = session.QueryOver<Modulo>()
                                     .Where(c => c.id == this.id)
                                     .List<Modulo>();

                    SessionManager.EndTasks();                    

                    return modulos;
                }
                catch (Exception ex)
                {
                    Logs.GetInstance.LogError(ex.Message, "Modulo.getRoles()");
                    MessageBox.Show(ex.Message, "Exception Msg");

                    return null;
                }
            }
        }
    }
}
