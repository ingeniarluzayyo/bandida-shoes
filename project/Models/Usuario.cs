﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Zapateria.Libs;
using NHibernate.Criterion;
using NHibernate;
using NHibernate.Transform;
using System.Windows.Forms;

namespace Zapateria.Models
{
    public class Usuario
    {
        public virtual String username { get; set; }
        public virtual String password { get; set; }
        public virtual String email { get; set; }
        public virtual Rol rol { get; set; }
        public virtual Sucursal sucursal_default { get; set; }

        public virtual DataGridView customColumns(DataGridView dgw){
        
            dgw.Columns["username"].HeaderText = "Usuario";
            dgw.Columns["password"].HeaderText = "Password";
            dgw.Columns["email"].HeaderText = "Email";
            dgw.Columns["rol"].HeaderText = "Rol";
            dgw.Columns["sucursal_default"].HeaderText = "Sucursal por defecto";

            ModDataGridView.ocultarColumna(dgw, "password");

            return dgw;
        }

        public virtual Boolean save() {
            using (var session = SessionManager.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    try
                    {
                        session.Save(this);
                        transaction.Commit();

                        SessionManager.EndTasks();

                        return true;
                    }catch (Exception ex)
                    {
                        transaction.Rollback();
                        Logs.GetInstance.LogError(ex.Message, "Usuarios.save()");
                        MessageBox.Show(ex.Message, "Exception Msg");

                        return false;
                    }

                    
                }
            }
        }

        public virtual Boolean update()
        {
            using (var session = SessionManager.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    try
                    {
                        session.Update(this);
                        transaction.Commit();

                        SessionManager.EndTasks();

                        return true;
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        Logs.GetInstance.LogError(ex.Message, "Usuarios.update()");
                        MessageBox.Show(ex.Message, "Exception Msg");

                        return false;
                    }
                }
            }
        }

        public virtual Boolean delete()
        {
            using (var session = SessionManager.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    try
                    {
                        session.Delete(this);
                        transaction.Commit();

                        SessionManager.EndTasks();

                        return true;
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        Logs.GetInstance.LogError(ex.Message, "Usuarios.delete()");
                        MessageBox.Show(ex.Message, "Exception Msg");

                        return false;
                    }
                }
            }
        }

        public virtual Usuario getByUsername(string name)
        {
            using (var session = SessionManager.OpenSession())
            {
                try
                {
                    var results = session.QueryOver<Usuario>()
                                     .Where(c => c.username == name)
                                     .List<Usuario>();

                    SessionManager.EndTasks();

                    if (results.Count == 1)
                    {
                        Usuario result = results[0];

                        return result;
                    }
                    return null;
                }
                catch (Exception ex)
                {
                    Logs.GetInstance.LogError(ex.Message, "Usuarios.getByUsername()");
                    MessageBox.Show(ex.Message, "Exception Msg");

                    return null;
                }
            }
        }

        public virtual IList<Usuario> getUsuarioLike()
        {
            using (var session = SessionManager.OpenSession())
            {
                try
                {
                    var users = session.QueryOver<Usuario>()
                                     .Where(c => c.username.IsLike(this.username, MatchMode.Anywhere))
                                     .List<Usuario>();

                    SessionManager.EndTasks();

                    return users;
                }
                catch (Exception ex)
                {
                    Logs.GetInstance.LogError(ex.Message, "Usuarios.getUsuarioLike()");
                    MessageBox.Show(ex.Message, "Exception Msg");

                    return null;
                }
            }
        }

        public virtual IList<Usuario> getUsuarios()
        {
            using (var session = SessionManager.OpenSession())
            {
               try
               {              
                   var users = session.QueryOver<Usuario>()
                                    .Where(c => c.username == this.username && 
                                                c.password == Security.Encriptar(this.password))

                                    .List<Usuario>();

                   SessionManager.EndTasks();

                   return users;
               }
               catch (Exception ex)
               {
                   Logs.GetInstance.LogError(ex.Message, "Usuarios.getUsuarios()");
                   MessageBox.Show(ex.Message, "Exception Msg");

                   return null;
               }
            }
        }

    }
}
