﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using NHibernate;
using NHibernate.Transform;
using System.Windows.Forms;
using Zapateria.Libs;

namespace Zapateria.Models
{
    class PagoProveedor
    {
        public virtual int id { get; set; }
        public virtual DateTime fecha { get; set; }
        public virtual Proveedor proveedor { get; set; }
        public virtual FormaPago forma_de_pago { get; set; }
        public virtual decimal monto { get; set; }
        public virtual bool anulada { get; set; }

        public PagoProveedor()
        {
        }

        public virtual DataGridView customColumns(DataGridView dgw){
        
            /*
            dgw.Columns["id"].HeaderText = "Id";
            dgw.Columns["nombre"].HeaderText = "Nombre";

            ModDataGridView.ocultarColumna(dgw, "id");
            */
            return dgw;
        }

        public virtual Boolean save() {
            using (var session = SessionManager.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    try
                    {
                        session.Save(this);
                        transaction.Commit();

                        SessionManager.EndTasks();
                        //session.Close();

                        CtaCteProveedor ctaCte = new CtaCteProveedor();
                        ctaCte = ctaCte.getByProveedor(proveedor.id);
                        if (ctaCte == null)
                        {
                            ctaCte = new CtaCteProveedor();
                            ctaCte.proveedor = proveedor; 
                            ctaCte.total_facturado = 0;
                        }

                        ctaCte.total_pagado += monto;
                        ctaCte.saldo = ctaCte.total_pagado - ctaCte.total_facturado;
                        ctaCte.ultima_actualizacion = DateTime.Now;

                        ctaCte.save();

                        return true;
                    }catch (Exception ex)
                    {
                        transaction.Rollback();
                        Logs.GetInstance.LogError(ex.Message, "PagoProveedor.save()");
                        MessageBox.Show(ex.Message, "Exception Msg");

                        return false;
                    }

                    
                }
            }
        }

        public virtual Boolean update()
        {
            using (var session = SessionManager.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    try
                    {                       
                        session.Update(this);
                        transaction.Commit();

                        SessionManager.EndTasks();
                        //session.Close();

                        if (anulada)
                        {
                            CtaCteProveedor ctaCte = new CtaCteProveedor();
                            ctaCte = ctaCte.getByProveedor(proveedor.id);
                            if (ctaCte == null)
                            {
                                ctaCte = new CtaCteProveedor();
                                ctaCte.proveedor = proveedor;
                                ctaCte.total_facturado = 0;
                            }

                            ctaCte.total_pagado -= monto;
                            ctaCte.saldo = ctaCte.total_pagado - ctaCte.total_facturado;
                            ctaCte.ultima_actualizacion = DateTime.Now;

                            ctaCte.save();
                        }

                        return true;
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        Logs.GetInstance.LogError(ex.Message, "PagoProveedor.update()");
                        MessageBox.Show(ex.Message, "Exception Msg");

                        return false;
                    }
                }
            }
        }

        public virtual Boolean delete()
        {            
            using (var session = SessionManager.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    try
                    {
                        session.Delete(this);
                        transaction.Commit();

                        SessionManager.EndTasks();
                        //session.Close();

                        return true;
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        Logs.GetInstance.LogError(ex.Message, "PagoProveedor.delete()");
                        MessageBox.Show(ex.Message, "Exception Msg");

                        return false;
                    }
                }
            }
        }

        public virtual PagoProveedor getById(int rol_id)
        {
            using (var session = SessionManager.OpenSession())
            {
                try
                {
                    var results = session.QueryOver<PagoProveedor>()
                                     .Where(c => c.id == rol_id)
                                     .List<PagoProveedor>();

                    SessionManager.EndTasks();
                    //session.Close();

                    if (results.Count == 1)
                    {
                        PagoProveedor result = results[0];                        

                        return result;
                    }
                    return null;
                }
                catch (Exception ex)
                {
                    Logs.GetInstance.LogError(ex.Message, "PagoProveedor.getById()");
                    MessageBox.Show(ex.Message, "Exception Msg");

                    return null;
                }
            }
        }
    }
}
