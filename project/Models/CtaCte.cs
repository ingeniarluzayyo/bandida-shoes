﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using NHibernate;
using NHibernate.Transform;
using System.Windows.Forms;
using Zapateria.Libs;

namespace Zapateria.Models
{
    public class CtaCte
    {
        public virtual int id { get; set; }
        public virtual Cliente cliente { get; set; }
        public virtual DateTime ultima_actualizacion { get; set; }
        public virtual decimal total_facturado { get; set; }
        public virtual decimal total_pagado { get; set; }
        public virtual decimal saldo { get; set; }

        public CtaCte()
        {
        }

        public virtual DataGridView customColumns(DataGridView dgw,int client_id = 0){

            dgw.Columns["id"].HeaderText = "Id";
            dgw.Columns["cliente"].HeaderText = "Cliente";
            dgw.Columns["total_facturado"].HeaderText = "Total Facturado";
            dgw.Columns["total_pagado"].HeaderText = "Total Pagado";
            dgw.Columns["saldo"].HeaderText = "Saldo";
            dgw.Columns["ultima_actualizacion"].HeaderText = "Ultima Actualización";

            dgw.Columns["ultima_actualizacion"].Width = 150;
            dgw.Columns["cliente"].Width = 150;
            dgw.Columns["total_facturado"].Width = 120;
            dgw.Columns["total_pagado"].Width = 120;
            dgw.Columns["saldo"].Width = 120;

            DataGridViewColumn col = new DataGridViewColumn();
            col.Name = "Cliente_Id";
            col.CellTemplate = new DataGridViewTextBoxCell();
            dgw.Columns.Insert(1, col);
            
            foreach (DataGridViewRow row in dgw.Rows)
            {
                if (client_id == 0)
                {
                    row.Cells["Cliente_Id"].Value = ((Cliente)row.Cells["Cliente"].Value).id;
                }
                else
                {
                    row.Cells["Cliente_Id"].Value = client_id;
                }
            }

            ModDataGridView.ocultarColumna(dgw, "id");
            ModDataGridView.ocultarColumna(dgw, "Cliente_Id");
            ModDataGridView.agregarBoton(dgw, "Ver Detalle");
           
            return dgw;
        }

        public virtual Boolean save() {
            using (var session = SessionManager.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    try
                    {
                        session.SaveOrUpdate(this);
                        transaction.Commit();

                        SessionManager.EndTasks();
                        //session.Close();

                        return true;
                    }catch (Exception ex)
                    {
                        transaction.Rollback();
                        Logs.GetInstance.LogError(ex.Message, "CtaCte.save()");
                        MessageBox.Show(ex.Message, "Exception Msg");

                        return false;
                    }

                    
                }
            }
        }

        public virtual Boolean update()
        {
            using (var session = SessionManager.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    try
                    {                       
                        session.Update(this);
                        transaction.Commit();

                        SessionManager.EndTasks();
                        //session.Close();

                        return true;
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        Logs.GetInstance.LogError(ex.Message, "CtaCte.update()");
                        MessageBox.Show(ex.Message, "Exception Msg");

                        return false;
                    }
                }
            }
        }

        public virtual Boolean delete()
        {            
            using (var session = SessionManager.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    try
                    {
                        session.Delete(this);
                        transaction.Commit();

                        SessionManager.EndTasks();
                        //session.Close();

                        return true;
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        Logs.GetInstance.LogError(ex.Message, "CtaCte.delete()");
                        MessageBox.Show(ex.Message, "Exception Msg");

                        return false;
                    }
                }
            }
        }

        public virtual IList<CtaCte> getAll()
        {
            using (var session = SessionManager.OpenSession())
            {
                try
                {
                    var results = session.QueryOver<CtaCte>()                                     
                                     .List<CtaCte>();

                    SessionManager.EndTasks();
                    //session.Close();

                    return results;
                }
                catch (Exception ex)
                {
                    Logs.GetInstance.LogError(ex.Message, "CtaCte.getAll()");
                    MessageBox.Show(ex.Message, "Exception Msg");

                    return null;
                }
            }
        }

        public virtual CtaCte getByCliente(int id)
        {
            using (var session = SessionManager.OpenSession())
            {
                try
                {
                    var results = session.QueryOver<CtaCte>()
                                     .Where(c => c.cliente.id == id)
                                     .List<CtaCte>();

                    SessionManager.EndTasks();
                    //session.Close();

                    if (results.Count == 1)
                    {
                        CtaCte result = results[0];                        

                        return result;
                    }
                    return null;
                }
                catch (Exception ex)
                {
                    Logs.GetInstance.LogError(ex.Message, "CtaCte.getByCliente()");
                    MessageBox.Show(ex.Message, "Exception Msg");

                    return null;
                }
            }
        }
    }
}
