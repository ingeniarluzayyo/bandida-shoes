﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using NHibernate;
using NHibernate.Transform;
using System.Windows.Forms;
using Zapateria.Libs;
using MySql.Data.MySqlClient;
using System.Data;

namespace Zapateria.Models
{
    public class Sucursal
    {
        public virtual int id { get; set; }
        public virtual String nombre { get; set; }        

        public Sucursal() { }

        public override string ToString()
        {
            return nombre;
        }
        public virtual Sucursal getById(int id)
        {
            using (var session = SessionManager.OpenSession())
            {
                try
                {
                    var results = session.QueryOver<Sucursal>()
                                     .Where(c => c.id == id)
                                     .List<Sucursal>();

                    SessionManager.EndTasks();
                    //session.Close();

                    if (results.Count == 1)
                    {
                        Sucursal result = results[0];

                        return result;
                    }
                    return null;
                }
                catch (Exception ex)
                {
                    Logs.GetInstance.LogError(ex.Message, "Sucursal.getById()");
                    MessageBox.Show(ex.Message, "Exception Msg");

                    return null;
                }
            }
        }

        public static IList<Sucursal> getAll()
        {
            using (var session = SessionManager.OpenSession())
            {
                try
                {
                    var results = session.QueryOver<Sucursal>()                                     
                                     .List<Sucursal>();
                    SessionManager.EndTasks();
                    //session.Close();

                    return results;
                }
                catch (Exception ex)
                {
                    Logs.GetInstance.LogError(ex.Message, "Sucursal.getAll()");
                    MessageBox.Show(ex.Message, "Exception Msg");

                    return null;
                }
            }
        }
    }
}
