﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Zapateria.Libs;
using System.Windows.Forms;
using NHibernate;
using NHibernate.Criterion;

namespace Zapateria.Models
{
    public class SubfamiliaGasto
    {
        public virtual int id { get; set; }
        public virtual String nombre { get; set; }
        public virtual FamiliaGasto familia_gasto { get; set; }


        public SubfamiliaGasto()
        {
        }

        public SubfamiliaGasto(int id, String nombre, FamiliaGasto familia_gasto)
        {
            this.id = id;
            this.nombre = nombre;
            this.familia_gasto = familia_gasto;
        }

        public override string ToString()
        {
            return nombre;
        }

        public virtual DataGridView customColumns(DataGridView dgw){
        
            dgw.Columns["id"].HeaderText = "Id";
            dgw.Columns["nombre"].HeaderText = "Nombre";
            dgw.Columns["familia_gasto"].HeaderText = "Familia";

            dgw.Columns["nombre"].Width = 250;
            dgw.Columns["familia_gasto"].Width = 250;

            ModDataGridView.ocultarColumna(dgw, "id");

            return dgw;
        }

        public virtual Boolean save() {
            using (var session = SessionManager.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    try
                    {
                        session.Save(this);
                        transaction.Commit();

                        SessionManager.EndTasks();
                        //session.Close();

                        return true;
                    }catch (Exception ex)
                    {
                        transaction.Rollback();
                        Logs.GetInstance.LogError(ex.Message, "SubFamiliaGasto.save()");
                        
                        if (ex.InnerException.Message.Contains("UNIQUE"))
                        {
                            MessageBox.Show("Error, ya existe una subfamilia con el mismo nombre en esta familia.", "SubFamiliaGasto", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        else
                        {
                            MessageBox.Show(ex.Message, "Exception Msg");
                        }

                        return false;
                    }

                    
                }
            }
        }

        public virtual Boolean update()
        {
            using (var session = SessionManager.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    try
                    {                       
                        session.Update(this);
                        transaction.Commit();

                        SessionManager.EndTasks();
                        //session.Close();

                        return true;
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        Logs.GetInstance.LogError(ex.Message, "SubFamiliaGasto.update()");
                        MessageBox.Show(ex.Message, "Exception Msg");

                        return false;
                    }
                }
            }
        }

        public virtual Boolean delete()
        {            
            using (var session = SessionManager.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    try
                    {
                        session.Delete(this);
                        transaction.Commit();

                        SessionManager.EndTasks();
                        //session.Close();

                        return true;
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        Logs.GetInstance.LogError(ex.Message, "SubFamiliaGasto.delete()");
                        MessageBox.Show(ex.Message, "Exception Msg");

                        return false;
                    }
                }
            }
        }

        public virtual IList<SubfamiliaGasto> getLike()
        {
            using (var session = SessionManager.OpenSession())
            {
                try
                {
                    var results = session.QueryOver<SubfamiliaGasto>()
                                     .Where(c => c.nombre.IsLike(this.nombre, MatchMode.Anywhere))
                                     .List<SubfamiliaGasto>();

                    SessionManager.EndTasks();
                    //session.Close();

                    return results;
                }
                catch (Exception ex)
                {
                    Logs.GetInstance.LogError(ex.Message, "SubFamiliaGasto.getLike()");
                    MessageBox.Show(ex.Message, "Exception Msg");

                    return null;
                }
            }
        }

        public virtual IList<SubfamiliaGasto> getAll()
        {
            using (var session = SessionManager.OpenSession())
            {
                try
                {
                    var results = session.QueryOver<SubfamiliaGasto>()
                                     .List<SubfamiliaGasto>();

                    SessionManager.EndTasks();
                    //session.Close();

                    return results;
                }
                catch (Exception ex)
                {
                    Logs.GetInstance.LogError(ex.Message, "SubFamiliaGasto.getAll()");
                    MessageBox.Show(ex.Message, "Exception Msg");

                    return null;
                }
            }
        }

        public virtual SubfamiliaGasto getById(int rol_id)
        {
            using (var session = SessionManager.OpenSession())
            {
                try
                {
                    var results = session.QueryOver<SubfamiliaGasto>()
                                     .Where(c => c.id == rol_id)
                                     .List<SubfamiliaGasto>();

                    SessionManager.EndTasks();
                    //session.Close();

                    if (results.Count == 1)
                    {
                        SubfamiliaGasto result = results[0];                        

                        return result;
                    }
                    return null;
                }
                catch (Exception ex)
                {
                    Logs.GetInstance.LogError(ex.Message, "SubFamiliaGasto.getById()");
                    MessageBox.Show(ex.Message, "Exception Msg");

                    return null;
                }
            }
        }

    }
}
