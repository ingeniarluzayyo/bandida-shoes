﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using NHibernate;
using NHibernate.Transform;
using System.Windows.Forms;
using Zapateria.Libs;
using MySql.Data.MySqlClient;
using System.Data;

namespace Zapateria.Models
{
    public class Zona
    {
        public virtual int id { get; set; }
        public virtual String nombre { get; set; }        

        public Zona() { }

        public override string ToString()
        {
            return nombre;
        }
        public virtual Zona getById(int id)
        {
            using (var session = SessionManager.OpenSession())
            {
                try
                {
                    var results = session.QueryOver<Zona>()
                                     .Where(c => c.id == id)
                                     .List<Zona>();

                    SessionManager.EndTasks();
                    //session.Close();

                    if (results.Count == 1)
                    {
                        Zona result = results[0];

                        return result;
                    }
                    return null;
                }
                catch (Exception ex)
                {
                    Logs.GetInstance.LogError(ex.Message, "Zona.getById()");
                    MessageBox.Show(ex.Message, "Exception Msg");

                    return null;
                }
            }
        }

        public static IList<Zona> getAll()
        {
            using (var session = SessionManager.OpenSession())
            {
                try
                {
                    var results = session.QueryOver<Zona>()                                     
                                     .List<Zona>();
                    SessionManager.EndTasks();
                    //session.Close();

                    return results;
                }
                catch (Exception ex)
                {
                    Logs.GetInstance.LogError(ex.Message, "Zona.getAll()");
                    MessageBox.Show(ex.Message, "Exception Msg");

                    return null;
                }
            }
        }

        public virtual Zona Search(string nombre)
        {
            using (var session = SessionManager.OpenSession())
            {
                try
                {
                    var query = session.QueryOver<Zona>();

                    if (nombre != " " && nombre != "")
                    {
                        query = query.Where(c => c.nombre == nombre);
                    }

                    Zona zona = query.SingleOrDefault<Zona>();

                    SessionManager.EndTasks();

                    return zona;
                }
                catch (Exception ex)
                {
                    Logs.GetInstance.LogError(ex.Message, "Zona.Search()");
                    MessageBox.Show(ex.Message, "Exception Msg");

                    return null;
                }
            }
        }
    }
}
