﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using NHibernate;
using NHibernate.Transform;
using System.Windows.Forms;
using Zapateria.Libs;
using MySql.Data.MySqlClient;
using System.Data;

namespace Zapateria.Models
{
    public class Gasto
    {
        public virtual int id { get; set; }
        public virtual Sucursal sucursal { get; set; }
        public virtual DateTime fecha { get; set; }
        public virtual FamiliaGasto familia { get; set; }
        public virtual SubfamiliaGasto subfamilia_gasto { get; set; }
        public virtual Vendedor vendedor { get; set; }
        public virtual string descripcion { get; set; }
        public virtual decimal monto { get; set; }
        public virtual FormaPago forma_de_pago { get; set; }
        public virtual bool anulado { get; set; }

        public Gasto() { }

        public virtual DataGridView customColumns(DataGridView dgw)
        {

            dgw.Columns["id"].HeaderText = "Numero";
            dgw.Columns["sucursal"].HeaderText = "Sucursal";
            dgw.Columns["fecha"].HeaderText = "Fecha";
            dgw.Columns["vendedor"].HeaderText = "Vendedor";
            dgw.Columns["familia"].HeaderText = "Familia";
            dgw.Columns["subfamilia_gasto"].HeaderText = "Subfamilia";
            dgw.Columns["forma_de_pago"].HeaderText = "Forma de Pago";
            dgw.Columns["monto"].HeaderText = "Monto";
            dgw.Columns["anulado"].HeaderText = "Anulado";
            dgw.Columns["descripcion"].HeaderText = "Descripcion";

            //ModDataGridView.ocultarColumna(dgw, "descripcion");

            dgw.Columns["anulado"].Visible = false;
            dgw.Columns["sucursal"].Width = 150;
            dgw.Columns["descripcion"].Width = 160;
            dgw.Columns["familia"].Width = 140;
            dgw.Columns["fecha"].Width = 80;
            dgw.Columns["id"].Width = 60; 
            dgw.Columns["forma_de_pago"].Width = 80;

            return dgw;
        }

        public virtual Boolean save()
        {
            using (var session = SessionManager.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    try
                    {
                        session.Save(this);
                        transaction.Commit();

                        SessionManager.EndTasks();
                        //session.Close();

                        return true;
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        Logs.GetInstance.LogError(ex.Message, "Gasto.save()");
                        MessageBox.Show(ex.Message, "Exception Msg");

                        return false;
                    }


                }
            }
        }

        public virtual Boolean update()
        {
            using (var session = SessionManager.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    try
                    {
                        session.Update(this);
                        transaction.Commit();

                        SessionManager.EndTasks();
                        //session.Close();

                        return true;
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        Logs.GetInstance.LogError(ex.Message, "Gasto.update()");
                        MessageBox.Show(ex.Message, "Exception Msg");

                        return false;
                    }
                }
            }
        }

        public virtual Boolean delete()
        {
            using (var session = SessionManager.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    try
                    {
                        session.Delete(this);
                        transaction.Commit();

                        SessionManager.EndTasks();
                        //session.Close();

                        return true;
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        Logs.GetInstance.LogError(ex.Message, "Gasto.delete()");
                        MessageBox.Show(ex.Message, "Exception Msg");

                        return false;
                    }
                }
            }
        }

        public virtual Gasto getById(int id)
        {
            using (var session = SessionManager.OpenSession())
            {
                try
                {
                    var results = session.QueryOver<Gasto>()
                                     .Where(c => c.id == id)
                                     .List<Gasto>();

                    SessionManager.EndTasks();
                    //session.Close();

                    if (results.Count == 1)
                    {
                        Gasto result = results[0];

                        return result;
                    }
                    return null;
                }
                catch (Exception ex)
                {
                    Logs.GetInstance.LogError(ex.Message, "Gasto.getById()");
                    MessageBox.Show(ex.Message, "Exception Msg");

                    return null;
                }
            }
        }

        public virtual IList<Gasto> Search(bool anulado, int familia_id, int subfamilia_id, int sucursal_id, string desde, string hasta)
        {
            using (var session = SessionManager.OpenSession())
            {
                try
                {
                    var query = session.QueryOver<Gasto>();

                    query = query.Where(c => c.anulado == anulado);

                    if (familia_id != 0)
                    {
                        query = query.Where(c => c.familia.id == familia_id);
                    }

                    if (subfamilia_id != 0)
                    {
                        query = query.Where(c => c.subfamilia_gasto.id == subfamilia_id);
                    }

                    if (sucursal_id != 0)
                    {
                        query = query.Where(c => c.sucursal.id == sucursal_id);
                    }

                    if (desde != " " && desde != "")
                    {
                        query = query.Where(c => c.fecha >= DateTime.Parse(desde));
                    }

                    if (hasta != " " && hasta != "")
                    {
                        query = query.Where(c => c.fecha <= DateTime.Parse(hasta));
                    }

                    query = query.OrderBy(x => x.fecha).Desc.OrderBy(x => x.id).Desc;

                    var list = query.List<Gasto>();
                    SessionManager.EndTasks();
                    //session.Close();

                    return list;
                }
                catch (Exception ex)
                {
                    Logs.GetInstance.LogError(ex.Message, "Gasto.Search()");
                    MessageBox.Show(ex.Message, "Exception Msg");

                    return null;
                }
            }
        }

        public virtual DataTable Estadistica(string desde, string hasta,string familia)
        {
            using (var session = SessionManager.OpenSession())
            {
                try
                {
                // Set cursor as hourglass
                Cursor.Current = Cursors.WaitCursor;

                    string Consulta = "";


                    if (familia == null)
                    {
                        Consulta = "SELECT fecha as Fecha,FM.nombre as Familia,SFM.nombre as Subfamilia,sum(monto) as Importe FROM gastos as G";
                        Consulta += " JOIN familias_gastos as FM on FM.id=G.familia_gasto";
                        Consulta += " LEFT JOIN subfamilia_gastos as SFM on SFM.id=G.subfamilia_gasto";
                        Consulta += " WHERE fecha >= '" + desde + "' and fecha <= '" + hasta + "' and anulado=0";
                        Consulta += " GROUP BY FM.id,SFM.id ORDER BY fecha ASC";
                    }
                    else {
                        Consulta = "SELECT fecha as Fecha,FM.nombre as Familia,SFM.nombre as Subfamilia,sum(monto) as Importe FROM gastos as G";
                        Consulta += " JOIN familias_gastos as FM on FM.id=G.familia_gasto";
                        Consulta += " LEFT JOIN subfamilia_gastos as SFM on SFM.id=G.subfamilia_gasto";
                        Consulta += " WHERE fecha >= '" + desde + "' and fecha <= '" + hasta + "' and anulado=0 and G.familia_gasto = " + familia;
                        Consulta += " GROUP BY FM.id,SFM.id ORDER BY fecha ASC";
                    }
                    

                    List<MySqlParameter> param = new List<MySqlParameter>();

                    DataTable dt = BaseDeDatos.GetInstance.ExecuteCustomQuery(Consulta, param, "Estadisticas");

                    if (dt != null) {
                        dt.Columns.Add("Acumulado");
                        decimal saldo = 0;

                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            decimal total = Convert.ToDecimal(dt.Rows[i]["Importe"].ToString());
                            saldo = saldo + total;
                            dt.Rows[i]["Acumulado"] = saldo;          
                        }
                    }

                    return dt;

                }
                catch (Exception ex)
                {
                    Logs.GetInstance.LogError(ex.Message, "Gasto.Estadistica()");
                    MessageBox.Show(ex.Message, "Exception Msg");

                    return null;
                }
            }
        }
    }
}
