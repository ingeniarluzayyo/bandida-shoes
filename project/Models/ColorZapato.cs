﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Zapateria.Libs;
using NHibernate;
using NHibernate.Criterion;

namespace Zapateria.Models
{
    public class ColorZapato
    {
        public virtual Int32 id { get; set; }
        public virtual String nombre { get; set; }

        public ColorZapato()
        {

        }

        public override string ToString()
        {
            return nombre;
        }

        public virtual DataGridView customColumns(DataGridView dgw)
        {
            dgw.Columns["id"].HeaderText = "id";
            dgw.Columns["nombre"].HeaderText = "Nombre";

            ModDataGridView.ocultarColumna(dgw, "id");
            ModDataGridView.widthColumna(dgw, "nombre", 200);

            return dgw;
        }

        public virtual Boolean save()
        {
            using (var session = SessionManager.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    try
                    {
                        session.Save(this);
                        transaction.Commit();

                        SessionManager.EndTasks();

                        return true;
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        Logs.GetInstance.LogError(ex.Message, "ColorZapato.save()");
                        MessageBox.Show(ex.Message, "Exception Msg");

                        return false;
                    }
                }
            }
        }

        public virtual Boolean update()
        {
            using (var session = SessionManager.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    try
                    {
                        session.Update(this);
                        transaction.Commit();

                        SessionManager.EndTasks();

                        return true;
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        Logs.GetInstance.LogError(ex.Message, "ColorZapato.update()");
                        MessageBox.Show(ex.Message, "Exception Msg");

                        return false;
                    }
                }
            }
        }

        public virtual Boolean delete()
        {
            using (var session = SessionManager.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    try
                    {
                        session.Delete(this);
                        transaction.Commit();

                        SessionManager.EndTasks();

                        return true;
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        Logs.GetInstance.LogError(ex.Message, "ColorZapato.delete()");
                        MessageBox.Show(ex.Message, "Exception Msg");

                        return false;
                    }
                }
            }
        }

        public virtual IList<ColorZapato> getColoresLike()
        {
            using (var session = SessionManager.OpenSession())
            {
                try
                {
                    var colores = session.QueryOver<ColorZapato>()
                                     .Where(c => c.nombre.IsLike(this.nombre, MatchMode.Anywhere))
                                     .List<ColorZapato>();

                    SessionManager.EndTasks();

                    return colores;
                }
                catch (Exception ex)
                {
                    Logs.GetInstance.LogError(ex.Message, "ColorZapato.getColoresLike()");
                    MessageBox.Show(ex.Message, "Exception Msg");

                    return null;
                }
            }
        }

        public virtual ColorZapato getById(int id_value)
        {
            using (var session = SessionManager.OpenSession())
            {
                try
                {
                    var results = session.QueryOver<ColorZapato>()
                                     .Where(c => c.id == id_value)
                                     .List<ColorZapato>();

                    SessionManager.EndTasks();

                    if (results.Count == 1)
                    {
                        ColorZapato result = results[0];

                        return result;
                    }
                    return null;
                }
                catch (Exception ex)
                {
                    Logs.GetInstance.LogError(ex.Message, "ColorZapato.getById()");
                    MessageBox.Show(ex.Message, "Exception Msg");

                    return null;
                }
            }
        }

        public virtual ColorZapato Search(string nombre)
        {
            using (var session = SessionManager.OpenSession())
            {
                try
                {
                    var query = session.QueryOver<ColorZapato>();

                    if (nombre != " " && nombre != "")
                    {
                        query = query.Where(c => c.nombre == nombre);
                    }

                    ColorZapato color = query.SingleOrDefault<ColorZapato>();

                    SessionManager.EndTasks();

                    return color;
                }
                catch (Exception ex)
                {
                    Logs.GetInstance.LogError(ex.Message, "Talle.Search()");
                    MessageBox.Show(ex.Message, "Exception Msg");

                    return null;
                }
            }
        }
    }
}
