﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NHibernate.Criterion;
using NHibernate;
using NHibernate.Transform;
using System.Windows.Forms;
using Zapateria.Libs;

namespace Zapateria.Models
{
    public class Vendedor
    {
        public virtual int id { get; set; }
        public virtual Sucursal sucursal { get; set; }
        public virtual string nombre { get; set; }
        public virtual string apellido { get; set; }
        public virtual string dni { get; set; }
        public virtual DateTime fechanacimiento { get; set; }
        public virtual string tel { get; set; }
        public virtual string cel { get; set; }
        public virtual string direccion { get; set; }
        public virtual string mail { get; set; }
        public virtual string observacion { get; set; }
        public virtual decimal comision { get; set; }
        public virtual string username { get; set; }
        public virtual string password { get; set; }
        public virtual string habilitado { get; set; }

        public Vendedor() { }

        public virtual DataGridView customColumns(DataGridView dgw)
        {

            /*dgw.Columns["id"].HeaderText = "Id";
            dgw.Columns["nombre"].HeaderText = "Nombre";

            ModDataGridView.ocultarColumna(dgw, "id");
            ModDataGridView.ocultarColumna(dgw, "modulos");*/

            return dgw;
        }

        public virtual Boolean save()
        {
            using (var session = SessionManager.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    try
                    {
                        session.Save(this);
                        transaction.Commit();

                        SessionManager.EndTasks();
                        //session.Close();

                        return true;
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        Logs.GetInstance.LogError(ex.Message, "Vendedor.save()");
                        MessageBox.Show(ex.Message, "Exception Msg");

                        return false;
                    }


                }
            }
        }

        public virtual Boolean update()
        {
            using (var session = SessionManager.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    try
                    {
                        session.Update(this);
                        transaction.Commit();

                        SessionManager.EndTasks();
                        //session.Close();

                        return true;
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        Logs.GetInstance.LogError(ex.Message, "Vendedor.update()");
                        MessageBox.Show(ex.Message, "Exception Msg");

                        return false;
                    }
                }
            }
        }

        public virtual Boolean delete()
        {
            using (var session = SessionManager.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    try
                    {
                        session.Delete(this);
                        transaction.Commit();

                        SessionManager.EndTasks();
                        //session.Close();

                        return true;
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        Logs.GetInstance.LogError(ex.Message, "Vendedor.delete()");
                        MessageBox.Show(ex.Message, "Exception Msg");

                        return false;
                    }
                }
            }
        }

        public virtual Vendedor getById(int id_value)
        {
            using (var session = SessionManager.OpenSession())
            {
                try
                {
                    var results = session.QueryOver<Vendedor>()
                                     .Where(c => c.id == id_value)
                                     .List<Vendedor>();

                    SessionManager.EndTasks();
                    //session.Close();

                    if (results.Count == 1)
                    {
                        Vendedor result = results[0];

                        return result;
                    }
                    return null;
                }
                catch (Exception ex)
                {
                    Logs.GetInstance.LogError(ex.Message, "Vendedor.getById()");
                    MessageBox.Show(ex.Message, "Exception Msg");

                    return null;
                }
            }
        }

        public virtual IList<Vendedor> getLike()
        {
            using (var session = SessionManager.OpenSession())
            {
                try
                {
                    var roles = session.QueryOver<Vendedor>()
                                     .Where(c => c.nombre.IsLike(this.nombre, MatchMode.Anywhere))
                                     .List<Vendedor>();

                    SessionManager.EndTasks();
                    //session.Close();

                    return roles;
                }
                catch (Exception ex)
                {
                    Logs.GetInstance.LogError(ex.Message, "Vendedor.getLike()");
                    MessageBox.Show(ex.Message, "Exception Msg");

                    return null;
                }
            }
        }

        public virtual IList<Vendedor> Search(string nombre, string apellido, string dni)
        {
            using (var session = SessionManager.OpenSession())
            {
                try
                {
                    var query = session.QueryOver<Vendedor>();

                    if (nombre != " " && nombre != "")
                    {
                        query = query.Where(c => c.nombre.IsLike(nombre, MatchMode.Anywhere));
                    }

                    if (apellido != " " && apellido != "")
                    {
                        query = query.Where(c => c.apellido.IsLike(apellido, MatchMode.Anywhere));
                    }

                    if (dni != " " && dni != "")
                    {
                        query = query.Where(c => c.dni == dni);
                    }


                    var list = query.List<Vendedor>();
                    SessionManager.EndTasks();          

                    return list;
                }
                catch (Exception ex)
                {
                    Logs.GetInstance.LogError(ex.Message, "Vendedor.Search()");
                    MessageBox.Show(ex.Message, "Exception Msg");

                    return null;
                }
            }
        }

        public virtual IList<FacturaVenta> Estadistica(string desde, string hasta)
        {
            using (var session = SessionManager.OpenSession())
            {
                try
                {
                    var query = session.QueryOver<FacturaVenta>();

                    if (desde != " " && desde != "")
                    {
                        query = query.Where(c => c.fecha >= DateTime.Parse(desde));
                    }

                    if (hasta != " " && hasta != "")
                    {
                        query = query.Where(c => c.fecha <= DateTime.Parse(hasta));
                    }

                    query = query.OrderBy(x => x.total_comision).Desc;

                    var list = query.List<FacturaVenta>();
                    SessionManager.EndTasks();


                    return list;
                }
                catch (Exception ex)
                {
                    Logs.GetInstance.LogError(ex.Message, "Vendedor.Estadistica()");
                    MessageBox.Show(ex.Message, "Exception Msg");

                    return null;
                }
            }
        }

        public override string ToString()
        {
            return apellido + " " + nombre;
        }
    }
}
