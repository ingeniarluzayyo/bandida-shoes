﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using NHibernate;
using NHibernate.Transform;
using System.Windows.Forms;
using Zapateria.Libs;

namespace Zapateria.Models
{
    public class FacturaVenta
    {
        public virtual int id { get; set; }
        public virtual Sucursal sucursal { get; set; }
        public virtual DateTime fecha { get; set; }
        public virtual Cliente cliente { get; set; }
        public virtual Vendedor vendedor { get; set; }
        public virtual FormaPago forma_de_pago { get; set; }
        public virtual IList<ItemVenta> items { get; set; }
        public virtual decimal descuento { get; set; }
        public virtual decimal total { get; set; }
        public virtual decimal total_comision { get; set; }
        public virtual bool anulada { get; set; }

        public FacturaVenta()
        {
            items = new List<ItemVenta>();
        }

        public virtual DataGridView customColumns(DataGridView dgw){
        
            dgw.Columns["id"].HeaderText = "Numero";
            dgw.Columns["fecha"].HeaderText = "Fecha";
            dgw.Columns["cliente"].HeaderText = "Cliente";
            dgw.Columns["sucursal"].HeaderText = "Sucursal";
            dgw.Columns["vendedor"].HeaderText = "Vendedor";
            dgw.Columns["forma_de_pago"].HeaderText = "Forma de Pago";
            dgw.Columns["descuento"].HeaderText = "Descuento";
            dgw.Columns["total"].HeaderText = "Total";
            dgw.Columns["total_comision"].HeaderText = "Total Comision";
            dgw.Columns["anulada"].HeaderText = "Anulada";            

            ModDataGridView.ocultarColumna(dgw, "items");
            ModDataGridView.ocultarColumna(dgw, "anulada");
            ModDataGridView.ocultarColumna(dgw, "total_comision");
         
            dgw.Columns["id"].Width = 90;
            dgw.Columns["cliente"].Width = 160;
            dgw.Columns["vendedor"].Width = 160;
            dgw.Columns["total_comision"].Width = 100;
            dgw.Columns["forma_de_pago"].Width = 120;

            return dgw;
        }

        public virtual Boolean save() {

            // Set cursor as hourglass
            Cursor.Current = Cursors.WaitCursor;

            using (var session = SessionManager.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    try
                    { 
                        session.SaveOrUpdate(this);
                        transaction.Commit();

                        SessionManager.EndTasks();
                        //session.Close();

                        CtaCte ctaCte = new CtaCte();
                        ctaCte = ctaCte.getByCliente(cliente.id);
                        if (ctaCte == null)
                        {
                            ctaCte = new CtaCte();
                            ctaCte.cliente = cliente;
                            ctaCte.total_pagado = 0;
                            ctaCte.total_facturado = 0;
                        }

                        ctaCte.total_facturado += total;

                        if (!forma_de_pago.esACredito())
                        {
                            ctaCte.total_pagado += total;
                        }

                        ctaCte.saldo = ctaCte.total_pagado - ctaCte.total_facturado;
                        ctaCte.ultima_actualizacion = DateTime.Now;

                        ctaCte.save();

                        return true;
                    }catch (Exception ex)
                    {
                        transaction.Rollback();
                        Logs.GetInstance.LogError(ex.Message, "FacturaVenta.save()");
                        MessageBox.Show(ex.Message, "Exception Msg");

                        return false;
                    }
                    finally
                    {
                        // Set cursor as default arrow
                        Cursor.Current = Cursors.Default;
                    }

                    
                }
            }
        }

        public virtual Boolean update()
        {
            // Set cursor as hourglass
            Cursor.Current = Cursors.WaitCursor;

            using (var session = SessionManager.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    try
                    {                       
                        session.Update(this);
                        transaction.Commit();

                        SessionManager.EndTasks();
                        //session.Close();

                        CtaCte ctaCte = new CtaCte();
                        ctaCte = ctaCte.getByCliente(cliente.id);
                        if (ctaCte == null)
                        {
                            ctaCte = new CtaCte();
                            ctaCte.cliente = cliente;
                            ctaCte.total_pagado = 0;
                            ctaCte.total_facturado = 0;
                        }

                        ctaCte.total_facturado -= total;

                        if (!forma_de_pago.esACredito())
                        {
                            ctaCte.total_pagado -= total;
                        }

                        ctaCte.saldo = ctaCte.total_pagado - ctaCte.total_facturado;
                        ctaCte.ultima_actualizacion = DateTime.Now;

                        ctaCte.save();

                        return true;
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        Logs.GetInstance.LogError(ex.Message, "FacturaVenta.update()");
                        MessageBox.Show(ex.Message, "Exception Msg");

                        return false;
                    }
                    finally
                    {
                        // Set cursor as default arrow
                        Cursor.Current = Cursors.Default;
                    }
                }
            }
        }        

        /**
         * Parameter partial: indica si debe traer todo el objeto (incluyendo la coleccion de items) o solo los parametros basicos simples, ej: para un update.
         * 
         */
        public virtual FacturaVenta getById(int id_value, bool partial = false)
        {
            // Set cursor as hourglass
            Cursor.Current = Cursors.WaitCursor;

            using (var session = SessionManager.OpenSession())
            {
                try
                {
                    var roles = session.QueryOver<FacturaVenta>()
                                     .Where(c => c.id == id_value)
                                     .List<FacturaVenta>();

                    SessionManager.EndTasks();
                    //session.Close();

                    if (roles.Count == 1)
                    {
                        FacturaVenta result = roles[0];
                        if (!partial)
                        {
                            result.items = SelectItems(id_value);
                        }

                        return result;
                    }
                    return null;
                }
                catch (Exception ex)
                {
                    Logs.GetInstance.LogError(ex.Message, "FacturaVenta.getById()");
                    MessageBox.Show(ex.Message, "Exception Msg");

                    return null;
                }
                finally
                {
                    // Set cursor as default arrow
                    Cursor.Current = Cursors.Default;
                }
            }
        }

        public virtual IList<FacturaVenta> getFacturas()
        {
            // Set cursor as hourglass
            Cursor.Current = Cursors.WaitCursor;

            using (var session = SessionManager.OpenSession())
            {
               try
               {
                   var results = session.QueryOver<FacturaVenta>()
                                    .Where(c => c.id == this.id)
                                    .List<FacturaVenta>();

                   SessionManager.EndTasks();
                   //session.Close();

                   return results;
               }
               catch (Exception ex)
               {
                   Logs.GetInstance.LogError(ex.Message, "FacturaVenta.getFacturas()");
                   MessageBox.Show(ex.Message, "Exception Msg");

                   return null;
               }
               finally
               {
                   // Set cursor as default arrow
                   Cursor.Current = Cursors.Default;
               }
            }
        }

        public virtual IList<ItemVenta> SelectItems(int id_value)
        {
            // Set cursor as hourglass
            Cursor.Current = Cursors.WaitCursor;

            using (var session = SessionManager.OpenSession())
            {
                try
                {
                    var items = session.QueryOver<ItemVenta>()
                            .Right.JoinQueryOver<FacturaVenta>(x => x.factura)
                            .Where(c => c.id == id_value)
                            .List();

                    SessionManager.EndTasks();
                    //session.Close();

                    return items;
                }
                catch (Exception ex)
                {
                    Logs.GetInstance.LogError(ex.Message, "FacturaVenta.SelectItemsCompra()");
                    MessageBox.Show(ex.Message, "Exception Msg");

                    return null;
                }
                finally
                {
                    // Set cursor as default arrow
                    Cursor.Current = Cursors.Default;
                }
            }
        }

        public virtual IList<FacturaVenta> Search(bool anulada, int venta_id, int cliente_id, int venderor_id, string desde, string hasta,int sucursal_id)
        {
            // Set cursor as hourglass
            Cursor.Current = Cursors.WaitCursor;

            using (var session = SessionManager.OpenSession())
            {
                try
                {
                    var query = session.QueryOver<FacturaVenta>().JoinAlias(x => x.cliente, () => cliente);

                    if (venta_id > 0)
                    {
                        query = query.Where(c => c.id == venta_id);
                    }

                    if (sucursal_id > 0)
                    {
                        query = query.Where(c => c.sucursal.id == sucursal_id);
                    }

                    if (cliente_id > 0)
                    {
                        query = query.Where(c => c.cliente.id == cliente_id);
                    }

                    if (venderor_id > 0)
                    {
                        query = query.Where(c => c.vendedor.id == venderor_id);
                    }

                    if (desde != " " && desde != "")
                    {
                        query = query.Where(c => c.fecha >= DateTime.Parse(desde));
                    }

                    if (hasta != " " && hasta != "")
                    {
                        query = query.Where(c => c.fecha <= DateTime.Parse(hasta));
                    }

                    query = query.Where(c => c.anulada == anulada);
                    query = query.OrderBy(c => c.id).Desc;

                   
                    var list = query.List<FacturaVenta>();
                    SessionManager.EndTasks();
                    //session.Close();

                    return list;
                }
                catch (Exception ex)
                {
                    Logs.GetInstance.LogError(ex.Message, "FacturaVenta.Search()");
                    MessageBox.Show(ex.Message, "Exception Msg");

                    return null;
                }
                finally {
                    // Set cursor as default arrow
                    Cursor.Current = Cursors.Default;
                }
            }
        }

        public virtual IList<FacturaVenta> Estadistica(string desde, string hasta)
        {
            // Set cursor as hourglass
            Cursor.Current = Cursors.WaitCursor;

            using (var session = SessionManager.OpenSession())
            {
                try
                {
                    var query = session.QueryOver<FacturaVenta>();

                    query = query.Where(c => c.anulada == false);

                    if (desde != " " && desde != "")
                    {
                        query = query.Where(c => c.fecha >= DateTime.Parse(desde));
                    }

                    if (hasta != " " && hasta != "")
                    {
                        query = query.Where(c => c.fecha <= DateTime.Parse(hasta));
                    }

                    query = query.OrderBy(x => x.total).Desc;

                    var list = query.List<FacturaVenta>();
                    SessionManager.EndTasks();
                    

                    return list;
                }
                catch (Exception ex)
                {
                    Logs.GetInstance.LogError(ex.Message, "FacturaVenta.Estadistica()");
                    MessageBox.Show(ex.Message, "Exception Msg");

                    return null;
                }
                finally
                {
                    // Set cursor as default arrow
                    Cursor.Current = Cursors.Default;
                }
            }
        }
    }
}
