﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using NHibernate;
using NHibernate.Transform;
using System.Windows.Forms;

using Zapateria.Libs;

namespace Zapateria.Models
{
    public class FacturaCompra
    {
        public virtual int id { get; set; }
        public virtual Sucursal sucursal { get; set; }
        public virtual String num_fact_externo { get; set; }
        public virtual DateTime fecha { get; set; }
        public virtual Proveedor proveedor { get; set; }
        public virtual FormaPago forma_de_pago { get; set; }
        public virtual IList<ItemCompra> items { get; set; }
        public virtual decimal descuento { get; set; }
        public virtual decimal total { get; set; }
        public virtual bool anulada { get; set; }

        public FacturaCompra()
        {
            items = new List<ItemCompra>();
        }

        public virtual DataGridView customColumns(DataGridView dgw){
        
            dgw.Columns["id"].HeaderText = "Numero";
            dgw.Columns["fecha"].HeaderText = "Fecha";
            dgw.Columns["proveedor"].HeaderText = "Proveedor";
            dgw.Columns["descuento"].HeaderText = "Descuento";
            dgw.Columns["total"].HeaderText = "Total";
            dgw.Columns["num_fact_externo"].HeaderText = "N° Fact Externo";
            dgw.Columns["forma_de_pago"].HeaderText = "Forma de Pago";
            
            ModDataGridView.ocultarColumna(dgw, "items");
            ModDataGridView.ocultarColumna(dgw, "anulada");

            dgw.Columns["id"].Width = 90;

            return dgw;
        }

        public virtual Boolean save() {

            // Set cursor as hourglass
            Cursor.Current = Cursors.WaitCursor;

            using (var session = SessionManager.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    try
                    {
                        session.SaveOrUpdate(this);
                        transaction.Commit();

                        SessionManager.EndTasks();
                        //session.Close();

                        CtaCteProveedor ctaCte = new CtaCteProveedor();
                        ctaCte = ctaCte.getByProveedor(proveedor.id);
                        if (ctaCte == null)
                        {
                            ctaCte = new CtaCteProveedor();
                            ctaCte.proveedor = proveedor;
                            ctaCte.total_pagado = 0;
                            ctaCte.total_facturado = 0;
                        }

                        ctaCte.total_facturado += total;

                        if (!forma_de_pago.esACredito())
                        {
                            ctaCte.total_pagado += total;
                        }

                        ctaCte.saldo = ctaCte.total_pagado - ctaCte.total_facturado;
                        ctaCte.ultima_actualizacion = DateTime.Now;

                        ctaCte.save();

                        return true;
                    }catch (Exception ex)
                    {
                        transaction.Rollback();
                        Logs.GetInstance.LogError(ex.Message, "FacturaCompra.save()");
                        MessageBox.Show(ex.Message, "Exception Msg");

                        return false;
                    }
                    finally
                    {
                        // Set cursor as default arrow
                        Cursor.Current = Cursors.Default;
                    }

                    
                }
            }
        }

        public virtual Boolean update()
        {
            // Set cursor as hourglass
            Cursor.Current = Cursors.WaitCursor;

            using (var session = SessionManager.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    try
                    {                       
                        session.Update(this);
                        transaction.Commit();

                        SessionManager.EndTasks();
                        //session.Close();

                        return true;
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        Logs.GetInstance.LogError(ex.Message, "Rol.update()");
                        MessageBox.Show(ex.Message, "Exception Msg");

                        return false;
                    }
                    finally
                    {
                        // Set cursor as default arrow
                        Cursor.Current = Cursors.Default;
                    }
                }
            }
        }        

        /**
         * Parameter partial: indica si debe traer todo el objeto (incluyendo la coleccion de items) o solo los parametros basicos simples, ej: para un update.
         * 
         */
        public virtual FacturaCompra getById(int id_value,bool partial = false)
        {
            // Set cursor as hourglass
            Cursor.Current = Cursors.WaitCursor;

            using (var session = SessionManager.OpenSession())
            {
                try
                {
                    var roles = session.QueryOver<FacturaCompra>()
                                     .Where(c => c.id == id_value)
                                     .List<FacturaCompra>();

                    SessionManager.EndTasks();
                    //session.Close();

                    if (roles.Count == 1)
                    {
                        FacturaCompra result = roles[0];
                        if (!partial)
                        {
                            result.items = SelectItemsCompra(id_value);
                        }

                        return result;
                    }
                    return null;
                }
                catch (Exception ex)
                {
                    Logs.GetInstance.LogError(ex.Message, "FacturaCompra.getById()");
                    MessageBox.Show(ex.Message, "Exception Msg");

                    return null;
                }
                finally
                {
                    // Set cursor as default arrow
                    Cursor.Current = Cursors.Default;
                }
            }
        }

        public virtual IList<FacturaCompra> getFacturas()
        {
            using (var session = SessionManager.OpenSession())
            {
               try
               {
                   var results = session.QueryOver<FacturaCompra>()
                                    .Where(c => c.id == this.id)
                                    .List<FacturaCompra>();

                   SessionManager.EndTasks();
                   //session.Close();

                   return results;
               }
               catch (Exception ex)
               {
                   Logs.GetInstance.LogError(ex.Message, "Factura.getFacturas()");
                   MessageBox.Show(ex.Message, "Exception Msg");

                   return null;
               }
            }
        }

        public virtual IList<ItemCompra> SelectItemsCompra(int id_value)
        {
            // Set cursor as hourglass
            Cursor.Current = Cursors.WaitCursor;

            using (var session = SessionManager.OpenSession())
            {
                try
                {
                    var items = session.QueryOver<ItemCompra>()
                            .Right.JoinQueryOver<FacturaCompra>(x => x.factura)
                            .Where(c => c.id == id_value)
                            .List();

                    SessionManager.EndTasks();
                    //session.Close();

                    return items;
                }
                catch (Exception ex)
                {
                    Logs.GetInstance.LogError(ex.Message, "FacturaCompra.SelectItemsCompra()");
                    MessageBox.Show(ex.Message, "Exception Msg");

                    return null;
                }
                finally
                {
                    // Set cursor as default arrow
                    Cursor.Current = Cursors.Default;
                }
            }
        }

        public virtual IList<FacturaCompra> Search(bool anulada, int compra_id, int proveedor_id, string desde, string hasta, int producto_id,int sucursal_id)
        {
            // Set cursor as hourglass
            Cursor.Current = Cursors.WaitCursor;

            using (var session = SessionManager.OpenSession())
            {
                try
                {
                    var query = session.QueryOver<FacturaCompra>();

                    if (compra_id > 0)
                    {
                        query = query.Where(c => c.id == compra_id);
                    }

                    if (proveedor_id > 0)
                    {
                        query = query.Where(c => c.proveedor.id == proveedor_id);
                    }

                    if (sucursal_id > 0)
                    {
                        query = query.Where(c => c.sucursal.id == sucursal_id);
                    }

                    if (desde != " " && desde != "")
                    {
                        query = query.Where(c => c.fecha >= DateTime.Parse(desde));
                    }

                    if (hasta != " " && hasta != "")
                    {
                        query = query.Where(c => c.fecha <= DateTime.Parse(hasta));
                    }

                    query = query.Where(c => c.anulada == anulada).OrderBy(c => c.id).Desc;

                    var list = query.List<FacturaCompra>();
                    SessionManager.EndTasks();
                    //session.Close();

                    if(producto_id > 0 )
                        list = list.Where(c => c.items.Any(m => m.producto.id == producto_id)).ToList();

                    return list;
                }
                catch (Exception ex)
                {
                    Logs.GetInstance.LogError(ex.Message, "FacturaCompra.Search()");
                    MessageBox.Show(ex.Message, "Exception Msg");

                    return null;
                }
                finally
                {
                    // Set cursor as default arrow
                    Cursor.Current = Cursors.Default;
                }
            }
        }

        public virtual IList<FacturaCompra> Estadistica(string desde, string hasta)
        {
            // Set cursor as hourglass
            Cursor.Current = Cursors.WaitCursor;

            using (var session = SessionManager.OpenSession())
            {
                try
                {
                    var query = session.QueryOver<FacturaCompra>();

                    query = query.Where(c => c.anulada == false);

                    if (desde != " " && desde != "")
                    {
                        query = query.Where(c => c.fecha >= DateTime.Parse(desde));
                    }

                    if (hasta != " " && hasta != "")
                    {
                        query = query.Where(c => c.fecha <= DateTime.Parse(hasta));
                    }

                    query = query.OrderBy(x => x.total).Desc;

                    var list = query.List<FacturaCompra>();
                    SessionManager.EndTasks();


                    return list;
                }
                catch (Exception ex)
                {
                    Logs.GetInstance.LogError(ex.Message, "FacturaCompra.Estadistica()");
                    MessageBox.Show(ex.Message, "Exception Msg");

                    return null;
                }
                finally
                {
                    // Set cursor as default arrow
                    Cursor.Current = Cursors.Default;
                }
            }
        }
    }
}
