﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Zapateria.Libs;
using NHibernate;
using System.Windows.Forms;

namespace Zapateria.Models
{
    class Notificaciones
    {
        public virtual int id { get; set; }
        public virtual string descripcion { get; set; }
        public virtual string fecha { get; set; }
        public virtual string estado { get; set; }

        public Notificaciones()
        {
        }


        public virtual Boolean save()
        {
            // Set cursor as hourglass
            Cursor.Current = Cursors.WaitCursor;

            using (var session = FluentNHibernateDB.CreateSessionFactory().OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    try
                    {
                        session.Save(this);
                        transaction.Commit();

                        session.Flush();
                        session.Close();

                        return true;
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        Logs.GetInstance.LogError(ex.Message, "Notificaciones.save()");
                        MessageBox.Show(ex.Message, "Exception Msg");

                        return false;
                    }
                    finally
                    {
                        // Set cursor as default arrow
                        Cursor.Current = Cursors.Default;
                    }


                }
            }
        }

        public virtual Boolean update()
        {
            // Set cursor as hourglass
            Cursor.Current = Cursors.WaitCursor;

            using (var session = FluentNHibernateDB.CreateSessionFactory().OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    try
                    {
                        session.Update(this);
                        transaction.Commit();

                        session.Flush();
                        session.Close();

                        return true;
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        Logs.GetInstance.LogError(ex.Message, "Notificaciones.update()");
                        MessageBox.Show(ex.Message, "Exception Msg");

                        return false;
                    }
                    finally
                    {
                        // Set cursor as default arrow
                        Cursor.Current = Cursors.Default;
                    }
                }
            }
        }

        public virtual Boolean delete()
        {
            // Set cursor as hourglass
            Cursor.Current = Cursors.WaitCursor;

            using (var session = FluentNHibernateDB.CreateSessionFactory().OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    try
                    {
                        session.Delete(this);
                        transaction.Commit();

                        session.Flush();
                        session.Close();

                        return true;
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        Logs.GetInstance.LogError(ex.Message, "Notificaciones.delete()");
                        MessageBox.Show(ex.Message, "Exception Msg");

                        return false;
                    }
                    finally
                    {
                        // Set cursor as default arrow
                        Cursor.Current = Cursors.Default;
                    }
                }
            }
        }

        public virtual Notificaciones getLast()
        {
            // Set cursor as hourglass
            Cursor.Current = Cursors.WaitCursor;

            using (var session = SessionManager.OpenSession())
            {
                try
                {
                    var results = session.QueryOver<Notificaciones>()
                                     .OrderBy(c => c.id).Desc
                                     .List<Notificaciones>();

                    SessionManager.EndTasks();
                    //session.Close();

                    if (results.Count >= 1)
                    {
                        Notificaciones result = results[0];

                        return result;
                    }
                    return null;
                }
                catch (Exception ex)
                {
                    Logs.GetInstance.LogError(ex.Message, "Notificaciones.getLast()");
                    MessageBox.Show(ex.Message, "Exception Msg");

                    return null;
                }
                finally
                {
                    // Set cursor as default arrow
                    Cursor.Current = Cursors.Default;
                }
            }
        }

        public virtual Notificaciones getEnEspera()
        {
            // Set cursor as hourglass
            Cursor.Current = Cursors.WaitCursor;

            using (var session = SessionManager.OpenSession())
            {
                try
                {
                    var results = session.QueryOver<Notificaciones>()
                                     .Where(c => c.estado == "En espera" || c.estado == "En proceso")
                                     .And(c => c.descripcion == "Lista de precios")
                                     .List<Notificaciones>();

                    SessionManager.EndTasks();
                    //session.Close();

                    if (results.Count >= 1)
                    {
                        Notificaciones result = results[0];

                        return result;
                    }
                    return null;
                }
                catch (Exception ex)
                {
                    Logs.GetInstance.LogError(ex.Message, "Notificaciones.getEnEspera()");
                    MessageBox.Show(ex.Message, "Exception Msg");

                    return null;
                }
                finally
                {
                    // Set cursor as default arrow
                    Cursor.Current = Cursors.Default;
                }
            }
        }


    }
}
