﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NHibernate.Criterion;
using NHibernate;
using NHibernate.Transform;
using System.Windows.Forms;
using Zapateria.Libs;
using MySql.Data.MySqlClient;
using System.Data;

namespace Zapateria.Models
{
    public class Producto
    {
        public virtual int id { get; set; }
        public virtual Familia familia  { get; set; }
        public virtual String nombre { get; set; }
        public virtual String observacion { get; set; }
        public virtual decimal stock { get; set; }
        public virtual bool unidad { get; set; }
        public virtual bool fraccionado { get; set; }
        public virtual decimal precio_compra { get; set; }
        public virtual decimal precio_venta_x3 { get; set; }
        public virtual decimal precio_venta_x6 { get; set; }
        public virtual decimal precio_venta_x12 { get; set; }
        public virtual string habilitado { get; set; }
        public virtual int orden { get; set; }

        public virtual IList<ProductoXFormatoCompra> producto_x_formato_compra { get; set; }
        public virtual IList<ProductoXFormatoVenta> producto_x_formato_venta { get; set; }

        public Producto()
        {
        }

        public override string ToString()
        {
            return nombre;
        }

        public static DataTable addSucursalesStock(DataTable dt)
        {
            if (dt != null)
            {
                IList<Sucursal> sucursales = Sucursal.getAll();
                foreach(Sucursal s in sucursales)
                {
                    dt.Columns.Add(new DataColumn(s.nombre));
                }

                foreach (DataRow dr in dt.Rows)
                {
                    foreach(Sucursal s in sucursales)
                    {
                        //StockProductoSucursal stock = StockProductoSucursal.getBy(dr['producto_id'],s.id);
                        //dr[s.nombre] = stock.value;
                    }
                }
            }

            return dt;
        }

        public virtual DataGridView customColumns(DataGridView dgw){
        
            dgw.Columns["id"].HeaderText = "Id";
            dgw.Columns["nombre"].HeaderText = "Nombre";

            ModDataGridView.ocultarColumna(dgw, "id");

            return dgw;
        }

        public virtual DataGridView customColumnsSelect(DataGridView dgw)
        {
            dgw.Columns["Producto"].Width = 450;
            dgw.Columns["Familia"].Width = 200;
            //dgw.Columns["Stock"].Width = 60;

            dgw.Columns["Codigo"].HeaderText = "Código";
            dgw.Columns["Producto"].HeaderText = "Nombre";

            //ModDataGridView.ocultarColumna(dgw, "producto_formato_id");
            //ModDataGridView.ocultarColumna(dgw, "Stock");
            //ModDataGridView.ocultarColumna(dgw, "Tipo de Stock");
            //ModDataGridView.ocultarColumna(dgw, "Formato Kg");

            return dgw;
        }

        public virtual Boolean save() {
            // Set cursor as hourglass
            Cursor.Current = Cursors.WaitCursor;

            using (var session = SessionManager.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    try
                    {
                        session.Save(this);
                        transaction.Commit();

                        SessionManager.EndTasks();
                        //session.Close();

                        return true;
                    }catch (Exception ex)
                    {
                        transaction.Rollback();
                        Logs.GetInstance.LogError(ex.Message, "Producto.save()");

                        if (ex.InnerException.Message.Contains("UNIQUE"))
                        {
                            MessageBox.Show("Error, ya existe un producto con el mismo nombre.", "Producto", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        else {
                            MessageBox.Show(ex.Message, "Exception Msg");
                        }


                        return false;
                    }
                    finally
                    {
                        // Set cursor as default arrow
                        Cursor.Current = Cursors.Default;
                    }

                    
                }
            }
        }

        public virtual Boolean update()
        {
            // Set cursor as hourglass
            Cursor.Current = Cursors.WaitCursor;

            using (var session = SessionManager.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    try
                    {                       
                        session.Update(this);
                        transaction.Commit();

                        SessionManager.EndTasks();
                        //session.Close();

                        return true;
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        Logs.GetInstance.LogError(ex.Message, "Producto.update()");
                        MessageBox.Show(ex.Message, "Exception Msg");

                        return false;
                    }
                    finally
                    {
                        // Set cursor as default arrow
                        Cursor.Current = Cursors.Default;
                    }
                }
            }
        }

        public virtual IList<Producto> getLike()
        {
            using (var session = SessionManager.OpenSession())
            {
                try
                {
                    var results = session.QueryOver<Producto>()
                                     .Where(c => c.nombre.IsLike(this.nombre, MatchMode.Anywhere))
                                     .List<Producto>();

                    SessionManager.EndTasks();
                    //session.Close();

                    return results;
                }
                catch (Exception ex)
                {
                    Logs.GetInstance.LogError(ex.Message, "Producto.getLike()");
                    MessageBox.Show(ex.Message, "Exception Msg");

                    return null;
                }
            }
        }

        public virtual Producto getById(int id)
        {
            // Set cursor as hourglass
            Cursor.Current = Cursors.WaitCursor;

            using (var session = SessionManager.OpenSession())
            {
                try
                {
                    var results = session.QueryOver<Producto>()
                                     .Where(c => c.id == id)
                                     .List<Producto>();

                    SessionManager.EndTasks();
                    //session.Close();

                    if (results.Count == 1)
                    {
                        Producto result = results[0];                        

                        return result;
                    }
                    return null;
                }
                catch (Exception ex)
                {
                    Logs.GetInstance.LogError(ex.Message, "Producto.getById()");
                    MessageBox.Show(ex.Message, "Exception Msg");

                    return null;
                }
                finally
                {
                    // Set cursor as default arrow
                    Cursor.Current = Cursors.Default;
                }
            }
        }

        public virtual Boolean delete()
        {
            // Set cursor as hourglass
            Cursor.Current = Cursors.WaitCursor;

            using (var session = SessionManager.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    try
                    {
                        session.Delete(this);
                        transaction.Commit();

                        SessionManager.EndTasks();
                        //session.Close();

                        return true;
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        Logs.GetInstance.LogError(ex.Message, "Usuarios.delete()");
                        MessageBox.Show(ex.Message, "Exception Msg");

                        return false;
                    }
                    finally
                    {
                        // Set cursor as default arrow
                        Cursor.Current = Cursors.Default;
                    }
                }
            }
        }

        public virtual IList<Producto> GetAll(string habilitado, bool order = false)
        {
            // Set cursor as hourglass
            Cursor.Current = Cursors.WaitCursor;

            using (var session = SessionManager.OpenSession())
            {
                try
                {
                    var query = session.QueryOver<Producto>()
                                .Where(c => c.habilitado == habilitado);

                    if(order)
                        query = query.OrderBy(c => c.familia).Asc;

                    
                    var items = query.List();
                            

                    SessionManager.EndTasks();
                    //session.Close();

                    return items;
                }
                catch (Exception ex)
                {
                    Logs.GetInstance.LogError(ex.Message, "Producto.GetAll()");
                    MessageBox.Show(ex.Message, "Exception Msg");

                    return null;
                }
                finally
                {
                    // Set cursor as default arrow
                    Cursor.Current = Cursors.Default;
                }
            }
        }

        public IList<Producto> DoSearch(IList<Producto> entities, string id,string nombre,string familia)
        {
            var query = entities;

            if (id != "")
            {
                query = query.Where(e => e.id.Equals(Convert.ToInt32(id))).ToList();
            }

            if (nombre != "") {
               query = query.Where(e => e.nombre.Contains(nombre)).ToList();
            }

            if (familia != "")
            {
                query = query.Where(e => e.familia.nombre.Equals(familia)).ToList();
            }

            IList<Producto> results = query.ToList();

            return results;
        }


        public virtual DataTable SearchCompras()
        {
            // Set cursor as hourglass
            Cursor.Current = Cursors.WaitCursor;

            try
            {
                string Consulta = "SELECT CAST( p.id AS CHAR( 20 ) ) as Codigo,F.nombre as Familia,p.nombre as Producto, p.precio_compra as 'Ultimo precio de compra'";
                Consulta += " FROM `productos` as p";
                Consulta += " JOIN familias as F on F.id=p.familia";
                Consulta += " AND p.habilitado = '1' ORDER BY p.orden asc";

                List<MySqlParameter> param = new List<MySqlParameter>();

                return BaseDeDatos.GetInstance.ExecuteCustomQuery(Consulta, param, "SearchCompras");
            }
            catch (Exception ex)
            {
                Logs.GetInstance.LogError(ex.Message, "Producto.SearchCompras()");
                MessageBox.Show(ex.Message, "Exception Msg");

                return null;
            }
            finally
            {
                // Set cursor as default arrow
                Cursor.Current = Cursors.Default;
            }

        }

        public virtual DataTable SearchVentas()
        {
            // Set cursor as hourglass
            Cursor.Current = Cursors.WaitCursor;

            try
            {
                string Consulta = "SELECT CAST( p.id AS CHAR( 20 ) ) as Codigo,F.nombre as Familia,p.nombre as Producto,";
                Consulta += "IF(p.unidad=1,p.stock,ROUND(p.stock/1000,2)) as 'Stock', ";
                Consulta += "IF(p.unidad=1,'Unidad','Fraccionado') as 'Tipo de Stock', ";
                Consulta += "IF(p.fraccionado=1,ROUND(FV.formato/1000,2),'') as 'Formato Kg', ";
                Consulta += "IF(p.fraccionado=1,PFV.precio_venta,p.precio_venta_x3) as 'Precio de Venta',"; // ???
                Consulta += "PFV.id as producto_formato_id";
                Consulta += " FROM `productos` as p";
                Consulta += " JOIN familias as F on F.id=p.familia";
                Consulta += " LEFT JOIN producto_x_formato_venta as PFV on PFV.producto_id=p.id";
                Consulta += " LEFT JOIN formato_venta as FV on FV.id=PFV.formato_venta_id";
                Consulta += " WHERE p.habilitado = '1' ORDER BY p.orden asc";

                List<MySqlParameter> param = new List<MySqlParameter>();

                return BaseDeDatos.GetInstance.ExecuteCustomQuery(Consulta, param, "SearchVentas");
            }
            catch (Exception ex)
            {
                Logs.GetInstance.LogError(ex.Message, "Producto.SearchVentas()");
                MessageBox.Show(ex.Message, "Exception Msg");

                return null;
            }
            finally
            {
                // Set cursor as default arrow
                Cursor.Current = Cursors.Default;
            }

        }



        public virtual DataTable Estadistica(string desde, string hasta)
        {
            // Set cursor as hourglass
            Cursor.Current = Cursors.WaitCursor;

            try
            {
                string Consulta = "SELECT CAST( producto AS CHAR( 20 ) ) as Codigo,P.nombre as Nombre,IF(IFV.formato = '', '',IFV.formato/1000) as Formato, sum(IFV.cantidad) as Cantidad,sum(IFV.descuento) as Descuento FROM factura_venta as FV";
                Consulta += " JOIN item_factura_venta as IFV on IFV.factura = FV.id";
                Consulta += " JOIN productos as P on P.id = IFV.producto";
                Consulta += " WHERE fecha >= '"+ desde +"' and fecha <= '" + hasta + "'";
                Consulta += " GROUP BY producto,formato/1000 Order by P.orden asc";

                List<MySqlParameter> param = new List<MySqlParameter>();

                return BaseDeDatos.GetInstance.ExecuteCustomQuery(Consulta, param, "Estadisticas");
            }
            catch (Exception ex)
            {
                Logs.GetInstance.LogError(ex.Message, "Producto.Estadistica()");
                MessageBox.Show(ex.Message, "Exception Msg");

                return null;
            }
            finally
            {
                // Set cursor as default arrow
                Cursor.Current = Cursors.Default;
            }
           
        }

        public decimal getPrecioVentaX3()
        {
            return this.precio_venta_x3;
        }

        public decimal getPrecioVentaX6()
        {
            return this.precio_venta_x6;
        }

        public decimal getPrecioVentaX12()
        {
            return this.precio_venta_x12;
        }

        public decimal getStockKg() {
            return (this.stock / 1000);
        }

        public int GetStock(int producto_id)
        {
            try
            {
                string Consulta = "SELECT COALESCE(SUM(stock), 0) AS stock FROM stock WHERE producto = " + producto_id;

                List<MySqlParameter> param = new List<MySqlParameter>();

                DataTable dataTable = BaseDeDatos.GetInstance.ExecuteCustomQuery(Consulta, param, "Producto");

                object result = dataTable.Rows[0]["stock"];

                return Int32.Parse(result.ToString());
            }
            catch (Exception ex)
            {
                Logs.GetInstance.LogError(ex.Message, "Producto.GetStock()");
                MessageBox.Show(ex.Message, "Exception Msg");

                return -1;
            }
        }
    }
}
