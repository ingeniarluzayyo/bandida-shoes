﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using NHibernate;
using NHibernate.Transform;
using System.Windows.Forms;
using Mayorista.Libs;

namespace Mayorista.Models
{
    public class OrdenPedido
    {
        public virtual int id { get; set; }
        public virtual DateTime fecha { get; set; }
        public virtual Cliente cliente { get; set; }
        public virtual Vendedor vendedor { get; set; }
        public virtual FormaPago forma_de_pago { get; set; }
        public virtual IList<ItemPedido> items { get; set; }
        public virtual decimal descuento { get; set; }
        public virtual decimal total { get; set; }
        public virtual decimal total_comision { get; set; }

        public OrdenPedido()
        {
            items = new List<ItemPedido>();
        }

        public virtual DataGridView customColumns(DataGridView dgw){
        
            dgw.Columns["id"].HeaderText = "Numero";
            dgw.Columns["fecha"].HeaderText = "Fecha";
            dgw.Columns["cliente"].HeaderText = "Cliente";
            dgw.Columns["vendedor"].HeaderText = "Vendedor";
            dgw.Columns["forma_de_pago"].HeaderText = "Forma de Pago";
            dgw.Columns["descuento"].HeaderText = "Descuento";
            dgw.Columns["total"].HeaderText = "Total";
            dgw.Columns["total_comision"].HeaderText = "Total Comision";

            ModDataGridView.ocultarColumna(dgw, "items");
            ModDataGridView.ocultarColumna(dgw, "total_comision");
         
            dgw.Columns["id"].Width = 90;
            dgw.Columns["cliente"].Width = 160;
            dgw.Columns["vendedor"].Width = 160;
            dgw.Columns["total_comision"].Width = 100;
            dgw.Columns["forma_de_pago"].Width = 120;

            return dgw;
        }

        public virtual Boolean save() {

            // Set cursor as hourglass
            Cursor.Current = Cursors.WaitCursor;

            using (var session = SessionManager.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    try
                    {
                        session.SaveOrUpdate(this);
                        transaction.Commit();

                        SessionManager.EndTasks();
                        //session.Close();

                        return true;
                    }catch (Exception ex)
                    {
                        transaction.Rollback();
                        Logs.GetInstance.LogError(ex.Message, "OrdenPedido.save()");
                        MessageBox.Show(ex.Message, "Exception Msg");

                        return false;
                    }
                    finally
                    {
                        // Set cursor as default arrow
                        Cursor.Current = Cursors.Default;
                    }

                    
                }
            }
        }

        public virtual Boolean update()
        {
            // Set cursor as hourglass
            Cursor.Current = Cursors.WaitCursor;

            using (var session = SessionManager.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    try
                    {                       
                        session.Update(this);
                        transaction.Commit();

                        SessionManager.EndTasks();
                        //session.Close();

                        return true;
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        Logs.GetInstance.LogError(ex.Message, "OrdenPedido.update()");
                        MessageBox.Show(ex.Message, "Exception Msg");

                        return false;
                    }
                    finally
                    {
                        // Set cursor as default arrow
                        Cursor.Current = Cursors.Default;
                    }
                }
            }
        }

        public virtual Boolean delete()
        {
            using (var session = FluentNHibernateDB.CreateSessionFactory().OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    try
                    {
                        session.Delete(this);
                        transaction.Commit();

                        session.Flush();
                        session.Close();

                        return true;
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        Logs.GetInstance.LogError(ex.Message, "OrdenPedido.delete()");
                        MessageBox.Show(ex.Message, "Exception Msg");

                        return false;
                    }
                }
            }
        }

        /**
         * Parameter partial: indica si debe traer todo el objeto (incluyendo la coleccion de items) o solo los parametros basicos simples, ej: para un update.
         * 
         */
        public virtual OrdenPedido getById(int id_value, bool partial = false)
        {
            // Set cursor as hourglass
            Cursor.Current = Cursors.WaitCursor;

            using (var session = SessionManager.OpenSession())
            {
                try
                {
                    var roles = session.QueryOver<OrdenPedido>()
                                     .Where(c => c.id == id_value)
                                     .List<OrdenPedido>();

                    SessionManager.EndTasks();
                    //session.Close();

                    if (roles.Count == 1)
                    {
                        OrdenPedido result = roles[0];
                        if (!partial)
                        {
                            result.items = SelectItems(id_value);
                        }

                        return result;
                    }
                    return null;
                }
                catch (Exception ex)
                {
                    Logs.GetInstance.LogError(ex.Message, "OrdenPedido.getById()");
                    MessageBox.Show(ex.Message, "Exception Msg");

                    return null;
                }
                finally
                {
                    // Set cursor as default arrow
                    Cursor.Current = Cursors.Default;
                }
            }
        }

        public virtual IList<OrdenPedido> getPedidos()
        {
            // Set cursor as hourglass
            Cursor.Current = Cursors.WaitCursor;

            using (var session = SessionManager.OpenSession())
            {
               try
               {
                   var results = session.QueryOver<OrdenPedido>()
                                    .Where(c => c.id == this.id)
                                    .List<OrdenPedido>();

                   SessionManager.EndTasks();
                   //session.Close();

                   return results;
               }
               catch (Exception ex)
               {
                   Logs.GetInstance.LogError(ex.Message, "OrdenPedido.getFacturas()");
                   MessageBox.Show(ex.Message, "Exception Msg");

                   return null;
               }
               finally
               {
                   // Set cursor as default arrow
                   Cursor.Current = Cursors.Default;
               }
            }
        }

        public virtual IList<ItemPedido> SelectItems(int id_value)
        {
            // Set cursor as hourglass
            Cursor.Current = Cursors.WaitCursor;

            using (var session = SessionManager.OpenSession())
            {
                try
                {
                    var items = session.QueryOver<ItemPedido>()
                            .Right.JoinQueryOver<OrdenPedido>(x => x.pedido)
                            .Where(c => c.id == id_value)
                            .List();

                    SessionManager.EndTasks();
                    //session.Close();

                    return items;
                }
                catch (Exception ex)
                {
                    Logs.GetInstance.LogError(ex.Message, "OrdenPedido.SelectItemsCompra()");
                    MessageBox.Show(ex.Message, "Exception Msg");

                    return null;
                }
                finally
                {
                    // Set cursor as default arrow
                    Cursor.Current = Cursors.Default;
                }
            }
        }

        public virtual IList<OrdenPedido> Search(int venta_id,int cliente_id,int venderor_id, string desde,string hasta)
        {
            // Set cursor as hourglass
            Cursor.Current = Cursors.WaitCursor;

            using (var session = SessionManager.OpenSession())
            {
                try
                {
                    var query = session.QueryOver<OrdenPedido>();

                    if (venta_id > 0)
                    {
                        query = query.Where(c => c.id == venta_id);
                    }

                    if (cliente_id > 0)
                    {
                        query = query.Where(c => c.cliente.id == cliente_id);
                    }

                    if (venderor_id > 0)
                    {
                        query = query.Where(c => c.vendedor.id == venderor_id);
                    }

                    if (desde != " " && desde != "")
                    {
                        query = query.Where(c => c.fecha >= DateTime.Parse(desde));
                    }

                    if (hasta != " " && hasta != "")
                    {
                        query = query.Where(c => c.fecha <= DateTime.Parse(hasta));
                    }

                    var list = query.List<OrdenPedido>();
                    SessionManager.EndTasks();
                    //session.Close();

                    return list;
                }
                catch (Exception ex)
                {
                    Logs.GetInstance.LogError(ex.Message, "OrdenPedido.Search()");
                    MessageBox.Show(ex.Message, "Exception Msg");

                    return null;
                }
                finally {
                    // Set cursor as default arrow
                    Cursor.Current = Cursors.Default;
                }
            }
        }

      


    }
}
