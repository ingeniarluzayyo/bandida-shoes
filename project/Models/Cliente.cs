﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MySql.Data.MySqlClient;

using System.Data;
using NHibernate.Criterion;
using NHibernate;
using NHibernate.Transform;
using System.Windows.Forms;
using Zapateria.Libs;

namespace Zapateria.Models
{
    public class Cliente
    {
        public virtual int id { get; set; }
        public virtual string razon_social { get; set; }
        public virtual string cuit { get; set; }
        public virtual string tel { get; set; }
        public virtual string direccion { get; set; }
        public virtual string localidad { get; set; }
        public virtual string observacion { get; set; }
        public virtual int orden { get; set; }
        public virtual string habilitado { get; set; }

        public Cliente() { }

        public virtual DataGridView customColumns(DataGridView dgw)
        {

            /*dgw.Columns["id"].HeaderText = "Id";
            dgw.Columns["nombre"].HeaderText = "Nombre";

            ModDataGridView.ocultarColumna(dgw, "id");
            ModDataGridView.ocultarColumna(dgw, "modulos");*/

            return dgw;
        }

        public virtual Boolean save()
        {
            using (var session = SessionManager.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    try
                    {
                        session.Save(this);
                        transaction.Commit();

                        SessionManager.EndTasks();

                        return true;
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        Logs.GetInstance.LogError(ex.Message, "Cliente.save()");
                        MessageBox.Show(ex.Message, "Exception Msg");

                        return false;
                    }


                }
            }
        }

        public virtual Boolean update()
        {
            using (var session = SessionManager.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    try
                    {
                        session.Update(this);
                        transaction.Commit();

                        SessionManager.EndTasks();

                        return true;
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        Logs.GetInstance.LogError(ex.Message, "Cliente.update()");
                        MessageBox.Show(ex.Message, "Exception Msg");

                        return false;
                    }
                }
            }
        }

        public virtual Boolean delete()
        {
            using (var session = SessionManager.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    try
                    {
                        session.Delete(this);
                        transaction.Commit();

                        SessionManager.EndTasks();

                        return true;
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        Logs.GetInstance.LogError(ex.Message, "Cliente.delete()");
                        MessageBox.Show(ex.Message, "Exception Msg");

                        return false;
                    }
                }
            }
        }

        public virtual Cliente getById(int id_value)
        {
            using (var session = SessionManager.OpenSession())
            {
                try
                {
                    var results = session.QueryOver<Cliente>()
                                     .Where(c => c.id == id_value)
                                     .List<Cliente>();

                    SessionManager.EndTasks();

                    if (results.Count == 1)
                    {
                        Cliente result = results[0];

                        return result;
                    }
                    return null;
                }
                catch (Exception ex)
                {
                    Logs.GetInstance.LogError(ex.Message, "Cliente.getById()");
                    MessageBox.Show(ex.Message, "Exception Msg");

                    return null;
                }
            }
        }

        public virtual IList<Cliente> getLike()
        {
            using (var session = SessionManager.OpenSession())
            {
                try
                {
                    var roles = session.QueryOver<Cliente>()
                                     //.Where(c => c.cliente.IsLike(this.cliente, MatchMode.Anywhere))
                                     .List<Cliente>();

                    SessionManager.EndTasks();

                    return roles;
                }
                catch (Exception ex)
                {
                    Logs.GetInstance.LogError(ex.Message, "Cliente.getLike()");
                    MessageBox.Show(ex.Message, "Exception Msg");

                    return null;
                }
            }
        }

        public override string ToString()
        {
            return razon_social;
        }
    }
}
