﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Zapateria.Libs;
using NHibernate;
using NHibernate.Criterion;

namespace Zapateria.Models
{
    public class Reparto
    {
        public virtual Int32 id { get; set; }
        public virtual String nombre { get; set; }

        public Reparto()
        {

        }

         public override string ToString()
         {
             return nombre;
         }

        public virtual DataGridView customColumns(DataGridView dgw)
        {
            dgw.Columns["id"].HeaderText = "id";
            dgw.Columns["nombre"].HeaderText = "Nombre";

            ModDataGridView.ocultarColumna(dgw, "id");
            ModDataGridView.widthColumna(dgw, "nombre", 200);

            return dgw;
        }

        public virtual Boolean save()
        {
            using (var session = SessionManager.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    try
                    {
                        session.Save(this);
                        transaction.Commit();

                        SessionManager.EndTasks();                        

                        return true;
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        Logs.GetInstance.LogError(ex.Message, "Reparto.save()");
                        MessageBox.Show(ex.Message, "Exception Msg");

                        return false;
                    }


                }
            }
        }

        public virtual Boolean update()
        {
            using (var session = SessionManager.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    try
                    {
                        session.Update(this);
                        transaction.Commit();

                        SessionManager.EndTasks();                        

                        return true;
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        Logs.GetInstance.LogError(ex.Message, "Reparto.update()");
                        MessageBox.Show(ex.Message, "Exception Msg");

                        return false;
                    }
                }
            }
        }

        public virtual Boolean delete()
        {
            using (var session = SessionManager.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    try
                    {
                        session.Delete(this);
                        transaction.Commit();

                        SessionManager.EndTasks();                        

                        return true;
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        Logs.GetInstance.LogError(ex.Message, "Reparto.delete()");
                        MessageBox.Show(ex.Message, "Exception Msg");

                        return false;
                    }
                }
            }
        }

        public virtual IList<Reparto> getRepartosLike()
        {
            using (var session = SessionManager.OpenSession())
            {
                try
                {
                    var categorias = session.QueryOver<Reparto>()
                                     .Where(c => c.nombre.IsLike(this.nombre, MatchMode.Anywhere))
                                     .List<Reparto>();

                    SessionManager.EndTasks();                    

                    return categorias;
                }
                catch (Exception ex)
                {
                    Logs.GetInstance.LogError(ex.Message, "Reparto.getCategorias()");
                    MessageBox.Show(ex.Message, "Exception Msg");

                    return null;
                }
            }
        }

        public virtual Reparto getById(int id_value)
        {
            using (var session = SessionManager.OpenSession())
            {
                try
                {
                    var results = session.QueryOver<Reparto>()
                                     .Where(c => c.id == id_value)
                                     .List<Reparto>();

                    SessionManager.EndTasks();                   

                    if (results.Count == 1)
                    {
                        Reparto result = results[0];

                        return result;
                    }
                    return null;
                }
                catch (Exception ex)
                {
                    Logs.GetInstance.LogError(ex.Message, "Reparto.getById()");
                    MessageBox.Show(ex.Message, "Exception Msg");

                    return null;
                }
            }
        }

    }
}
