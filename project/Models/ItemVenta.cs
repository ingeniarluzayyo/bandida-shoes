﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using NHibernate;
using NHibernate.Transform;
using System.Windows.Forms;
using Zapateria.Libs;

namespace Zapateria.Models
{
    public class ItemVenta
    {
        public virtual int id { get; set; }
        public virtual FacturaVenta factura { get; set; }
        public virtual Producto producto { get; set; }
        public virtual Talle talle { get; set; }
        public virtual ColorZapato color { get; set; }
        public virtual Sucursal sucursal { get; set; }
        public virtual Zona zona { get; set; }
        public virtual decimal precio { get; set; }
        public virtual string formato { get; set; }
        public virtual int cantidad { get; set; }
        public virtual int descuento { get; set; }

        public ItemVenta() { }

        public virtual DataGridView customColumns(DataGridView dgw){
        
            dgw.Columns["id"].HeaderText = "Id";
            dgw.Columns["producto"].HeaderText = "Producto";
            dgw.Columns["talle"].HeaderText = "Talle";
            dgw.Columns["color"].HeaderText = "Color";
            dgw.Columns["precio"].HeaderText = "Precio";
            dgw.Columns["cantidad"].HeaderText = "Cantidad";
            dgw.Columns["descuento"].HeaderText = "Descuento";
            dgw.Columns["formato"].HeaderText = "Formato";

            ModDataGridView.ocultarColumna(dgw, "id");
            ModDataGridView.ocultarColumna(dgw, "factura");     

            return dgw;
        }

        public virtual Boolean save() {
            using (var session = SessionManager.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    try
                    {
                        session.Save(this);
                        transaction.Commit();

                        SessionManager.EndTasks();
                        //session.Close();

                        return true;
                    }catch (Exception ex)
                    {
                        transaction.Rollback();
                        Logs.GetInstance.LogError(ex.Message, "ItemVenta.save()");
                        MessageBox.Show(ex.Message, "Exception Msg");

                        return false;
                    }

                    
                }
            }
        }

        public virtual Boolean update()
        {
            using (var session = SessionManager.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    try
                    {                       
                        session.Update(this);
                        transaction.Commit();

                        SessionManager.EndTasks();
                        //session.Close();

                        return true;
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        Logs.GetInstance.LogError(ex.Message, "ItemVenta.update()");
                        MessageBox.Show(ex.Message, "Exception Msg");

                        return false;
                    }
                }
            }
        }

        public virtual IList<ItemVenta> getItemsOf(FacturaVenta fact)
        {
            using (var session = FluentNHibernateDB.CreateSessionFactory().OpenSession())
            {
                try
                {
                    var items = session.QueryOver<ItemVenta>()
                                     .Where(c => c.factura.id == fact.id)
                                     .List<ItemVenta>();

                    SessionManager.EndTasks();
                    

                    return items;
                }
                catch (Exception ex)
                {
                    Logs.GetInstance.LogError(ex.Message, "ItemVenta.getItemsOf()");
                    MessageBox.Show(ex.Message, "Exception Msg");

                    return null;
                }
            }
        }

        public override string ToString()
        {
            return id.ToString();
        }
    }
}
