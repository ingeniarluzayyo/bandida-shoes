﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NHibernate.Criterion;
using NHibernate;
using NHibernate.Transform;
using System.Windows.Forms;
using Zapateria.Libs;

namespace Zapateria.Models
{
    public class Familia
    {
        public virtual int id { get; set; }
        public virtual String nombre { get; set; }
        //public virtual IList<TipoCliente> tipo_clientes { get; set; }

        public Familia()
        {
            //tipo_clientes = new List<TipoCliente>();
        }

        public Familia(int id,String nombre)
        {
            this.id = id;
            this.nombre = nombre;
        }

        public override string ToString()
        {
            return nombre;
        }

        public virtual DataGridView customColumns(DataGridView dgw){
        
            dgw.Columns["id"].HeaderText = "Id";
            dgw.Columns["nombre"].HeaderText = "Nombre";

            ModDataGridView.ocultarColumna(dgw, "id");

            return dgw;
        }

        public virtual Boolean save() {
            using (var session = SessionManager.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    try
                    {
                        session.Save(this);
                        transaction.Commit();

                        SessionManager.EndTasks();
                        //session.Close();

                        return true;
                    }catch (Exception ex)
                    {
                        transaction.Rollback();
                        Logs.GetInstance.LogError(ex.Message, "Familia.save()");
                        
                        if (ex.InnerException.Message.Contains("UNIQUE"))
                        {
                            MessageBox.Show("Error, ya existe una familia con el mismo nombre.", "Familia", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        else
                        {
                            MessageBox.Show(ex.Message, "Exception Msg");
                        }

                        return false;
                    }

                    
                }
            }
        }

        public virtual Boolean update()
        {
            using (var session = SessionManager.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    try
                    {                       
                        session.Update(this);
                        transaction.Commit();

                        SessionManager.EndTasks();
                        //session.Close();

                        return true;
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        Logs.GetInstance.LogError(ex.Message, "Familia.update()");
                        MessageBox.Show(ex.Message, "Exception Msg");

                        return false;
                    }
                }
            }
        }

        public virtual Boolean delete()
        {            
            using (var session = SessionManager.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    try
                    {
                        session.Delete(this);
                        transaction.Commit();

                        SessionManager.EndTasks();
                        //session.Close();

                        return true;
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        Logs.GetInstance.LogError(ex.Message, "Familia.delete()");
                        MessageBox.Show(ex.Message, "Exception Msg");

                        return false;
                    }
                }
            }
        }

        public virtual IList<Familia> getLike()
        {
            using (var session = SessionManager.OpenSession())
            {
                try
                {
                    var results = session.QueryOver<Familia>()
                                     .Where(c => c.nombre.IsLike(this.nombre, MatchMode.Anywhere))
                                     .List<Familia>();

                    SessionManager.EndTasks();
                    //session.Close();

                    return results;
                }
                catch (Exception ex)
                {
                    Logs.GetInstance.LogError(ex.Message, "Familia.getLike()");
                    MessageBox.Show(ex.Message, "Exception Msg");

                    return null;
                }
            }
        }

        public virtual IList<Familia> getAll()
        {
            using (var session = SessionManager.OpenSession())
            {
                try
                {
                    var results = session.QueryOver<Familia>()
                                     .List<Familia>();

                    SessionManager.EndTasks();
                    //session.Close();

                    return results;
                }
                catch (Exception ex)
                {
                    Logs.GetInstance.LogError(ex.Message, "Familia.getAll()");
                    MessageBox.Show(ex.Message, "Exception Msg");

                    return null;
                }
            }
        }

        public virtual Familia getById(int rol_id)
        {
            using (var session = SessionManager.OpenSession())
            {
                try
                {
                    var results = session.QueryOver<Familia>()
                                     .Where(c => c.id == rol_id)
                                     .List<Familia>();

                    SessionManager.EndTasks();
                    //session.Close();

                    if (results.Count == 1)
                    {
                        Familia result = results[0];                        

                        return result;
                    }
                    return null;
                }
                catch (Exception ex)
                {
                    Logs.GetInstance.LogError(ex.Message, "Familia.getById()");
                    MessageBox.Show(ex.Message, "Exception Msg");

                    return null;
                }
            }
        }


    }
}
