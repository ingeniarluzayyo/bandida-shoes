﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MySql.Data.MySqlClient;
using System.Data;
using NHibernate.Criterion;
using NHibernate;
using NHibernate.Transform;
using System.Windows.Forms;
using Zapateria.Libs;

namespace Zapateria.Models
{
    public class Proveedor
    {
        public virtual int id { get; set; }
        public virtual String razon_social { get; set; }
        public virtual String cuit { get; set; }
        public virtual String tel { get; set; }
        public virtual String cel { get; set; }
        public virtual String direccion { get; set; }
        public virtual String mail { get; set; }
        public virtual String observacion { get; set; }

        public Proveedor()
        {
        }

        public override string ToString()
        {
            return razon_social;
        }

        public virtual DataGridView customColumns(DataGridView dgw){
        
            dgw.Columns["id"].HeaderText = "Id";
            dgw.Columns["razon_social"].HeaderText = "Razon Social";
            dgw.Columns["cuit"].HeaderText = "CUIT";
            dgw.Columns["tel"].HeaderText = "Teléfono";
            dgw.Columns["cel"].HeaderText = "Celular";
            dgw.Columns["mail"].HeaderText = "Email";

            ModDataGridView.ocultarColumna(dgw, "id");
            ModDataGridView.ocultarColumna(dgw, "direccion");
            ModDataGridView.ocultarColumna(dgw, "observacion");            

            return dgw;
        }

        public virtual Boolean save() {
            using (var session = SessionManager.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    try
                    {
                        session.Save(this);
                        transaction.Commit();

                        SessionManager.EndTasks();
                        //session.Close();

                        return true;
                    }catch (Exception ex)
                    {
                        transaction.Rollback();
                        Logs.GetInstance.LogError(ex.Message, "Proveedor.save()");
                        MessageBox.Show(ex.Message, "Exception Msg");

                        return false;
                    }

                    
                }
            }
        }

        public virtual Boolean update()
        {
            using (var session = SessionManager.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    try
                    {                       
                        session.Update(this);
                        transaction.Commit();

                        SessionManager.EndTasks();
                        //session.Close();

                        return true;
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        Logs.GetInstance.LogError(ex.Message, "Proveedor.update()");
                        MessageBox.Show(ex.Message, "Exception Msg");

                        return false;
                    }
                }
            }
        }

        public virtual Boolean delete()
        {
            using (var session = SessionManager.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    try
                    {
                        session.Delete(this);
                        transaction.Commit();

                        SessionManager.EndTasks();
                        //session.Close();

                        return true;
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        Logs.GetInstance.LogError(ex.Message, "Proveedor.delete()");
                        MessageBox.Show(ex.Message, "Exception Msg");

                        return false;
                    }
                }
            }
        }

        public virtual IList<Proveedor> getLike()
        {
            using (var session = SessionManager.OpenSession())
            {
                try
                {
                    var roles = session.QueryOver<Proveedor>()
                                     .Where(c => c.razon_social.IsLike(this.razon_social, MatchMode.Anywhere))
                                     .List<Proveedor>();

                    SessionManager.EndTasks();
                    //session.Close();

                    return roles;
                }
                catch (Exception ex)
                {
                    Logs.GetInstance.LogError(ex.Message, "Proveedor.getLike()");
                    MessageBox.Show(ex.Message, "Exception Msg");

                    return null;
                }
            }
        }

        public virtual Proveedor getById(int id_value)
        {
            using (var session = SessionManager.OpenSession())
            {
                try
                {
                    var roles = session.QueryOver<Proveedor>()
                                     .Where(c => c.id == id_value)
                                     .List<Proveedor>();

                    SessionManager.EndTasks();
                    //session.Close();

                    if (roles.Count == 1)
                    {
                        Proveedor result = roles[0];                        

                        return result;
                    }
                    return null;
                }
                catch (Exception ex)
                {
                    Logs.GetInstance.LogError(ex.Message, "Proveedor.getById()");
                    MessageBox.Show(ex.Message, "Exception Msg");

                    return null;
                }
            }
        }
        
        public virtual void get_proveedor(string id)
        {
            string Consulta = "SELECT * FROM proveedores WHERE id=" + id;

            List<MySqlParameter> param = new List<MySqlParameter>();

            DataTable dt = BaseDeDatos.GetInstance.ExecuteCustomQuery(Consulta, param, "");

            id = dt.Rows[0]["id"].ToString();
            razon_social = dt.Rows[0]["razon_social"].ToString();
            cuit = dt.Rows[0]["cuit"].ToString();
            tel = dt.Rows[0]["tel"].ToString();
            cel = dt.Rows[0]["cel"].ToString();
            direccion = dt.Rows[0]["direccion"].ToString();
            mail = dt.Rows[0]["mail"].ToString();
            observacion = dt.Rows[0]["observacion"].ToString();
        }
    }
}
