﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using NHibernate;
using NHibernate.Transform;
using System.Windows.Forms;
using Zapateria.Libs;

namespace Zapateria.Models
{
    public class CtaCteProveedor
    {
        public virtual int id { get; set; }
        public virtual Proveedor proveedor { get; set; }
        public virtual DateTime ultima_actualizacion { get; set; }
        public virtual decimal total_facturado { get; set; }
        public virtual decimal total_pagado { get; set; }
        public virtual decimal saldo { get; set; }

        public CtaCteProveedor()
        {
        }

        public virtual DataGridView customColumns(DataGridView dgw, int proveedor_id = 0)
        {

            dgw.Columns["id"].HeaderText = "Id";
            dgw.Columns["proveedor"].HeaderText = "Proveedor";
            dgw.Columns["total_facturado"].HeaderText = "Total Facturado";
            dgw.Columns["total_pagado"].HeaderText = "Total Pagado";
            dgw.Columns["saldo"].HeaderText = "Saldo";
            dgw.Columns["ultima_actualizacion"].HeaderText = "Ultima Actualización";

            dgw.Columns["ultima_actualizacion"].Width = 150;
            dgw.Columns["proveedor"].Width = 150;
            dgw.Columns["total_facturado"].Width = 120;
            dgw.Columns["total_pagado"].Width = 120;
            dgw.Columns["saldo"].Width = 120;

            DataGridViewColumn col = new DataGridViewColumn();
            col.Name = "Proveedor_Id";
            col.CellTemplate = new DataGridViewTextBoxCell();
            dgw.Columns.Insert(1, col);

            foreach (DataGridViewRow row in dgw.Rows)
            {
                if (proveedor_id == 0)
                {
                    row.Cells["Proveedor_Id"].Value = ((Proveedor)row.Cells["Proveedor"].Value).id;
                }
                else
                {
                    row.Cells["Proveedor_Id"].Value = proveedor_id;
                }
            }

            ModDataGridView.ocultarColumna(dgw, "id");
            ModDataGridView.ocultarColumna(dgw, "Proveedor_Id");
            ModDataGridView.agregarBoton(dgw, "Ver Detalle");

            return dgw;
        }

        public virtual Boolean save()
        {
            using (var session = SessionManager.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    try
                    {
                        session.SaveOrUpdate(this);
                        transaction.Commit();

                        SessionManager.EndTasks();
                        //session.Close();

                        return true;
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        Logs.GetInstance.LogError(ex.Message, "CtaCteProveedor.save()");
                        MessageBox.Show(ex.Message, "Exception Msg");

                        return false;
                    }


                }
            }
        }

        public virtual Boolean update()
        {
            using (var session = SessionManager.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    try
                    {
                        session.Update(this);
                        transaction.Commit();

                        SessionManager.EndTasks();
                        //session.Close();

                        return true;
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        Logs.GetInstance.LogError(ex.Message, "CtaCteProveedor.update()");
                        MessageBox.Show(ex.Message, "Exception Msg");

                        return false;
                    }
                }
            }
        }

        public virtual Boolean delete()
        {
            using (var session = SessionManager.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    try
                    {
                        session.Delete(this);
                        transaction.Commit();

                        SessionManager.EndTasks();
                        //session.Close();

                        return true;
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        Logs.GetInstance.LogError(ex.Message, "CtaCteProveedor.delete()");
                        MessageBox.Show(ex.Message, "Exception Msg");

                        return false;
                    }
                }
            }
        }

        public virtual IList<CtaCteProveedor> getAll()
        {
            using (var session = SessionManager.OpenSession())
            {
                try
                {
                    var results = session.QueryOver<CtaCteProveedor>()
                                     .List<CtaCteProveedor>();

                    SessionManager.EndTasks();
                    //session.Close();

                    return results;
                }
                catch (Exception ex)
                {
                    Logs.GetInstance.LogError(ex.Message, "CtaCteProveedor.getAll()");
                    MessageBox.Show(ex.Message, "Exception Msg");

                    return null;
                }
            }
        }

        public virtual CtaCteProveedor getByProveedor(int id)
        {
            using (var session = SessionManager.OpenSession())
            {
                try
                {
                    var results = session.QueryOver<CtaCteProveedor>()
                                     .Where(c => c.proveedor.id == id)
                                     .List<CtaCteProveedor>();

                    SessionManager.EndTasks();
                    //session.Close();

                    if (results.Count == 1)
                    {
                        CtaCteProveedor result = results[0];

                        return result;
                    }
                    return null;
                }
                catch (Exception ex)
                {
                    Logs.GetInstance.LogError(ex.Message, "CtaCteProveedor.getByCliente()");
                    MessageBox.Show(ex.Message, "Exception Msg");

                    return null;
                }
            }
        }
    }
}
