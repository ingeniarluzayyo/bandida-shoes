﻿namespace Zapateria
{
    partial class Main
    {
        /// <summary>
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben eliminar; false en caso contrario, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            this.clientesButton = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.ventasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.comprasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.proveedoresToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.proveedoresToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.ctaCteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pagosToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.productosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clientesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clientesToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.ctaCteToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.pagosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.estadisticasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.usuariosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.usuariosToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.rolesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vendedoresToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gastosVariosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.configuracionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.categoriasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.familiasGastosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.subfamiliasGastosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.formatoCompraToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.formatoVentaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.coloresToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tallesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.formaDePagoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.emailToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.miEmpresaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ayudaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.proveedoresButton = new System.Windows.Forms.Button();
            this.productosButton = new System.Windows.Forms.Button();
            this.comprasButton = new System.Windows.Forms.Button();
            this.ventasButton = new System.Windows.Forms.Button();
            this.ctacteButton = new System.Windows.Forms.Button();
            this.ctaCteProvButton = new System.Windows.Forms.Button();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // clientesButton
            // 
            this.clientesButton.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clientesButton.Image = ((System.Drawing.Image)(resources.GetObject("clientesButton.Image")));
            this.clientesButton.Location = new System.Drawing.Point(3, 120);
            this.clientesButton.Name = "clientesButton";
            this.clientesButton.Size = new System.Drawing.Size(111, 89);
            this.clientesButton.TabIndex = 5;
            this.clientesButton.Text = "Clientes";
            this.clientesButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.clientesButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.clientesButton.UseVisualStyleBackColor = true;
            this.clientesButton.Click += new System.EventHandler(this.EquiposButton_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.CornflowerBlue;
            this.menuStrip1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ventasToolStripMenuItem,
            this.comprasToolStripMenuItem,
            this.proveedoresToolStripMenuItem,
            this.productosToolStripMenuItem,
            this.clientesToolStripMenuItem,
            this.estadisticasToolStripMenuItem,
            this.usuariosToolStripMenuItem,
            this.vendedoresToolStripMenuItem,
            this.gastosVariosToolStripMenuItem,
            this.configuracionToolStripMenuItem,
            this.ayudaToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(943, 24);
            this.menuStrip1.TabIndex = 4;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // ventasToolStripMenuItem
            // 
            this.ventasToolStripMenuItem.ForeColor = System.Drawing.Color.Black;
            this.ventasToolStripMenuItem.Name = "ventasToolStripMenuItem";
            this.ventasToolStripMenuItem.Size = new System.Drawing.Size(62, 20);
            this.ventasToolStripMenuItem.Text = "Ventas";
            this.ventasToolStripMenuItem.Click += new System.EventHandler(this.ventasToolStripMenuItem_Click);
            // 
            // comprasToolStripMenuItem
            // 
            this.comprasToolStripMenuItem.Name = "comprasToolStripMenuItem";
            this.comprasToolStripMenuItem.Size = new System.Drawing.Size(76, 20);
            this.comprasToolStripMenuItem.Text = "Compras";
            this.comprasToolStripMenuItem.Click += new System.EventHandler(this.comprasToolStripMenuItem_Click);
            // 
            // proveedoresToolStripMenuItem
            // 
            this.proveedoresToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.proveedoresToolStripMenuItem1,
            this.ctaCteToolStripMenuItem,
            this.pagosToolStripMenuItem1});
            this.proveedoresToolStripMenuItem.Name = "proveedoresToolStripMenuItem";
            this.proveedoresToolStripMenuItem.Size = new System.Drawing.Size(100, 20);
            this.proveedoresToolStripMenuItem.Text = "Proveedores";
            // 
            // proveedoresToolStripMenuItem1
            // 
            this.proveedoresToolStripMenuItem1.Name = "proveedoresToolStripMenuItem1";
            this.proveedoresToolStripMenuItem1.Size = new System.Drawing.Size(156, 22);
            this.proveedoresToolStripMenuItem1.Text = "Proveedores";
            this.proveedoresToolStripMenuItem1.Click += new System.EventHandler(this.proveedoresToolStripMenuItem1_Click);
            // 
            // ctaCteToolStripMenuItem
            // 
            this.ctaCteToolStripMenuItem.Name = "ctaCteToolStripMenuItem";
            this.ctaCteToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.ctaCteToolStripMenuItem.Text = "Cta. Cte.";
            this.ctaCteToolStripMenuItem.Click += new System.EventHandler(this.ctaCteToolStripMenuItem_Click_1);
            // 
            // pagosToolStripMenuItem1
            // 
            this.pagosToolStripMenuItem1.Name = "pagosToolStripMenuItem1";
            this.pagosToolStripMenuItem1.Size = new System.Drawing.Size(156, 22);
            this.pagosToolStripMenuItem1.Text = "Pagos";
            this.pagosToolStripMenuItem1.Click += new System.EventHandler(this.pagosToolStripMenuItem1_Click);
            // 
            // productosToolStripMenuItem
            // 
            this.productosToolStripMenuItem.Name = "productosToolStripMenuItem";
            this.productosToolStripMenuItem.Size = new System.Drawing.Size(83, 20);
            this.productosToolStripMenuItem.Text = "Productos";
            this.productosToolStripMenuItem.Click += new System.EventHandler(this.pruebaToolStripMenuItem_Click);
            // 
            // clientesToolStripMenuItem
            // 
            this.clientesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.clientesToolStripMenuItem1,
            this.ctaCteToolStripMenuItem1,
            this.pagosToolStripMenuItem});
            this.clientesToolStripMenuItem.Name = "clientesToolStripMenuItem";
            this.clientesToolStripMenuItem.Size = new System.Drawing.Size(71, 20);
            this.clientesToolStripMenuItem.Text = "Clientes";
            // 
            // clientesToolStripMenuItem1
            // 
            this.clientesToolStripMenuItem1.Name = "clientesToolStripMenuItem1";
            this.clientesToolStripMenuItem1.Size = new System.Drawing.Size(130, 22);
            this.clientesToolStripMenuItem1.Text = "Clientes";
            this.clientesToolStripMenuItem1.Click += new System.EventHandler(this.clientesToolStripMenuItem1_Click);
            // 
            // ctaCteToolStripMenuItem1
            // 
            this.ctaCteToolStripMenuItem1.Name = "ctaCteToolStripMenuItem1";
            this.ctaCteToolStripMenuItem1.Size = new System.Drawing.Size(130, 22);
            this.ctaCteToolStripMenuItem1.Text = "Cta. Cte.";
            // 
            // pagosToolStripMenuItem
            // 
            this.pagosToolStripMenuItem.Name = "pagosToolStripMenuItem";
            this.pagosToolStripMenuItem.Size = new System.Drawing.Size(130, 22);
            this.pagosToolStripMenuItem.Text = "Pagos";
            this.pagosToolStripMenuItem.Click += new System.EventHandler(this.pagosToolStripMenuItem_Click);
            // 
            // estadisticasToolStripMenuItem
            // 
            this.estadisticasToolStripMenuItem.ForeColor = System.Drawing.Color.Black;
            this.estadisticasToolStripMenuItem.Name = "estadisticasToolStripMenuItem";
            this.estadisticasToolStripMenuItem.Size = new System.Drawing.Size(93, 20);
            this.estadisticasToolStripMenuItem.Text = "Estadísticas";
            this.estadisticasToolStripMenuItem.Click += new System.EventHandler(this.estadisticasToolStripMenuItem_Click);
            // 
            // usuariosToolStripMenuItem
            // 
            this.usuariosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.usuariosToolStripMenuItem1,
            this.rolesToolStripMenuItem});
            this.usuariosToolStripMenuItem.Name = "usuariosToolStripMenuItem";
            this.usuariosToolStripMenuItem.Size = new System.Drawing.Size(74, 20);
            this.usuariosToolStripMenuItem.Text = "Usuarios";
            // 
            // usuariosToolStripMenuItem1
            // 
            this.usuariosToolStripMenuItem1.Name = "usuariosToolStripMenuItem1";
            this.usuariosToolStripMenuItem1.Size = new System.Drawing.Size(130, 22);
            this.usuariosToolStripMenuItem1.Text = "Usuarios";
            this.usuariosToolStripMenuItem1.Click += new System.EventHandler(this.usuariosToolStripMenuItem1_Click);
            // 
            // rolesToolStripMenuItem
            // 
            this.rolesToolStripMenuItem.Name = "rolesToolStripMenuItem";
            this.rolesToolStripMenuItem.Size = new System.Drawing.Size(130, 22);
            this.rolesToolStripMenuItem.Text = "Roles";
            this.rolesToolStripMenuItem.Click += new System.EventHandler(this.rolesToolStripMenuItem_Click);
            // 
            // vendedoresToolStripMenuItem
            // 
            this.vendedoresToolStripMenuItem.ForeColor = System.Drawing.Color.Black;
            this.vendedoresToolStripMenuItem.Name = "vendedoresToolStripMenuItem";
            this.vendedoresToolStripMenuItem.Size = new System.Drawing.Size(95, 20);
            this.vendedoresToolStripMenuItem.Text = "Vendedores";
            this.vendedoresToolStripMenuItem.Click += new System.EventHandler(this.vendedoresToolStripMenuItem_Click);
            // 
            // gastosVariosToolStripMenuItem
            // 
            this.gastosVariosToolStripMenuItem.ForeColor = System.Drawing.Color.Black;
            this.gastosVariosToolStripMenuItem.Name = "gastosVariosToolStripMenuItem";
            this.gastosVariosToolStripMenuItem.Size = new System.Drawing.Size(105, 20);
            this.gastosVariosToolStripMenuItem.Text = "Gastos Varios";
            this.gastosVariosToolStripMenuItem.Click += new System.EventHandler(this.gastosVariosToolStripMenuItem_Click);
            // 
            // configuracionToolStripMenuItem
            // 
            this.configuracionToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.categoriasToolStripMenuItem,
            this.familiasGastosToolStripMenuItem,
            this.subfamiliasGastosToolStripMenuItem,
            this.formatoCompraToolStripMenuItem,
            this.formatoVentaToolStripMenuItem,
            this.coloresToolStripMenuItem,
            this.tallesToolStripMenuItem,
            this.formaDePagoToolStripMenuItem,
            this.emailToolStripMenuItem,
            this.miEmpresaToolStripMenuItem});
            this.configuracionToolStripMenuItem.Name = "configuracionToolStripMenuItem";
            this.configuracionToolStripMenuItem.Size = new System.Drawing.Size(109, 20);
            this.configuracionToolStripMenuItem.Text = "Configuración";
            // 
            // categoriasToolStripMenuItem
            // 
            this.categoriasToolStripMenuItem.Name = "categoriasToolStripMenuItem";
            this.categoriasToolStripMenuItem.Size = new System.Drawing.Size(197, 22);
            this.categoriasToolStripMenuItem.Text = "Familias Productos";
            this.categoriasToolStripMenuItem.Click += new System.EventHandler(this.categoriasToolStripMenuItem_Click);
            // 
            // familiasGastosToolStripMenuItem
            // 
            this.familiasGastosToolStripMenuItem.Name = "familiasGastosToolStripMenuItem";
            this.familiasGastosToolStripMenuItem.Size = new System.Drawing.Size(197, 22);
            this.familiasGastosToolStripMenuItem.Text = "Familias Gastos";
            this.familiasGastosToolStripMenuItem.Click += new System.EventHandler(this.familiasGastosToolStripMenuItem_Click);
            // 
            // subfamiliasGastosToolStripMenuItem
            // 
            this.subfamiliasGastosToolStripMenuItem.Name = "subfamiliasGastosToolStripMenuItem";
            this.subfamiliasGastosToolStripMenuItem.Size = new System.Drawing.Size(197, 22);
            this.subfamiliasGastosToolStripMenuItem.Text = "Subfamilias Gastos";
            this.subfamiliasGastosToolStripMenuItem.Click += new System.EventHandler(this.subfamiliasGastosToolStripMenuItem_Click);
            // 
            // formatoCompraToolStripMenuItem
            // 
            this.formatoCompraToolStripMenuItem.Name = "formatoCompraToolStripMenuItem";
            this.formatoCompraToolStripMenuItem.Size = new System.Drawing.Size(197, 22);
            this.formatoCompraToolStripMenuItem.Text = "Formato Compra";
            this.formatoCompraToolStripMenuItem.Click += new System.EventHandler(this.formatoCompraToolStripMenuItem_Click);
            // 
            // formatoVentaToolStripMenuItem
            // 
            this.formatoVentaToolStripMenuItem.Name = "formatoVentaToolStripMenuItem";
            this.formatoVentaToolStripMenuItem.Size = new System.Drawing.Size(197, 22);
            this.formatoVentaToolStripMenuItem.Text = "Formato Venta";
            this.formatoVentaToolStripMenuItem.Click += new System.EventHandler(this.formatoVentaToolStripMenuItem_Click);
            //
            // coloresToolStripMenuItem
            //
            this.coloresToolStripMenuItem.Name = "coloresToolStripMenuItem";
            this.coloresToolStripMenuItem.Size = new System.Drawing.Size(197, 22);
            this.coloresToolStripMenuItem.Text = "Colores";
            this.coloresToolStripMenuItem.Click += new System.EventHandler(this.coloresToolStripMenuItem_Click);
            //
            // tallesToolStripMenuItem
            //
            this.tallesToolStripMenuItem.Name = "tallesToolStripMenuItem";
            this.tallesToolStripMenuItem.Size = new System.Drawing.Size(197, 22);
            this.tallesToolStripMenuItem.Text = "Talles";
            this.tallesToolStripMenuItem.Click += new System.EventHandler(this.tallesToolStripMenuItem_Click);
            // 
            // formaDePagoToolStripMenuItem
            // 
            this.formaDePagoToolStripMenuItem.Name = "formaDePagoToolStripMenuItem";
            this.formaDePagoToolStripMenuItem.Size = new System.Drawing.Size(197, 22);
            this.formaDePagoToolStripMenuItem.Text = "Forma de pago";
            this.formaDePagoToolStripMenuItem.Click += new System.EventHandler(this.formaDePagoToolStripMenuItem_Click);
            // 
            // emailToolStripMenuItem
            // 
            this.emailToolStripMenuItem.Name = "emailToolStripMenuItem";
            this.emailToolStripMenuItem.Size = new System.Drawing.Size(197, 22);
            this.emailToolStripMenuItem.Text = "Email";
            this.emailToolStripMenuItem.Click += new System.EventHandler(this.emailToolStripMenuItem_Click);
            // 
            // miEmpresaToolStripMenuItem
            // 
            this.miEmpresaToolStripMenuItem.Name = "miEmpresaToolStripMenuItem";
            this.miEmpresaToolStripMenuItem.Size = new System.Drawing.Size(197, 22);
            this.miEmpresaToolStripMenuItem.Text = "Mi Empresa";
            this.miEmpresaToolStripMenuItem.Click += new System.EventHandler(this.miEmpresaToolStripMenuItem_Click);
            // 
            // ayudaToolStripMenuItem
            // 
            this.ayudaToolStripMenuItem.Name = "ayudaToolStripMenuItem";
            this.ayudaToolStripMenuItem.Size = new System.Drawing.Size(60, 20);
            this.ayudaToolStripMenuItem.Text = "Ayuda";
            this.ayudaToolStripMenuItem.Click += new System.EventHandler(this.ayudaToolStripMenuItem_Click);
            // 
            // proveedoresButton
            // 
            this.proveedoresButton.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.proveedoresButton.Image = ((System.Drawing.Image)(resources.GetObject("proveedoresButton.Image")));
            this.proveedoresButton.Location = new System.Drawing.Point(345, 27);
            this.proveedoresButton.Name = "proveedoresButton";
            this.proveedoresButton.Size = new System.Drawing.Size(111, 89);
            this.proveedoresButton.TabIndex = 4;
            this.proveedoresButton.Text = "Proveedores";
            this.proveedoresButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.proveedoresButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.proveedoresButton.UseVisualStyleBackColor = true;
            this.proveedoresButton.Click += new System.EventHandler(this.button1_Click);
            // 
            // productosButton
            // 
            this.productosButton.BackColor = System.Drawing.SystemColors.Control;
            this.productosButton.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.productosButton.Image = ((System.Drawing.Image)(resources.GetObject("productosButton.Image")));
            this.productosButton.Location = new System.Drawing.Point(231, 27);
            this.productosButton.Name = "productosButton";
            this.productosButton.Size = new System.Drawing.Size(111, 89);
            this.productosButton.TabIndex = 3;
            this.productosButton.Text = "Productos";
            this.productosButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.productosButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.productosButton.UseVisualStyleBackColor = false;
            this.productosButton.Click += new System.EventHandler(this.button2_Click);
            // 
            // comprasButton
            // 
            this.comprasButton.BackColor = System.Drawing.SystemColors.Control;
            this.comprasButton.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comprasButton.Image = ((System.Drawing.Image)(resources.GetObject("comprasButton.Image")));
            this.comprasButton.Location = new System.Drawing.Point(117, 27);
            this.comprasButton.Name = "comprasButton";
            this.comprasButton.Size = new System.Drawing.Size(111, 89);
            this.comprasButton.TabIndex = 2;
            this.comprasButton.Text = "Compras";
            this.comprasButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.comprasButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.comprasButton.UseVisualStyleBackColor = false;
            this.comprasButton.Click += new System.EventHandler(this.button3_Click);
            // 
            // ventasButton
            // 
            this.ventasButton.BackColor = System.Drawing.SystemColors.Control;
            this.ventasButton.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ventasButton.Image = ((System.Drawing.Image)(resources.GetObject("ventasButton.Image")));
            this.ventasButton.Location = new System.Drawing.Point(3, 27);
            this.ventasButton.Name = "ventasButton";
            this.ventasButton.Size = new System.Drawing.Size(111, 89);
            this.ventasButton.TabIndex = 1;
            this.ventasButton.Text = "Ventas";
            this.ventasButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.ventasButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.ventasButton.UseVisualStyleBackColor = false;
            this.ventasButton.Click += new System.EventHandler(this.button4_Click);
            // 
            // ctacteButton
            // 
            this.ctacteButton.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctacteButton.Image = ((System.Drawing.Image)(resources.GetObject("ctacteButton.Image")));
            this.ctacteButton.Location = new System.Drawing.Point(117, 120);
            this.ctacteButton.Name = "ctacteButton";
            this.ctacteButton.Size = new System.Drawing.Size(111, 89);
            this.ctacteButton.TabIndex = 7;
            this.ctacteButton.Text = "Cta. Cte. Cliente";
            this.ctacteButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.ctacteButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.ctacteButton.UseVisualStyleBackColor = true;
            this.ctacteButton.Click += new System.EventHandler(this.button6_Click);
            // 
            // ctaCteProvButton
            // 
            this.ctaCteProvButton.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctaCteProvButton.Image = ((System.Drawing.Image)(resources.GetObject("ctaCteProvButton.Image")));
            this.ctaCteProvButton.Location = new System.Drawing.Point(231, 120);
            this.ctaCteProvButton.Name = "ctaCteProvButton";
            this.ctaCteProvButton.Size = new System.Drawing.Size(111, 89);
            this.ctaCteProvButton.TabIndex = 9;
            this.ctaCteProvButton.Text = "Cta. Cte. Proveedor";
            this.ctaCteProvButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.ctaCteProvButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.ctaCteProvButton.UseVisualStyleBackColor = true;
            this.ctaCteProvButton.Click += new System.EventHandler(this.button1_Click_2);
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(943, 562);
            this.Controls.Add(this.ctaCteProvButton);
            this.Controls.Add(this.ctacteButton);
            this.Controls.Add(this.ventasButton);
            this.Controls.Add(this.comprasButton);
            this.Controls.Add(this.productosButton);
            this.Controls.Add(this.proveedoresButton);
            this.Controls.Add(this.clientesButton);
            this.Controls.Add(this.menuStrip1);
            this.DoubleBuffered = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Main";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Sistema de Gestión";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Main_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button clientesButton;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem clientesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem proveedoresToolStripMenuItem;
        private System.Windows.Forms.Button proveedoresButton;
        private System.Windows.Forms.ToolStripMenuItem usuariosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem configuracionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem formaDePagoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem categoriasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem productosToolStripMenuItem;
        private System.Windows.Forms.Button productosButton;
        private System.Windows.Forms.Button comprasButton;
        private System.Windows.Forms.Button ventasButton;
        private System.Windows.Forms.ToolStripMenuItem ventasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem comprasToolStripMenuItem;
        private System.Windows.Forms.Button ctacteButton;
        private System.Windows.Forms.ToolStripMenuItem estadisticasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rolesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem usuariosToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem coloresToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tallesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem vendedoresToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ctaCteToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem gastosVariosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem clientesToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem pagosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ayudaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem miEmpresaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem emailToolStripMenuItem;
        private System.Windows.Forms.Button ctaCteProvButton;
        private System.Windows.Forms.ToolStripMenuItem proveedoresToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem ctaCteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pagosToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem familiasGastosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem formatoCompraToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem formatoVentaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem subfamiliasGastosToolStripMenuItem;

    }
}

