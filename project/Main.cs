﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Zapateria.Models;

using MySql.Data.MySqlClient;
using Zapateria.Libs;
using Zapateria.Abm_Clientes;
using Zapateria.Abm_Proveedores;
using Zapateria.Modules.ABM_Usuarios;
using Zapateria.Abm_Categorias;
using Zapateria.Abm_Formas_de_pago;
using Zapateria.Abm_Articulos;
using Zapateria.Abm_Compras;
using Zapateria.Abm_Ventas;
using Zapateria.AbmPagos;
using Zapateria.Modules.Cta_Cte;
using Zapateria.Estadisticas;
using Zapateria.Modules.Abm_Roles;
using Zapateria.ABM_Tipo_Cliente;
using Zapateria.Models;
using Zapateria.Abm_Vendedores;
using Zapateria.Modules.Abm_Gastos_Varios;
using Zapateria.Modules.Abm_Empresa;
using Zapateria.Modules.Configuracion;
using Zapateria.Modules.Cta_Cte_Proveedor;
using Zapateria.AbmPagosProveedores;
using Zapateria.Abm_Familias_Gastos;
using Zapateria.Abm_Formato_Compra;
using Zapateria.Abm_Formato_Venta;
using Zapateria.Abm_SubFamilias_Gastos;
using Zapateria.Abm_Colores;
using Zapateria.Abm_Talles;
using System.Threading;

namespace Zapateria
{
    public partial class Main : Form
    {
        //Usuario usuario;

        private static Usuario usuario;

        public Main(Usuario u)
        {
            InitializeComponent();
            usuario = u;
        }

        public static Usuario getUsuario()
        {
            return usuario;
        }

        private void hideAll()
        {
            ventasToolStripMenuItem.Visible = false;
            ventasButton.Visible = false;
            comprasToolStripMenuItem.Visible = false;
            comprasButton.Visible = false;
            proveedoresToolStripMenuItem.Visible = false;
            proveedoresButton.Visible = false;            
            productosToolStripMenuItem.Visible = false;
            productosButton.Visible = false;
            ctaCteProvButton.Visible = false;

            ctacteButton.Visible = false;
            clientesToolStripMenuItem.Visible = false;
            clientesButton.Visible = false;
            estadisticasToolStripMenuItem.Visible = false;
            usuariosToolStripMenuItem.Visible = false;
            configuracionToolStripMenuItem.Visible = false;


            gastosVariosToolStripMenuItem.Visible = false;
            vendedoresToolStripMenuItem.Visible = false;
        }

        private void Main_Load(object sender, EventArgs e)
        {
            hideAll();

            usuario.rol.modulos = usuario.rol.SelectModulosByRol(usuario.rol.id);
            foreach (Modulo m in usuario.rol.modulos)
            {
                switch(m.id)
                {
                    case 1://Config
                    {
                        configuracionToolStripMenuItem.Visible = true;
                        break;
                    }
                    case 2://Productos
                        {
                            productosToolStripMenuItem.Visible = true;
                            productosButton.Visible = true;
                            break;
                        }
                    case 3://Ventas
                        {
                            ventasToolStripMenuItem.Visible = true;
                            ventasButton.Visible = true;
                            break;
                        }
                    case 4://GG
                        {
                            gastosVariosToolStripMenuItem.Visible = true;
                            break;
                        }
                    case 5://Config
                        {
                            configuracionToolStripMenuItem.Visible = true;
                            break;
                        }
                    case 6://compras
                        {
                            comprasToolStripMenuItem.Visible = true;
                            comprasButton.Visible = true;
                            break;
                        }
                    case 7://usuarios
                        {
                            usuariosToolStripMenuItem.Visible = true;
                            break;
                        }
                    case 8://estadisticas
                        {
                            estadisticasToolStripMenuItem.Visible = true;
                            break;
                        }
                    case 10://vendedores
                        {
                            vendedoresToolStripMenuItem.Visible = true;
                            break;
                        }
                    case 11://Proveedores
                        {
                            proveedoresToolStripMenuItem.Visible = true;
                            proveedoresButton.Visible = true;
                            ctaCteProvButton.Visible = true;
                            break;
                        }
                    case 13://Clientes
                        {
                            clientesToolStripMenuItem.Visible = true;
                            clientesButton.Visible = true;
                            ctacteButton.Visible = true;
                            break;
                        }
                }
            }

        }

        private void EquiposButton_Click(object sender, EventArgs e)
        {
            ClientesForm abrir = new ClientesForm();
            abrir.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ProveedoresForm abrir = new ProveedoresForm();
            abrir.Show();
        }

        private void categoriasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CategoriasForm abrir = new CategoriasForm();
            abrir.Show();
        }        

        private void formaDePagoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormaPagoForm abrir = new FormaPagoForm();
            abrir.Show();
        }

        private void pruebaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ArticulosForm abrir = new ArticulosForm();
            abrir.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ArticulosForm abrir = new ArticulosForm();
            abrir.Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            CompraForm abrir = new CompraForm();
            abrir.Show();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            VentaForm abrir = new VentaForm();
            abrir.Show();
        }

        private void ventasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            VentaForm abrir = new VentaForm();
            abrir.Show();
        }

        private void comprasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CompraForm abrir = new CompraForm();
            abrir.Show();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            PagosForm abrir = new PagosForm();
            abrir.Show();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            ListCtaCteForm abrir = new ListCtaCteForm();
            abrir.Show();
        }

        private void estadisticasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            EstadisticasForm abrir = new EstadisticasForm();
            abrir.Show();
        }

        private void ctaCteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ListCtaCteForm abrir = new ListCtaCteForm();
            abrir.Show();
        }

        private void creditosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PagosForm abrir = new PagosForm();
            abrir.Show();
        }

        private void rolesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RolesForm f = new RolesForm();
            f.Show();
        }

        private void usuariosToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            UsuariosForm abrir = new UsuariosForm();
            abrir.Show();
        }

        private void tipoClientesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            TipoClienteForm abrir = new TipoClienteForm();
            abrir.Show();
        }

        private void coloresToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ColoresForm abrir = new ColoresForm();
            abrir.Show();
        }

        private void tallesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            TallesForm abrir = new TallesForm();
            abrir.Show();
        }

        private void clientesToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            ClientesForm abrir = new ClientesForm();
            abrir.Show();
        }

        private void vendedoresToolStripMenuItem_Click(object sender, EventArgs e)
        {
            VendedorForm f = new VendedorForm();
            f.Show();
        }

        private void pagosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PagosForm f = new PagosForm();
            f.Show();
        }

        private void ayudaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            String text = "Mayorista Desktop APP\n";
            text += "Versión: " + Zapateria.Properties.Settings.Default.Version + "\n";
            text += "IngeniAR - Sofware Development - 2017\n";
            text += "http://ingeniar.com.ar";

            MessageBox.Show(text, "IngeniAR");
        }

        private void gastosVariosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GastosVariosForm f = new GastosVariosForm();
            f.Show();
        }

        private void miEmpresaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            EmpresaForm f = new EmpresaForm();
            f.Show();
        }

        private void emailToolStripMenuItem_Click(object sender, EventArgs e)
        {
            EmailForm f = new EmailForm();
            f.Show();
        }

        private void button1_Click_2(object sender, EventArgs e)
        {
            ListCtaCteProveedorForm abrir = new ListCtaCteProveedorForm();
            abrir.Show();
        }

        private void ctaCteToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            ListCtaCteProveedorForm abrir = new ListCtaCteProveedorForm();
            abrir.Show();
        }

        private void proveedoresToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            ProveedoresForm abrir = new ProveedoresForm();
            abrir.Show();
        }

        private void pagosToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            PagosProveedoresForm f = new PagosProveedoresForm();
            f.Show();
        }

        private void familiasGastosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FamiliaGastoForm f = new FamiliaGastoForm();
            f.Show();
        }

        private void formatoCompraToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormatoCompraForm f = new FormatoCompraForm();
            f.Show();
        }

        private void formatoVentaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormatoVentaForm f = new FormatoVentaForm();
            f.Show();
        }

        private void subfamiliasGastosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SubFamiliaGastoForm f = new SubFamiliaGastoForm();
            f.Show();
        }

       
    }
}
