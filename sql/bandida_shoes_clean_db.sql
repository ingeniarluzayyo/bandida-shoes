-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3306
-- Tiempo de generación: 29-10-2019 a las 12:07:15
-- Versión del servidor: 5.7.23
-- Versión de PHP: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `zapateria_new`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientes`
--

DROP TABLE IF EXISTS `clientes`;
CREATE TABLE IF NOT EXISTS `clientes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `razon_social` varchar(300) NOT NULL,
  `cuit` varchar(25) DEFAULT NULL,
  `tel` varchar(300) NOT NULL,
  `direccion` varchar(500) NOT NULL,
  `localidad` varchar(300) NOT NULL,
  `observacion` text NOT NULL,
  `orden` int(11) NOT NULL,
  `habilitado` varchar(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `colores`
--

DROP TABLE IF EXISTS `colores`;
CREATE TABLE IF NOT EXISTS `colores` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `colores`
--

INSERT INTO `colores` (`id`, `nombre`) VALUES
(1, 'Blanco'),
(2, 'Negro'),
(3, 'Rojo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `creditos_pagos`
--

DROP TABLE IF EXISTS `creditos_pagos`;
CREATE TABLE IF NOT EXISTS `creditos_pagos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fecha` date NOT NULL,
  `cliente` int(11) NOT NULL,
  `monto` decimal(10,2) NOT NULL,
  `anulada` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `cliente` (`cliente`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cta_cte`
--

DROP TABLE IF EXISTS `cta_cte`;
CREATE TABLE IF NOT EXISTS `cta_cte` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cliente` int(11) NOT NULL,
  `ultima_actualizacion` datetime NOT NULL,
  `total_facturado` decimal(10,2) NOT NULL,
  `total_pagado` decimal(10,2) NOT NULL,
  `saldo` decimal(10,2) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `cliente_2` (`cliente`),
  KEY `cliente` (`cliente`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cta_cte_proveedores`
--

DROP TABLE IF EXISTS `cta_cte_proveedores`;
CREATE TABLE IF NOT EXISTS `cta_cte_proveedores` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `proveedor` int(11) NOT NULL,
  `ultima_actualizacion` datetime NOT NULL,
  `total_facturado` decimal(10,2) NOT NULL,
  `total_pagado` decimal(10,2) NOT NULL,
  `saldo` decimal(10,2) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `proveedor_2` (`proveedor`),
  KEY `proveedor` (`proveedor`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empresa`
--

DROP TABLE IF EXISTS `empresa`;
CREATE TABLE IF NOT EXISTS `empresa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `razon_social` text NOT NULL,
  `cuit` text NOT NULL,
  `direccion` text NOT NULL,
  `telefono` text NOT NULL,
  `imagen` longblob NOT NULL,
  `etiqueta` text,
  `servidor` text NOT NULL,
  `puerto` int(11) NOT NULL,
  `email` text NOT NULL,
  `pass` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `empresa`
--

INSERT INTO `empresa` (`id`, `razon_social`, `cuit`, `direccion`, `telefono`, `imagen`, `etiqueta`, `servidor`, `puerto`, `email`, `pass`) VALUES
(1, 'MGL Digon', '99-99999999-9', 'Presidente Juan Domingo Peron 1855', '00000-00000', '', '', 'gmail', 0, 'mgldigon@gmail.com', '1423419digon');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `factura_compra`
--

DROP TABLE IF EXISTS `factura_compra`;
CREATE TABLE IF NOT EXISTS `factura_compra` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sucursal` int(11) NOT NULL,
  `num_fact_externo` varchar(45) DEFAULT NULL,
  `fecha` date NOT NULL,
  `proveedor` int(11) NOT NULL,
  `forma_de_pago` int(11) NOT NULL,
  `descuento` decimal(10,2) NOT NULL,
  `total` decimal(10,2) NOT NULL,
  `anulada` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `proveedor` (`proveedor`),
  KEY `Fk_forma_de_pago_idx` (`forma_de_pago`),
  KEY `Fk_fdp_compras_idx` (`forma_de_pago`),
  KEY `sucursal` (`sucursal`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `factura_venta`
--

DROP TABLE IF EXISTS `factura_venta`;
CREATE TABLE IF NOT EXISTS `factura_venta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sucursal` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `cliente` int(11) NOT NULL,
  `vendedor` int(11) NOT NULL,
  `forma_de_pago` int(11) NOT NULL,
  `descuento` decimal(10,2) NOT NULL,
  `total` decimal(10,2) NOT NULL,
  `total_comision` decimal(10,2) NOT NULL,
  `anulada` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `cliente` (`cliente`),
  KEY `forma_de_pago` (`forma_de_pago`),
  KEY `vendedor` (`vendedor`),
  KEY `sucursal` (`sucursal`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `familias`
--

DROP TABLE IF EXISTS `familias`;
CREATE TABLE IF NOT EXISTS `familias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(300) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nombre` (`nombre`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `familias_gastos`
--

DROP TABLE IF EXISTS `familias_gastos`;
CREATE TABLE IF NOT EXISTS `familias_gastos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(300) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nombre` (`nombre`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `forma_de_pago`
--

DROP TABLE IF EXISTS `forma_de_pago`;
CREATE TABLE IF NOT EXISTS `forma_de_pago` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(300) NOT NULL,
  `a_credito` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nombre` (`nombre`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `forma_de_pago`
--

INSERT INTO `forma_de_pago` (`id`, `nombre`, `a_credito`) VALUES
(1, 'Efectivo', 0),
(2, 'Crédito', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `gastos`
--

DROP TABLE IF EXISTS `gastos`;
CREATE TABLE IF NOT EXISTS `gastos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fecha` date NOT NULL,
  `sucursal` int(11) NOT NULL,
  `vendedor` int(11) DEFAULT NULL,
  `titulo` text,
  `familia_gasto` int(11) NOT NULL,
  `subfamilia_gasto` int(11) DEFAULT NULL,
  `monto` decimal(10,2) NOT NULL,
  `forma_de_pago` int(11) NOT NULL,
  `descripcion` text NOT NULL,
  `anulado` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `forma_de_pago` (`forma_de_pago`),
  KEY `sucursal` (`sucursal`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `item_factura_compra`
--

DROP TABLE IF EXISTS `item_factura_compra`;
CREATE TABLE IF NOT EXISTS `item_factura_compra` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `factura` int(11) NOT NULL,
  `producto` int(10) UNSIGNED ZEROFILL NOT NULL,
  `talle` int(11) NOT NULL,
  `color` int(11) NOT NULL,
  `precio` decimal(10,2) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `formato` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `factura` (`factura`),
  KEY `articulo` (`producto`),
  KEY `articulo_2` (`producto`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `item_factura_venta`
--

DROP TABLE IF EXISTS `item_factura_venta`;
CREATE TABLE IF NOT EXISTS `item_factura_venta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `factura` int(11) NOT NULL,
  `producto` int(10) UNSIGNED ZEROFILL NOT NULL,
  `talle` int(11) NOT NULL,
  `color` int(11) NOT NULL,
  `sucursal` int(11) NOT NULL,
  `zona` int(11) NOT NULL,
  `precio` decimal(10,2) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `descuento` int(11) NOT NULL,
  `formato` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `factura` (`factura`),
  KEY `articulo` (`producto`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `modulos`
--

DROP TABLE IF EXISTS `modulos`;
CREATE TABLE IF NOT EXISTS `modulos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `modulos`
--

INSERT INTO `modulos` (`id`, `nombre`) VALUES
(2, 'Modulo Productos'),
(3, 'Modulo de Ventas'),
(4, 'Modulo Gastos Varios'),
(5, 'Modulo Configuración'),
(6, 'Modulo de Compras'),
(7, 'Modulo de Usuarios'),
(8, 'Modulo de Estadisticas'),
(10, 'Modulo de Vendedores'),
(11, 'Modulo Proveedores'),
(13, 'Modulo Clientes'),
(14, 'Modulo Orden de pedido');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `notificaciones`
--

DROP TABLE IF EXISTS `notificaciones`;
CREATE TABLE IF NOT EXISTS `notificaciones` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(100) NOT NULL,
  `fecha` datetime NOT NULL,
  `estado` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pagos`
--

DROP TABLE IF EXISTS `pagos`;
CREATE TABLE IF NOT EXISTS `pagos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fecha` date NOT NULL,
  `cliente` int(11) NOT NULL,
  `forma_de_pago` int(11) NOT NULL,
  `monto` decimal(10,2) NOT NULL,
  `anulada` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `cliente` (`cliente`),
  KEY `forma_de_pago` (`forma_de_pago`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pagos_proveedor`
--

DROP TABLE IF EXISTS `pagos_proveedor`;
CREATE TABLE IF NOT EXISTS `pagos_proveedor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fecha` date NOT NULL,
  `proveedor` int(11) NOT NULL,
  `forma_de_pago` int(11) NOT NULL,
  `monto` decimal(10,2) NOT NULL,
  `anulada` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `proveedor` (`proveedor`,`forma_de_pago`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

DROP TABLE IF EXISTS `productos`;
CREATE TABLE IF NOT EXISTS `productos` (
  `id` int(10) UNSIGNED ZEROFILL NOT NULL COMMENT 'codigo',
  `familia` int(11) NOT NULL,
  `nombre` varchar(300) NOT NULL,
  `observacion` text NOT NULL,
  `stock` decimal(10,2) NOT NULL,
  `unidad` tinyint(1) NOT NULL,
  `fraccionado` tinyint(1) NOT NULL,
  `precio_compra` decimal(10,2) DEFAULT NULL,
  `precio_venta_x3` decimal(10,2) DEFAULT NULL,
  `precio_venta_x6` decimal(10,2) DEFAULT NULL,
  `precio_venta_x12` decimal(10,2) DEFAULT NULL,
  `habilitado` varchar(1) DEFAULT '1',
  `orden` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nombre_UNIQUE` (`nombre`),
  KEY `FK_Productos_Familias` (`familia`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proveedores`
--

DROP TABLE IF EXISTS `proveedores`;
CREATE TABLE IF NOT EXISTS `proveedores` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `razon_social` varchar(300) NOT NULL,
  `cuit` varchar(300) NOT NULL,
  `direccion` varchar(500) NOT NULL,
  `tel` varchar(300) NOT NULL,
  `cel` varchar(300) NOT NULL,
  `mail` varchar(300) NOT NULL,
  `observacion` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

DROP TABLE IF EXISTS `roles`;
CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `roles`
--

INSERT INTO `roles` (`id`, `nombre`) VALUES
(1, 'administrador'),
(2, 'entrega 1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles_x_modulos`
--

DROP TABLE IF EXISTS `roles_x_modulos`;
CREATE TABLE IF NOT EXISTS `roles_x_modulos` (
  `rol` int(11) NOT NULL,
  `modulo` int(11) NOT NULL,
  PRIMARY KEY (`rol`,`modulo`),
  KEY `FK_Roles_X_Modulos_Modulos` (`modulo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `roles_x_modulos`
--

INSERT INTO `roles_x_modulos` (`rol`, `modulo`) VALUES
(1, 2),
(2, 2),
(1, 3),
(2, 3),
(1, 4),
(2, 4),
(1, 5),
(2, 5),
(1, 6),
(2, 6),
(1, 7),
(2, 7),
(1, 8),
(2, 8),
(1, 10),
(2, 10),
(1, 11),
(2, 11),
(1, 13),
(2, 13),
(1, 14);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `stock`
--

DROP TABLE IF EXISTS `stock`;
CREATE TABLE IF NOT EXISTS `stock` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `producto` int(11) NOT NULL,
  `sucursal` int(11) NOT NULL,
  `zona` int(11) NOT NULL,
  `talle` int(11) NOT NULL,
  `color` int(11) NOT NULL,
  `stock` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `producto` (`producto`),
  KEY `sucursal` (`sucursal`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `stock_producto_x_sucursal`
--

DROP TABLE IF EXISTS `stock_producto_x_sucursal`;
CREATE TABLE IF NOT EXISTS `stock_producto_x_sucursal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `producto` int(11) NOT NULL,
  `sucursal` int(11) NOT NULL,
  `stock` decimal(10,2) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `producto` (`producto`),
  KEY `sucursal` (`sucursal`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `subfamilia_gastos`
--

DROP TABLE IF EXISTS `subfamilia_gastos`;
CREATE TABLE IF NOT EXISTS `subfamilia_gastos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(200) NOT NULL,
  `familia_gasto` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nombre` (`nombre`,`familia_gasto`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sucursal`
--

DROP TABLE IF EXISTS `sucursal`;
CREATE TABLE IF NOT EXISTS `sucursal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `sucursal`
--

INSERT INTO `sucursal` (`id`, `nombre`) VALUES
(1, 'Sucursal 1'),
(2, 'Sucursal 2');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `talles`
--

DROP TABLE IF EXISTS `talles`;
CREATE TABLE IF NOT EXISTS `talles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `talles`
--

INSERT INTO `talles` (`id`, `nombre`) VALUES
(1, '37'),
(2, '38'),
(3, '39');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
CREATE TABLE IF NOT EXISTS `usuarios` (
  `username` varchar(25) NOT NULL,
  `password` text NOT NULL,
  `rol` int(11) NOT NULL,
  `email` text NOT NULL,
  `sucursal_default` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`username`),
  KEY `FK_Usuario_Rol` (`rol`),
  KEY `sucursal_default` (`sucursal_default`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`username`, `password`, `rol`, `email`, `sucursal_default`) VALUES
('admin', '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918', 1, '', 2),
('GER', '552cb75e8b82760680bcdda8abf56169c114466929ba5cf64555673577bda742', 1, 'germandigon@hotmail.com', 2),
('ludigon', 'e0baf3221498f540a7413c12567e1033f321e0e6c5269d1447a574f90a660442', 1, 'mgldigon@gmail.com', 1),
('mayorista', '27ab0f2b73ed793a20c0263772adef0bc564727a99ae03bfdacf4cc7baf120ca', 2, '', 1),
('MGL', '4ba67d97862892d5654db894d4542d42a943c76db9fd19cc0ac9939764ef4a96', 2, 'm_digon@hotmail.com', 1),
('ruben', '4b40f51d9386a871c2e2b751ce9ac59924307acc43d3169800a9941f3921c06a', 1, 'rdigon@live.com.ar', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vendedores`
--

DROP TABLE IF EXISTS `vendedores`;
CREATE TABLE IF NOT EXISTS `vendedores` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sucursal` int(11) NOT NULL,
  `nombre` varchar(300) NOT NULL,
  `apellido` varchar(300) NOT NULL,
  `dni` int(11) NOT NULL,
  `fechanacimiento` date NOT NULL,
  `tel` varchar(300) NOT NULL,
  `cel` varchar(300) NOT NULL,
  `direccion` varchar(300) NOT NULL,
  `mail` varchar(300) NOT NULL,
  `observacion` text NOT NULL,
  `comision` decimal(10,2) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `login_token` varchar(100) DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  `habilitado` varchar(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  KEY `sucursal` (`sucursal`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `vendedores`
--

INSERT INTO `vendedores` (`id`, `sucursal`, `nombre`, `apellido`, `dni`, `fechanacimiento`, `tel`, `cel`, `direccion`, `mail`, `observacion`, `comision`, `username`, `password`, `login_token`, `last_login`, `habilitado`) VALUES
(1, 2, 'Vendedor', 'Prueba', 0, '2017-05-09', '', '', '', '', '', '30.00', 'admin', '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918', '9d41f51adaffbb0d0a6f29133e770dd711b2786457a00947a83414e254195c88', '2019-04-12 10:29:07', '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `zona`
--

DROP TABLE IF EXISTS `zona`;
CREATE TABLE IF NOT EXISTS `zona` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `zona`
--

INSERT INTO `zona` (`id`, `nombre`) VALUES
(1, 'Local'),
(2, 'Depósito');

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `creditos_pagos`
--
ALTER TABLE `creditos_pagos`
  ADD CONSTRAINT `fk_cliente_creditos` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`);

--
-- Filtros para la tabla `cta_cte`
--
ALTER TABLE `cta_cte`
  ADD CONSTRAINT `fk_cliente_cta_cte` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`);

--
-- Filtros para la tabla `cta_cte_proveedores`
--
ALTER TABLE `cta_cte_proveedores`
  ADD CONSTRAINT `cta_cte_proveedores_ibfk_1` FOREIGN KEY (`proveedor`) REFERENCES `proveedores` (`id`);

--
-- Filtros para la tabla `factura_compra`
--
ALTER TABLE `factura_compra`
  ADD CONSTRAINT `Foren_key_proveedor_compra` FOREIGN KEY (`proveedor`) REFERENCES `proveedores` (`id`),
  ADD CONSTRAINT `fk_fdp` FOREIGN KEY (`forma_de_pago`) REFERENCES `forma_de_pago` (`id`);

--
-- Filtros para la tabla `factura_venta`
--
ALTER TABLE `factura_venta`
  ADD CONSTRAINT `factura_venta_ibfk_1` FOREIGN KEY (`vendedor`) REFERENCES `vendedores` (`id`),
  ADD CONSTRAINT `foreng_key_cliente_venta` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `foreng_key_pago_venta` FOREIGN KEY (`forma_de_pago`) REFERENCES `forma_de_pago` (`id`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `gastos`
--
ALTER TABLE `gastos`
  ADD CONSTRAINT `gastos_ibfk_1` FOREIGN KEY (`forma_de_pago`) REFERENCES `forma_de_pago` (`id`);

--
-- Filtros para la tabla `item_factura_compra`
--
ALTER TABLE `item_factura_compra`
  ADD CONSTRAINT `foreng_item_compra_producto` FOREIGN KEY (`producto`) REFERENCES `productos` (`id`),
  ADD CONSTRAINT `foreng_key_fact_compra` FOREIGN KEY (`factura`) REFERENCES `factura_compra` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `item_factura_venta`
--
ALTER TABLE `item_factura_venta`
  ADD CONSTRAINT `fk_fact_venta_item` FOREIGN KEY (`factura`) REFERENCES `factura_venta` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_item_venta_productos` FOREIGN KEY (`producto`) REFERENCES `productos` (`id`);

--
-- Filtros para la tabla `pagos`
--
ALTER TABLE `pagos`
  ADD CONSTRAINT `pagos_ibfk_1` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `pagos_ibfk_2` FOREIGN KEY (`forma_de_pago`) REFERENCES `forma_de_pago` (`id`);

--
-- Filtros para la tabla `productos`
--
ALTER TABLE `productos`
  ADD CONSTRAINT `FK_Productos_Familias` FOREIGN KEY (`familia`) REFERENCES `familias` (`id`);

--
-- Filtros para la tabla `roles_x_modulos`
--
ALTER TABLE `roles_x_modulos`
  ADD CONSTRAINT `FK_Roles_X_Modulos` FOREIGN KEY (`rol`) REFERENCES `roles` (`id`),
  ADD CONSTRAINT `FK_Roles_X_Modulos_Modulos` FOREIGN KEY (`modulo`) REFERENCES `modulos` (`id`);

--
-- Filtros para la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD CONSTRAINT `FK_Usuario_Rol` FOREIGN KEY (`rol`) REFERENCES `roles` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
